==================================
Welcome to DST project landing page 
===================================

|code-quality| |build-status| |docs| |creative-lic|


Docuemntation for the DST Project
=================================
The doucmentation for the data storage tag project is created using ``Sphinx`` and can be read at https://jumaili.bitbucket.io
 

Getting Started 
===============
This repository contains the hardware, software and documentation used to create the data storage tag.

This documentation for this repository can be found under the the ``doc`` folderin the root folder.

Cloning this repository can be done by done with git:
.. code-block:: shell

   $ git clone https://jumaili@bitbucket.org/jumaili/data-storage-tag-project.git


Prerequisites
-------------

- GNU/Linux Debian 7
- Git
- Sphinx 1.1.3 or higher
- Inkscape

For PDF output
~~~~~~~~~~~~~~

- texlive
- texlive-fonts-recommended
- texlive-latex-extra

Installing Sphinx
-----------------

$ apt-get install python-sphinx
$ apt-get install python3-sphinx-rtd-theme

If it not already present, this will install Python for you.

Building the HTML output
------------------------

$ git clone https://jumaili@bitbucket.org/jumaili/data-storage-tag-project.git

$ cd data-storage-tag-project/docs

$ make html


Building the PDF output
------------------------

$ git clone https://jumaili@bitbucket.org/jumaili/data-storage-tag-project.git

$ cd data-storage-tag-project/docs

$ make latexpdf

Repository content
-------------------

The hardware files for the circuit board are located in this repository can be downloaded as a zip packages:


.. |build-status| image:: https://circleci.com/bb/jumaili/data-storage-tag-project.png
    :alt: build status
    :scale: 100%

.. |docs| image:: https://readthedocs.org/projects/data-storage-tag-project/badge/?version=latest
        :alt: Documentation Status
        :target: https://data-storage-tag-project.readthedocs.io/en/latest/?badge=latest

.. |creative-lic| image:: https://img.shields.io/badge/License-CC%20BY--NC%204.0-lightgrey.png
        :alt: License: CC BY-NC 4.0
        :scale: 100%
        :target: https://creativecommons.org/licenses/by-nc/4.0/

.. |code-quality| image:: https://api.codacy.com/project/badge/Grade/5e2aecc6f69f4d71b09f450e13dcedab
        :alt: Codacy
        :scale: 100%
        :target: https://www.codacy.com/app/ali.jum/Data-Storage-Tag-Project?utm_source=jumaili@bitbucket.org&amp;utm_medium=referral&amp;utm_content=jumaili/data-storage-tag-project&amp;utm_campaign=Badge_Grade

License
-------
The documentation is licensed under `Creative Common Attribution-NonCommercial 4.0 International License`_. The software files in `/SW` folder are under the `MIT license`_.

.. _Creative Common Attribution-NonCommercial 4.0 International License: LICENSE

.. _MIT license: https://opensource.org/licenses/MIT
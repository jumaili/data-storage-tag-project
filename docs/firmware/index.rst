.. _ firmware:

##############
Firmware Code 
##############

.. contents:: Contents
   :local:
   :depth: 2


main.c
=======

.. literalinclude:: /src/main.c
   :language: c
   :linenos:



Core Layer
==========

i2c.c
-----
.. literalinclude:: /src/core/i2c.c
   :language: c
   :linenos:

i2c.h
-----
.. literalinclude:: /src/core/i2c.h
   :language: c
   :linenos:


spi.c
-----

.. literalinclude:: /src/core/spi.c
   :language: c
   :linenos:

spi.h
-----
.. literalinclude:: /src/core/spi.h
   :language: c
   :linenos:


mcu.c
-----

.. literalinclude:: /src/core/mcu.c
   :language: c
   :linenos:


mcu.h
-----
.. literalinclude:: /src/core/mcu.h
   :language: c
   :linenos:

timer.c
-------
.. literalinclude:: /src/core/timer.h
   :language: c
   :linenos:

timer.h
-------
.. literalinclude:: /src/core/timer.h
   :language: c
   :linenos:


Application Layer
=================


fsm.c
------------

.. literalinclude:: /src/application_layer/fsm.c
   :language: c


fsm.h
------------

.. literalinclude:: /src/application_layer/fsm.h
   :language: c


settings.c
---------------------

.. literalinclude:: /src/application_layer/settings.c
   :language: c


setings.h
---------------------
.. literalinclude:: /src/application_layer/settings.h
   :language: c




Devices Layer
==============


tmp117.c
-------
.. literalinclude:: /src/devices/tmp117.c
	:language: c

tmp117.h
-------

.. literalinclude:: /src/devices/tmp117.h
	:language: c

ms58370.c
-------------

.. literalinclude:: /src/devices/ms58370.c
	:language: c

ms58370.h
-------------

.. literalinclude:: /src/devices/ms58370.h
	:language: c

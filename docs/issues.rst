.. _ issues:

#################
Known Limitations
#################

.. contents:: Contents
   :local:

Problem 1 - Unable to connect to the target : Unknown device
------------------------------------------------------------
This is a known problem with the Spy-Bi-Wire (2-wire JTAG) protocol on MSP-TS430RHA40A and  MSP430FR5739 target board. The issue can occur when programming the newer version of the MSP-FET (V2.06) that has a CE sticker label on the case. The issue has been narrowed down to the RST/SBWTDIO signal. Specifically, the capacitance present on that bi-directional net which affects the rise and fall times during debug.

This issue is confirmed by TI and for the full discussion check this post thread:

MSP-TS430RHA40A: Problem using SBW with MSP-FET
 - https://e2e.ti.com/support/microcontrollers/msp430/f/166/t/799408


Problem 2 - Outputting MCLK; SMCLK and ACKL to port pins not oscillating 
------------------------------------------------------------------

Using the PxSELx register one can output the frequencies of the clocks and measure them using an oscilloscope.
If there are other components placed there such as resistors, LEDS or other components, the load of the components would stop the oscillations of these components. A possible solution is to remove the components before testing or using drivelib get frequency functions to get a numerical value (based on the configured registers).


More info can be read this post: MSP430FR5738: MCLK, SMCLK and ACLK
 - https://e2e.ti.com/support/microcontrollers/msp430/f/166/p/799846/2959558#2959558


Problem 3 - Measuring XIN/XOUT by probing crystal directly on pads
---------------------------------------------------------------------
The probes add an additional capacitance depending on this capacitance this can stop the crystal from oscillation. For example the probes that come with the Analog Discovery 2 add 25 pF which is enough to stop the crystal from operating when both C1 and C2 are fitted.


Problem 4 - Building the project fails
---------------------------------------------
A common problem that can occur is related to using non-alphanumeric characters in the directory name. This can cause build issues in CCS.
For example having a username "C:\Users\Øyvind-Stein\DST-Project" might case CCS to fail during project build. It is therefor to use due to the letter 'Ø'. It is therefor recommended to avoid such characters in project names, source/header files, CCS workspace folder names, System temp folder, etc.
One exception is the underscore character '_' which is normally accepted.


Spaces in the path can also cause build errors in CCS. Creating a workspace in a new path which doesn't contain any spaces is highly recommended.

More information can be found here: Build errors in CCS 
 - http://software-dl.ti.com/ccs/esd/documents/sdto_ccs_build-errors.html#general



Problem 5 - Problems with building driverlib or FRAM-Utilities
--------------------------------------------------------------
CCS can fail to detect MSP-FRAM-UTILITIES or driverlib. Although these files are now included in the project, problems can occur when changing code. It is therefor recommended to install these directly into CCS by going to:
Window->Preferences->Code Composer Studio->Products, and click on Rediscover 
(note that the path for MSP-FRAM-UTILITES and driverlib should be in the discovery path)

.. figure:: /img/ccs-install-fram-util-autodiscover.png
 :scale: 45 %
 :alt: Autodiscover of Library When Starting CCS (Place Library in TI home folder)



.. figure:: /img/ccs-install-fram-util.png
 :scale: 45 %
 :alt: Installing a Missing Library


Problem 6 - Handling System Reset Events Fails
----------------------------------------------

The placement of the ISR for SYSRSTIV (System Reset Interrupt Vector Register) is important. For exact details on this issue read this e2e post. 
[FAQ] Handling MSP430 System Reset Events 
 - https://e2e.ti.com/support/microcontrollers/msp430/f/166/t/746272?tisearch=e2e-sitesearch&keymatch=faq:true

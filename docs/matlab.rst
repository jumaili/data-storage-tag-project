#################
Matlab Simulation
#################

.. contents:: Contents
   :local:
   :depth: 3

Code for Matlab Simulation
==========================

.. literalinclude:: /src/matlab.m
   :language: matlab
   :linenos:

Simulation Results
------------------

IGRF Simulation Results

|pic1| and |pic2|

|pic3| and |pic4|

.. |pic1| image:: /img/IGRF_Declination.png
   :width: 45%

.. |pic2| image:: /img/IGRF_Horizontal.png
   :width: 45%

.. |pic3| image:: /img/IGRF_Inclination.png
   :width: 45%

.. |pic4| image:: /img/IGRF_Total.png
   :width: 45%


Results from SST Simulation
---------------------------

|pic5|


.. |pic5| image:: /img/sst-full.png
   :width: 45%
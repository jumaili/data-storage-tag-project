[![Documentation Status](https://readthedocs.org/projects/data-storage-tag-project/badge/?version=latest)](https://data-storage-tag-project.readthedocs.io/en/latest/?badge=latest) [![License: CC BY-NC 4.0](https://licensebuttons.net/l/by-nc/4.0/80x15.png)](https://creativecommons.org/licenses/by-nc/4.0/)


This folder contains the source files for the documentation.

The online doucmentation (HTML) and LaTeX (PDF) is avaliable at https://ntnu-dst.bitbucket.io 
or a backup version using a different theme on https://data-storage-tag-project.readthedocs.io

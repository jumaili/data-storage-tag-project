.. _ directory:

.. index:: !directory
Directory Structure
===================

+-------------------------+----------------------------------------------------+
| Directory               | Description                                        |
+=========================+====================================================+
| \\SW                    | Software source code for code composer studio IDE. |
+-------------------------+----------------------------------------------------+
| \\HW                    | Production files folder and CAD folder             |
+-------------------------+----------------------------------------------------+
|                     	  | All PCB revisions. Board and Schematics files for  |
| \\HW\\CAD               | both Autodesk Eagle and Altium Designer (Rev.1.0.0 |
|                         | to Rev.2.0.0). 3D step files for all components.   |
+-------------------------+----------------------------------------------------+
| \\build                 | All built code from the compile script goes        |
|                         | here.                                              |
+-------------------------+----------------------------------------------------+
| \\dist                  | Distributable Python wheels go here after the      |
|                         | build script has run.                              |
+-------------------------+----------------------------------------------------+
| \\doc                   | Source code for DST documentation. Sphinx is the   |
|                         | tool used to create the online documentation.      |
+-------------------------+----------------------------------------------------+
| \\doc\\extra            | Source rst files for the documentation.            |
+-------------------------+----------------------------------------------------+
| \\doc\\guides\\         | All the example code                               |
+-------------------------+----------------------------------------------------+
| \\doc\\img              | Images used in the documentation.                  |
+-------------------------+----------------------------------------------------+
| \\doc\\build\\html      | After making the documentation, all the HTML code  |
|                         | goes here. Look at this in a web browser to see    |
|                         | what the documentation will look like.             |
+-------------------------+----------------------------------------------------+
| \\examples              | Example code showing how to use Arcade.            |
+-------------------------+----------------------------------------------------+
| \\tests                 | Unit tests. Most unit tests are part of the        |
|                         | docstrings.                                        |
+-------------------------+----------------------------------------------------+

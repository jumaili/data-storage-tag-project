/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
****************************************************************************
               {spi.c} - Driver for the MSP430FR5738 SPI Interface
*****************************************************************************
*/

/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */
#include <core/spi.h>
#include <driverlib.h>

void SPI_Init(void)
{
    /* initialize eUSCI SPI master mode */
    EUSCI_A_SPI_masterInit(EUSCI_A0_BASE,
                           EUSCI_A_SPI_CLOCKSOURCE_SMCLK,
                           12000000, /* SMCLK Speed */
                           1000000,  /* SPI Bus Bitrate */
                           EUSCI_A_SPI_LSB_FIRST,
                           EUSCI_A_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT,
                           EUSCI_A_SPI_CLOCKPOLARITY_INACTIVITY_LOW,
                           EUSCI_A_SPI_3PIN);

    /* enable eUSCI SPI */
    EUSCI_A_SPI_enable(EUSCI_A0_BASE);
}


/*  ======== eUSCI_A0 Interrupt Service Routine ========
*/
#pragma vector=USCI_A0_VECTOR
__interrupt void USCI_A0_ISR_HOOK(void)
{
   /* USER CODE START (section: USCI_A0_ISR_HOOK) */
   /* replace this comment with your code */
   /* USER CODE END (section: USCI_A0_ISR_HOOK) */
}

/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
****************************************************************************
               {timer.c} - Driver for timer MSP430FR5738
*****************************************************************************
*/

/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */
#include <core/timer.h>
#include <driverlib/MSP430FR57xx/timer_b.h>

/* Starts Timer, Enters LPM3.5 and  */
void Timer_Low_Power_Delay_ms(uint32_t ms)
{
    /* Capture Compare  */
    TB0CCR0 = ms*10;


    /* Setup the timer in UpMode*/
    TB0CTL |= TIMER_B_UP_MODE;

    /*Sleep in LPM3*/
    __bis_SR_register(LPM3_bits + GIE);

    /*Stop Timer*/
    TB0CTL |= MC__STOP;
}


void Timer_B0_Init(void)
{
    /* TBCCR0 Capture Compare interrupt enabled */
    TB0CCTL0 = CCIE;

    /* Timer for time */
    TB0CCR0 = 10;

    /* Timer Source ACLK */
    TB0CTL = TASSEL__ACLK;
}


/* Timer B1 "Time" interrupt service routine */
#pragma vector = TIMER0_B0_VECTOR
__interrupt void Timer0_B0_ISR(void)
{
    LPM3_EXIT; /* Exit LPM3 */
}

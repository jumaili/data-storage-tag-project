/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
****************************************************************************
                  {fsm.c} - Driver for the Finite State Machine
*****************************************************************************
*/

/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */
#include <fsm.h>
#include <settings.h>
#include <nfc_request_handler.h>
#include <ctpl.h>
#include <ms58370.h>
/*For Testing, shall be moved to an event handler */

void FSM_Init(void)
{
    switch(CurrentState_t)
    {
    case Start:
        //MAKE LED BLINK

        I2C_Send_Single_Byte(MS5837_CONVERT_D1_8192);
        //__delay_cycles(24000*2); // wait 3 ms for the pressure sensor to get restarted. This should be accomplished by timers now.

        MS5738_Convert_And_Read_ADC();

        //
        //   __delay_cycles(1000000);               // Delay between transmissions

        //   while (EUSCI_B_I2C_SENDING_STOP == EUSCI_B_I2C_masterIsStopSent(EUSCI_B0_BASE));
        //   __delay_cycles(1000000);               // Delay between transmissions
        //        //

        /* Enter into LPM3.5 with restore on reset disabled. The RTC interrupt will wake up the MCU */
        ctpl_enterLpm35(CTPL_DISABLE_RESTORE_ON_RESET);
        break;
    case Power_Lost:

        break;

    case Data_logger_Init:

        break;

    case Wait_For_Command:

        //check NFC
        break;

    case Process_command:

        /* Switch Depending On Received Command */

        switch(Command_Received_t)
        {
        case Start_CMD:

            break;

        case Stop_CMD:

            break;

        case Clear_Data_CMD:

            break;

        case Reset_CMD:

            break;

        }

        break;
//
//    default:
//        CurrentState_t = Wait_For_Command;
//        break;
    }
}



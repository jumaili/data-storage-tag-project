/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
 *****************************************************************************
             {kmx62.c} - Driver for the accelerometer and magnetometer
 *****************************************************************************

/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */
#include <core/i2c.h>
#include <devices/kmx62.h>

/* Check whether KMX62 is connected and returns:
 *  true: Device is present
 *  false: device is not acknowledging I2C address
 */
bool KMX62_Is_Connected(void)
{
    //Check device ID
    // If Device ID matches datasheet then device is connected and operational.
}

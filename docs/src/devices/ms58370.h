/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
 *****************************************************************************
             {ms5738.h} - Driver for the pressure sensor
 *****************************************************************************
*/

#ifndef DEVICES_MS58370_H_
#define DEVICES_MS58370_H_


/* ------------------------------------------------------------------------------------------------
 *                                           Constants
 * ------------------------------------------------------------------------------------------------
 */
//const float MS5837_PA = 100.0f;
//const float MS5837_BAR = 0.001f;
//const float MS5837_MBAR = 1.0f;



/* ------------------------------------------------------------------------------------------------
 *                                           Defines
 * ------------------------------------------------------------------------------------------------
 */
/* MS5737 Device Commands */
#define MS5837_ADDR                                                 0x76
#define MS5837_RESET                                                0x1E
#define MS5837_ADC_READ                                             0x00
#define MS5837_PROM_READ                                            0xA0
#define MS5837_START_PRESSURE_ADC_CONVERSION                        0x40
#define MS5837_START_TEMPERATURE_ADC_CONVERSION                     0x50


#define MS5837_CONVERT_D1_256                                       0x40
#define MS5837_CONVERT_D1_1024                                      0x44
#define MS5837_CONVERT_D1_2048                                      0x46
#define MS5837_CONVERT_D1_4096                                      0x48
#define MS5837_CONVERT_D1_8192                                      0x4A

#define MS5837_CONVERT_D2_256                                       0x50
#define MS5837_CONVERT_D2_1024                                      0x54
#define MS5837_CONVERT_D2_2048                                      0x56
#define MS5837_CONVERT_D2_4096                                      0x58
#define MS5837_CONVERT_D2_8192                                      0x5A

/* PROM READ ADDRESSES */
#define MS5837_PROM_ADDRESS_0                                       0xA0
#define MS5837_PROM_ADDRESS_1                                       0xA2
#define MS5837_PROM_ADDRESS_2                                       0xA4
#define MS5837_PROM_ADDRESS_3                                       0xA6
#define MS5837_PROM_ADDRESS_4                                       0xA8
#define MS5837_PROM_ADDRESS_5                                       0xAA
#define MS5837_PROM_ADDRESS_6                                       0xAC
#define MS5837_PROM_ADDRESS_7                                       0xAE

/* Coefficients indexes for temperature and pressure computation */

#define MS5837_CRC_INDEX                                            0
#define MS5837_PRESSURE_SENSITIVITY_INDEX                           1
#define MS5837_PRESSURE_OFFSET_INDEX                                2
#define MS5837_TEMP_COEFF_OF_PRESSURE_SENSITIVITY_INDEX             3
#define MS5837_TEMP_COEFF_OF_PRESSURE_OFFSET_INDEX                  4
#define MS5837_REFERENCE_TEMPERATURE_INDEX                          5
#define MS5837_TEMP_COEFF_OF_TEMPERATURE_INDEX                      6
#define MS5837_COEFFICIENT_NUMBERS                                  7


/* ------------------------------------------------------------------------------------------------
 *                                      Function Prototypes
 * ------------------------------------------------------------------------------------------------
 */
void MS5837_Init(void);
void MS5837_Convert_And_Read_ADC(void);
#endif /* DEVICES_MS58370_H_ */

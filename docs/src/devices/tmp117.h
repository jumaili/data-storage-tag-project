/**
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
 * File {tmp117.h}
 */

#ifndef DEVICES_TMP117_H_
#define DEVICES_TMP117_H_
/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                           Defines
 * ------------------------------------------------------------------------------------------------
 */
/* TMP117 Addresses */
#define TMP117_ADDR                0x48            /*  ADDR connected to GND */
#define TMP117_DeviceID2           0x49            /*  Vcc */
#define TMP117_DeviceID3           0x4A            /*  SDA */
#define TMP117_DeviceID4           0x4B            /*  SCL */

/* TMP117 Registers */
#define TMP117_REG_TEMPERATURESULT 0x00
#define TMP117_REG_CONFIGURATION 0x01
#define TMP117_REG_ALERTHIGH 0x02
#define TMP117_REG_ALERTLOW 0x03
#define TMP117_REG_EEPROMUNLOCK 0x04
#define TMP117_REG_EEPROM1 0x05
#define TMP117_REG_EEPROM2 0x06
#define TMP117_REG_OFFSET 0x07
#define TMP117_REG_EEPROM3 0x08
#define TMP117_REG_DEVICEID 0x0F



//from below is unverfied.
#define TMP117CONFIGREGISTERPOL 8
#define TMP117CONFIGREGISTERTnA 16
#define TMP117CONFIGREGISTERAVG0 32
#define TMP117CONFIGREGISTERAVG1 64
#define TMP117CONFIGREGISTERCONV0 128

#define TMP117CONFIGREGISTERCONV1 1
#define TMP117CONFIGREGISTERCONV2 2
#define TMP117CONFIGREGISTERMOD0 4
#define TMP117CONFIGREGISTERMOD1 8
#define TMP117CONFIGREGISTEREEPROMBUSY 16
#define TMP117CONFIGREGISTERDATAREADY 32
#define TMP117CONFIGREGISTERLOWALERT 64
#define TMP117CONFIGREGISTERHIGHALERT 128

#define TMP117SHUTDOWN 0x04
#define TMP117ONESHOT 0x06


/* ------------------------------------------------------------------------------------------------
 *                                      Function Prototypes
 * ------------------------------------------------------------------------------------------------
 */
void TMP117_Init(void);

#endif /* DEVICES_TMP117_H_ */

	%% Generate magnetic field parameters using WMM and IGRF models as well to SST data
	% Made by Ali Aljumaili (Ali.jum@outlook.com) for data storage tag project

	clear all
	close all
	clc

	%% Make 'datadir' folder in Current Folder

	datadir = fullfile(pwd, 'datadir');
	if ~exist(datadir, 'dir')
	   mkdir(datadir)
	end

	datafile = @(filename)fullfile(datadir, filename);


	%% set the latitude and longitude limits
	latlimits = [63 79];
	lonlimits = [0 27];


	%% NetCDF (SST DATA)
	% https://data.nodc.noaa.gov/cgi-bin/iso?id=gov.noaa.nodc:AVHRR_Pathfinder-NCEI-L3C-v5.3
	% ftp://ftp.nodc.noaa.gov/pub/data.nodc/pathfinder/Version5.3/L3C/
	% http://coastwatch.pfeg.noaa.gov/erddap/griddap/erdMH1sstdmday.html
	% the file example: A20172742017304.L3m_MO_SST_sst_4km.nc

	[file,folder] = uigetfile('*.nc', 'Select the NetCDF file');
	filename = fullfile(folder,file)

	finfo = ncinfo(filename);
	%disp(finfo);

	AttrNames = {finfo.Attributes.Name};
	minMatch = strncmpi(AttrNames,'data_minimun',10);
	maxMatch = strncmpi(AttrNames,'data_maximun',10);
	dateMatch = strncmpi(AttrNames,'date_created',10);

	AttrVal = {finfo.Attributes.Value};
	minVal = cell2mat(AttrVal(find(minMatch,1)));
	maxVal = cell2mat(AttrVal(find(maxMatch,1)));
	dateVal = AttrVal(find(dateMatch,1));

	ncid = netcdf.open(filename);

	varname_sst = netcdf.inqVar(ncid,0);
	varname_lat = netcdf.inqVar(ncid,2);
	varname_lon = netcdf.inqVar(ncid,3);
	varname_pal = netcdf.inqVar(ncid,4);

	varid_sst = netcdf.inqVarID(ncid,varname_sst);
	varid_lat = netcdf.inqVarID(ncid,varname_lat);
	varid_lon = netcdf.inqVarID(ncid,varname_lon);
	varid_pal = netcdf.inqVarID(ncid,varname_pal);

	sst = netcdf.getVar(ncid,varid_sst);
	sst(sst<=-32767 | sst>=32767)=nan;
	sst = double(sst);
	sstC = sst*(maxVal-minVal)/(max(sst(:))-min(sst(:)));
	lat = netcdf.getVar(ncid,varid_lat);
	lon = netcdf.getVar(ncid,varid_lon);


	% plot full lat-lon range
	figure(1)
	[latgrid, longrid] = meshgrid(lat,lon);
	geoshow(latgrid,longrid,uint8(sstC),'DisplayType', 'texturemap')
	hcb = colorbar('eastoutside');
	title(['Sea Surface Temperature  T,C   ', dateVal])
	xlabel('Longitude')
	ylabel('Latitude')


	% select lat-lon range
	rangelat = find(lat>latlimits(1) & lat<latlimits(2));
	rangelon = find(lon>lonlimits(1) & lon<lonlimits(2));
	sstC_clip = sstC(rangelon,rangelat);
	lat_clip = double(lat(rangelat));
	lon_clip = double(lon(rangelon));


	% plot selected lat-lon range
	figure(2)
	[latgrid, longrid] = meshgrid(lat_clip,lon_clip);
	geoshow(latgrid,longrid,uint8(sstC_clip),'DisplayType', 'texturemap')
	hcb = colorbar('eastoutside');
	title(['Sea Surface Temperature  T,C   ', dateVal])
	xlabel('Longitude')
	ylabel('Latitude')


	R = georefcells([lat_clip(end) lat_clip(1)],[lon_clip(1) lon_clip(end)],...
	    size(sstC_clip'),'ColumnsStartFrom','north');

	filename_geotiff = datafile('sst.tif');
	geotiffwrite(filename_geotiff,sstC_clip',R)



	%% WMM (World Magnetic model)

	listlat = lat_clip';
	listlon = lon_clip';

	%% Magneticfield parameters
	%declination, inclination, horizontal component, north component,
	%east component, vertical component, and total field
	xyz = zeros(length(listlat), length(listlon), 3);
	h = zeros(length(listlat), length(listlon));
	dec = zeros(length(listlat), length(listlon));
	dip = zeros(length(listlat), length(listlon));
	f = zeros(length(listlat), length(listlon));

	for i = 1:length(listlat)
	    for j = 1:length(listlon)
	        [xyz(i,j,1:3), h(i,j), dec(i,j), dip(i,j), f(i,j)] = ...
	            wrldmagm(0, listlat(i), listlon(j), decyear(2017,12,5), '2015');
	    end
	end


	R = georefcells([listlat(end) listlat(1)],[listlon(1) listlon(end)],...
	    size(f),'ColumnsStartFrom','north');

	filename_geotiff = datafile('WMM_F.tif');
	geotiffwrite(filename_geotiff,f,R)

	filename_geotiff = datafile('WMM_H.tif');
	geotiffwrite(filename_geotiff,h,R)

	filename_geotiff = datafile('WMM_Dec.tif');
	geotiffwrite(filename_geotiff,dec,R)

	filename_geotiff = datafile('WMM_Dip.tif');
	geotiffwrite(filename_geotiff,dip,R)


	%% IGRF (International Geomagnetic Reference Field)

	listlat = lat_clip';
	listlon = lon_clip';
	%% Magneticfield parameters
	%declination, inclination, horizontal component, north component,
	%east component, vertical component, and total field
	%xyz = zeros(length(listlat), length(listlon), 3);
	h = zeros(length(listlat), length(listlon));
	dec = zeros(length(listlat), length(listlon));
	dip = zeros(length(listlat), length(listlon));
	f = zeros(length(listlat), length(listlon));

	for i = 1:length(listlat)
	    for j = 1:length(listlon)
	        [~,h(i,j),dec(i,j),dip(i,j),f(i,j)] ...
	            = igrfmagm(0,listlat(i), listlon(j), decyear(2017,12,5),12);
	    end
	end


	R = georefcells([listlat(end) listlat(1)],[listlon(1) listlon(end)],...
	    size(f),'ColumnsStartFrom','north');

	filename_geotiff = datafile('IGRF_F.tif');
	geotiffwrite(filename_geotiff,f,R)

	filename_geotiff = datafile('IGRF_H.tif');
	geotiffwrite(filename_geotiff,h,R)

	filename_geotiff = datafile('IGRF_Dec.tif');
	geotiffwrite(filename_geotiff,dec,R)

	filename_geotiff = datafile('IGRF_Dip.tif');
	geotiffwrite(filename_geotiff,dip,R)

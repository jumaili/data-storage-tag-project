######################################
Data Storage Tag Documentation Website 
######################################
..
  |code-quality| |build-status| |docs| |creative-lic|

.. |build-status| image:: https://circleci.com/bb/jumaili/data-storage-tag-project.svg?style=svg
    :alt: build status
    :scale: 100%

.. |docs| image:: https://readthedocs.org/projects/data-storage-tag-project/badge/?version=latest
        :alt: Documentation Status
        :target: https://data-storage-tag-project.readthedocs.io/en/latest/?badge=latest

.. |creative-lic| image:: https://img.shields.io/badge/License-CC%20BY--NC%204.0-lightgrey.svg
        :alt: License: CC BY-NC 4.0
        :scale: 100%
        :target: https://creativecommons.org/licenses/by-nc/4.0/

.. |code-quality| image:: https://api.codacy.com/project/badge/Grade/5e2aecc6f69f4d71b09f450e13dcedab
        :alt: Codacy
        :scale: 100%
        :target: https://www.codacy.com/app/ali.jum/Data-Storage-Tag-Project?utm_source=jumaili@bitbucket.org&amp;utm_medium=referral&amp;utm_content=jumaili/data-storage-tag-project&amp;utm_campaign=Badge_Grade


.. sidebar:: :fa:`fa fa-history` Recent Changes

   .. git_changelog::
      :revisions: 4

:ref:`Keyword Index <genindex>`, :ref:`Search Page <search>`


.. toctree::
  :maxdepth: 2

  getting-started/index.rst

  guides/index.rst

  issues.rst 

  code-examples-and-style/index.rst
  firmware/index.rst
  matlab.rst

  licenses.rst 




There are no more guides. You are now guideless. Good luck.

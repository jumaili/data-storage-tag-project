.. _ licenses:
..
    |code-quality| |build-status| |docs| |mit-lic|

#############
Code Licenses
#############

.. |build-status| image:: https://circleci.com/bb/jumaili/data-storage-tag-project.png
    :alt: build status
    :scale: 100%

.. |docs| image:: https://readthedocs.org/projects/data-storage-tag-project/badge/?version=latest
        :alt: Documentation Status
        :target: https://data-storage-tag-project.readthedocs.io/en/latest/?badge=latest

.. |mit-lic| image:: https://img.shields.io/badge/License-MIT-yellow.png
        :alt: License: MIT
        :scale: 100%
        :target: https://opensource.org/licenses/MIT

.. |code-quality| image:: https://api.codacy.com/project/badge/Grade/5e2aecc6f69f4d71b09f450e13dcedab
        :alt: Codacy
        :scale: 100%
        :target: https://www.codacy.com/app/ali.jum/Data-Storage-Tag-Project?utm_source=jumaili@bitbucket.org&amp;utm_medium=referral&amp;utm_content=jumaili/data-storage-tag-project&amp;utm_campaign=Badge_Grade


Firmware License for DST Project
================================

**Copyright 2019 Ali Aljumaili**::

    /; Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, 
    ; including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do ; so, subject to the following conditions:
    ;
    ; The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    ;
    ; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
    ; NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
    ; IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

**Copyright Notice for Code Examples from Texas Instruments**::

    /; --COPYRIGHT--,BSD_EX
    ; Copyright (c) 2012, Texas Instruments Incorporated
    ; All rights reserved.
    ;
    ; Redistribution and use in source and binary forms, with or without
    ; modification, are permitted provided that the following conditions
    ; are met:
    ;
    ; *  Redistributions of source code must retain the above copyright
    ;    notice, this list of conditions and the following disclaimer.
    ;
    ; *  Redistributions in binary form must reproduce the above copyright
    ;    notice, this list of conditions and the following disclaimer in the
    ;    documentation and/or other materials provided with the distribution.
    ;
    ; *  Neither the name of Texas Instruments Incorporated nor the names of
    ;    its contributors may be used to endorse or promote products derived
    ;    from this software without specific prior written permission.
    ;
    ; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
    ; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    ; THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    ; PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
    ; CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    ; EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    ; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    ; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    ; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    ; OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
    ; EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
    MSP430 CODE EXAMPLE DISCLAIMER
    ; ; MSP430 code examples are self -contained low -level programs that typically ; demonstrate a single peripheral function or device feature in a highly ; concise manner. For this the code may rely on the device ’s power -on default ; register values and settings such as the clock configuration and care must ; be taken when combining code from several examples to avoid potential side ; effects. Also see www.ti.com/grace for a GUI - and www.ti.com/msp430ware ; for an API functional library -approach to peripheral configuration. ;
    ;
    ; --/COPYRIGHT --
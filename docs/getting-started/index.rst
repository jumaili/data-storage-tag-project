.. _ intro:

###############
Getting Started
###############
This guide is written for documentation of the data storage tag project firmware and hardware. 

.. contents:: Contents
   :local:


Importing the Firmware to CCS
-----------------------------
The software can be imported into Code Composer Studio (CCS) by doing the following procedure:
Launch CCS --> Project --> Import CCS Project --> Browse --> Navigate to the repository and choose the SW folder.  



Repository Content 
------------------
This repository contains the hardware, software and documentation used to create the data storage tag.

This documentation for this repository can be found under the the ``doc`` folder in the root folder.

Cloning this repository can be done by done with git:
::
    $ git clone https://jumaili@bitbucket.org/jumaili/data-storage-tag-project.git

Folder Structure of Project

.. code-block:: matlab

    ﻿│ data storage tag project
    │
    ├── Extra
    │    ├── Custom Settings
    │    │    ├── CCS-Darktheme.xml
    │    │    ├── TexStudio_DarkTheme.txsprofile 
    │    │    └── waveforms-i2c-protocol.dwf3work
    │    │
    │    ├── Datasheets and Technical Reports
    │    │    ├── Devices
    │    │    │    ├── CM315D_E-annotated.pdf
    │    │    │    ├── RF430CL330H-NFC -annotated.pdf
    │    │    │    .
    │    │    │    .
    │    │    │    .
    │    │    │    ├── TMP117-annotated.pdf
    │    │    │    └── XC6201_LDO.pdf
    │    │    │
    │    │    └── NFC
    │    │         ├── DLP_DESIGN_RFID2_MODULE_Schematic.pdf
    │    │         └── dlp-rf430bp.pdf
    │    │
    │    ├── Research Budgets
    │    │    ├── Battery-Life-Worst-Case-Estimate.xlsx
    │    │    ├── Memory Budget.pdf
    │    │    └── Memory Budget.xlsx
    │    │
    │    ├── TI-Code-Examples
    │    │    ├── MSP430FR57xx_Code_Examples
    │    │    │    └── ...
    │    │    │
    │    │    ├── NFC Example Software/nfc-keyboard
    │    │    │    └── ...
    │    │    │
    │    │    ├── Over The Air Upgrade using The Bootloader Examples/MSP430FRBoot_1_02_00_00
    │    │    │    └── ...
    │    │    │
    │    │    ├── Software Snapshot
    │    │    │    └── ...
    │    │    │
    │    │    └── data-logger-software
    │    │         └── ...
    │    │
    │    └── Elprolab Dokumentasjon.zip
    │
    ├── HW
    │    ├── CAD
    │    │    ├── 3D-Step-Models-Files
    │    │    │    ├── 0805-NO.step
    │    │    │    ├── 1X03.step
    │    │    │    .
    │    │    │    .
    │    │    │    .
    │    │    │
    │    │    └── Rev.2.0.0 - Altium Designer
    │    │         ├── A4-NTNU.SchDoc
    │    │         ├── DST-Rev.2.0.0.OutJob
    │    │         .
    │    │         .
    │    │         .
    │    │         └── Sheet5-DST-Rev.2.0.0.SchDoc
    │    │
    │    └── Production-PCB-V2
    │         ├── Assembly.PDF
    │         ├── DST-BOM-V2.xlsx
    │         ├── Gerber V2.0.0.zip
    │         ├── PCB-3D.pdf
    │         └── PCB-Layers.pdf
    │
    ├── SW
    │    ├── DST_DevKit_FR5739
    │    │    ├── application_layer
    │    │    │     └── ...
    │    │    ├── driverlib/MSP430FR57xx
    │    │    │     └── ...
    │    │    .
    │    │    .
    │    │    .
    │    │    └── main.c
    │    │
    │    └── DST_MainBoard_FR5738v
    │         ├── application_layer
    │         │     └── ...
    │         ├── core
    │         │     └── ...
    │         .
    │         .
    │         .
    │         └── main.c
    │
    ├── docs
    │    .
    │    .
    │    .
    │    └── requirements.txt
    │
    ├── .gitinorge
    ├── .readthedocs.yml
    ├── LICENSE
    ├── README.rst




In the folder structure above:

- ``HW`` is the folder with all of the design files (schematics, board layout, BOM, Gerber files, assembly drawings) for Altium Designer and Eagle.
- ``doc`` is the directory where the Sphinx documentation reside these are used to generate the `Data Storage Tag User Guide Webs <https://dst-ntnu.bitbucket.io>`_.
- ``SW`` is the directory with the firmware software for the main prototype (MSP430FR5738 - 28 pin MCU) as well to software for the development kit (MSP430FR5739 - 40 pin MCU).
- ``extra`` is a directory that contains extra content like CCS configuration settings, device datasheets and code examples for the MSP430FR57xx MCU.

.. note::

	 ``data-storage-tag-project`` is referred to as the `Repository root`, while the folder
	``data-storage-tag-project/docs`` will be referred to `Sphinx root` or equivalently `Documentation root` folder.


License
-------
The documentation is licensed under `Creative Common Attribution-NonCommercial 4.0 International License`_ . The software files in ``/SW`` folder are under the `MIT license`_ .

.. _Creative Common Attribution-NonCommercial 4.0 International License: LICENSE

.. _MIT license: https://opensource.org/licenses/MIT

.. _ code:

##############################
Code Examples and Style Guide 
##############################

.. contents:: Contents
   :local:
   :depth: 3

=============
Code Examples
=============

The ``extra/TI-Code-Example`` folder has many well-written code examples that can be read to learn about embedded system software architect. 

================
Code Style Style
================

There are multiple style guides avaliable <https://www.maultech.com/chrislott/resources/cstyle/>.

This guide is intended to explain the coding conventions used for writing the firmware in C language.

The goal of this guide is *not* to be best way to write C code, but rather provide *consistency* in the firmware code. 

When in doubt, use your best judgement. Look at other examples by e.g. using Texas Instruments refrence guides.


--------------------
Function Declaration
--------------------
The documentation should be written like this.

.. code-block:: c

   /********************************************************
    * @fn          I2C_Init
    *
    * @brief       Initialize the I2C eUSCI Interface.
    *
    * @param       none
    *
    * @return      nonenn
    ********************************************************
    void I2C_Init(void)
  

-----------
Indentation
-----------

Use 4 spaces per indentation level.

-------------------
Maximum Line Length
-------------------

Keeping lines under the `PEP 8 recommendation <https://www.python.org/dev/peps/pep-0008/#maximum-line-length>`_ to a maximum of 79 (or 99)
characters helps readers easily parse the code



Code Snippet for MSP430FR2433 DCO Tuning
========================================

.. literalinclude:: /src/dco-tune.c
   :language: c


More information on the DCO tuning can be read on the e2e forum:
 - https://e2e.ti.com/support/microcontrollers/msp430/f/166/t/806342?MSP430FR5738-Setting-DCO-frequency


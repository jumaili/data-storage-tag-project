# Data Storage Tag - Reivsion 1.2.0

##Directory explanation

* DST-Rev.1.2.0: PCB board design file openable in Autodesk Eagle.
* DST-Rev.1.2.0: Schematics design file openable in Autodesk Eagle.
* 3D-Model.stp: 3D .stp file
* 3D-Model-Rev1.2.0.pdf: Adobe Acrobat 3D model

## Author
Created by **Ali Aljumaili** February 2019
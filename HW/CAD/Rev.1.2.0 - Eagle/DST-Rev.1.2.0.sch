<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="4" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="16" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Flex-Kleb" color="12" fill="1" visible="yes" active="yes"/>
<layer number="102" name="fp2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="fp4" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="MPL" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="tTestdril" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="bTestdril" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DST_Project">
<description>This library contain all of the needed parts for the prototype made october 2017 at NTNU as a project.
&lt;p&gt;
By Ali Al Jumaili 18.10.2017</description>
<packages>
<package name="LGA-16" urn="urn:adsk.eagle:footprint:793888/2" locally_modified="yes">
<description>LGA-16 3x3x1.1</description>
<wire x1="-0.0508" y1="-1.2192" x2="1.2192" y2="0.0508" width="0.0254" layer="21"/>
<wire x1="2.159" y1="0.0508" x2="1.905" y2="0.0508" width="0" layer="51"/>
<wire x1="1.651" y1="0.0508" x2="1.397" y2="0.0508" width="0" layer="51"/>
<wire x1="1.143" y1="0.0508" x2="0.889" y2="0.0508" width="0" layer="51"/>
<wire x1="-0.0508" y1="-0.4064" x2="-0.0508" y2="-0.6604" width="0" layer="51"/>
<wire x1="-0.0508" y1="-0.889" x2="-0.0508" y2="-1.143" width="0" layer="51"/>
<wire x1="-0.0508" y1="-1.397" x2="-0.0508" y2="-1.651" width="0" layer="51"/>
<wire x1="-0.0508" y1="-1.905" x2="-0.0508" y2="-2.159" width="0" layer="51"/>
<wire x1="-0.0508" y1="-2.3876" x2="-0.0508" y2="-2.6416" width="0" layer="51"/>
<wire x1="0.889" y1="-3.0988" x2="1.143" y2="-3.0988" width="0" layer="51"/>
<wire x1="1.397" y1="-3.0988" x2="1.651" y2="-3.0988" width="0" layer="51"/>
<wire x1="1.905" y1="-3.0988" x2="2.159" y2="-3.0988" width="0" layer="51"/>
<wire x1="3.0988" y1="-2.6416" x2="3.0988" y2="-2.3876" width="0" layer="51"/>
<wire x1="3.0988" y1="-2.159" x2="3.0988" y2="-1.905" width="0" layer="51"/>
<wire x1="3.0988" y1="-1.651" x2="3.0988" y2="-1.397" width="0" layer="51"/>
<wire x1="3.0988" y1="-1.143" x2="3.0988" y2="-0.889" width="0" layer="51"/>
<wire x1="3.0988" y1="-0.6604" x2="3.0988" y2="-0.4064" width="0" layer="51"/>
<wire x1="-0.0508" y1="-3.0988" x2="3.0988" y2="-3.0988" width="0.0254" layer="21"/>
<wire x1="3.0988" y1="-3.0988" x2="3.0988" y2="0.0508" width="0.0254" layer="25"/>
<wire x1="3.0988" y1="0.0508" x2="-0.0508" y2="0.0508" width="0.0254" layer="21"/>
<wire x1="-0.0508" y1="0.0508" x2="-0.0508" y2="-3.0988" width="0.0254" layer="21"/>
<smd name="1" x="0.1016" y="-0.5334" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="2" x="0.1016" y="-1.016" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="3" x="0.1016" y="-1.524" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="4" x="0.1016" y="-2.032" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="5" x="0.1016" y="-2.5146" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="6" x="1.016" y="-2.9464" dx="0.254" dy="0.7112" layer="1" rot="R180"/>
<smd name="7" x="1.524" y="-2.9464" dx="0.254" dy="0.7112" layer="1" rot="R180"/>
<smd name="8" x="2.032" y="-2.9464" dx="0.254" dy="0.7112" layer="1" rot="R180"/>
<smd name="9" x="2.9464" y="-2.5146" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="10" x="2.9464" y="-2.032" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="11" x="2.9464" y="-1.524" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="12" x="2.9464" y="-1.016" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="13" x="2.9464" y="-0.5334" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="14" x="2.032" y="-0.1016" dx="0.254" dy="0.7112" layer="1" rot="R180"/>
<smd name="15" x="1.524" y="-0.1016" dx="0.254" dy="0.7112" layer="1" rot="R180"/>
<smd name="16" x="1.016" y="-0.1016" dx="0.254" dy="0.7112" layer="1" rot="R180"/>
<circle x="0.111" y="-0.016" radius="0.25" width="0.127" layer="21"/>
<text x="2.643" y="1.556" size="0.5" layer="25" rot="R180">&gt;Name</text>
<text x="2.135" y="-3.85" size="0.5" layer="27" rot="R180">&gt;Value</text>
<polygon width="0.127" layer="41">
<vertex x="3.173" y="0.046"/>
<vertex x="3.173" y="-3.173"/>
<vertex x="-0.1095" y="-3.173"/>
<vertex x="-0.1095" y="0.1095"/>
</polygon>
<polygon width="0.127" layer="42">
<vertex x="-1" y="-4"/>
<vertex x="-1" y="1"/>
<vertex x="4" y="1"/>
<vertex x="4" y="-4"/>
</polygon>
<polygon width="0.127" layer="43">
<vertex x="-0.5" y="-3.5"/>
<vertex x="-0.5" y="0.5"/>
<vertex x="3.5" y="0.5"/>
<vertex x="3.5" y="-3.5"/>
</polygon>
</package>
<package name="MS5837-30BA" urn="urn:adsk.eagle:footprint:793890/1" library_version="3" library_locally_modified="yes">
<wire x1="-1.65" y1="1.65" x2="1.65" y2="1.65" width="0.127" layer="21"/>
<wire x1="1.65" y1="1.65" x2="1.65" y2="-1.65" width="0.127" layer="21"/>
<wire x1="1.65" y1="-1.65" x2="-1.65" y2="-1.65" width="0.127" layer="21"/>
<wire x1="-1.65" y1="-1.65" x2="-1.65" y2="1.65" width="0.127" layer="21"/>
<smd name="1" x="-1" y="1" dx="1.3" dy="1.3" layer="1"/>
<smd name="2" x="-1" y="-1" dx="1.3" dy="1.3" layer="1"/>
<smd name="3" x="1" y="-1" dx="1.3" dy="1.3" layer="1"/>
<smd name="4" x="1" y="1" dx="1.3" dy="1.3" layer="1"/>
<text x="-2" y="2.5" size="1.27" layer="25">&gt;Name</text>
<text x="-2" y="-4" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="SOT25" urn="urn:adsk.eagle:footprint:793903/2" library_version="9" library_locally_modified="yes">
<smd name="1" x="-0.95" y="-1.3032" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3032" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3032" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3032" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.9754" y="1.3032" dx="0.55" dy="1.2" layer="1"/>
<text x="-1.5" y="2" size="1.27" layer="25">&gt;Name</text>
<text x="-1.5" y="-3.5" size="1.27" layer="27">&gt;Value</text>
<circle x="-1.016" y="-0.381" radius="0.127" width="0.1016" layer="25"/>
<wire x1="1.422" y1="0.785" x2="1.422" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.785" x2="-1.422" y2="-0.785" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.785" x2="-1.422" y2="0.785" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.785" x2="1.422" y2="0.785" width="0.1524" layer="51"/>
<wire x1="-0.522" y1="0.785" x2="0.522" y2="0.785" width="0.1524" layer="21"/>
<wire x1="-0.428" y1="-0.785" x2="-0.522" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="0.522" y1="-0.785" x2="0.428" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="-1.328" y1="-0.785" x2="-1.422" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.785" x2="1.328" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="1.328" y1="0.785" x2="1.422" y2="0.785" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.785" x2="-1.328" y2="0.785" width="0.1524" layer="21"/>
<rectangle x1="-1.2" y1="-1.373" x2="-0.7" y2="-0.723" layer="51"/>
<rectangle x1="0.6898" y1="-1.3476" x2="1.1898" y2="-0.6976" layer="51"/>
<rectangle x1="-0.2398" y1="-1.3984" x2="0.2602" y2="-0.7484" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
</package>
<package name="UDFN-8" urn="urn:adsk.eagle:footprint:8841322/1" library_version="9" library_locally_modified="yes">
<smd name="1" x="1.01" y="4.445" dx="1.1" dy="0.45" layer="1"/>
<smd name="2" x="1.01" y="3.175" dx="1.1" dy="0.45" layer="1"/>
<smd name="3" x="1.01" y="1.905" dx="1.1" dy="0.45" layer="1"/>
<smd name="4" x="1.01" y="0.635" dx="1.1" dy="0.45" layer="1"/>
<smd name="5" x="6.61" y="0.635" dx="1.1" dy="0.45" layer="1"/>
<smd name="6" x="6.61" y="1.905" dx="1.1" dy="0.45" layer="1"/>
<smd name="7" x="6.61" y="3.175" dx="1.1" dy="0.45" layer="1"/>
<smd name="8" x="6.61" y="4.445" dx="1.1" dy="0.45" layer="1"/>
<smd name="9" x="3.81" y="2.54" dx="4.2" dy="3.6" layer="1" rot="R90"/>
<text x="2.54" y="6.35" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="0.21" y1="5.34" x2="7.41" y2="5.34" width="0.05" layer="51"/>
<wire x1="7.41" y1="5.34" x2="7.41" y2="-0.26" width="0.05" layer="51"/>
<wire x1="7.41" y1="-0.26" x2="0.21" y2="-0.26" width="0.05" layer="51"/>
<wire x1="0.21" y1="-0.26" x2="0.21" y2="5.34" width="0.05" layer="51"/>
<wire x1="0.81" y1="5.04" x2="6.81" y2="5.04" width="0.1" layer="51"/>
<wire x1="6.81" y1="5.04" x2="6.81" y2="0.04" width="0.1" layer="51"/>
<wire x1="6.81" y1="0.04" x2="0.81" y2="0.04" width="0.1" layer="51"/>
<wire x1="0.81" y1="0.04" x2="0.81" y2="5.04" width="0.1" layer="51"/>
<wire x1="0.81" y1="3.54" x2="2.31" y2="5.04" width="0.1" layer="51"/>
<circle x="0.46" y="5.17" radius="0.125" width="0.25" layer="25"/>
</package>
<package name="VQFN-32(RHB)" urn="urn:adsk.eagle:footprint:8841408/2" library_version="3">
<smd name="1" x="-2.4" y="1.750003125" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="2" x="-2.4" y="1.250003125" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="3" x="-2.4" y="0.75" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="4" x="-2.4" y="0.250003125" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="5" x="-2.4" y="-0.25" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="6" x="-2.4" y="-0.75" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="7" x="-2.4" y="-1.249996875" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="8" x="-2.4" y="-1.75" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="9" x="-1.75" y="-2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="-1.25" y="-2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="-0.75" y="-2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="12" x="-0.25" y="-2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="13" x="0.250003125" y="-2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="14" x="0.75" y="-2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="1.25" y="-2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="16" x="1.750003125" y="-2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="2.4" y="-1.75" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="18" x="2.4" y="-1.249996875" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="19" x="2.4" y="-0.75" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="20" x="2.4" y="-0.25" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="21" x="2.4" y="0.250003125" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="22" x="2.4" y="0.75" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="23" x="2.4" y="1.250003125" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="24" x="2.4" y="1.750003125" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="25" x="1.750003125" y="2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="26" x="1.25" y="2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="27" x="0.75" y="2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="28" x="0.250003125" y="2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="29" x="-0.25" y="2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="30" x="-0.75" y="2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="31" x="-1.25" y="2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="32" x="-1.75" y="2.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="33" x="0" y="0.000003125" dx="3.45" dy="3.45" layer="1" cream="no"/>
<smd name="V" x="0" y="1.475" dx="0.5" dy="0.5" layer="1" roundness="100"/>
<wire x1="-2.7178" y1="-2.7178" x2="-2.5146" y2="-2.7178" width="0.2032" layer="21"/>
<wire x1="-2.7178" y1="-2.7178" x2="-2.7178" y2="-2.5146" width="0.2032" layer="21"/>
<wire x1="2.5146" y1="-2.7178" x2="2.7178" y2="-2.7178" width="0.2032" layer="21"/>
<wire x1="2.7178" y1="-2.7178" x2="2.7178" y2="-2.5146" width="0.2032" layer="21"/>
<wire x1="2.7178" y1="2.5146" x2="2.7178" y2="2.7178" width="0.2032" layer="21"/>
<wire x1="2.5146" y1="2.7178" x2="2.7178" y2="2.7178" width="0.2032" layer="21"/>
<wire x1="-2.7178" y1="2.7178" x2="-2.286" y2="2.7178" width="0.2032" layer="21"/>
<wire x1="-2.7178" y1="2.286" x2="-2.7178" y2="2.7178" width="0.2032" layer="21"/>
<wire x1="-2.5654" y1="-2.5654" x2="-2.5654" y2="2.5654" width="0.1524" layer="51"/>
<wire x1="2.5654" y1="-2.5654" x2="2.5654" y2="2.5654" width="0.1524" layer="51"/>
<wire x1="-2.5654" y1="2.5654" x2="2.5654" y2="2.5654" width="0.1524" layer="51"/>
<wire x1="-2.5654" y1="-2.5654" x2="2.5654" y2="-2.5654" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="1.7272" x2="-2.1082" y2="1.7272" width="0.1016" layer="25" curve="-180"/>
<wire x1="-2.1082" y1="1.7272" x2="-1.4986" y2="1.7272" width="0.1016" layer="25" curve="-180"/>
<text x="-3.1242" y="3.937" size="1.27" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-3.1242" y="-4.953" size="1.27" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="-1.585996875"/>
<vertex x="1.586" y="-1.585996875"/>
<vertex x="1.586" y="-0.099996875"/>
<vertex x="0.1" y="-0.099996875"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-0.099996875" y="-0.099996875"/>
<vertex x="-1.585996875" y="-0.099996875"/>
<vertex x="-1.585996875" y="-1.585996875"/>
<vertex x="-0.099996875" y="-1.585996875"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-0.099996875" y="1.586"/>
<vertex x="-1.585996875" y="1.586"/>
<vertex x="-1.585996875" y="0.1"/>
<vertex x="-0.099996875" y="0.1"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="0.1"/>
<vertex x="1.586" y="0.1"/>
<vertex x="1.586" y="1.586"/>
<vertex x="0.1" y="1.586"/>
</polygon>
</package>
<package name="VQFN-16(RGT)" urn="urn:adsk.eagle:footprint:793897/1" library_version="8" library_locally_modified="yes">
<wire x1="-1.5748" y1="-1.5748" x2="1.5748" y2="-1.5748" width="0.1524" layer="51"/>
<wire x1="-1.5748" y1="1.5748" x2="1.5748" y2="1.5748" width="0.1524" layer="51"/>
<wire x1="1.5748" y1="-1.5748" x2="1.5748" y2="1.5748" width="0.1524" layer="51"/>
<wire x1="-1.5748" y1="-1.5748" x2="-1.5748" y2="1.5748" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="0.762" x2="-1.1684" y2="0.762" width="0.1016" layer="51" curve="-180"/>
<wire x1="-1.1684" y1="0.762" x2="-0.7874" y2="0.762" width="0.1016" layer="51" curve="-180"/>
<wire x1="-1.7272" y1="-1.7272" x2="-1.524" y2="-1.7272" width="0.2032" layer="21"/>
<wire x1="-1.7272" y1="-1.7272" x2="-1.7272" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="1.524" y1="-1.7272" x2="1.7272" y2="-1.7272" width="0.2032" layer="21"/>
<wire x1="1.7272" y1="-1.7272" x2="1.7272" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="1.7272" y1="1.524" x2="1.7272" y2="1.7272" width="0.2032" layer="21"/>
<wire x1="1.524" y1="1.7272" x2="1.7272" y2="1.7272" width="0.2032" layer="21"/>
<wire x1="-1.7272" y1="1.7272" x2="-1.0922" y2="1.7272" width="0.2032" layer="21"/>
<wire x1="-1.7272" y1="1.0922" x2="-1.7272" y2="1.7272" width="0.2032" layer="21"/>
<smd name="1" x="-1.4" y="0.75" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="2" x="-1.4" y="0.250003125" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="3" x="-1.4" y="-0.25" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="4" x="-1.4" y="-0.75" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="5" x="-0.75" y="-1.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="6" x="-0.25" y="-1.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="0.250003125" y="-1.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="8" x="0.75" y="-1.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="1.4" y="-0.75" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="10" x="1.4" y="-0.25" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="11" x="1.4" y="0.250003125" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="12" x="1.4" y="0.75" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R180"/>
<smd name="13" x="0.75" y="1.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="14" x="0.250003125" y="1.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="-0.25" y="1.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="16" x="-0.75" y="1.4" dx="0.36" dy="0.24" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="0.000003125" y="0" dx="1.7" dy="1.7" layer="1" cream="no"/>
<text x="-3.81" y="-3.81" size="1.778" layer="27">&gt;Value</text>
<text x="-3.81" y="2.54" size="1.778" layer="27">&gt;Name</text>
<polygon width="0.0254" layer="31">
<vertex x="-0.775" y="-0.775"/>
<vertex x="0.775" y="-0.775"/>
<vertex x="0.775" y="0.775"/>
<vertex x="-0.775" y="0.775"/>
</polygon>
</package>
<package name="DFN-6" urn="urn:adsk.eagle:footprint:793901/1" locally_modified="yes">
<circle x="-1" y="1" radius="0.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<smd name="1" x="-1.45" y="1" dx="0.85" dy="0.45" layer="1"/>
<smd name="2" x="-1.45" y="0" dx="0.85" dy="0.45" layer="1"/>
<smd name="3" x="-1.45" y="-1" dx="0.85" dy="0.45" layer="1"/>
<smd name="4" x="1.45" y="-1" dx="0.85" dy="0.45" layer="1"/>
<smd name="5" x="1.45" y="0" dx="0.85" dy="0.45" layer="1"/>
<smd name="6" x="1.45" y="1" dx="0.85" dy="0.45" layer="1"/>
<smd name="PADDLE" x="0" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-1.5" y="2" size="0.5" layer="25">&gt;Name</text>
<text x="-1.5" y="-2.5" size="0.5" layer="27">&gt;Value</text>
<polygon width="0.127" layer="40">
<vertex x="-1.778" y="-1.524"/>
<vertex x="-1.778" y="1.778"/>
<vertex x="1.651" y="1.778"/>
<vertex x="1.778" y="1.778"/>
<vertex x="1.778" y="-1.651"/>
<vertex x="1.778" y="-1.778"/>
<vertex x="-1.778" y="-1.778"/>
<vertex x="-1.778" y="-1.651"/>
</polygon>
</package>
<package name="C0603" urn="urn:adsk.eagle:footprint:23123/1" locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.729" x2="1.473" y2="0.729" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.729" x2="1.473" y2="-0.729" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.729" x2="-1.473" y2="-0.729" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.729" x2="-1.473" y2="0.729" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-1.651" y1="0.762" x2="1.651" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.762" x2="1.651" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.762" x2="-1.651" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.127" layer="21"/>
</package>
<package name="R603" urn="urn:adsk.eagle:footprint:8849198/1" locally_modified="yes">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.729" x2="1.473" y2="0.729" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.729" x2="1.473" y2="-0.729" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.729" x2="-1.473" y2="-0.729" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.729" x2="-1.473" y2="0.729" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<wire x1="-1.651" y1="0.889" x2="1.651" y2="0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.889" x2="1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.889" x2="-1.651" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-0.889" x2="-1.651" y2="0.889" width="0.127" layer="21"/>
</package>
<package name="CRYSTAL" urn="urn:adsk.eagle:footprint:8849199/2" library_version="11">
<smd name="1" x="0" y="-1.0668" dx="1.9" dy="1.1" layer="1" rot="R90"/>
<smd name="2" x="3.1" y="-1.0668" dx="1.9" dy="1.1" layer="1" rot="R90"/>
<text x="-2" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-5" size="1.27" layer="25">&gt;VALUE</text>
<wire x1="-0.5" y1="-1.9" x2="3.6" y2="-1.9" width="0.127" layer="51"/>
<wire x1="3.6" y1="-1.9" x2="3.6" y2="-0.3" width="0.127" layer="51"/>
<wire x1="3.6" y1="-0.3" x2="-0.5" y2="-0.3" width="0.127" layer="51"/>
<wire x1="-0.5" y1="-0.3" x2="-0.5" y2="-1.9" width="0.127" layer="51"/>
</package>
<package name="C0201" urn="urn:adsk.eagle:footprint:23196/1" library_version="10" library_locally_modified="yes">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C0504" urn="urn:adsk.eagle:footprint:23122/1" library_version="10" library_locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0402K" urn="urn:adsk.eagle:footprint:23186/1" library_version="10" library_locally_modified="yes">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="TC2030-MCP-NL" urn="urn:adsk.eagle:footprint:8896795/1" locally_modified="yes">
<description>&lt;B&gt;TAG-CONNECT ICSP Connector&lt;/B&gt;&lt;BR&gt;&lt;I&gt;Manufacturer:&lt;/I&gt; &lt;a href="www.tag-connect.com"&gt;Tag-Connect&lt;/a&gt;&lt;BR&gt;
&lt;BR&gt;Cable for easy In-Circuit Serial Programming. Designed for Microchip ICD2, suitable for many others.&lt;BR&gt;

&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt;&lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;MCLR/Vpp&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;Vdd&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;GND&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;PGD (ISPDAT)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;PGC (ISPCLK)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;nc (used for LVP)&lt;/TD&gt;&lt;/TR&gt;
&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;

©2009 ROFA.cz - modified and updated by Robert Darlington &amp;#8249;rdarlington@gmail.com&amp;#8250;</description>
<smd name="2" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="4" x="2.54" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="5" x="3.81" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="3" x="2.54" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="1" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;name</text>
<hole x="0" y="0" drill="0.889"/>
<hole x="5.08" y="-1.016" drill="0.889"/>
<hole x="5.08" y="1.016" drill="0.889"/>
<polygon width="0.0254" layer="39">
<vertex x="1.27" y="-0.2413"/>
<vertex x="1.6637" y="-0.2413"/>
<vertex x="1.6637" y="-0.635"/>
<vertex x="2.1463" y="-0.635"/>
<vertex x="2.1463" y="-0.2413"/>
<vertex x="2.9337" y="-0.2413"/>
<vertex x="2.9337" y="-0.635"/>
<vertex x="3.4163" y="-0.635"/>
<vertex x="3.4163" y="-0.2413"/>
<vertex x="3.81" y="-0.2413"/>
<vertex x="3.81" y="0.2413"/>
<vertex x="3.4163" y="0.2413"/>
<vertex x="3.4163" y="0.635"/>
<vertex x="2.9337" y="0.635"/>
<vertex x="2.9337" y="0.2413"/>
<vertex x="2.1463" y="0.2413"/>
<vertex x="2.1463" y="0.635"/>
<vertex x="1.6637" y="0.635"/>
<vertex x="1.6637" y="0.2413"/>
<vertex x="1.27" y="0.2413"/>
</polygon>
<smd name="6" x="3.81" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
</package>
<package name="TC2030-MCP" urn="urn:adsk.eagle:footprint:8896794/1" library_version="11">
<description>&lt;b&gt;TAG-CONNECT ICSP Connector&lt;/b&gt; - Legged version&lt;BR&gt;&lt;I&gt;Manufacturer:&lt;/I&gt; &lt;a href="http://www.tag-connect.com"&gt;Tag-Connect&lt;/a&gt;
&lt;p&gt;
Cable for easy In-Circuit Serial Programming. Designed for Microchip ICD2, suitable for many others.
&lt;p&gt;
&lt;b&gt;NOTE:&lt;/b&gt; Eagle's default spacing for drill holes does not leave sufficent room for routing traces for this footprint and should be adjusted. &lt;br&gt;
This setting can be found in the board layout editor under the Edit menu.  Select "Design Rules" and then the Distance tab.  8 mils for Drill/Hole works well.
&lt;br&gt;
&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt;&lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;MCLR/Vpp&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;Vdd&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;GND&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;PGD (ISPDAT)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;PGC (ISPCLK)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;nc (used for LVP)&lt;/TD&gt;&lt;/TR&gt;
&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;
©2009 ROFA.cz - modified and updated by Robert Darlington &amp;#8249;rdarlington@gmail.com&amp;#8250;</description>
<smd name="2" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="4" x="2.54" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="6" x="3.81" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="5" x="3.81" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="3" x="2.54" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="1" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<text x="-1.27" y="-2.54" size="1.27" layer="25" rot="R90">&gt;name</text>
<hole x="0" y="0" drill="0.889"/>
<hole x="5.08" y="-1.016" drill="0.889"/>
<hole x="5.08" y="1.016" drill="0.889"/>
<hole x="0" y="2.54" drill="2.3748"/>
<hole x="0" y="-2.54" drill="2.3748"/>
<hole x="3.175" y="-2.54" drill="2.3748"/>
<hole x="3.175" y="2.54" drill="2.3748"/>
<polygon width="0.0254" layer="39">
<vertex x="1.27" y="-0.2413"/>
<vertex x="1.6637" y="-0.2413"/>
<vertex x="1.6637" y="-0.635"/>
<vertex x="2.1463" y="-0.635"/>
<vertex x="2.1463" y="-0.2413"/>
<vertex x="2.9337" y="-0.2413"/>
<vertex x="2.9337" y="-0.635"/>
<vertex x="3.4163" y="-0.635"/>
<vertex x="3.4163" y="-0.2413"/>
<vertex x="3.81" y="-0.2413"/>
<vertex x="3.81" y="0.2413"/>
<vertex x="3.4163" y="0.2413"/>
<vertex x="3.4163" y="0.635"/>
<vertex x="2.9337" y="0.635"/>
<vertex x="2.9337" y="0.2413"/>
<vertex x="2.1463" y="0.2413"/>
<vertex x="2.1463" y="0.635"/>
<vertex x="1.6637" y="0.635"/>
<vertex x="1.6637" y="0.2413"/>
<vertex x="1.27" y="0.2413"/>
</polygon>
</package>
<package name="TC2030-MCP-NL-CP" urn="urn:adsk.eagle:footprint:8896796/1" library_version="11">
<description>&lt;B&gt;TAG-CONNECT ICSP Connector&lt;/B&gt;&lt;I&gt;- with optional copper pads for steel alignment pins&lt;/I&gt;&lt;BR&gt;&lt;I&gt;Manufacturer:&lt;/I&gt; &lt;a href="http://www.tag-connect.com"&gt;Tag-Connect&lt;/a&gt;&lt;BR&gt;
&lt;BR&gt;Cable for easy In-Circuit Serial Programming. Designed for Microchip ICD2, suitable for many others.
&lt;p&gt;
&lt;b&gt;NOTE:&lt;/b&gt; Eagle's default spacing for drill holes does not leave sufficent room for routing traces for this footprint and should be adjusted. &lt;br&gt;
This setting can be found in the board layout editor under the Edit menu.  Select "Design Rules" and then the Distance tab.  8 mils for Drill/Hole works well.
&lt;br&gt;
&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt;&lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;MCLR/Vpp&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;Vdd&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;GND&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;PGD (ISPDAT)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;PGC (ISPCLK)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;nc (used for LVP)&lt;/TD&gt;&lt;/TR&gt;
&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;

&lt;B&gt;Note:&lt;/B&gt; Suitable Receptacle pins are 0295-0-15-xx-06-xx-10-0 series from &lt;a href="www.mill-max.com"&gt;Mill-Max&lt;/a&gt;&lt;BR&gt;&lt;BR&gt;

©2009 ROFA.cz - modified and updated by Robert Darlington &amp;#8249;rdarlington@gmail.com&amp;#8250;</description>
<smd name="2" x="1.27" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="4" x="2.54" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="6" x="3.81" y="0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="5" x="3.81" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="3" x="2.54" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<smd name="1" x="1.27" y="-0.635" dx="0.7874" dy="0.7874" layer="1" roundness="100" cream="no"/>
<text x="-1.27" y="1.905" size="1.27" layer="25">&gt;name</text>
<hole x="0" y="0" drill="1.6"/>
<hole x="5.08" y="1.016" drill="1.6"/>
<hole x="5.08" y="-1.016" drill="1.6"/>
<polygon width="0.0254" layer="39">
<vertex x="1.27" y="-0.2413"/>
<vertex x="1.6637" y="-0.2413"/>
<vertex x="1.6637" y="-0.635"/>
<vertex x="2.1463" y="-0.635"/>
<vertex x="2.1463" y="-0.2413"/>
<vertex x="2.9337" y="-0.2413"/>
<vertex x="2.9337" y="-0.635"/>
<vertex x="3.4163" y="-0.635"/>
<vertex x="3.4163" y="-0.2413"/>
<vertex x="3.81" y="-0.2413"/>
<vertex x="3.81" y="0.2413"/>
<vertex x="3.4163" y="0.2413"/>
<vertex x="3.4163" y="0.635"/>
<vertex x="2.9337" y="0.635"/>
<vertex x="2.9337" y="0.2413"/>
<vertex x="2.1463" y="0.2413"/>
<vertex x="2.1463" y="0.635"/>
<vertex x="1.6637" y="0.635"/>
<vertex x="1.6637" y="0.2413"/>
<vertex x="1.27" y="0.2413"/>
</polygon>
<polygon width="0.0254" layer="16">
<vertex x="3.556" y="-1.016" curve="90"/>
<vertex x="5.08" y="-2.54" curve="90"/>
<vertex x="6.604" y="-1.016"/>
<vertex x="6.604" y="1.016" curve="90"/>
<vertex x="5.08" y="2.54" curve="90"/>
<vertex x="3.556" y="1.016"/>
</polygon>
<polygon width="0.0254" layer="16">
<vertex x="-1.524" y="0" curve="-90"/>
<vertex x="0" y="1.524" curve="-90"/>
<vertex x="1.524" y="0" curve="-90"/>
<vertex x="0" y="-1.524" curve="-90"/>
</polygon>
<polygon width="0.254" layer="30">
<vertex x="3.556" y="-1.016" curve="90"/>
<vertex x="5.08" y="-2.54" curve="90"/>
<vertex x="6.604" y="-1.016"/>
<vertex x="6.604" y="1.016" curve="90"/>
<vertex x="5.08" y="2.54" curve="90"/>
<vertex x="3.556" y="1.016"/>
</polygon>
<polygon width="0.254" layer="30">
<vertex x="-1.524" y="0" curve="-90"/>
<vertex x="0" y="1.524" curve="-90"/>
<vertex x="1.524" y="0" curve="-90"/>
<vertex x="0" y="-1.524" curve="-90"/>
</polygon>
</package>
<package name="TP">
<pad name="TP$1" x="0" y="0" drill="0.2"/>
<text x="0.381" y="0.508" size="0.3048" layer="25">&gt;NAME</text>
<text x="0.381" y="-0.635" size="0.3048" layer="27">&gt;VALUE</text>
</package>
<package name="VQFN-32_MANUAL">
<smd name="1" x="-2.55" y="1.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="2" x="-2.55" y="1.250003125" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="3" x="-2.55" y="0.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="4" x="-2.55" y="0.250003125" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="5" x="-2.55" y="-0.25" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="6" x="-2.55" y="-0.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="7" x="-2.55" y="-1.249996875" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="8" x="-2.55" y="-1.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="9" x="-1.7" y="-2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="10" x="-1.2" y="-2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="11" x="-0.7" y="-2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="12" x="-0.2" y="-2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="13" x="0.300003125" y="-2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="14" x="0.8" y="-2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="1.3" y="-2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="16" x="1.800003125" y="-2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="2.55" y="-1.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="18" x="2.55" y="-1.249996875" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="19" x="2.55" y="-0.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="20" x="2.55" y="-0.25" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="21" x="2.55" y="0.250003125" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="22" x="2.55" y="0.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="23" x="2.55" y="1.250003125" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="24" x="2.55" y="1.750003125" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="25" x="1.750003125" y="2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="26" x="1.25" y="2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="27" x="0.75" y="2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="28" x="0.25" y="2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="29" x="-0.25" y="2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="30" x="-0.75" y="2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="31" x="-1.25" y="2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="32" x="-1.75" y="2.55" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="33" x="0" y="0.000003125" dx="3.45" dy="3.45" layer="1" cream="no"/>
<wire x1="-2.7178" y1="-2.7178" x2="-2.5146" y2="-2.7178" width="0.2032" layer="21"/>
<wire x1="-2.7178" y1="-2.7178" x2="-2.7178" y2="-2.5146" width="0.2032" layer="21"/>
<wire x1="2.5146" y1="-2.7178" x2="2.7178" y2="-2.7178" width="0.2032" layer="21"/>
<wire x1="2.7178" y1="-2.7178" x2="2.7178" y2="-2.5146" width="0.2032" layer="21"/>
<wire x1="2.7178" y1="2.5146" x2="2.7178" y2="2.7178" width="0.2032" layer="21"/>
<wire x1="2.5146" y1="2.7178" x2="2.7178" y2="2.7178" width="0.2032" layer="21"/>
<wire x1="-2.7178" y1="2.7178" x2="-2.286" y2="2.7178" width="0.2032" layer="21"/>
<wire x1="-2.7178" y1="2.286" x2="-2.7178" y2="2.7178" width="0.2032" layer="21"/>
<wire x1="-2.5654" y1="-2.5654" x2="-2.5654" y2="2.5654" width="0.1524" layer="51"/>
<wire x1="2.5654" y1="-2.5654" x2="2.5654" y2="2.5654" width="0.1524" layer="51"/>
<wire x1="-2.5654" y1="2.5654" x2="2.5654" y2="2.5654" width="0.1524" layer="51"/>
<wire x1="-2.5654" y1="-2.5654" x2="2.5654" y2="-2.5654" width="0.1524" layer="51"/>
<wire x1="-1.4986" y1="1.7272" x2="-2.1082" y2="1.7272" width="0.1016" layer="25" curve="-180"/>
<wire x1="-2.1082" y1="1.7272" x2="-1.4986" y2="1.7272" width="0.1016" layer="25" curve="-180"/>
<text x="-3.1242" y="3.937" size="1.27" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-3.1242" y="-4.953" size="1.27" layer="27" ratio="6" rot="SR0">&gt;VALUE</text>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="-1.585996875"/>
<vertex x="1.586" y="-1.585996875"/>
<vertex x="1.586" y="-0.099996875"/>
<vertex x="0.1" y="-0.099996875"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-0.099996875" y="-0.099996875"/>
<vertex x="-1.585996875" y="-0.099996875"/>
<vertex x="-1.585996875" y="-1.585996875"/>
<vertex x="-0.099996875" y="-1.585996875"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-0.099996875" y="1.586"/>
<vertex x="-1.585996875" y="1.586"/>
<vertex x="-1.585996875" y="0.1"/>
<vertex x="-0.099996875" y="0.1"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="0.1" y="0.1"/>
<vertex x="1.586" y="0.1"/>
<vertex x="1.586" y="1.586"/>
<vertex x="0.1" y="1.586"/>
</polygon>
</package>
<package name="VQFN-16_MANUAL">
<wire x1="-1.5748" y1="-1.5748" x2="1.5748" y2="-1.5748" width="0.1524" layer="51"/>
<wire x1="-1.5748" y1="1.5748" x2="1.5748" y2="1.5748" width="0.1524" layer="51"/>
<wire x1="1.5748" y1="-1.5748" x2="1.5748" y2="1.5748" width="0.1524" layer="51"/>
<wire x1="-1.5748" y1="-1.5748" x2="-1.5748" y2="1.5748" width="0.1524" layer="51"/>
<wire x1="-0.7874" y1="0.762" x2="-1.1684" y2="0.762" width="0.1016" layer="51" curve="-180"/>
<wire x1="-1.1684" y1="0.762" x2="-0.7874" y2="0.762" width="0.1016" layer="51" curve="-180"/>
<wire x1="-1.7272" y1="-1.7272" x2="-1.524" y2="-1.7272" width="0.2032" layer="21"/>
<wire x1="-1.7272" y1="-1.7272" x2="-1.7272" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="1.524" y1="-1.7272" x2="1.7272" y2="-1.7272" width="0.2032" layer="21"/>
<wire x1="1.7272" y1="-1.7272" x2="1.7272" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="1.7272" y1="1.524" x2="1.7272" y2="1.7272" width="0.2032" layer="21"/>
<wire x1="1.524" y1="1.7272" x2="1.7272" y2="1.7272" width="0.2032" layer="21"/>
<wire x1="-1.7272" y1="1.7272" x2="-1.0922" y2="1.7272" width="0.2032" layer="21"/>
<wire x1="-1.7272" y1="1.0922" x2="-1.7272" y2="1.7272" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="2" x="-1.6" y="0.250003125" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="3" x="-1.6" y="-0.25" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="4" x="-1.6" y="-0.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="5" x="-0.75" y="-1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="6" x="-0.25" y="-1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="0.250003125" y="-1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="8" x="0.75" y="-1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="1.6" y="-0.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="10" x="1.6" y="-0.25" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="11" x="1.6" y="0.250003125" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="12" x="1.6" y="0.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="13" x="0.75" y="1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="14" x="0.250003125" y="1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="-0.25" y="1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="16" x="-0.75" y="1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="17" x="0.000003125" y="0" dx="1.7" dy="1.7" layer="1" cream="no"/>
<text x="-3.81" y="-3.81" size="1.778" layer="27">&gt;Value</text>
<text x="-3.81" y="2.54" size="1.778" layer="27">&gt;Name</text>
<polygon width="0.0254" layer="31">
<vertex x="-0.775" y="-0.775"/>
<vertex x="0.775" y="-0.775"/>
<vertex x="0.775" y="0.775"/>
<vertex x="-0.775" y="0.775"/>
</polygon>
</package>
<package name="TP_PAD">
<text x="-0.635" y="1.016" size="0.3048" layer="25">&gt;NAME</text>
<text x="-0.762" y="-1.397" size="0.3048" layer="27">&gt;VALUE</text>
<smd name="P$1" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="70" rot="R90"/>
</package>
<package name="SOD-523" urn="urn:adsk.eagle:footprint:38401/1" locally_modified="yes">
<description>SOD-523 (Small Outline Diode)</description>
<smd name="C" x="0.85" y="0" dx="0.6" dy="0.8" layer="1"/>
<smd name="A" x="-0.85" y="0" dx="0.6" dy="0.8" layer="1"/>
<wire x1="-0.625" y1="-0.425" x2="0.625" y2="-0.425" width="0.127" layer="21"/>
<wire x1="0.625" y1="0.425" x2="-0.625" y2="0.425" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="-0.4" x2="0.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="-0.4" x2="0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.6" y1="-0.1" x2="0.6" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.1" x2="0.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.4" x2="0.3" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="-0.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.4" x2="-0.6" y2="0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.1" x2="-0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.1" x2="-0.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.1" x2="0.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.1" x2="0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.1" x2="0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.1" x2="-0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.1" x2="-0.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0.1" x2="-0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="1.3716" y1="0.3048" x2="1.3716" y2="-0.3048" width="0.2032" layer="21"/>
</package>
<package name="TP_PAD_SOLDER">
<text x="-0.635" y="1.016" size="0.3048" layer="25">&gt;NAME</text>
<text x="-0.762" y="-1.397" size="0.3048" layer="27">&gt;VALUE</text>
<smd name="P$1" x="0" y="0" dx="2" dy="2" layer="1" roundness="70" rot="R90"/>
</package>
<package name="DIOC0603_N">
<description>&lt;b&gt;0603&lt;/b&gt; chip &lt;p&gt;

0603 (imperial)&lt;br/&gt;
1608 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<smd name="K" x="-0.8" y="0" dx="1.05" dy="0.9" layer="1"/>
<smd name="A" x="0.8" y="0" dx="1.05" dy="0.9" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.8" y1="0.4" x2="-0.3" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.3" y1="0.4" x2="0.1" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.1" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="0.1" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.1" y1="-0.4" x2="-0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.3" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.7" x2="1.6" y2="0.7" width="0.1" layer="39"/>
<wire x1="1.6" y1="0.7" x2="-1.6" y2="0.7" width="0.1" layer="39"/>
<wire x1="-1.6" y1="0.7" x2="-1.6" y2="-0.7" width="0.1" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="1.6" y2="-0.7" width="0.1" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.2" layer="21"/>
<wire x1="0.1" y1="0.4" x2="-0.3" y2="0" width="0.127" layer="51"/>
<wire x1="-0.3" y1="0" x2="0.1" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.1" y1="-0.4" x2="0.1" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="51"/>
</package>
<package name="1X02" urn="urn:adsk.eagle:footprint:793894/1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="1X02/90" urn="urn:adsk.eagle:footprint:793895/1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<pad name="1" x="-1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-3.175" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="4.445" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
<package name="SMD_CON">
<smd name="P$1" x="-0.7" y="0" dx="1.27" dy="0.635" layer="1" roundness="80" rot="R90"/>
<smd name="P$2" x="0.7" y="0" dx="1.27" dy="0.635" layer="1" roundness="80" rot="R90"/>
</package>
</packages>
<packages3d>
<package3d name="LGA-16" urn="urn:adsk.eagle:package:793905/5" type="model">
<description>LGA-16 3x3x1.1</description>
</package3d>
<package3d name="MS5837-30BA" urn="urn:adsk.eagle:package:793906/2" type="box">
</package3d>
<package3d name="SOT95P210X110-5" urn="urn:adsk.eagle:package:793913/4" type="model">
</package3d>
<package3d name="UDFN-8" urn="urn:adsk.eagle:package:8841323/2" type="model">
</package3d>
<package3d name="DFN-6" urn="urn:adsk.eagle:package:793912/2" type="box">
</package3d>
<package3d name="C0603" urn="urn:adsk.eagle:package:23616/2" type="model">
<description>CAPACITOR</description>
</package3d>
<package3d name="R603" urn="urn:adsk.eagle:package:8849207/2" type="model">
</package3d>
<package3d name="XTAL320X155X90" urn="urn:adsk.eagle:package:8849208/3" type="model">
</package3d>
<package3d name="TC2030-MCP-NL" urn="urn:adsk.eagle:package:8896798/1" type="box">
<description>&lt;B&gt;TAG-CONNECT ICSP Connector&lt;/B&gt;&lt;BR&gt;&lt;I&gt;Manufacturer:&lt;/I&gt; &lt;a href="www.tag-connect.com"&gt;Tag-Connect&lt;/a&gt;&lt;BR&gt;
&lt;BR&gt;Cable for easy In-Circuit Serial Programming. Designed for Microchip ICD2, suitable for many others.&lt;BR&gt;

&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt;&lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;MCLR/Vpp&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;Vdd&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;GND&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;PGD (ISPDAT)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;PGC (ISPCLK)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;nc (used for LVP)&lt;/TD&gt;&lt;/TR&gt;
&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;

©2009 ROFA.cz - modified and updated by Robert Darlington &amp;#8249;rdarlington@gmail.com&amp;#8250;</description>
</package3d>
<package3d name="VQFN-32(RHB)" urn="urn:adsk.eagle:package:8841410/3" type="model">
</package3d>
<package3d name="VQFN-16(RGT)" urn="urn:adsk.eagle:package:793910/2" type="model">
</package3d>
<package3d name="C0201" urn="urn:adsk.eagle:package:23690/2" type="model">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
</package3d>
<package3d name="C0504" urn="urn:adsk.eagle:package:23624/2" type="model">
<description>CAPACITOR</description>
</package3d>
<package3d name="C0402K" urn="urn:adsk.eagle:package:23679/2" type="model">
<description>Ceramic Chip Capacitor KEMET 0204 reflow solder
Metric Code Size 1005</description>
</package3d>
<package3d name="TC2030-MCP" urn="urn:adsk.eagle:package:8896799/1" type="box">
<description>&lt;b&gt;TAG-CONNECT ICSP Connector&lt;/b&gt; - Legged version&lt;BR&gt;&lt;I&gt;Manufacturer:&lt;/I&gt; &lt;a href="http://www.tag-connect.com"&gt;Tag-Connect&lt;/a&gt;
&lt;p&gt;
Cable for easy In-Circuit Serial Programming. Designed for Microchip ICD2, suitable for many others.
&lt;p&gt;
&lt;b&gt;NOTE:&lt;/b&gt; Eagle's default spacing for drill holes does not leave sufficent room for routing traces for this footprint and should be adjusted. &lt;br&gt;
This setting can be found in the board layout editor under the Edit menu.  Select "Design Rules" and then the Distance tab.  8 mils for Drill/Hole works well.
&lt;br&gt;
&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt;&lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;MCLR/Vpp&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;Vdd&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;GND&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;PGD (ISPDAT)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;PGC (ISPCLK)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;nc (used for LVP)&lt;/TD&gt;&lt;/TR&gt;
&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;
©2009 ROFA.cz - modified and updated by Robert Darlington &amp;#8249;rdarlington@gmail.com&amp;#8250;</description>
</package3d>
<package3d name="TC2030-MCP-NL-CP" urn="urn:adsk.eagle:package:8896797/1" type="box">
<description>&lt;B&gt;TAG-CONNECT ICSP Connector&lt;/B&gt;&lt;I&gt;- with optional copper pads for steel alignment pins&lt;/I&gt;&lt;BR&gt;&lt;I&gt;Manufacturer:&lt;/I&gt; &lt;a href="http://www.tag-connect.com"&gt;Tag-Connect&lt;/a&gt;&lt;BR&gt;
&lt;BR&gt;Cable for easy In-Circuit Serial Programming. Designed for Microchip ICD2, suitable for many others.
&lt;p&gt;
&lt;b&gt;NOTE:&lt;/b&gt; Eagle's default spacing for drill holes does not leave sufficent room for routing traces for this footprint and should be adjusted. &lt;br&gt;
This setting can be found in the board layout editor under the Edit menu.  Select "Design Rules" and then the Distance tab.  8 mils for Drill/Hole works well.
&lt;br&gt;
&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt;&lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;MCLR/Vpp&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;Vdd&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;GND&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;PGD (ISPDAT)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;PGC (ISPCLK)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;nc (used for LVP)&lt;/TD&gt;&lt;/TR&gt;
&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;

&lt;B&gt;Note:&lt;/B&gt; Suitable Receptacle pins are 0295-0-15-xx-06-xx-10-0 series from &lt;a href="www.mill-max.com"&gt;Mill-Max&lt;/a&gt;&lt;BR&gt;&lt;BR&gt;

©2009 ROFA.cz - modified and updated by Robert Darlington &amp;#8249;rdarlington@gmail.com&amp;#8250;</description>
</package3d>
<package3d name="1X02" urn="urn:adsk.eagle:package:793908/2" type="model">
<description>PIN HEADER</description>
</package3d>
<package3d name="1X02/90" urn="urn:adsk.eagle:package:793909/1" type="box">
<description>PIN HEADER</description>
</package3d>
</packages3d>
<symbols>
<symbol name="KMX62">
<wire x1="-17.78" y1="12.7" x2="17.78" y2="12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="12.7" x2="17.78" y2="-33.02" width="0.254" layer="94"/>
<wire x1="17.78" y1="-33.02" x2="-17.78" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-33.02" x2="-17.78" y2="12.7" width="0.254" layer="94"/>
<pin name="ADDR" x="-22.86" y="-20.32" length="middle"/>
<pin name="CAP" x="22.86" y="-17.78" length="middle" direction="nc" rot="R180"/>
<pin name="GND_1" x="-22.86" y="-22.86" length="middle"/>
<pin name="GND_2" x="-22.86" y="-25.4" length="middle"/>
<pin name="GND_3" x="-22.86" y="-27.94" length="middle"/>
<pin name="GPIO1" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="GPIO2" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="IO_VDD" x="-22.86" y="5.08" length="middle"/>
<pin name="NC_1" x="22.86" y="-30.48" length="middle" direction="nc" rot="R180"/>
<pin name="NC_2" x="22.86" y="-27.94" length="middle" direction="nc" rot="R180"/>
<pin name="NC_3" x="22.86" y="-25.4" length="middle" direction="nc" rot="R180"/>
<pin name="NC_4" x="22.86" y="-22.86" length="middle" direction="nc" rot="R180"/>
<pin name="NC_5" x="22.86" y="-20.32" length="middle" direction="nc" rot="R180"/>
<pin name="SCL" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="SDA" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="VDD" x="-22.86" y="10.16" length="middle"/>
<text x="-12.7" y="15.24" size="3.048" layer="95">&gt;Name</text>
<text x="-7.62" y="-38.1" size="3.048" layer="95">&gt;Value</text>
</symbol>
<symbol name="MS5837-30BA">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="GND" x="-12.7" y="0" length="middle"/>
<pin name="SCL" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="SDA" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="VDD" x="-12.7" y="5.08" length="middle"/>
<text x="-7.62" y="10.16" size="3.048" layer="95">&gt;Name</text>
<text x="-7.62" y="-7.62" size="3.048" layer="95">&gt;Value</text>
</symbol>
<symbol name="XC6504A251MR-G">
<wire x1="7.62" y1="12.7" x2="7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="-7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-12.7" x2="-7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="-7.62" y1="12.7" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<pin name="CE" x="-12.7" y="-7.62" length="middle"/>
<pin name="NC" x="12.7" y="0" length="middle" direction="nc" rot="R180"/>
<pin name="VIN" x="-12.7" y="7.62" length="middle" direction="pwr"/>
<pin name="VOUT" x="12.7" y="7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS" x="12.7" y="-7.62" length="middle" direction="pwr" rot="R180"/>
<text x="-10.16" y="15.24" size="3.048" layer="95">&gt;Name</text>
<text x="-7.62" y="-17.78" size="3.048" layer="95">&gt;Value</text>
</symbol>
<symbol name="AT45DB641E">
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="-12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-15.24" x2="12.7" y2="-15.24" width="0.254" layer="94"/>
<pin name="!CS" x="-17.78" y="-7.62" length="middle"/>
<pin name="!RESET" x="-17.78" y="-12.7" length="middle"/>
<pin name="!WP" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="GND" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="SCK" x="-17.78" y="-2.54" length="middle"/>
<pin name="SI" x="-17.78" y="7.62" length="middle"/>
<pin name="SO" x="-17.78" y="2.54" length="middle"/>
<pin name="VCC" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="TP" x="17.78" y="-12.7" length="middle" rot="R180"/>
<text x="-12.7" y="12.7" size="3.048" layer="95">&gt;Name</text>
<text x="-17.78" y="-22.86" size="3.048" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="MSP430FR2633RHB">
<pin name="RST_NMI_SBWTDIO_N" x="-43.18" y="2.54" length="middle"/>
<pin name="TEST_SBWTCK" x="-43.18" y="-2.54" length="middle" direction="in"/>
<pin name="P1-4_UCA0TXD_UCA0SIMO_TA1-2_TCK_A4_VREF+" x="-43.18" y="-20.32" length="middle"/>
<pin name="P1-5_UCA0RXD_UCA0SOMI_TA1-1_TMS_A5" x="-43.18" y="-22.86" length="middle"/>
<pin name="P1-6_UCA0CLK_TA1CLK_TDI_TCLK_A6" x="-43.18" y="-33.02" length="middle"/>
<pin name="P1-7_UCA0STE_SMCLK_TDO_A7" x="43.18" y="-20.32" length="middle" rot="R180"/>
<pin name="P1-0_UCB0STE_TA0CLK_A0_VEREF+" x="-43.18" y="-27.94" length="middle"/>
<pin name="P1-1_UCB0CLK_TA0-1_A1" x="-43.18" y="-30.48" length="middle"/>
<pin name="P1-2_UCB0SIMO_UCB0SDA_TA0-2_A2_VEREF-" x="-43.18" y="-7.62" length="middle"/>
<pin name="P1-3_UCB0SOMI_UCB0SCL_MCLK_A3" x="-43.18" y="-12.7" length="middle"/>
<pin name="P2-2_SYNC_ACLK" x="43.18" y="-22.86" length="middle" rot="R180"/>
<pin name="P3-0_CAP0-0" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="CAP0-1" x="43.18" y="-38.1" length="middle" direction="pas" rot="R180"/>
<pin name="P2-3_CAP0-2" x="43.18" y="-27.94" length="middle" rot="R180"/>
<pin name="CAP0-3" x="43.18" y="-30.48" length="middle" direction="pas" rot="R180"/>
<pin name="P3-1_UCA1STE_CAP1-0" x="-43.18" y="-60.96" length="middle"/>
<pin name="P2-4_UCA1CLK_CAP1-1" x="-43.18" y="-53.34" length="middle"/>
<pin name="P2-5_UCA1RXD_UCA1SOMI_CAP1-2" x="-43.18" y="-55.88" length="middle"/>
<pin name="P2-6_UCA1TXD_UCA1SIMO_CAP1-3" x="-43.18" y="-58.42" length="middle"/>
<pin name="VREG" x="43.18" y="0" length="middle" direction="pwr" rot="R180"/>
<pin name="CAP2-0" x="43.18" y="-40.64" length="middle" direction="pas" rot="R180"/>
<pin name="CAP2-1" x="43.18" y="-43.18" length="middle" direction="pas" rot="R180"/>
<pin name="CAP2-2" x="43.18" y="-45.72" length="middle" direction="pas" rot="R180"/>
<pin name="CAP2-3" x="43.18" y="-48.26" length="middle" direction="pas" rot="R180"/>
<pin name="P2-7_CAP3-0" x="43.18" y="-10.16" length="middle" rot="R180"/>
<pin name="CAP3-1" x="43.18" y="-50.8" length="middle" direction="pas" rot="R180"/>
<pin name="P3-2_CAP3-2" x="43.18" y="-7.62" length="middle" rot="R180"/>
<pin name="CAP3-3" x="43.18" y="-53.34" length="middle" direction="pas" rot="R180"/>
<pin name="P2-0_XOUT" x="-43.18" y="-38.1" length="middle"/>
<pin name="P2-1_XIN" x="-43.18" y="-45.72" length="middle"/>
<pin name="DVSS" x="43.18" y="-58.42" length="middle" direction="pwr" rot="R180"/>
<pin name="DVCC" x="43.18" y="2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="PAD" x="43.18" y="-60.96" length="middle" direction="pwr" rot="R180"/>
<wire x1="-38.1" y1="-63.5" x2="38.1" y2="-63.5" width="0.2032" layer="94"/>
<wire x1="38.1" y1="-63.5" x2="38.1" y2="5.08" width="0.2032" layer="94"/>
<wire x1="38.1" y1="5.08" x2="-38.1" y2="5.08" width="0.2032" layer="94"/>
<wire x1="-38.1" y1="5.08" x2="-38.1" y2="-63.5" width="0.2032" layer="94"/>
<text x="-37.7444" y="6.5786" size="2.54" layer="95" ratio="6" rot="SR0">&gt;NAME</text>
<text x="-38.3794" y="-67.0814" size="2.54" layer="96" ratio="6" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="RF430CL330HRGT">
<wire x1="-15.24" y1="-25.4" x2="15.24" y2="-25.4" width="0.2032" layer="94"/>
<wire x1="15.24" y1="-25.4" x2="15.24" y2="22.86" width="0.2032" layer="94"/>
<wire x1="15.24" y1="22.86" x2="-15.24" y2="22.86" width="0.2032" layer="94"/>
<wire x1="-15.24" y1="22.86" x2="-15.24" y2="-25.4" width="0.2032" layer="94"/>
<pin name="ANT1" x="20.32" y="7.62" length="middle" direction="in" rot="R180"/>
<pin name="ANT2" x="20.32" y="-2.54" length="middle" direction="in" rot="R180"/>
<pin name="E0" x="-20.32" y="-10.16" length="middle" direction="in"/>
<pin name="E1" x="-20.32" y="-12.7" length="middle"/>
<pin name="E2" x="-20.32" y="-15.24" length="middle" direction="in"/>
<pin name="INTO" x="20.32" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="NC" x="20.32" y="-22.86" length="middle" direction="nc" rot="R180"/>
<pin name="NC_2" x="20.32" y="-17.78" length="middle" direction="nc" rot="R180"/>
<pin name="RST_N" x="-20.32" y="10.16" length="middle" direction="in"/>
<pin name="SCK" x="-20.32" y="-2.54" length="middle" direction="in"/>
<pin name="SCMS_CS_N" x="-20.32" y="-5.08" length="middle" direction="in"/>
<pin name="SI_SDA" x="-20.32" y="2.54" length="middle"/>
<pin name="SO_SCL" x="-20.32" y="5.08" length="middle"/>
<pin name="TP" x="-20.32" y="-22.86" length="middle"/>
<pin name="VCC" x="-20.32" y="17.78" length="middle" direction="pwr"/>
<pin name="VCORE" x="20.32" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS" x="-20.32" y="-20.32" length="middle" direction="pwr"/>
<text x="-14.8844" y="24.3586" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-7.8994" y="-28.9814" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
<symbol name="SI7053">
<wire x1="-17.78" y1="10.16" x2="17.78" y2="10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="10.16" x2="17.78" y2="-7.62" width="0.254" layer="94"/>
<wire x1="17.78" y1="-7.62" x2="-17.78" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-7.62" x2="-17.78" y2="10.16" width="0.254" layer="94"/>
<pin name="DNC1" x="-22.86" y="7.62" length="middle"/>
<pin name="DNC2" x="-22.86" y="5.08" length="middle"/>
<pin name="GND" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="SCL" x="-22.86" y="-5.08" length="middle"/>
<pin name="SDA" x="-22.86" y="0" length="middle"/>
<pin name="TP" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="VDD" x="22.86" y="7.62" length="middle" rot="R180"/>
<text x="-17.78" y="12.7" size="3.048" layer="95">&gt;Name</text>
<text x="-17.78" y="-15.24" size="3.048" layer="95">&gt;Value</text>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="-2.54" x2="1.905" y2="-2.54" width="0.254" layer="94"/>
<text x="-4.572" y="-8.382" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="0" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="CAPACITOR">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="VCC">
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CRYSTAL">
<wire x1="-1.27" y1="2.54" x2="1.397" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.397" y1="2.54" x2="1.397" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.397" y1="-2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="2.3368" y1="2.54" x2="2.3368" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-2.286" y1="2.54" x2="-2.286" y2="-2.54" width="0.4064" layer="94"/>
<text x="-5.08" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="TC2030">
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="11.43" y2="-7.62" width="0.254" layer="94"/>
<wire x1="11.43" y1="-7.62" x2="11.43" y2="10.16" width="0.254" layer="94"/>
<wire x1="11.43" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<circle x="8.128" y="0.508" radius="0.5679" width="0.254" layer="94"/>
<circle x="7.112" y="-5.588" radius="0.5679" width="0.254" layer="94"/>
<circle x="9.144" y="-5.588" radius="0.5679" width="0.254" layer="94"/>
<pin name="SBWTDIO" x="-12.7" y="7.62" length="short"/>
<pin name="FET_VCC_OUT" x="-12.7" y="5.08" length="short" direction="nc"/>
<pin name="TCK" x="-12.7" y="2.54" length="short"/>
<pin name="VCC_IN" x="-12.7" y="0" length="short" direction="pwr"/>
<pin name="VSS" x="-12.7" y="-2.54" length="short" direction="pwr"/>
<pin name="SBWTCK" x="-12.7" y="-5.08" length="short"/>
<polygon width="0.254" layer="94">
<vertex x="8.382" y="-1.016" curve="-90"/>
<vertex x="8.89" y="-0.508" curve="-90"/>
<vertex x="9.398" y="-1.016" curve="-90"/>
<vertex x="8.89" y="-1.524" curve="-90"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="6.858" y="-1.016" curve="-90"/>
<vertex x="7.366" y="-0.508" curve="-90"/>
<vertex x="7.874" y="-1.016" curve="-90"/>
<vertex x="7.366" y="-1.524" curve="-90"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="8.382" y="-2.54" curve="-90"/>
<vertex x="8.89" y="-2.032" curve="-90"/>
<vertex x="9.398" y="-2.54" curve="-90"/>
<vertex x="8.89" y="-3.048" curve="-90"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="6.858" y="-2.54" curve="-90"/>
<vertex x="7.366" y="-2.032" curve="-90"/>
<vertex x="7.874" y="-2.54" curve="-90"/>
<vertex x="7.366" y="-3.048" curve="-90"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="6.858" y="-4.064" curve="-90"/>
<vertex x="7.366" y="-3.556" curve="-90"/>
<vertex x="7.874" y="-4.064" curve="-90"/>
<vertex x="7.366" y="-4.572" curve="-90"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="8.382" y="-4.064" curve="-90"/>
<vertex x="8.89" y="-3.556" curve="-90"/>
<vertex x="9.398" y="-4.064" curve="-90"/>
<vertex x="8.89" y="-4.572" curve="-90"/>
</polygon>
<text x="-10.16" y="-10.16" size="1.778" layer="95">&gt;VALUE</text>
<text x="-11.176" y="11.176" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="TEST-POINT">
<pin name="TP" x="5.08" y="0" visible="off" length="middle" rot="R180"/>
<circle x="0" y="0" radius="0.915809375" width="0.127" layer="94"/>
<text x="0" y="1.524" size="1.27" layer="95">&gt;NAME</text>
<text x="0" y="-2.54" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="SCHOTTKY">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="-2.286" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
</symbol>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 1</text>
<text x="2.54" y="0" size="0.4064" layer="99" align="center">SpiceOrder 2</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="KMX62-1031-SR">
<description>&lt;b&gt;Tri-Axis Magnetometer/ Tri-Axis Accelerometer&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="KMX62" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LGA-16">
<connects>
<connect gate="G$1" pin="ADDR" pad="7"/>
<connect gate="G$1" pin="CAP" pad="2"/>
<connect gate="G$1" pin="GND_1" pad="3"/>
<connect gate="G$1" pin="GND_2" pad="5"/>
<connect gate="G$1" pin="GND_3" pad="12"/>
<connect gate="G$1" pin="GPIO1" pad="11"/>
<connect gate="G$1" pin="GPIO2" pad="9"/>
<connect gate="G$1" pin="IO_VDD" pad="1"/>
<connect gate="G$1" pin="NC_1" pad="8"/>
<connect gate="G$1" pin="NC_2" pad="10"/>
<connect gate="G$1" pin="NC_3" pad="13"/>
<connect gate="G$1" pin="NC_4" pad="15"/>
<connect gate="G$1" pin="NC_5" pad="16"/>
<connect gate="G$1" pin="SCL" pad="4"/>
<connect gate="G$1" pin="SDA" pad="6"/>
<connect gate="G$1" pin="VDD" pad="14"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:793905/5"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MS5837-30BA">
<description>&lt;b&gt;Pressure sensor&lt;/b&gt;&lt;p&gt;
 High resolution pressure sensors with I2C bus interface</description>
<gates>
<gate name="G$1" symbol="MS5837-30BA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MS5837-30BA">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="SCL" pad="3"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="VDD" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:793906/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XC6504A251MR-G">
<description>&lt;b&gt;LDO&lt;/b&gt;&lt;p&gt;

Low Quiescent Current Voltage Regulator with ON/OFF Control</description>
<gates>
<gate name="G$1" symbol="XC6504A251MR-G" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT25">
<connects>
<connect gate="G$1" pin="CE" pad="3"/>
<connect gate="G$1" pin="NC" pad="4"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
<connect gate="G$1" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:793913/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AT45DB641E-UDFN">
<description>&lt;b&gt;ADESTO TECHNOLOGIES - AT45DB641E-MHN-Y - MEMORY, SERIAL FLASH, 64MBIT, UDFN-8&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.mouser.com/ds/2/590/DS-45DB641E-027-534005.pdf"&gt; Datasheet &lt;/a&gt;
&lt;p&gt; Created By Ali Aljumaili  - 2019 &lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="AT45DB641E" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UDFN-8">
<connects>
<connect gate="G$1" pin="!CS" pad="4"/>
<connect gate="G$1" pin="!RESET" pad="3"/>
<connect gate="G$1" pin="!WP" pad="5"/>
<connect gate="G$1" pin="GND" pad="7"/>
<connect gate="G$1" pin="SCK" pad="2"/>
<connect gate="G$1" pin="SI" pad="1"/>
<connect gate="G$1" pin="SO" pad="8"/>
<connect gate="G$1" pin="TP" pad="9"/>
<connect gate="G$1" pin="VCC" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8841323/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="ADESTO TECHNOLOGIES - AT45DB641E-MHN-Y - MEMORY, SERIAL FLASH, 64MBIT, UDFN-8" constant="no"/>
<attribute name="HEIGHT" value="0.6mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Adesto Technologies" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="AT45DB641E-MHN-Y" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MSP430FR2633IRHB" prefix="U">
<description>&lt;b&gt;Texas Instruments | MCU - MSP430FR2633&lt;/b&gt;&lt;p&gt;
Ultra-low-power MSP430™ microcontrollers with CapTIvate technology - the lowest power, most noise-immune capacitive touch</description>
<gates>
<gate name="A" symbol="MSP430FR2633RHB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="VQFN-32(RHB)">
<connects>
<connect gate="A" pin="CAP0-1" pad="13"/>
<connect gate="A" pin="CAP0-3" pad="15"/>
<connect gate="A" pin="CAP2-0" pad="21"/>
<connect gate="A" pin="CAP2-1" pad="22"/>
<connect gate="A" pin="CAP2-2" pad="23"/>
<connect gate="A" pin="CAP2-3" pad="24"/>
<connect gate="A" pin="CAP3-1" pad="26"/>
<connect gate="A" pin="CAP3-3" pad="28"/>
<connect gate="A" pin="DVCC" pad="32"/>
<connect gate="A" pin="DVSS" pad="31"/>
<connect gate="A" pin="P1-0_UCB0STE_TA0CLK_A0_VEREF+" pad="7"/>
<connect gate="A" pin="P1-1_UCB0CLK_TA0-1_A1" pad="8"/>
<connect gate="A" pin="P1-2_UCB0SIMO_UCB0SDA_TA0-2_A2_VEREF-" pad="9"/>
<connect gate="A" pin="P1-3_UCB0SOMI_UCB0SCL_MCLK_A3" pad="10"/>
<connect gate="A" pin="P1-4_UCA0TXD_UCA0SIMO_TA1-2_TCK_A4_VREF+" pad="3"/>
<connect gate="A" pin="P1-5_UCA0RXD_UCA0SOMI_TA1-1_TMS_A5" pad="4"/>
<connect gate="A" pin="P1-6_UCA0CLK_TA1CLK_TDI_TCLK_A6" pad="5"/>
<connect gate="A" pin="P1-7_UCA0STE_SMCLK_TDO_A7" pad="6"/>
<connect gate="A" pin="P2-0_XOUT" pad="29"/>
<connect gate="A" pin="P2-1_XIN" pad="30"/>
<connect gate="A" pin="P2-2_SYNC_ACLK" pad="11"/>
<connect gate="A" pin="P2-3_CAP0-2" pad="14"/>
<connect gate="A" pin="P2-4_UCA1CLK_CAP1-1" pad="17"/>
<connect gate="A" pin="P2-5_UCA1RXD_UCA1SOMI_CAP1-2" pad="18"/>
<connect gate="A" pin="P2-6_UCA1TXD_UCA1SIMO_CAP1-3" pad="19"/>
<connect gate="A" pin="P2-7_CAP3-0" pad="25"/>
<connect gate="A" pin="P3-0_CAP0-0" pad="12"/>
<connect gate="A" pin="P3-1_UCA1STE_CAP1-0" pad="16"/>
<connect gate="A" pin="P3-2_CAP3-2" pad="27"/>
<connect gate="A" pin="PAD" pad="33"/>
<connect gate="A" pin="RST_NMI_SBWTDIO_N" pad="1"/>
<connect gate="A" pin="TEST_SBWTCK" pad="2"/>
<connect gate="A" pin="VREG" pad="20"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8841410/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="ALTERNATEPARTNUMBER" value="MSP430FR2633IRHBT" constant="no"/>
<attribute name="APPLICATION_BUILDNUMBER" value="*" constant="no"/>
<attribute name="COMPTYPE" value="IC" constant="no"/>
<attribute name="MANUFACTURER" value="Texas Instruments" constant="no"/>
<attribute name="MOUNTTYPE" value="SMT" constant="no"/>
<attribute name="PACKAGEREFERENCE" value="RHB0032E" constant="no"/>
<attribute name="PARTNUMBER" value="MSP430FR2633IRHBR" constant="no"/>
<attribute name="PIN_COUNT" value="33" constant="no"/>
<attribute name="REFDES" value="RefDes" constant="no"/>
<attribute name="TYPE" value="TYPE" constant="no"/>
<attribute name="VALUE" value="Value" constant="no"/>
</technology>
</technologies>
</device>
<device name="VQFN-32_MANUAL" package="VQFN-32_MANUAL">
<connects>
<connect gate="A" pin="CAP0-1" pad="13"/>
<connect gate="A" pin="CAP0-3" pad="15"/>
<connect gate="A" pin="CAP2-0" pad="21"/>
<connect gate="A" pin="CAP2-1" pad="22"/>
<connect gate="A" pin="CAP2-2" pad="23"/>
<connect gate="A" pin="CAP2-3" pad="24"/>
<connect gate="A" pin="CAP3-1" pad="26"/>
<connect gate="A" pin="CAP3-3" pad="28"/>
<connect gate="A" pin="DVCC" pad="32"/>
<connect gate="A" pin="DVSS" pad="31"/>
<connect gate="A" pin="P1-0_UCB0STE_TA0CLK_A0_VEREF+" pad="7"/>
<connect gate="A" pin="P1-1_UCB0CLK_TA0-1_A1" pad="8"/>
<connect gate="A" pin="P1-2_UCB0SIMO_UCB0SDA_TA0-2_A2_VEREF-" pad="9"/>
<connect gate="A" pin="P1-3_UCB0SOMI_UCB0SCL_MCLK_A3" pad="10"/>
<connect gate="A" pin="P1-4_UCA0TXD_UCA0SIMO_TA1-2_TCK_A4_VREF+" pad="3"/>
<connect gate="A" pin="P1-5_UCA0RXD_UCA0SOMI_TA1-1_TMS_A5" pad="4"/>
<connect gate="A" pin="P1-6_UCA0CLK_TA1CLK_TDI_TCLK_A6" pad="5"/>
<connect gate="A" pin="P1-7_UCA0STE_SMCLK_TDO_A7" pad="6"/>
<connect gate="A" pin="P2-0_XOUT" pad="29"/>
<connect gate="A" pin="P2-1_XIN" pad="30"/>
<connect gate="A" pin="P2-2_SYNC_ACLK" pad="11"/>
<connect gate="A" pin="P2-3_CAP0-2" pad="14"/>
<connect gate="A" pin="P2-4_UCA1CLK_CAP1-1" pad="17"/>
<connect gate="A" pin="P2-5_UCA1RXD_UCA1SOMI_CAP1-2" pad="18"/>
<connect gate="A" pin="P2-6_UCA1TXD_UCA1SIMO_CAP1-3" pad="19"/>
<connect gate="A" pin="P2-7_CAP3-0" pad="25"/>
<connect gate="A" pin="P3-0_CAP0-0" pad="12"/>
<connect gate="A" pin="P3-1_UCA1STE_CAP1-0" pad="16"/>
<connect gate="A" pin="P3-2_CAP3-2" pad="27"/>
<connect gate="A" pin="PAD" pad="33"/>
<connect gate="A" pin="RST_NMI_SBWTDIO_N" pad="1"/>
<connect gate="A" pin="TEST_SBWTCK" pad="2"/>
<connect gate="A" pin="VREG" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RF430CL330HIRGTR">
<description>&lt;b&gt;Texas Instruments - RF430CL330H -  Dynamic Dual Interface NFC Transponder
 - VQFN-16&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.ti.com/lit/gpn/rf430cl330h"&gt; Datasheet &lt;/a&gt;
&lt;p&gt; Created By Ali Aljumaili  - 2019 &lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RF430CL330HRGT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="VQFN-16(RGT)">
<connects>
<connect gate="G$1" pin="ANT1" pad="1"/>
<connect gate="G$1" pin="ANT2" pad="2"/>
<connect gate="G$1" pin="E0" pad="4"/>
<connect gate="G$1" pin="E1" pad="5"/>
<connect gate="G$1" pin="E2" pad="6"/>
<connect gate="G$1" pin="INTO" pad="7"/>
<connect gate="G$1" pin="NC" pad="14"/>
<connect gate="G$1" pin="NC_2" pad="16"/>
<connect gate="G$1" pin="RST_N" pad="3"/>
<connect gate="G$1" pin="SCK" pad="9"/>
<connect gate="G$1" pin="SCMS_CS_N" pad="8"/>
<connect gate="G$1" pin="SI_SDA" pad="11"/>
<connect gate="G$1" pin="SO_SCL" pad="10"/>
<connect gate="G$1" pin="TP" pad="17"/>
<connect gate="G$1" pin="VCC" pad="15"/>
<connect gate="G$1" pin="VCORE" pad="12"/>
<connect gate="G$1" pin="VSS" pad="13"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:793910/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VQFN-16_MANUAL" package="VQFN-16_MANUAL">
<connects>
<connect gate="G$1" pin="ANT1" pad="1"/>
<connect gate="G$1" pin="ANT2" pad="2"/>
<connect gate="G$1" pin="E0" pad="4"/>
<connect gate="G$1" pin="E1" pad="5"/>
<connect gate="G$1" pin="E2" pad="6"/>
<connect gate="G$1" pin="INTO" pad="7"/>
<connect gate="G$1" pin="NC" pad="14"/>
<connect gate="G$1" pin="NC_2" pad="16"/>
<connect gate="G$1" pin="RST_N" pad="3"/>
<connect gate="G$1" pin="SCK" pad="9"/>
<connect gate="G$1" pin="SCMS_CS_N" pad="8"/>
<connect gate="G$1" pin="SI_SDA" pad="11"/>
<connect gate="G$1" pin="SO_SCL" pad="10"/>
<connect gate="G$1" pin="TP" pad="17"/>
<connect gate="G$1" pin="VCC" pad="15"/>
<connect gate="G$1" pin="VCORE" pad="12"/>
<connect gate="G$1" pin="VSS" pad="13"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SI7053">
<description>&lt;b&gt;Temperature sensor&lt;/b&gt;&lt;p&gt;Digital Temperature Sensor with I2C Interface</description>
<gates>
<gate name="G$1" symbol="SI7053" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DFN-6">
<connects>
<connect gate="G$1" pin="DNC1" pad="3"/>
<connect gate="G$1" pin="DNC2" pad="4"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="SCL" pad="6"/>
<connect gate="G$1" pin="SDA" pad="1"/>
<connect gate="G$1" pin="TP" pad="PADDLE"/>
<connect gate="G$1" pin="VDD" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:793912/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND">
<description>&lt;b&gt;GROUND SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR">
<gates>
<gate name="C$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="C0201" package="C0201">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23690/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23624/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402" package="C0402K">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23679/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="C0603">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC">
<description>&lt;b&gt;VCC POWER SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R603">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8849207/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CM315D">
<gates>
<gate name="Y$1" symbol="CRYSTAL" x="-5.08" y="0"/>
</gates>
<devices>
<device name="" package="CRYSTAL">
<connects>
<connect gate="Y$1" pin="1" pad="1"/>
<connect gate="Y$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8849208/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TC2030" prefix="J" uservalue="yes">
<description>&lt;h3&gt;TAG-CONNECT ICSP Connector&lt;/h3&gt;&lt;BR&gt;&lt;I&gt;Manufacturer:&lt;/I&gt; &lt;a href="http://www.tag-connect.com"&gt;Tag-Connect&lt;/a&gt;&lt;BR&gt;
&lt;BR&gt;Cable for easy In-Circuit Serial Programming. Designed for Microchip ICD2, suitable for many others.&lt;BR&gt;
Two variants - one "with legs" (for hands-free fit on PCB) and another "without legs" for quick programming.

&lt;p&gt;
&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt;&lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;MCLR/Vpp&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;Vdd&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;GND&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;PGD (ISPDAT)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;PGC (ISPCLK)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;nc (used for LVP)&lt;/TD&gt;&lt;/TR&gt;
&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;

©2009 ROFA.cz - modified and updated by Robert Darlington &amp;#8249;rdarlington@gmail.com&amp;#8250;</description>
<gates>
<gate name="G$1" symbol="TC2030" x="0" y="0"/>
</gates>
<devices>
<device name="-MCP" package="TC2030-MCP">
<connects>
<connect gate="G$1" pin="FET_VCC_OUT" pad="2"/>
<connect gate="G$1" pin="SBWTCK" pad="6"/>
<connect gate="G$1" pin="SBWTDIO" pad="1"/>
<connect gate="G$1" pin="TCK" pad="3"/>
<connect gate="G$1" pin="VCC_IN" pad="4"/>
<connect gate="G$1" pin="VSS" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8896799/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-MCP-NL" package="TC2030-MCP-NL">
<connects>
<connect gate="G$1" pin="FET_VCC_OUT" pad="2"/>
<connect gate="G$1" pin="SBWTCK" pad="6"/>
<connect gate="G$1" pin="SBWTDIO" pad="1"/>
<connect gate="G$1" pin="TCK" pad="3"/>
<connect gate="G$1" pin="VCC_IN" pad="4"/>
<connect gate="G$1" pin="VSS" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8896798/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="TC2030-MCP-NL-CP">
<connects>
<connect gate="G$1" pin="FET_VCC_OUT" pad="2"/>
<connect gate="G$1" pin="SBWTCK" pad="6"/>
<connect gate="G$1" pin="SBWTDIO" pad="1"/>
<connect gate="G$1" pin="TCK" pad="3"/>
<connect gate="G$1" pin="VCC_IN" pad="4"/>
<connect gate="G$1" pin="VSS" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8896797/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP" prefix="TP">
<description>Test Point</description>
<gates>
<gate name="G$1" symbol="TEST-POINT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TP">
<connects>
<connect gate="G$1" pin="TP" pad="TP$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TEST_PAD" package="TP_PAD">
<connects>
<connect gate="G$1" pin="TP" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOLDER_PAD" package="TP_PAD_SOLDER">
<connects>
<connect gate="G$1" pin="TP" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CDBU0130L">
<gates>
<gate name="G$1" symbol="SCHOTTKY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="DIOC0603_N">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE">
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIOC0603_N">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X2" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="TEST-POINT" x="-5.08" y="5.08"/>
<gate name="G$2" symbol="TEST-POINT" x="-5.08" y="-2.54"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="TP" pad="1"/>
<connect gate="G$2" pin="TP" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:793908/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X02/90">
<connects>
<connect gate="G$1" pin="TP" pad="1"/>
<connect gate="G$2" pin="TP" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:793909/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD" package="SMD_CON">
<connects>
<connect gate="G$1" pin="TP" pad="P$1"/>
<connect gate="G$2" pin="TP" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC" urn="urn:adsk.eagle:symbol:13874/1" library_version="1">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" urn="urn:adsk.eagle:component:13926/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="led" urn="urn:adsk.eagle:library:259">
<description>&lt;b&gt;LEDs&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;br&gt;
Extended by Federico Battaglin &lt;author&gt;&amp;lt;federico.rd@fdpinternational.com&amp;gt;&lt;/author&gt; with DUOLED</description>
<packages>
<package name="SML1206" urn="urn:adsk.eagle:footprint:15684/1" library_version="1">
<description>&lt;b&gt;SML10XXKH-TR (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;SML10R3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10E3KH-TR&lt;/td&gt;&lt;td&gt;SUPER REDSUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10O3KH-TR&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PY3KH-TR&lt;/td&gt;&lt;td&gt;PURE YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10OY3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10AG3KH-TR&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10BG3KH-TR&lt;/td&gt;&lt;td&gt;BLUE GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PB1KH-TR&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10CW1KH-TR&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;

Source: http://www.ledtronics.com/ds/smd-1206/dstr0094.PDF</description>
<wire x1="-1.5" y1="0.5" x2="-1.5" y2="-0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="1.5" y1="-0.5" x2="1.5" y2="0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<circle x="-0.725" y="0.525" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="0.4" x2="-1.15" y2="0.8" layer="51"/>
<rectangle x1="-1.6" y1="-0.8" x2="-1.15" y2="-0.4" layer="51"/>
<rectangle x1="-1.175" y1="-0.6" x2="-1" y2="-0.275" layer="51"/>
<rectangle x1="1.15" y1="-0.8" x2="1.6" y2="-0.4" layer="51" rot="R180"/>
<rectangle x1="1.15" y1="0.4" x2="1.6" y2="0.8" layer="51" rot="R180"/>
<rectangle x1="1" y1="0.275" x2="1.175" y2="0.6" layer="51" rot="R180"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
</package>
<package name="1206" urn="urn:adsk.eagle:footprint:15651/1" library_version="1">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260" urn="urn:adsk.eagle:footprint:15652/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5" urn="urn:adsk.eagle:footprint:15653/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED3MM" urn="urn:adsk.eagle:footprint:15654/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM" urn="urn:adsk.eagle:footprint:15655/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260" urn="urn:adsk.eagle:footprint:15656/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181" urn="urn:adsk.eagle:footprint:15657/1" library_version="1">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152" urn="urn:adsk.eagle:footprint:15658/1" library_version="1">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-0.254" x2="-2.9718" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.1082" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153" urn="urn:adsk.eagle:footprint:15659/1" library_version="1">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-0.254" x2="-5.5118" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-4.6482" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.2129" y1="0.0539" x2="-0.0539" y2="2.2129" width="0.1524" layer="51" curve="-90.010616"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155" urn="urn:adsk.eagle:footprint:15660/1" library_version="1">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156" urn="urn:adsk.eagle:footprint:15661/1" library_version="1">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0539" y1="-2.2129" x2="2.2129" y2="-0.0539" width="0.1524" layer="51" curve="90.005308"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480" urn="urn:adsk.eagle:footprint:15662/1" library_version="1">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482" urn="urn:adsk.eagle:footprint:15650/1" library_version="1">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32" urn="urn:adsk.eagle:footprint:15640/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A" urn="urn:adsk.eagle:footprint:15663/1" library_version="1">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2" urn="urn:adsk.eagle:footprint:15664/1" library_version="1">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED" urn="urn:adsk.eagle:footprint:15665/1" library_version="1">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="29"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="29"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED" urn="urn:adsk.eagle:footprint:15666/1" library_version="1">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED" urn="urn:adsk.eagle:footprint:15667/1" library_version="1">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG" urn="urn:adsk.eagle:footprint:15668/1" library_version="1">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED" urn="urn:adsk.eagle:footprint:15669/1" library_version="1">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4" urn="urn:adsk.eagle:footprint:15670/1" library_version="1">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603" urn="urn:adsk.eagle:footprint:15671/1" library_version="1">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805" urn="urn:adsk.eagle:footprint:15672/1" library_version="1">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA" urn="urn:adsk.eagle:footprint:15673/1" library_version="1">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805" urn="urn:adsk.eagle:footprint:15674/1" library_version="1">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="0.925" x2="0.35" y2="0.925" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="-0.35" y1="-0.925" x2="0.35" y2="-0.925" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.3" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206" urn="urn:adsk.eagle:footprint:15675/1" library_version="1">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.6" x2="0.4" y2="1.6" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED_0603" urn="urn:adsk.eagle:footprint:15676/1" library_version="1">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW" urn="urn:adsk.eagle:footprint:15677/1" library_version="1">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW" urn="urn:adsk.eagle:footprint:15678/1" library_version="1">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="LUMILED+" urn="urn:adsk.eagle:footprint:15679/1" library_version="1">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; with cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="1">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LUMILED" urn="urn:adsk.eagle:footprint:15680/1" library_version="1">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; without cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LED10MM" urn="urn:adsk.eagle:footprint:15681/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
10 mm, round</description>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.254" layer="21" curve="-306.869898"/>
<wire x1="4.445" y1="0" x2="0" y2="-4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="3.81" y1="0" x2="0" y2="-3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="3.175" y1="0" x2="0" y2="-3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.445" y1="0" x2="0" y2="4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.81" y1="0" x2="0" y2="3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="0" x2="0" y2="3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="0" y2="2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="21"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="6.35" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KA-3528ASYC" urn="urn:adsk.eagle:footprint:15682/1" library_version="1">
<description>&lt;b&gt;SURFACE MOUNT LED LAMP&lt;/b&gt; 3.5x2.8mm&lt;p&gt;
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<wire x1="-1.55" y1="1.35" x2="1.55" y2="1.35" width="0.1016" layer="21"/>
<wire x1="1.55" y1="1.35" x2="1.55" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-1.35" x2="-1.55" y2="-1.35" width="0.1016" layer="21"/>
<wire x1="-1.55" y1="-1.35" x2="-1.55" y2="1.35" width="0.1016" layer="51"/>
<wire x1="-0.65" y1="0.95" x2="0.65" y2="0.95" width="0.1016" layer="21" curve="-68.40813"/>
<wire x1="0.65" y1="-0.95" x2="-0.65" y2="-0.95" width="0.1016" layer="21" curve="-68.40813"/>
<circle x="0" y="0" radius="1.15" width="0.1016" layer="51"/>
<smd name="A" x="-1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<smd name="C" x="1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.75" y1="0.6" x2="-1.6" y2="1.1" layer="51"/>
<rectangle x1="-1.75" y1="-1.1" x2="-1.6" y2="-0.6" layer="51"/>
<rectangle x1="1.6" y1="-1.1" x2="1.75" y2="-0.6" layer="51" rot="R180"/>
<rectangle x1="1.6" y1="0.6" x2="1.75" y2="1.1" layer="51" rot="R180"/>
<polygon width="0.1016" layer="51">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-0.625"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="21">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-1.175"/>
<vertex x="1" y="-1.175"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
</package>
<package name="SML0805" urn="urn:adsk.eagle:footprint:15683/1" library_version="1">
<description>&lt;b&gt;SML0805-2CW-TR (0805 PROFILE)&lt;/b&gt; COOL WHITE&lt;p&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0093.pdf</description>
<wire x1="-0.95" y1="-0.55" x2="0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="-0.55" x2="0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="0.55" x2="-0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="-0.95" y1="0.55" x2="-0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="-0.175" y1="-0.025" x2="0" y2="0.15" width="0.0634" layer="21"/>
<wire x1="0" y1="0.15" x2="0.15" y2="0" width="0.0634" layer="21"/>
<wire x1="0.15" y1="0" x2="-0.025" y2="-0.175" width="0.0634" layer="21"/>
<wire x1="-0.025" y1="-0.175" x2="-0.175" y2="-0.025" width="0.0634" layer="21"/>
<circle x="-0.275" y="0.4" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SML0603" urn="urn:adsk.eagle:footprint:15685/1" library_version="1">
<description>&lt;b&gt;SML0603-XXX (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;AG3K&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;B1K&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R1K&lt;/td&gt;&lt;td&gt;SUPER RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R3K&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3K&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3KH&lt;/td&gt;&lt;td&gt;SOFT ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3KH&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3K&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;2CW&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0092.pdf</description>
<wire x1="-0.75" y1="0.35" x2="0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="0.35" x2="0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="-0.35" x2="-0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="-0.35" x2="-0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.3" x2="-0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="0.45" y1="0.3" x2="0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="0.35" x2="0.2" y2="0.35" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-0.35" x2="0.2" y2="-0.35" width="0.1016" layer="21"/>
<smd name="C" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4" y1="0.175" x2="0" y2="0.4" layer="51"/>
<rectangle x1="-0.25" y1="0.175" x2="0" y2="0.4" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="SML0603" urn="urn:adsk.eagle:package:15832/1" type="box" library_version="4">
<description>SML0603-XXX (HIGH INTENSITY) LED

AG3KAQUA GREEN
B1KSUPER BLUE
R1KSUPER RED
R3KULTRA RED
O3KSUPER ORANGE
O3KHSOFT ORANGE
Y3KHSUPER YELLOW
Y3KSUPER YELLOW
2CWWHITE

Source: http://www.ledtronics.com/ds/smd-0603/Dstr0092.pdf</description>
</package3d>
<package3d name="1206" urn="urn:adsk.eagle:package:15796/2" type="model" library_version="4">
<description>CHICAGO MINIATURE LAMP, INC.
7022X Series SMT LEDs 1206 Package Size</description>
</package3d>
<package3d name="LD260" urn="urn:adsk.eagle:package:15794/1" type="box" library_version="4">
<description>LED
5 mm, square, Siemens</description>
</package3d>
<package3d name="LED2X5" urn="urn:adsk.eagle:package:15800/1" type="box" library_version="4">
<description>LED
2 x 5 mm, rectangle</description>
</package3d>
<package3d name="LED3MM" urn="urn:adsk.eagle:package:15797/1" type="box" library_version="4">
<description>LED
3 mm, round</description>
</package3d>
<package3d name="LED5MM" urn="urn:adsk.eagle:package:15799/2" type="model" library_version="4">
<description>LED
5 mm, round</description>
</package3d>
<package3d name="LSU260" urn="urn:adsk.eagle:package:15805/1" type="box" library_version="4">
<description>LED
1 mm, round, Siemens</description>
</package3d>
<package3d name="LZR181" urn="urn:adsk.eagle:package:15808/1" type="box" library_version="4">
<description>LED BLOCK
1 LED, Siemens</description>
</package3d>
<package3d name="Q62902-B152" urn="urn:adsk.eagle:package:15803/1" type="box" library_version="4">
<description>LED HOLDER
Siemens</description>
</package3d>
<package3d name="Q62902-B153" urn="urn:adsk.eagle:package:15804/1" type="box" library_version="4">
<description>LED HOLDER
Siemens</description>
</package3d>
<package3d name="Q62902-B155" urn="urn:adsk.eagle:package:15807/1" type="box" library_version="4">
<description>LED HOLDER
Siemens</description>
</package3d>
<package3d name="Q62902-B156" urn="urn:adsk.eagle:package:15806/1" type="box" library_version="4">
<description>LED HOLDER
Siemens</description>
</package3d>
<package3d name="SFH480" urn="urn:adsk.eagle:package:15809/1" type="box" library_version="4">
<description>IR LED
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking
Inifineon</description>
</package3d>
<package3d name="SFH482" urn="urn:adsk.eagle:package:15795/1" type="box" library_version="4">
<description>IR LED
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking
Inifineon</description>
</package3d>
<package3d name="U57X32" urn="urn:adsk.eagle:package:15789/1" type="box" library_version="4">
<description>LED
rectangle, 5.7 x 3.2 mm</description>
</package3d>
<package3d name="IRL80A" urn="urn:adsk.eagle:package:15810/1" type="box" library_version="4">
<description>IR LED
IR transmitter Siemens</description>
</package3d>
<package3d name="P-LCC-2" urn="urn:adsk.eagle:package:15817/1" type="box" library_version="4">
<description>TOPLED® High-optical Power LED (HOP)
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
</package3d>
<package3d name="OSRAM-MINI-TOP-LED" urn="urn:adsk.eagle:package:15811/1" type="box" library_version="4">
<description>BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
</package3d>
<package3d name="OSRAM-SIDELED" urn="urn:adsk.eagle:package:15812/1" type="box" library_version="4">
<description>Super SIDELED® High-Current LED
LG A672, LP A672 
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
</package3d>
<package3d name="SMART-LED" urn="urn:adsk.eagle:package:15814/1" type="box" library_version="4">
<description>SmartLEDTM Hyper-Bright LED
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
</package3d>
<package3d name="P-LCC-2-TOPLED-RG" urn="urn:adsk.eagle:package:15813/1" type="box" library_version="4">
<description>Hyper TOPLED® RG Hyper-Bright LED
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
</package3d>
<package3d name="MICRO-SIDELED" urn="urn:adsk.eagle:package:15815/1" type="box" library_version="4">
<description>Hyper Micro SIDELED®
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
</package3d>
<package3d name="P-LCC-4" urn="urn:adsk.eagle:package:15816/1" type="box" library_version="4">
<description>Power TOPLED®
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
</package3d>
<package3d name="CHIP-LED0603" urn="urn:adsk.eagle:package:15819/3" type="model" library_version="4">
<description>Hyper CHIPLED Hyper-Bright LED
LB Q993
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
</package3d>
<package3d name="CHIP-LED0805" urn="urn:adsk.eagle:package:15818/2" type="model" library_version="4">
<description>Hyper CHIPLED Hyper-Bright LED
LB R99A
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
</package3d>
<package3d name="MINI-TOPLED-SANTANA" urn="urn:adsk.eagle:package:15820/1" type="box" library_version="4">
<description>Mini TOPLED Santana®
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
</package3d>
<package3d name="CHIPLED_0805" urn="urn:adsk.eagle:package:15821/2" type="model" library_version="4">
<description>CHIPLED
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
</package3d>
<package3d name="CHIPLED_1206" urn="urn:adsk.eagle:package:15823/2" type="model" library_version="4">
<description>CHIPLED
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
</package3d>
<package3d name="CHIPLED_0603" urn="urn:adsk.eagle:package:15822/2" type="model" library_version="4">
<description>CHIPLED
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
</package3d>
<package3d name="CHIPLED-0603-TTW" urn="urn:adsk.eagle:package:15824/1" type="box" library_version="4">
<description>CHIPLED-0603
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603
Package able to withstand TTW-soldering heat
Package suitable for TTW-soldering
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
</package3d>
<package3d name="SMARTLED-TTW" urn="urn:adsk.eagle:package:15825/1" type="box" library_version="4">
<description>SmartLED TTW
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603
Package able to withstand TTW-soldering heat
Package suitable for TTW-soldering
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
</package3d>
<package3d name="LUMILED+" urn="urn:adsk.eagle:package:15826/1" type="box" library_version="4">
<description>Lumileds Lighting. LUXEON® with cool pad
Source: K2.pdf</description>
</package3d>
<package3d name="LUMILED" urn="urn:adsk.eagle:package:15827/1" type="box" library_version="4">
<description>Lumileds Lighting. LUXEON® without cool pad
Source: K2.pdf</description>
</package3d>
<package3d name="LED10MM" urn="urn:adsk.eagle:package:15828/1" type="box" library_version="4">
<description>LED
10 mm, round</description>
</package3d>
<package3d name="KA-3528ASYC" urn="urn:adsk.eagle:package:15831/1" type="box" library_version="4">
<description>SURFACE MOUNT LED LAMP 3.5x2.8mm
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
</package3d>
<package3d name="SML0805" urn="urn:adsk.eagle:package:15830/1" type="box" library_version="4">
<description>SML0805-2CW-TR (0805 PROFILE) COOL WHITE
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0093.pdf</description>
</package3d>
<package3d name="SML1206" urn="urn:adsk.eagle:package:15829/1" type="box" library_version="4">
<description>SML10XXKH-TR (HIGH INTENSITY) LED

SML10R3KH-TRULTRA RED
SML10E3KH-TRSUPER REDSUPER BLUE
SML10O3KH-TRSUPER ORANGE
SML10PY3KH-TRPURE YELLOW
SML10OY3KH-TRULTRA YELLOW
SML10AG3KH-TRAQUA GREEN
SML10BG3KH-TRBLUE GREEN
SML10PB1KH-TRSUPER BLUE
SML10CW1KH-TRWHITE


Source: http://www.ledtronics.com/ds/smd-1206/dstr0094.PDF</description>
</package3d>
</packages3d>
<symbols>
<symbol name="LED" urn="urn:adsk.eagle:symbol:15639/2" library_version="4">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" urn="urn:adsk.eagle:component:15916/9" prefix="LED" uservalue="yes" library_version="4">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;

- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K
&lt;p&gt;
Source: http://www.osram.convergy.de&lt;p&gt;

&lt;u&gt;LUXEON:&lt;/u&gt;&lt;br&gt;
- &lt;u&gt;LUMILED®&lt;/u&gt;&lt;br&gt;
LXK2-PW12-R00, LXK2-PW12-S00, LXK2-PW14-U00, LXK2-PW14-V00&lt;br&gt;
LXK2-PM12-R00, LXK2-PM12-S00, LXK2-PM14-U00&lt;br&gt;
LXK2-PE12-Q00, LXK2-PE12-R00, LXK2-PE12-S00, LXK2-PE14-T00, LXK2-PE14-U00&lt;br&gt;
LXK2-PB12-K00, LXK2-PB12-L00, LXK2-PB12-M00, LXK2-PB14-N00, LXK2-PB14-P00, LXK2-PB14-Q00&lt;br&gt;
LXK2-PR12-L00, LXK2-PR12-M00, LXK2-PR14-Q00, LXK2-PR14-R00&lt;br&gt;
LXK2-PD12-Q00, LXK2-PD12-R00, LXK2-PD12-S00&lt;br&gt;
LXK2-PH12-R00, LXK2-PH12-S00&lt;br&gt;
LXK2-PL12-P00, LXK2-PL12-Q00, LXK2-PL12-R00
&lt;p&gt;
Source: www.luxeon.com&lt;p&gt;

&lt;u&gt;KINGBRIGHT:&lt;/U&gt;&lt;p&gt;
KA-3528ASYC&lt;br&gt;
Source: www.kingbright.com</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15796/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15794/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15800/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15797/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15799/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15805/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15808/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15803/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15804/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15807/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15806/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15809/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15795/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15789/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15810/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15817/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15811/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15812/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15814/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15813/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15815/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15816/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15819/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15818/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15820/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15821/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15823/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15822/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15824/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15825/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED+" package="LUMILED+">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15826/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED" package="LUMILED">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15827/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15828/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA-3528ASYC" package="KA-3528ASYC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15831/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0805" package="SML0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15830/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML1206" package="SML1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15829/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0603" package="SML0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15832/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="R2" library="DST_Project" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/2" value="10k"/>
<part name="R1" library="DST_Project" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/2" value="10k"/>
<part name="U7" library="DST_Project" deviceset="SI7053" device="" package3d_urn="urn:adsk.eagle:package:793912/2">
<attribute name="MF" value=""/>
<attribute name="MPN" value=""/>
<attribute name="OC_NEWARK" value="unknown"/>
</part>
<part name="GND21" library="DST_Project" deviceset="GND" device=""/>
<part name="U3" library="DST_Project" deviceset="KMX62-1031-SR" device="" package3d_urn="urn:adsk.eagle:package:793905/5">
<attribute name="MF" value=""/>
<attribute name="MPN" value=""/>
<attribute name="OC_NEWARK" value="unknown"/>
</part>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U6" library="DST_Project" deviceset="MS5837-30BA" device="" package3d_urn="urn:adsk.eagle:package:793906/2">
<attribute name="MF" value=""/>
<attribute name="MPN" value=""/>
<attribute name="OC_NEWARK" value="unknown"/>
</part>
<part name="GND29" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U2" library="DST_Project" deviceset="XC6504A251MR-G" device="" package3d_urn="urn:adsk.eagle:package:793913/4">
<attribute name="MF" value=""/>
<attribute name="MPN" value=""/>
<attribute name="OC_NEWARK" value="unknown"/>
</part>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device="">
<attribute name="REV" value="1."/>
</part>
<part name="C1" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="1 uF"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C2" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="100 nF"/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+3" library="DST_Project" deviceset="VCC" device="" value="2V5"/>
<part name="C9" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="100nF"/>
<part name="C10" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="100nF"/>
<part name="C17" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="100nF"/>
<part name="C6" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="1 nF "/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="FRAME2" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="GND10" library="DST_Project" deviceset="GND" device=""/>
<part name="P+10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device="" value="2V5"/>
<part name="P+11" library="DST_Project" deviceset="VCC" device="" value="2V5"/>
<part name="P+12" library="DST_Project" deviceset="VCC" device="" value="2V5"/>
<part name="U5" library="DST_Project" deviceset="AT45DB641E-UDFN" device="" package3d_urn="urn:adsk.eagle:package:8841323/2">
<attribute name="SPICEPREFIX" value="X"/>
</part>
<part name="C18" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="100 nF"/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C12" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="100nF"/>
<part name="C11" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="1uF"/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device="" value="2V5"/>
<part name="C13" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="0.47uF"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND15" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND16" library="DST_Project" deviceset="GND" device=""/>
<part name="P+8" library="DST_Project" deviceset="VCC" device="" value="2V5"/>
<part name="C16" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="100nF"/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+9" library="DST_Project" deviceset="VCC" device="" value="2V5"/>
<part name="P+5" library="DST_Project" deviceset="VCC" device="" value="2V5"/>
<part name="R4" library="DST_Project" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/2" value="470"/>
<part name="D1" library="led" library_urn="urn:adsk.eagle:library:259" deviceset="LED" device="SML0603" package3d_urn="urn:adsk.eagle:package:15832/1" value="RED"/>
<part name="GND18" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device="">
<attribute name="SPICEGROUND" value=""/>
</part>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C14" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="35 pF"/>
<part name="U1" library="DST_Project" deviceset="MSP430FR2633IRHB" device="VQFN-32_MANUAL" value="MSP430FR2633IRHBVQFN-32_MANUAL">
<attribute name="SPICEPREFIX" value="X"/>
</part>
<part name="U4" library="DST_Project" deviceset="RF430CL330HIRGTR" device="VQFN-16_MANUAL" value="RF430CL330HIRGTRVQFN-16_MANUAL"/>
<part name="C7" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="22pF"/>
<part name="C8" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="22pF"/>
<part name="U$4" library="DST_Project" deviceset="GND" device=""/>
<part name="Y1" library="DST_Project" deviceset="CM315D" device="" package3d_urn="urn:adsk.eagle:package:8849208/3" value="32.768 KHz"/>
<part name="C3" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="100 nF"/>
<part name="C4" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="10 uF"/>
<part name="U$2" library="DST_Project" deviceset="GND" device=""/>
<part name="U$3" library="DST_Project" deviceset="GND" device=""/>
<part name="U$6" library="DST_Project" deviceset="GND" device=""/>
<part name="2V5" library="DST_Project" deviceset="VCC" device="" value="2V5"/>
<part name="C5" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="1 uF"/>
<part name="U$7" library="DST_Project" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="J1" library="DST_Project" deviceset="TC2030" device="-MCP-NL" package3d_urn="urn:adsk.eagle:package:8896798/1"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device="" value="2V5"/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$13" library="DST_Project" deviceset="GND" device=""/>
<part name="U$17" library="DST_Project" deviceset="GND" device=""/>
<part name="TP1" library="DST_Project" deviceset="TP" device="TEST_PAD" value="BATT"/>
<part name="TP2" library="DST_Project" deviceset="TP" device="SOLDER_PAD" value="GND"/>
<part name="TP3" library="DST_Project" deviceset="TP" device="SOLDER_PAD" value="VCC"/>
<part name="TP4" library="DST_Project" deviceset="TP" device="TEST_PAD" value="SMCLK"/>
<part name="TP5" library="DST_Project" deviceset="TP" device="TEST_PAD" value="ACLK"/>
<part name="TP6" library="DST_Project" deviceset="TP" device="SOLDER_PAD" value="TRANS"/>
<part name="TP7" library="DST_Project" deviceset="TP" device="TEST_PAD" value="SIMO"/>
<part name="TP8" library="DST_Project" deviceset="TP" device="TEST_PAD" value="!CS!"/>
<part name="TP9" library="DST_Project" deviceset="TP" device="TEST_PAD" value="CLK"/>
<part name="TP10" library="DST_Project" deviceset="TP" device="TEST_PAD" value="SOMI"/>
<part name="TP11" library="DST_Project" deviceset="TP" device="TEST_PAD" value="TCK"/>
<part name="TP12" library="DST_Project" deviceset="TP" device="TEST_PAD" value="TDIO"/>
<part name="TP13" library="DST_Project" deviceset="TP" device="TEST_PAD" value="SCL"/>
<part name="TP14" library="DST_Project" deviceset="TP" device="TEST_PAD" value="SDA"/>
<part name="TP15" library="DST_Project" deviceset="TP" device="SOLDER_PAD" value="TX"/>
<part name="TP16" library="DST_Project" deviceset="TP" device="SOLDER_PAD" value="RX"/>
<part name="VD3" library="DST_Project" deviceset="CDBU0130L" device="0603" value="CDBU0130L0603"/>
<part name="VD4" library="DST_Project" deviceset="CDBU0130L" device="0603" value="CDBU0130L0603"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C15" library="DST_Project" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:23616/2" value="100uF"/>
<part name="R3" library="DST_Project" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/2" value="47k"/>
<part name="P+6" library="DST_Project" deviceset="VCC" device="" value="2V5"/>
<part name="VD1" library="DST_Project" deviceset="DIODE" device=""/>
<part name="VD2" library="DST_Project" deviceset="DIODE" device=""/>
<part name="JP1" library="DST_Project" deviceset="PINHD-1X2" device="SMD"/>
<part name="JP2" library="DST_Project" deviceset="PINHD-1X2" device="SMD"/>
</parts>
<sheets>
<sheet>
<description>Microcontroller, Power and Debugging interface</description>
<plain>
<text x="5.08" y="53.34" size="3.81" layer="90">Battery and LDO</text>
<text x="154.94" y="149.86" size="3.81" layer="90">MSP430 Microcontroller</text>
<text x="172.72" y="55.88" size="3.81" layer="90">Debug interface</text>
<text x="45.72" y="162.56" size="6.4516" layer="90">Data Storage Tag: Project Schematics (1/2)</text>
<text x="175.26" y="7.62" size="2.54" layer="94">By Ali Aljumaili</text>
<text x="217.17" y="20.32" size="2.54" layer="94">Rev. 1.2.0</text>
<text x="182.88" y="48.26" size="1.778" layer="96" rot="R180">TC2030-MCP-NLTC</text>
<wire x1="5.08" y1="157.48" x2="256.54" y2="157.48" width="0.508" layer="90" style="shortdash"/>
<wire x1="3.81" y1="50.8" x2="20.32" y2="50.8" width="0.254" layer="90"/>
<wire x1="20.32" y1="50.8" x2="22.86" y2="48.26" width="0.254" layer="90"/>
<wire x1="22.86" y1="48.26" x2="25.4" y2="50.8" width="0.254" layer="90"/>
<wire x1="25.4" y1="50.8" x2="45.72" y2="50.8" width="0.254" layer="90"/>
<wire x1="45.72" y1="50.8" x2="50.8" y2="58.42" width="0.254" layer="90"/>
<wire x1="3.81" y1="50.8" x2="3.81" y2="58.42" width="0.254" layer="90"/>
<wire x1="3.81" y1="58.42" x2="50.8" y2="58.42" width="0.254" layer="90"/>
<wire x1="5.08" y1="60.96" x2="38.1" y2="60.96" width="0.508" layer="90" style="shortdash"/>
<wire x1="38.1" y1="60.96" x2="38.1" y2="152.4" width="0.508" layer="90" style="shortdash"/>
<wire x1="38.1" y1="152.4" x2="5.08" y2="152.4" width="0.508" layer="90" style="shortdash"/>
<wire x1="5.08" y1="152.4" x2="5.08" y2="60.96" width="0.508" layer="90" style="shortdash"/>
<text x="30.48" y="149.86" size="2.54" layer="90" rot="R180">Test Points</text>
<text x="238.76" y="40.64" size="1.6764" layer="95" rot="R180">Add adapter here</text>
<wire x1="3.81" y1="61.214" x2="255.778" y2="61.214" width="0.508" layer="90" style="shortdash"/>
<wire x1="130.302" y1="3.81" x2="130.302" y2="60.706" width="0.508" layer="90" style="shortdash"/>
</plain>
<instances>
<instance part="R2" gate="G$1" x="81.28" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="79.7814" y="133.35" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="84.582" y="133.35" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R1" gate="G$1" x="73.66" y="137.16" smashed="yes" rot="R90">
<attribute name="NAME" x="72.1614" y="133.35" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="76.962" y="133.35" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U2" gate="G$1" x="88.9" y="33.02" smashed="yes">
<attribute name="OC_NEWARK" x="88.9" y="33.02" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="88.9" y="33.02" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="88.9" y="33.02" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="78.74" y="48.26" size="3.048" layer="95"/>
<attribute name="VALUE" x="81.28" y="15.24" size="3.048" layer="95"/>
</instance>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="217.17" y="15.24" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="217.17" y="10.16" size="2.286" layer="94"/>
<attribute name="SHEET" x="230.505" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="C1" gate="C$1" x="55.88" y="33.02" smashed="yes">
<attribute name="NAME" x="57.404" y="33.401" size="1.778" layer="95"/>
<attribute name="VALUE" x="57.404" y="28.321" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="106.68" y="20.32" smashed="yes">
<attribute name="VALUE" x="104.14" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="C2" gate="C$1" x="106.68" y="33.02" smashed="yes">
<attribute name="NAME" x="108.204" y="33.401" size="1.778" layer="95"/>
<attribute name="VALUE" x="108.204" y="28.321" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="55.88" y="17.78" smashed="yes">
<attribute name="VALUE" x="53.34" y="15.24" size="1.778" layer="96"/>
</instance>
<instance part="P+3" gate="G$1" x="106.68" y="45.72" smashed="yes">
<attribute name="VALUE" x="104.14" y="45.72" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C6" gate="C$1" x="101.6" y="144.78" smashed="yes" rot="R270">
<attribute name="NAME" x="101.981" y="143.256" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="96.901" y="143.256" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND7" gate="1" x="88.9" y="142.24" smashed="yes">
<attribute name="VALUE" x="86.36" y="139.7" size="1.778" layer="96"/>
</instance>
<instance part="P+5" gate="G$1" x="73.66" y="149.86" smashed="yes">
<attribute name="VALUE" x="71.12" y="149.86" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R4" gate="G$1" x="236.22" y="96.52" smashed="yes" rot="R180">
<attribute name="NAME" x="240.03" y="95.0214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="240.03" y="99.822" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D1" gate="G$1" x="241.3" y="86.36" smashed="yes">
<attribute name="NAME" x="244.856" y="81.788" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="247.015" y="81.788" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND18" gate="1" x="241.3" y="68.58" smashed="yes">
<attribute name="VALUE" x="238.76" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="GND20" gate="1" x="27.94" y="17.78" smashed="yes">
<attribute name="VALUE" x="25.4" y="15.24" size="1.778" layer="96"/>
</instance>
<instance part="U1" gate="A" x="175.26" y="134.62" smashed="yes">
<attribute name="NAME" x="137.5156" y="141.1986" size="2.54" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="136.8806" y="67.5386" size="2.54" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="C7" gate="C$1" x="101.6" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="101.219" y="98.044" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="106.299" y="98.044" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C8" gate="C$1" x="104.14" y="83.82" smashed="yes" rot="R270">
<attribute name="NAME" x="104.521" y="82.296" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="99.441" y="82.296" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U$4" gate="G$1" x="92.964" y="90.678" smashed="yes">
<attribute name="VALUE" x="88.392" y="82.296" size="1.778" layer="96"/>
</instance>
<instance part="Y1" gate="Y$1" x="113.538" y="91.44" smashed="yes" rot="MR90">
<attribute name="NAME" x="117.348" y="86.36" size="1.778" layer="95" rot="MR90"/>
<attribute name="VALUE" x="108.458" y="86.36" size="1.778" layer="96" rot="MR90"/>
</instance>
<instance part="C3" gate="C$1" x="241.3" y="132.08" smashed="yes">
<attribute name="NAME" x="242.824" y="132.461" size="1.778" layer="95"/>
<attribute name="VALUE" x="242.824" y="127.381" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="C$1" x="246.38" y="132.08" smashed="yes">
<attribute name="NAME" x="247.904" y="132.461" size="1.778" layer="95"/>
<attribute name="VALUE" x="247.904" y="127.381" size="1.778" layer="96"/>
</instance>
<instance part="U$2" gate="G$1" x="241.3" y="124.46" smashed="yes">
<attribute name="VALUE" x="236.728" y="116.078" size="1.778" layer="96"/>
</instance>
<instance part="U$3" gate="G$1" x="246.38" y="124.46" smashed="yes">
<attribute name="VALUE" x="241.808" y="116.078" size="1.778" layer="96"/>
</instance>
<instance part="U$6" gate="G$1" x="231.14" y="71.12" smashed="yes">
<attribute name="VALUE" x="226.568" y="62.738" size="1.778" layer="96"/>
</instance>
<instance part="2V5" gate="G$1" x="241.3" y="142.24" smashed="yes">
<attribute name="VALUE" x="238.76" y="142.24" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C5" gate="C$1" x="233.68" y="132.08" smashed="yes">
<attribute name="NAME" x="235.204" y="132.461" size="1.778" layer="95"/>
<attribute name="VALUE" x="235.204" y="127.381" size="1.778" layer="96"/>
</instance>
<instance part="U$7" gate="G$1" x="233.68" y="124.46" smashed="yes">
<attribute name="VALUE" x="229.108" y="116.078" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="170.18" y="38.1" smashed="yes" rot="MR180">
<attribute name="VALUE" x="160.02" y="48.26" size="1.778" layer="95" rot="MR180"/>
<attribute name="NAME" x="159.004" y="26.924" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="P+1" gate="VCC" x="152.4" y="35.56" smashed="yes" rot="R180">
<attribute name="VALUE" x="154.94" y="38.1" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND4" gate="1" x="154.94" y="48.26" smashed="yes" rot="R180">
<attribute name="VALUE" x="157.48" y="50.8" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="U$17" gate="G$1" x="220.98" y="71.12" smashed="yes">
<attribute name="VALUE" x="216.408" y="62.738" size="1.778" layer="96"/>
</instance>
<instance part="TP1" gate="G$1" x="10.16" y="142.24" smashed="yes">
<attribute name="NAME" x="10.16" y="143.764" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="139.7" size="1.27" layer="96"/>
</instance>
<instance part="TP2" gate="G$1" x="10.16" y="137.16" smashed="yes">
<attribute name="NAME" x="10.16" y="138.684" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="134.62" size="1.27" layer="96"/>
</instance>
<instance part="TP3" gate="G$1" x="10.16" y="132.08" smashed="yes">
<attribute name="NAME" x="10.16" y="133.604" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="129.54" size="1.27" layer="96"/>
</instance>
<instance part="TP4" gate="G$1" x="10.16" y="127" smashed="yes">
<attribute name="NAME" x="10.16" y="128.524" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="124.46" size="1.27" layer="96"/>
</instance>
<instance part="TP5" gate="G$1" x="10.16" y="121.92" smashed="yes">
<attribute name="NAME" x="10.16" y="123.444" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="119.38" size="1.27" layer="96"/>
</instance>
<instance part="TP6" gate="G$1" x="10.16" y="116.84" smashed="yes">
<attribute name="NAME" x="10.16" y="118.364" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="114.3" size="1.27" layer="96"/>
</instance>
<instance part="TP7" gate="G$1" x="10.16" y="111.76" smashed="yes">
<attribute name="NAME" x="10.16" y="113.284" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="109.22" size="1.27" layer="96"/>
</instance>
<instance part="TP8" gate="G$1" x="10.16" y="106.68" smashed="yes">
<attribute name="NAME" x="10.16" y="108.204" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="104.14" size="1.27" layer="96"/>
</instance>
<instance part="TP9" gate="G$1" x="10.16" y="101.6" smashed="yes">
<attribute name="NAME" x="10.16" y="103.124" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="99.06" size="1.27" layer="96"/>
</instance>
<instance part="TP10" gate="G$1" x="10.16" y="96.52" smashed="yes">
<attribute name="NAME" x="10.16" y="98.044" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="93.98" size="1.27" layer="96"/>
</instance>
<instance part="TP11" gate="G$1" x="10.16" y="91.44" smashed="yes">
<attribute name="NAME" x="10.16" y="92.964" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="88.9" size="1.27" layer="96"/>
</instance>
<instance part="TP12" gate="G$1" x="10.16" y="86.36" smashed="yes">
<attribute name="NAME" x="10.16" y="87.884" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="83.82" size="1.27" layer="96"/>
</instance>
<instance part="TP13" gate="G$1" x="10.16" y="81.28" smashed="yes">
<attribute name="NAME" x="10.16" y="82.804" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="78.74" size="1.27" layer="96"/>
</instance>
<instance part="TP14" gate="G$1" x="10.16" y="76.2" smashed="yes">
<attribute name="NAME" x="10.16" y="77.724" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="73.66" size="1.27" layer="96"/>
</instance>
<instance part="TP15" gate="G$1" x="10.16" y="71.12" smashed="yes">
<attribute name="NAME" x="10.16" y="72.644" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="68.58" size="1.27" layer="96"/>
</instance>
<instance part="TP16" gate="G$1" x="10.16" y="66.04" smashed="yes">
<attribute name="NAME" x="10.16" y="67.564" size="1.27" layer="95"/>
<attribute name="VALUE" x="10.16" y="63.5" size="1.27" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="124.46" y="147.32" smashed="yes" rot="R90">
<attribute name="NAME" x="122.9614" y="143.51" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="127.762" y="143.51" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+6" gate="G$1" x="124.46" y="154.94" smashed="yes">
<attribute name="VALUE" x="121.92" y="154.94" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="VD1" gate="G$1" x="27.94" y="27.94" smashed="yes">
<attribute name="NAME" x="30.48" y="28.4226" size="1.778" layer="95"/>
<attribute name="VALUE" x="30.48" y="25.6286" size="1.778" layer="96"/>
</instance>
<instance part="VD2" gate="G$1" x="27.94" y="40.64" smashed="yes">
<attribute name="NAME" x="30.48" y="41.1226" size="1.778" layer="95"/>
<attribute name="VALUE" x="30.48" y="38.3286" size="1.778" layer="96"/>
</instance>
<instance part="JP2" gate="G$1" x="15.24" y="27.94" smashed="yes">
<attribute name="NAME" x="15.24" y="29.464" size="1.27" layer="95"/>
<attribute name="VALUE" x="15.24" y="25.4" size="1.27" layer="96"/>
</instance>
<instance part="JP2" gate="G$2" x="15.24" y="22.86" smashed="yes">
<attribute name="NAME" x="15.24" y="24.384" size="1.27" layer="95"/>
<attribute name="VALUE" x="15.24" y="20.32" size="1.27" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="106.68" y1="22.86" x2="106.68" y2="25.4" width="0.1524" layer="91"/>
<pinref part="C2" gate="C$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="VSS"/>
<wire x1="106.68" y1="25.4" x2="106.68" y2="27.94" width="0.1524" layer="91"/>
<wire x1="101.6" y1="25.4" x2="106.68" y2="25.4" width="0.1524" layer="91"/>
<junction x="106.68" y="25.4"/>
</segment>
<segment>
<pinref part="C1" gate="C$1" pin="2"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="55.88" y1="27.94" x2="55.88" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C6" gate="C$1" pin="2"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="96.52" y1="144.78" x2="88.9" y2="144.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="27.94" y1="20.32" x2="27.94" y2="22.86" width="0.1524" layer="91"/>
<pinref part="JP2" gate="G$2" pin="TP"/>
<wire x1="20.32" y1="22.86" x2="27.94" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="C"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="241.3" y1="81.28" x2="241.3" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<pinref part="C3" gate="C$1" pin="2"/>
<wire x1="241.3" y1="124.46" x2="241.3" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$3" gate="G$1" pin="GND"/>
<pinref part="C4" gate="C$1" pin="2"/>
<wire x1="246.38" y1="127" x2="246.38" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="C$1" pin="2"/>
<pinref part="U$7" gate="G$1" pin="GND"/>
<wire x1="233.68" y1="127" x2="233.68" y2="124.46" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="VSS"/>
<wire x1="157.48" y1="40.64" x2="154.94" y2="40.64" width="0.1524" layer="91"/>
<wire x1="154.94" y1="40.64" x2="154.94" y2="45.72" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$6" gate="G$1" pin="GND"/>
<pinref part="U1" gate="A" pin="CAP2-0"/>
<wire x1="231.14" y1="93.98" x2="231.14" y2="91.44" width="0.1524" layer="91"/>
<wire x1="231.14" y1="91.44" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
<wire x1="231.14" y1="88.9" x2="231.14" y2="86.36" width="0.1524" layer="91"/>
<wire x1="231.14" y1="86.36" x2="231.14" y2="83.82" width="0.1524" layer="91"/>
<wire x1="231.14" y1="83.82" x2="231.14" y2="81.28" width="0.1524" layer="91"/>
<wire x1="231.14" y1="81.28" x2="231.14" y2="71.12" width="0.1524" layer="91"/>
<wire x1="218.44" y1="93.98" x2="231.14" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="CAP2-1"/>
<wire x1="218.44" y1="91.44" x2="231.14" y2="91.44" width="0.1524" layer="91"/>
<junction x="231.14" y="91.44"/>
<pinref part="U1" gate="A" pin="CAP2-2"/>
<wire x1="218.44" y1="88.9" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
<junction x="231.14" y="88.9"/>
<pinref part="U1" gate="A" pin="CAP2-3"/>
<wire x1="218.44" y1="86.36" x2="231.14" y2="86.36" width="0.1524" layer="91"/>
<junction x="231.14" y="86.36"/>
<pinref part="U1" gate="A" pin="CAP3-1"/>
<wire x1="218.44" y1="83.82" x2="231.14" y2="83.82" width="0.1524" layer="91"/>
<junction x="231.14" y="83.82"/>
<pinref part="U1" gate="A" pin="CAP3-3"/>
<wire x1="218.44" y1="81.28" x2="231.14" y2="81.28" width="0.1524" layer="91"/>
<junction x="231.14" y="81.28"/>
</segment>
<segment>
<wire x1="15.24" y1="137.16" x2="25.4" y2="137.16" width="0.1524" layer="91"/>
<label x="25.4" y="137.16" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP2" gate="G$1" pin="TP"/>
</segment>
<segment>
<pinref part="C8" gate="C$1" pin="2"/>
<pinref part="C7" gate="C$1" pin="1"/>
<wire x1="99.06" y1="96.52" x2="99.06" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U$4" gate="G$1" pin="GND"/>
<wire x1="99.06" y1="90.678" x2="99.06" y2="83.82" width="0.1524" layer="91"/>
<wire x1="99.06" y1="91.44" x2="99.06" y2="90.678" width="0.1524" layer="91"/>
<wire x1="99.06" y1="90.678" x2="92.964" y2="90.678" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="PAD"/>
<wire x1="218.44" y1="73.66" x2="220.98" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U$17" gate="G$1" pin="GND"/>
<wire x1="220.98" y1="73.66" x2="220.98" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="DVSS"/>
<wire x1="218.44" y1="76.2" x2="220.98" y2="76.2" width="0.1524" layer="91"/>
<wire x1="220.98" y1="76.2" x2="220.98" y2="73.66" width="0.1524" layer="91"/>
<junction x="220.98" y="73.66"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<wire x1="106.68" y1="45.72" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C2" gate="C$1" pin="1"/>
<pinref part="P+3" gate="G$1" pin="VCC"/>
<pinref part="U2" gate="G$1" pin="VOUT"/>
<wire x1="106.68" y1="40.64" x2="106.68" y2="35.56" width="0.1524" layer="91"/>
<wire x1="101.6" y1="40.64" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<junction x="106.68" y="40.64"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="P+5" gate="G$1" pin="VCC"/>
<wire x1="73.66" y1="142.24" x2="81.28" y2="142.24" width="0.1524" layer="91"/>
<wire x1="73.66" y1="149.86" x2="73.66" y2="142.24" width="0.1524" layer="91"/>
<junction x="73.66" y="142.24"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="DVCC"/>
<wire x1="218.44" y1="137.16" x2="241.3" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C3" gate="C$1" pin="1"/>
<wire x1="241.3" y1="137.16" x2="241.3" y2="134.62" width="0.1524" layer="91"/>
<wire x1="241.3" y1="137.16" x2="246.38" y2="137.16" width="0.1524" layer="91"/>
<junction x="241.3" y="137.16"/>
<pinref part="C4" gate="C$1" pin="1"/>
<wire x1="246.38" y1="137.16" x2="246.38" y2="134.62" width="0.1524" layer="91"/>
<pinref part="2V5" gate="G$1" pin="VCC"/>
<wire x1="241.3" y1="142.24" x2="241.3" y2="137.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="VCC_IN"/>
<wire x1="157.48" y1="38.1" x2="152.4" y2="38.1" width="0.1524" layer="91"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
</segment>
<segment>
<wire x1="15.24" y1="132.08" x2="25.4" y2="132.08" width="0.1524" layer="91"/>
<label x="25.4" y="132.08" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP3" gate="G$1" pin="TP"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="P+6" gate="G$1" pin="VCC"/>
<wire x1="124.46" y1="152.4" x2="124.46" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BATT" class="0">
<segment>
<wire x1="15.24" y1="142.24" x2="30.48" y2="142.24" width="0.1524" layer="91"/>
<label x="30.48" y="142.24" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP1" gate="G$1" pin="TP"/>
</segment>
<segment>
<pinref part="VD1" gate="G$1" pin="A"/>
<label x="12.7" y="30.48" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="JP2" gate="G$1" pin="TP"/>
<wire x1="20.32" y1="27.94" x2="25.4" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SBWTDIO" class="0">
<segment>
<wire x1="15.24" y1="86.36" x2="25.4" y2="86.36" width="0.1524" layer="91"/>
<label x="25.4" y="86.36" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP12" gate="G$1" pin="TP"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="SBWTDIO"/>
<wire x1="157.48" y1="30.48" x2="147.32" y2="30.48" width="0.1524" layer="91"/>
<label x="147.32" y="30.48" size="1.6764" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="119.38" y="137.16" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="RST_NMI_SBWTDIO_N"/>
<wire x1="132.08" y1="137.16" x2="124.46" y2="137.16" width="0.1524" layer="91"/>
<label x="106.68" y="137.16" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<label x="106.68" y="137.16" size="1.27" layer="95" font="vector" rot="R180" xref="yes"/>
<pinref part="C6" gate="C$1" pin="1"/>
<wire x1="124.46" y1="137.16" x2="116.84" y2="137.16" width="0.1524" layer="91"/>
<wire x1="116.84" y1="137.16" x2="106.68" y2="137.16" width="0.1524" layer="91"/>
<wire x1="104.14" y1="144.78" x2="116.84" y2="144.78" width="0.1524" layer="91"/>
<wire x1="116.84" y1="144.78" x2="116.84" y2="137.16" width="0.1524" layer="91"/>
<junction x="116.84" y="137.16"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="124.46" y1="142.24" x2="124.46" y2="137.16" width="0.1524" layer="91"/>
<junction x="124.46" y="137.16"/>
</segment>
</net>
<net name="XIN" class="0">
<segment>
<pinref part="U1" gate="A" pin="P2-1_XIN"/>
<label x="121.92" y="83.82" size="1.778" layer="95"/>
<wire x1="132.08" y1="88.9" x2="132.08" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C8" gate="C$1" pin="1"/>
<wire x1="132.08" y1="83.82" x2="113.538" y2="83.82" width="0.1524" layer="91"/>
<pinref part="Y1" gate="Y$1" pin="1"/>
<wire x1="113.538" y1="83.82" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<wire x1="113.538" y1="86.36" x2="113.538" y2="83.82" width="0.1524" layer="91"/>
<junction x="113.538" y="83.82"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="81.28" y1="132.08" x2="81.28" y2="127" width="0.1524" layer="91"/>
<label x="66.04" y="127" size="1.27" layer="95" rot="R180" xref="yes"/>
<label x="114.3" y="127" size="2.54" layer="95"/>
<pinref part="U1" gate="A" pin="P1-2_UCB0SIMO_UCB0SDA_TA0-2_A2_VEREF-"/>
<wire x1="132.08" y1="127" x2="81.28" y2="127" width="0.1524" layer="91"/>
<junction x="81.28" y="127"/>
<wire x1="81.28" y1="127" x2="66.04" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="15.24" y1="76.2" x2="25.4" y2="76.2" width="0.1524" layer="91"/>
<label x="25.4" y="76.2" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP14" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="73.66" y1="132.08" x2="73.66" y2="121.92" width="0.1524" layer="91"/>
<label x="52.832" y="121.92" size="1.27" layer="95" xref="yes"/>
<label x="114.3" y="121.92" size="2.54" layer="95"/>
<pinref part="U1" gate="A" pin="P1-3_UCB0SOMI_UCB0SCL_MCLK_A3"/>
<wire x1="132.08" y1="121.92" x2="73.66" y2="121.92" width="0.1524" layer="91"/>
<junction x="73.66" y="121.92"/>
<wire x1="73.66" y1="121.92" x2="66.04" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="15.24" y1="81.28" x2="25.4" y2="81.28" width="0.1524" layer="91"/>
<label x="25.4" y="81.28" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP13" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="SPI_SIMO" class="0">
<segment>
<pinref part="U1" gate="A" pin="P2-6_UCA1TXD_UCA1SIMO_CAP1-3"/>
<wire x1="132.08" y1="76.2" x2="78.74" y2="76.2" width="0.1524" layer="91"/>
<label x="58.42" y="76.2" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="15.24" y1="111.76" x2="20.32" y2="111.76" width="0.1524" layer="91"/>
<label x="20.32" y="111.76" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP7" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="SPI_SOMI" class="0">
<segment>
<pinref part="U1" gate="A" pin="P2-5_UCA1RXD_UCA1SOMI_CAP1-2"/>
<wire x1="132.08" y1="78.74" x2="78.74" y2="78.74" width="0.1524" layer="91"/>
<label x="78.74" y="78.74" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="15.24" y1="96.52" x2="20.32" y2="96.52" width="0.1524" layer="91"/>
<label x="20.32" y="96.52" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP10" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="SPI_CLK" class="0">
<segment>
<pinref part="U1" gate="A" pin="P2-4_UCA1CLK_CAP1-1"/>
<wire x1="132.08" y1="81.28" x2="78.74" y2="81.28" width="0.1524" layer="91"/>
<label x="59.69" y="81.026" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="15.24" y1="101.6" x2="20.32" y2="101.6" width="0.1524" layer="91"/>
<label x="20.32" y="101.6" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP9" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U1" gate="A" pin="VREG"/>
<pinref part="C5" gate="C$1" pin="1"/>
<wire x1="218.44" y1="134.62" x2="233.68" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SMCLK_OUT" class="0">
<segment>
<pinref part="U1" gate="A" pin="P1-7_UCA0STE_SMCLK_TDO_A7"/>
<label x="248.412" y="115.062" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="218.44" y1="114.3" x2="231.394" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="15.24" y1="127" x2="22.86" y2="127" width="0.1524" layer="91"/>
<label x="22.86" y="127" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP4" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="SBWTCK" class="0">
<segment>
<pinref part="U1" gate="A" pin="TEST_SBWTCK"/>
<label x="119.38" y="132.08" size="1.778" layer="95"/>
<label x="104.14" y="132.08" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="132.08" y1="132.08" x2="104.14" y2="132.08" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="15.24" y1="91.44" x2="25.4" y2="91.44" width="0.1524" layer="91"/>
<label x="25.4" y="91.44" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP11" gate="G$1" pin="TP"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="SBWTCK"/>
<wire x1="157.48" y1="43.18" x2="147.32" y2="43.18" width="0.1524" layer="91"/>
<label x="147.32" y="43.18" size="1.6764" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="241.3" y1="88.9" x2="241.3" y2="96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<pinref part="U1" gate="A" pin="CAP0-1"/>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="218.44" y1="96.52" x2="231.14" y2="96.52" width="0.1524" layer="91"/>
<label x="220.98" y="96.52" size="1.27" layer="95"/>
</segment>
</net>
<net name="XOUT" class="0">
<segment>
<pinref part="U1" gate="A" pin="P2-0_XOUT"/>
<pinref part="Y1" gate="Y$1" pin="2"/>
<wire x1="132.08" y1="96.52" x2="113.538" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C7" gate="C$1" pin="2"/>
<wire x1="106.68" y1="96.52" x2="113.538" y2="96.52" width="0.1524" layer="91"/>
<junction x="113.538" y="96.52"/>
<label x="121.92" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="ACLK_OUT" class="0">
<segment>
<wire x1="15.24" y1="121.92" x2="22.86" y2="121.92" width="0.1524" layer="91"/>
<label x="22.86" y="121.92" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP5" gate="G$1" pin="TP"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="P2-2_SYNC_ACLK"/>
<label x="247.396" y="112.014" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="218.44" y1="111.76" x2="231.648" y2="111.76" width="0.1524" layer="91"/>
<wire x1="231.648" y1="111.76" x2="231.648" y2="111.506" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPI_!CS!" class="0">
<segment>
<pinref part="U1" gate="A" pin="P3-1_UCA1STE_CAP1-0"/>
<wire x1="132.08" y1="73.66" x2="78.74" y2="73.66" width="0.1524" layer="91"/>
<label x="61.214" y="73.152" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="15.24" y1="106.68" x2="20.32" y2="106.68" width="0.1524" layer="91"/>
<label x="20.32" y="106.68" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP8" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="UART_TX" class="0">
<segment>
<pinref part="U1" gate="A" pin="P1-4_UCA0TXD_UCA0SIMO_TA1-2_TCK_A4_VREF+"/>
<wire x1="132.08" y1="114.3" x2="60.96" y2="114.3" width="0.1524" layer="91"/>
<label x="47.244" y="114.554" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="15.24" y1="71.12" x2="25.4" y2="71.12" width="0.1524" layer="91"/>
<label x="25.4" y="71.12" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP15" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="UART_RX" class="0">
<segment>
<pinref part="U1" gate="A" pin="P1-5_UCA0RXD_UCA0SOMI_TA1-1_TMS_A5"/>
<wire x1="132.08" y1="111.76" x2="60.96" y2="111.76" width="0.1524" layer="91"/>
<label x="60.96" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="15.24" y1="66.04" x2="25.4" y2="66.04" width="0.1524" layer="91"/>
<label x="25.4" y="66.04" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP16" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="MAG-ACC-INT1" class="0">
<segment>
<pinref part="U1" gate="A" pin="P1-0_UCB0STE_TA0CLK_A0_VEREF+"/>
<wire x1="132.08" y1="106.68" x2="68.58" y2="106.68" width="0.1524" layer="91"/>
<label x="68.58" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MAG-ACC-INT2" class="0">
<segment>
<pinref part="U1" gate="A" pin="P1-1_UCB0CLK_TA0-1_A1"/>
<wire x1="132.08" y1="104.14" x2="63.5" y2="104.14" width="0.1524" layer="91"/>
<label x="68.58" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NFC_INT" class="0">
<segment>
<pinref part="U1" gate="A" pin="P1-6_UCA0CLK_TA1CLK_TDI_TCLK_A6"/>
<wire x1="132.08" y1="101.6" x2="68.58" y2="101.6" width="0.1524" layer="91"/>
<label x="68.58" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="FLASH_!RST!" class="0">
<segment>
<pinref part="U1" gate="A" pin="P2-3_CAP0-2"/>
<label x="248.92" y="107.188" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="218.44" y1="106.68" x2="225.806" y2="106.68" width="0.1524" layer="91"/>
<wire x1="225.806" y1="106.68" x2="225.806" y2="106.934" width="0.1524" layer="91"/>
</segment>
</net>
<net name="NFC_!RST!" class="0">
<segment>
<pinref part="U1" gate="A" pin="CAP0-3"/>
<label x="246.126" y="103.886" size="1.27" layer="95" rot="R180" xref="yes"/>
<wire x1="218.44" y1="104.14" x2="226.06" y2="104.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TRANS_PIN" class="0">
<segment>
<pinref part="U1" gate="A" pin="P3-0_CAP0-0"/>
<wire x1="218.44" y1="129.54" x2="220.98" y2="129.54" width="0.1524" layer="91"/>
<label x="220.98" y="129.54" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="22.86" y1="116.84" x2="15.24" y2="116.84" width="0.1524" layer="91"/>
<label x="22.86" y="116.84" size="1.27" layer="95" font="vector" xref="yes"/>
<pinref part="TP6" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="POWER" class="0">
<segment>
<pinref part="VD2" gate="G$1" pin="A"/>
<wire x1="17.78" y1="40.64" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<label x="17.78" y="40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="VD2" gate="G$1" pin="C"/>
<wire x1="30.48" y1="40.64" x2="38.1" y2="40.64" width="0.1524" layer="91"/>
<wire x1="38.1" y1="40.64" x2="38.1" y2="27.94" width="0.1524" layer="91"/>
<wire x1="38.1" y1="27.94" x2="30.48" y2="27.94" width="0.1524" layer="91"/>
<pinref part="VD1" gate="G$1" pin="C"/>
<pinref part="U2" gate="G$1" pin="VIN"/>
<wire x1="76.2" y1="40.64" x2="71.12" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C1" gate="C$1" pin="1"/>
<wire x1="71.12" y1="40.64" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
<wire x1="55.88" y1="35.56" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="CE"/>
<wire x1="76.2" y1="25.4" x2="71.12" y2="25.4" width="0.1524" layer="91"/>
<wire x1="71.12" y1="25.4" x2="71.12" y2="40.64" width="0.1524" layer="91"/>
<junction x="71.12" y="40.64"/>
<wire x1="38.1" y1="40.64" x2="55.88" y2="40.64" width="0.1524" layer="91"/>
<junction x="38.1" y="40.64"/>
<junction x="55.88" y="40.64"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>RTC, NFC, Temp, Pressure, Magnometer, Flash</description>
<plain>
<text x="119.38" y="-116.84" size="2.54" layer="90">Pressure Sensor</text>
<text x="15.24" y="-78.74" size="2.54" layer="90">Magnometer and accelometer</text>
<text x="167.64" y="-127" size="2.54" layer="90">Temperature sensor</text>
<text x="162.56" y="-17.78" size="2.54" layer="90">NFC interface</text>
<text x="17.78" y="-121.92" size="2.54" layer="90">Flash memory</text>
<wire x1="2.54" y1="-12.7" x2="99.06" y2="-12.7" width="0.508" layer="90" style="shortdash"/>
<wire x1="99.06" y1="-12.7" x2="254" y2="-12.7" width="0.508" layer="90" style="shortdash"/>
<wire x1="99.06" y1="-12.7" x2="99.06" y2="-86.36" width="0.508" layer="90" style="shortdash"/>
<wire x1="2.54" y1="-86.36" x2="99.06" y2="-86.36" width="0.508" layer="90" style="shortdash"/>
<wire x1="99.06" y1="-86.36" x2="165.1" y2="-86.36" width="0.508" layer="90" style="shortdash"/>
<wire x1="165.1" y1="-86.36" x2="254" y2="-86.36" width="0.508" layer="90" style="shortdash"/>
<wire x1="2.54" y1="-132.08" x2="99.06" y2="-132.08" width="0.508" layer="90" style="shortdash"/>
<wire x1="99.06" y1="-132.08" x2="165.1" y2="-132.08" width="0.508" layer="90" style="shortdash"/>
<wire x1="165.1" y1="-132.08" x2="254" y2="-132.08" width="0.508" layer="90" style="shortdash"/>
<wire x1="165.1" y1="-86.36" x2="165.1" y2="-132.08" width="0.508" layer="90" style="shortdash"/>
<wire x1="99.06" y1="-86.36" x2="99.06" y2="-132.08" width="0.508" layer="90" style="shortdash"/>
<text x="45.72" y="-10.16" size="6.4516" layer="90">Data Storage Tag: Project Schematics (2/2)</text>
<text x="195.58" y="-83.82" size="1.778" layer="97">I2C Address = 01010000</text>
<text x="104.14" y="-124.46" size="1.778" layer="97">I2C Address = 1110110</text>
<text x="190.5" y="-129.54" size="1.778" layer="97">I2C Address = 01000000</text>
<text x="5.08" y="-83.82" size="1.778" layer="97">I2C Address = 00011101</text>
<text x="214.63" y="-154.94" size="2.54" layer="94">Rev. 1.2.0</text>
<text x="172.72" y="-167.64" size="2.54" layer="94">By Ali Aljumaili</text>
<text x="5.08" y="-139.7" size="2.54" layer="90">Silkscreen</text>
</plain>
<instances>
<instance part="U7" gate="G$1" x="208.28" y="-106.934" smashed="yes">
<attribute name="OC_NEWARK" x="208.28" y="-106.934" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="208.28" y="-106.934" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="208.28" y="-106.934" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="190.5" y="-94.234" size="3.048" layer="95"/>
<attribute name="VALUE" x="190.5" y="-122.174" size="3.048" layer="95"/>
</instance>
<instance part="GND21" gate="G$1" x="238.76" y="-114.3" smashed="yes">
<attribute name="VALUE" x="234.188" y="-122.682" size="1.778" layer="96"/>
</instance>
<instance part="U3" gate="G$1" x="43.18" y="-35.56" smashed="yes">
<attribute name="OC_NEWARK" x="43.18" y="-35.56" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="43.18" y="-35.56" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="43.18" y="-35.56" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="30.48" y="-20.32" size="3.048" layer="95"/>
<attribute name="VALUE" x="35.56" y="-73.66" size="3.048" layer="95"/>
</instance>
<instance part="GND23" gate="1" x="5.842" y="-42.926" smashed="yes">
<attribute name="VALUE" x="3.302" y="-45.466" size="1.778" layer="96"/>
</instance>
<instance part="U6" gate="G$1" x="129.54" y="-104.14" smashed="yes">
<attribute name="OC_NEWARK" x="129.54" y="-104.14" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="129.54" y="-104.14" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="129.54" y="-104.14" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="121.92" y="-93.98" size="3.048" layer="95"/>
<attribute name="VALUE" x="121.92" y="-111.76" size="3.048" layer="95"/>
</instance>
<instance part="GND29" gate="1" x="109.22" y="-111.76" smashed="yes">
<attribute name="VALUE" x="106.68" y="-114.3" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="C$1" x="5.842" y="-35.306" smashed="yes">
<attribute name="NAME" x="7.366" y="-34.925" size="1.778" layer="95"/>
<attribute name="VALUE" x="7.366" y="-40.005" size="1.778" layer="96"/>
</instance>
<instance part="C10" gate="C$1" x="15.24" y="-35.56" smashed="yes">
<attribute name="NAME" x="16.764" y="-35.179" size="1.778" layer="95"/>
<attribute name="VALUE" x="16.764" y="-40.259" size="1.778" layer="96"/>
</instance>
<instance part="C17" gate="C$1" x="109.22" y="-101.6" smashed="yes">
<attribute name="NAME" x="110.744" y="-101.219" size="1.778" layer="95"/>
<attribute name="VALUE" x="110.744" y="-106.299" size="1.778" layer="96"/>
</instance>
<instance part="FRAME2" gate="G$1" x="-2.54" y="-175.26" smashed="yes">
<attribute name="DRAWING_NAME" x="214.63" y="-160.02" size="2.54" layer="94"/>
<attribute name="LAST_DATE_TIME" x="214.63" y="-165.1" size="2.286" layer="94"/>
<attribute name="SHEET" x="227.965" y="-170.18" size="2.54" layer="94"/>
</instance>
<instance part="GND10" gate="G$1" x="20.32" y="-68.58" smashed="yes">
<attribute name="VALUE" x="15.748" y="-76.962" size="1.778" layer="96"/>
</instance>
<instance part="P+10" gate="VCC" x="238.76" y="-91.44" smashed="yes">
<attribute name="VALUE" x="236.22" y="-93.98" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+11" gate="G$1" x="109.22" y="-93.98" smashed="yes">
<attribute name="VALUE" x="106.68" y="-93.98" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+12" gate="G$1" x="5.842" y="-19.812" smashed="yes">
<attribute name="VALUE" x="3.302" y="-19.812" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U5" gate="G$1" x="60.96" y="-104.14" smashed="yes">
<attribute name="NAME" x="48.26" y="-91.44" size="3.048" layer="95"/>
<attribute name="VALUE" x="43.18" y="-127" size="3.048" layer="96"/>
</instance>
<instance part="C18" gate="C$1" x="238.76" y="-103.124" smashed="yes">
<attribute name="NAME" x="240.284" y="-102.743" size="1.778" layer="95"/>
<attribute name="VALUE" x="240.284" y="-107.823" size="1.778" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="137.16" y="-78.74" smashed="yes">
<attribute name="VALUE" x="134.62" y="-81.28" size="1.778" layer="96"/>
</instance>
<instance part="C12" gate="C$1" x="129.54" y="-33.02" smashed="yes" rot="R270">
<attribute name="NAME" x="129.921" y="-34.544" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="124.841" y="-34.544" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C11" gate="C$1" x="129.54" y="-22.86" smashed="yes" rot="R270">
<attribute name="NAME" x="129.921" y="-24.384" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="124.841" y="-24.384" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+7" gate="VCC" x="137.16" y="-22.352" smashed="yes">
<attribute name="VALUE" x="134.62" y="-24.892" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C13" gate="C$1" x="198.12" y="-33.02" smashed="yes" rot="R270">
<attribute name="NAME" x="198.501" y="-34.544" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="193.421" y="-34.544" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND14" gate="1" x="205.74" y="-38.1" smashed="yes">
<attribute name="VALUE" x="203.2" y="-40.64" size="1.778" layer="96"/>
</instance>
<instance part="GND15" gate="1" x="130.81" y="-78.74" smashed="yes">
<attribute name="VALUE" x="128.27" y="-81.28" size="1.778" layer="96"/>
</instance>
<instance part="GND16" gate="G$1" x="15.24" y="-40.386" smashed="yes">
<attribute name="VALUE" x="10.668" y="-48.768" size="1.778" layer="96"/>
</instance>
<instance part="P+8" gate="G$1" x="86.36" y="-93.98" smashed="yes">
<attribute name="VALUE" x="83.82" y="-93.98" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C16" gate="C$1" x="86.36" y="-108.712" smashed="yes">
<attribute name="NAME" x="87.884" y="-108.331" size="1.778" layer="95"/>
<attribute name="VALUE" x="87.884" y="-113.411" size="1.778" layer="96"/>
</instance>
<instance part="GND17" gate="1" x="86.36" y="-119.38" smashed="yes">
<attribute name="VALUE" x="83.82" y="-121.92" size="1.778" layer="96"/>
</instance>
<instance part="P+9" gate="G$1" x="182.88" y="-93.98" smashed="yes">
<attribute name="VALUE" x="180.34" y="-93.98" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C14" gate="C$1" x="210.82" y="-54.61" smashed="yes">
<attribute name="NAME" x="212.344" y="-54.229" size="1.778" layer="95"/>
<attribute name="VALUE" x="212.344" y="-59.309" size="1.778" layer="96"/>
</instance>
<instance part="U4" gate="G$1" x="160.02" y="-50.8" smashed="yes">
<attribute name="NAME" x="145.1356" y="-26.4414" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="152.1206" y="-79.7814" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="GND2" gate="1" x="124.46" y="-78.74" smashed="yes">
<attribute name="VALUE" x="121.92" y="-81.28" size="1.778" layer="96"/>
</instance>
<instance part="U$13" gate="G$1" x="116.84" y="-30.48" smashed="yes">
<attribute name="VALUE" x="112.268" y="-38.862" size="1.778" layer="96"/>
</instance>
<instance part="VD3" gate="G$1" x="226.06" y="-50.8" smashed="yes">
<attribute name="NAME" x="223.774" y="-48.895" size="1.778" layer="95"/>
<attribute name="VALUE" x="223.774" y="-54.229" size="1.778" layer="96"/>
</instance>
<instance part="VD4" gate="G$1" x="231.14" y="-60.96" smashed="yes">
<attribute name="NAME" x="228.854" y="-59.055" size="1.778" layer="95"/>
<attribute name="VALUE" x="228.854" y="-64.389" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="246.38" y="-76.2" smashed="yes">
<attribute name="VALUE" x="243.84" y="-78.74" size="1.778" layer="96"/>
</instance>
<instance part="C15" gate="C$1" x="246.38" y="-66.04" smashed="yes">
<attribute name="NAME" x="247.904" y="-65.659" size="1.778" layer="95"/>
<attribute name="VALUE" x="247.904" y="-70.739" size="1.778" layer="96"/>
</instance>
<instance part="JP1" gate="G$1" x="223.52" y="-43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="223.52" y="-44.704" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="223.52" y="-40.64" size="1.27" layer="96" rot="R180"/>
</instance>
<instance part="JP1" gate="G$2" x="223.52" y="-68.58" smashed="yes" rot="R180">
<attribute name="NAME" x="223.52" y="-70.104" size="1.27" layer="95" rot="R180"/>
<attribute name="VALUE" x="223.52" y="-66.04" size="1.27" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C17" gate="C$1" pin="2"/>
<pinref part="GND29" gate="1" pin="GND"/>
<wire x1="109.22" y1="-109.22" x2="109.22" y2="-106.68" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="GND"/>
<wire x1="116.84" y1="-104.14" x2="116.84" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-106.68" x2="109.22" y2="-106.68" width="0.1524" layer="91"/>
<junction x="109.22" y="-106.68"/>
</segment>
<segment>
<pinref part="GND21" gate="G$1" pin="GND"/>
<wire x1="238.76" y1="-108.204" x2="238.76" y2="-109.474" width="0.1524" layer="91"/>
<pinref part="C18" gate="C$1" pin="2"/>
<pinref part="U7" gate="G$1" pin="GND"/>
<wire x1="238.76" y1="-109.474" x2="238.76" y2="-112.014" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-112.014" x2="238.76" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-109.474" x2="238.76" y2="-109.474" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="TP"/>
<wire x1="231.14" y1="-112.014" x2="238.76" y2="-112.014" width="0.1524" layer="91"/>
<junction x="238.76" y="-109.474"/>
<junction x="238.76" y="-112.014"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND_1"/>
<wire x1="20.32" y1="-58.42" x2="20.32" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="GND_2"/>
<wire x1="20.32" y1="-60.96" x2="20.32" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-63.5" x2="20.32" y2="-68.58" width="0.1524" layer="91"/>
<junction x="20.32" y="-60.96"/>
<pinref part="U3" gate="G$1" pin="GND_3"/>
<junction x="20.32" y="-63.5"/>
<pinref part="GND10" gate="G$1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="ADDR"/>
<wire x1="20.32" y1="-55.88" x2="20.32" y2="-58.42" width="0.1524" layer="91"/>
<junction x="20.32" y="-58.42"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="C13" gate="C$1" pin="1"/>
<wire x1="200.66" y1="-33.02" x2="205.74" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-33.02" x2="205.74" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="C$1" pin="2"/>
<pinref part="GND16" gate="G$1" pin="GND"/>
<wire x1="15.24" y1="-40.64" x2="15.24" y2="-40.386" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C9" gate="C$1" pin="2"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C16" gate="C$1" pin="2"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="86.36" y1="-116.84" x2="86.36" y2="-111.76" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="TP"/>
<wire x1="78.74" y1="-116.84" x2="86.36" y2="-116.84" width="0.1524" layer="91"/>
<junction x="86.36" y="-116.84"/>
<pinref part="U5" gate="G$1" pin="GND"/>
<wire x1="78.74" y1="-111.76" x2="86.36" y2="-111.76" width="0.1524" layer="91"/>
<junction x="86.36" y="-111.76"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="SCMS_CS_N"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="139.7" y1="-55.88" x2="124.46" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-55.88" x2="124.46" y2="-76.2" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="SCK"/>
<wire x1="139.7" y1="-53.34" x2="124.46" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="124.46" y1="-53.34" x2="124.46" y2="-55.88" width="0.1524" layer="91"/>
<junction x="124.46" y="-55.88"/>
</segment>
<segment>
<wire x1="139.7" y1="-60.96" x2="130.81" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="E0"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="130.81" y1="-60.96" x2="130.81" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="E1"/>
<wire x1="130.81" y1="-63.5" x2="130.81" y2="-66.04" width="0.1524" layer="91"/>
<wire x1="130.81" y1="-66.04" x2="130.81" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="139.7" y1="-63.5" x2="130.81" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="E2"/>
<wire x1="139.7" y1="-66.04" x2="130.81" y2="-66.04" width="0.1524" layer="91"/>
<junction x="130.81" y="-66.04"/>
<junction x="130.81" y="-63.5"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="137.16" y1="-76.2" x2="137.16" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="TP"/>
<wire x1="137.16" y1="-73.66" x2="139.7" y2="-73.66" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VSS"/>
<wire x1="139.7" y1="-71.12" x2="137.16" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="137.16" y1="-71.12" x2="137.16" y2="-73.66" width="0.1524" layer="91"/>
<junction x="137.16" y="-73.66"/>
</segment>
<segment>
<pinref part="C11" gate="C$1" pin="2"/>
<wire x1="124.46" y1="-22.86" x2="121.92" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-22.86" x2="121.92" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="C12" gate="C$1" pin="2"/>
<wire x1="121.92" y1="-27.94" x2="121.92" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-33.02" x2="124.46" y2="-33.02" width="0.1524" layer="91"/>
<junction x="121.92" y="-27.94"/>
<wire x1="121.92" y1="-27.94" x2="116.84" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-27.94" x2="116.84" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="U$13" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND5" gate="1" pin="GND"/>
<pinref part="C15" gate="C$1" pin="2"/>
<wire x1="246.38" y1="-73.66" x2="246.38" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<wire x1="109.22" y1="-99.06" x2="109.22" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="C17" gate="C$1" pin="1"/>
<pinref part="P+11" gate="G$1" pin="VCC"/>
<pinref part="U6" gate="G$1" pin="VDD"/>
<wire x1="116.84" y1="-99.06" x2="109.22" y2="-99.06" width="0.1524" layer="91"/>
<junction x="109.22" y="-99.06"/>
</segment>
<segment>
<wire x1="238.76" y1="-100.584" x2="238.76" y2="-99.314" width="0.1524" layer="91"/>
<pinref part="P+10" gate="VCC" pin="VCC"/>
<pinref part="C18" gate="C$1" pin="1"/>
<pinref part="U7" gate="G$1" pin="VDD"/>
<wire x1="238.76" y1="-99.06" x2="238.76" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="231.14" y1="-99.314" x2="238.76" y2="-99.314" width="0.1524" layer="91"/>
<wire x1="238.76" y1="-99.314" x2="238.76" y2="-99.06" width="0.1524" layer="91"/>
<junction x="238.76" y="-99.06"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="DNC1"/>
<wire x1="185.42" y1="-99.314" x2="182.88" y2="-99.314" width="0.1524" layer="91"/>
<pinref part="P+9" gate="G$1" pin="VCC"/>
<wire x1="182.88" y1="-99.314" x2="182.88" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="U7" gate="G$1" pin="DNC2"/>
<wire x1="182.88" y1="-99.06" x2="182.88" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-101.854" x2="182.88" y2="-101.854" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-101.854" x2="182.88" y2="-99.314" width="0.1524" layer="91"/>
<junction x="182.88" y="-99.06"/>
</segment>
<segment>
<wire x1="86.36" y1="-106.172" x2="86.36" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="C16" gate="C$1" pin="1"/>
<pinref part="U5" gate="G$1" pin="VCC"/>
<wire x1="86.36" y1="-104.14" x2="86.36" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-96.52" x2="86.36" y2="-96.52" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="!WP"/>
<wire x1="78.74" y1="-104.14" x2="86.36" y2="-104.14" width="0.1524" layer="91"/>
<junction x="86.36" y="-104.14"/>
<pinref part="P+8" gate="G$1" pin="VCC"/>
<wire x1="86.36" y1="-96.52" x2="86.36" y2="-93.98" width="0.1524" layer="91"/>
<junction x="86.36" y="-96.52"/>
</segment>
<segment>
<pinref part="C9" gate="C$1" pin="1"/>
<wire x1="5.842" y1="-32.766" x2="5.842" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="P+12" gate="G$1" pin="VCC"/>
<pinref part="U3" gate="G$1" pin="IO_VDD"/>
<wire x1="5.842" y1="-25.4" x2="5.842" y2="-19.812" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-30.48" x2="15.24" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="C10" gate="C$1" pin="1"/>
<wire x1="15.24" y1="-30.48" x2="15.24" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-30.48" x2="15.24" y2="-25.4" width="0.1524" layer="91"/>
<junction x="15.24" y="-30.48"/>
<pinref part="U3" gate="G$1" pin="VDD"/>
<wire x1="20.32" y1="-25.4" x2="15.24" y2="-25.4" width="0.1524" layer="91"/>
<junction x="15.24" y="-25.4"/>
<wire x1="15.24" y1="-25.4" x2="5.842" y2="-25.4" width="0.1524" layer="91"/>
<junction x="5.842" y="-25.4"/>
</segment>
<segment>
<pinref part="C11" gate="C$1" pin="1"/>
<pinref part="C12" gate="C$1" pin="1"/>
<pinref part="U4" gate="G$1" pin="VCC"/>
<wire x1="139.7" y1="-33.02" x2="137.16" y2="-33.02" width="0.1524" layer="91"/>
<junction x="132.08" y="-33.02"/>
<wire x1="137.16" y1="-33.02" x2="132.08" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-33.02" x2="132.08" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="P+7" gate="VCC" pin="VCC"/>
<wire x1="137.16" y1="-24.892" x2="137.16" y2="-33.02" width="0.1524" layer="91"/>
<junction x="137.16" y="-33.02"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="C13" gate="C$1" pin="2"/>
<wire x1="187.96" y1="-33.02" x2="180.34" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-33.02" x2="193.04" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VCORE"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="C14" gate="C$1" pin="1"/>
<wire x1="210.82" y1="-52.07" x2="210.82" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="210.82" y1="-50.8" x2="218.44" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="ANT1"/>
<wire x1="218.44" y1="-50.8" x2="218.44" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-43.18" x2="210.82" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="210.82" y1="-43.18" x2="210.82" y2="-50.8" width="0.1524" layer="91"/>
<junction x="210.82" y="-50.8"/>
<pinref part="VD3" gate="G$1" pin="A"/>
<wire x1="223.52" y1="-50.8" x2="218.44" y2="-50.8" width="0.1524" layer="91"/>
<junction x="218.44" y="-50.8"/>
<pinref part="JP1" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="C14" gate="C$1" pin="2"/>
<wire x1="210.82" y1="-60.96" x2="218.44" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="ANT2"/>
<wire x1="218.44" y1="-60.96" x2="218.44" y2="-68.58" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-53.34" x2="205.74" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-53.34" x2="205.74" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="205.74" y1="-60.96" x2="210.82" y2="-60.96" width="0.1524" layer="91"/>
<wire x1="210.82" y1="-60.96" x2="210.82" y2="-59.69" width="0.1524" layer="91"/>
<junction x="210.82" y="-60.96"/>
<pinref part="VD4" gate="G$1" pin="A"/>
<junction x="218.44" y="-60.96"/>
<wire x1="218.44" y1="-60.96" x2="228.6" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="JP1" gate="G$2" pin="TP"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SDA"/>
<wire x1="66.04" y1="-25.4" x2="71.12" y2="-25.4" width="0.1524" layer="91"/>
<label x="71.12" y="-25.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="SI_SDA"/>
<wire x1="139.7" y1="-48.26" x2="116.84" y2="-48.26" width="0.1524" layer="91"/>
<label x="103.378" y="-48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="SDA"/>
<wire x1="142.24" y1="-99.06" x2="147.32" y2="-99.06" width="0.1524" layer="91"/>
<label x="147.32" y="-99.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="SDA"/>
<wire x1="185.42" y1="-106.934" x2="180.086" y2="-106.934" width="0.1524" layer="91"/>
<label x="180.34" y="-106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SCL"/>
<wire x1="66.04" y1="-30.48" x2="71.12" y2="-30.48" width="0.1524" layer="91"/>
<label x="71.12" y="-30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="SO_SCL"/>
<wire x1="139.7" y1="-45.72" x2="116.84" y2="-45.72" width="0.1524" layer="91"/>
<label x="116.84" y="-45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="SCL"/>
<wire x1="142.24" y1="-104.14" x2="147.32" y2="-104.14" width="0.1524" layer="91"/>
<label x="147.32" y="-104.14" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U7" gate="G$1" pin="SCL"/>
<wire x1="185.42" y1="-112.014" x2="180.34" y2="-112.014" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-112.014" x2="180.34" y2="-111.76" width="0.1524" layer="91"/>
<label x="180.34" y="-111.76" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NFC_!RST!" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="RST_N"/>
<wire x1="139.7" y1="-40.64" x2="121.92" y2="-40.64" width="0.1524" layer="91"/>
<label x="121.92" y="-40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_CLK" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SCK"/>
<wire x1="43.18" y1="-106.68" x2="38.1" y2="-106.68" width="0.1524" layer="91"/>
<label x="38.1" y="-106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_SOMI" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SO"/>
<wire x1="43.18" y1="-101.6" x2="38.1" y2="-101.6" width="0.1524" layer="91"/>
<label x="17.78" y="-101.6" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SPI_SIMO" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SI"/>
<wire x1="43.18" y1="-96.52" x2="38.1" y2="-96.52" width="0.1524" layer="91"/>
<label x="38.1" y="-96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_!CS!" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="!CS"/>
<wire x1="43.18" y1="-111.76" x2="38.1" y2="-111.76" width="0.1524" layer="91"/>
<label x="38.1" y="-111.76" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MAG-ACC-INT2" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="GPIO2"/>
<wire x1="66.04" y1="-40.64" x2="71.12" y2="-40.64" width="0.1524" layer="91"/>
<label x="71.12" y="-40.64" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="MAG-ACC-INT1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="GPIO1"/>
<wire x1="66.04" y1="-45.72" x2="71.12" y2="-45.72" width="0.1524" layer="91"/>
<label x="71.12" y="-45.72" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="NFC_INT" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="INTO"/>
<wire x1="180.34" y1="-60.96" x2="185.42" y2="-60.96" width="0.1524" layer="91"/>
<label x="185.42" y="-60.96" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="FLASH_!RST!" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="!RESET"/>
<wire x1="43.18" y1="-116.84" x2="38.1" y2="-116.84" width="0.1524" layer="91"/>
<label x="38.1" y="-116.84" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="POWER" class="0">
<segment>
<pinref part="VD4" gate="G$1" pin="C"/>
<wire x1="233.68" y1="-60.96" x2="246.38" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="C15" gate="C$1" pin="1"/>
<wire x1="246.38" y1="-60.96" x2="246.38" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="246.38" y1="-60.96" x2="246.38" y2="-50.8" width="0.1524" layer="91"/>
<junction x="246.38" y="-60.96"/>
<pinref part="VD3" gate="G$1" pin="C"/>
<wire x1="246.38" y1="-50.8" x2="246.38" y2="-47.752" width="0.1524" layer="91"/>
<wire x1="228.6" y1="-50.8" x2="246.38" y2="-50.8" width="0.1524" layer="91"/>
<junction x="246.38" y="-50.8"/>
<label x="246.38" y="-45.72" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>

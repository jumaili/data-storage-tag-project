# Data Storage Tag - Reivsion 1.0.0

##Directory explanation

* DST-Rev.1.1.0: PCB board design file openable in Autodesk Eagle.
* DST-Rev.1.1.0: Schematics design file openable in Autodesk Eagle.
* DST-Project.lbr: Eagle library with all devices.


## Author
Created by **Ali Aljumaili** (18/12/2017)
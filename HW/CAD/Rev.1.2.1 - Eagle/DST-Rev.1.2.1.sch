<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting keepoldvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="5" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="14" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="4" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="23" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Flex-Kleb" color="12" fill="1" visible="yes" active="yes"/>
<layer number="102" name="fp2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="fp4" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="MPL" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="tTestdril" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="bTestdril" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DST_Project" urn="urn:adsk.eagle:library:9209732">
<description>This library contain all of the needed parts for the prototype made october 2017 at NTNU as a project.
&lt;p&gt;
By Ali Al Jumaili 18.10.2017</description>
<packages>
<package name="LGA-16" urn="urn:adsk.eagle:footprint:793888/5" library_version="12" library_locally_modified="yes">
<description>LGA-16 3x3x1.1</description>
<wire x1="-1.5748" y1="0.3048" x2="-0.3048" y2="1.5748" width="0.0254" layer="21"/>
<wire x1="0.889" y1="1.3208" x2="0.635" y2="1.3208" width="0" layer="51"/>
<wire x1="0.381" y1="1.3208" x2="0.127" y2="1.3208" width="0" layer="51"/>
<wire x1="-0.127" y1="1.3208" x2="-0.381" y2="1.3208" width="0" layer="51"/>
<wire x1="-1.3208" y1="0.8636" x2="-1.3208" y2="0.6096" width="0" layer="51"/>
<wire x1="-1.3208" y1="0.381" x2="-1.3208" y2="0.127" width="0" layer="51"/>
<wire x1="-1.3208" y1="-0.127" x2="-1.3208" y2="-0.381" width="0" layer="51"/>
<wire x1="-1.3208" y1="-0.635" x2="-1.3208" y2="-0.889" width="0" layer="51"/>
<wire x1="-1.3208" y1="-1.1176" x2="-1.3208" y2="-1.3716" width="0" layer="51"/>
<wire x1="-0.381" y1="-1.8288" x2="-0.127" y2="-1.8288" width="0" layer="51"/>
<wire x1="0.127" y1="-1.8288" x2="0.381" y2="-1.8288" width="0" layer="51"/>
<wire x1="0.635" y1="-1.8288" x2="0.889" y2="-1.8288" width="0" layer="51"/>
<wire x1="1.8288" y1="-1.3716" x2="1.8288" y2="-1.1176" width="0" layer="51"/>
<wire x1="1.8288" y1="-0.889" x2="1.8288" y2="-0.635" width="0" layer="51"/>
<wire x1="1.8288" y1="-0.381" x2="1.8288" y2="-0.127" width="0" layer="51"/>
<wire x1="1.8288" y1="0.127" x2="1.8288" y2="0.381" width="0" layer="51"/>
<wire x1="1.8288" y1="0.6096" x2="1.8288" y2="0.8636" width="0" layer="51"/>
<wire x1="-1.5748" y1="-1.5748" x2="1.5748" y2="-1.5748" width="0.0254" layer="21"/>
<wire x1="1.5748" y1="-1.5748" x2="1.5748" y2="1.5748" width="0.0254" layer="21"/>
<wire x1="1.5748" y1="1.5748" x2="-1.5748" y2="1.5748" width="0.0254" layer="21"/>
<wire x1="-1.5748" y1="1.5748" x2="-1.5748" y2="-1.5748" width="0.0254" layer="21"/>
<smd name="1" x="-1.4224" y="0.9906" dx="0.254" dy="0.7112" layer="1" roundness="70" rot="R270"/>
<smd name="2" x="-1.4224" y="0.508" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="3" x="-1.4224" y="0" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="4" x="-1.4224" y="-0.508" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="5" x="-1.4224" y="-0.9906" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="6" x="-0.508" y="-1.4224" dx="0.254" dy="0.7112" layer="1" rot="R180"/>
<smd name="7" x="0" y="-1.4224" dx="0.254" dy="0.7112" layer="1" rot="R180"/>
<smd name="8" x="0.508" y="-1.4224" dx="0.254" dy="0.7112" layer="1" rot="R180"/>
<smd name="9" x="1.4224" y="-0.9906" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="10" x="1.4224" y="-0.508" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="11" x="1.4224" y="0" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="12" x="1.4224" y="0.508" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="13" x="1.4224" y="0.9906" dx="0.254" dy="0.7112" layer="1" rot="R270"/>
<smd name="14" x="0.508" y="1.4224" dx="0.254" dy="0.7112" layer="1" rot="R180"/>
<smd name="15" x="0" y="1.4224" dx="0.254" dy="0.7112" layer="1" rot="R180"/>
<smd name="16" x="-0.508" y="1.4224" dx="0.254" dy="0.7112" layer="1" rot="R180"/>
<circle x="-1.413" y="1.508" radius="0.25" width="0.127" layer="21"/>
<text x="1.119" y="3.08" size="0.5" layer="25" rot="R180">&gt;Name</text>
<text x="0.611" y="-2.326" size="0.5" layer="27" rot="R180">&gt;Value</text>
<polygon width="0.127" layer="42">
<vertex x="-1.53" y="-1.51"/>
<vertex x="-1.528" y="1.528"/>
<vertex x="1.528" y="1.522"/>
<vertex x="1.51" y="-1.51"/>
</polygon>
<polygon width="0.127" layer="43">
<vertex x="-1" y="-0.8"/>
<vertex x="-1.53" y="-0.8"/>
<vertex x="-1.53" y="-0.7"/>
<vertex x="-1" y="-0.7"/>
<vertex x="-1" y="-0.31"/>
<vertex x="-1.52" y="-0.31"/>
<vertex x="-1.52" y="-0.19"/>
<vertex x="-1" y="-0.19"/>
<vertex x="-1" y="0.19"/>
<vertex x="-1.52" y="0.19"/>
<vertex x="-1.52" y="0.31"/>
<vertex x="-1" y="0.31"/>
<vertex x="-1" y="0.7"/>
<vertex x="-1.52" y="0.7"/>
<vertex x="-1.52" y="0.8"/>
<vertex x="-1" y="0.8"/>
<vertex x="-1" y="1.19"/>
<vertex x="-1.52" y="1.19"/>
<vertex x="-1.52" y="1.52"/>
<vertex x="-0.7" y="1.52"/>
<vertex x="-0.7" y="1"/>
<vertex x="-0.31" y="1"/>
<vertex x="-0.31" y="1.52"/>
<vertex x="-0.19" y="1.52"/>
<vertex x="-0.19" y="1"/>
<vertex x="0.19" y="1"/>
<vertex x="0.19" y="1.52"/>
<vertex x="0.31" y="1.52"/>
<vertex x="0.31" y="1"/>
<vertex x="0.7" y="1"/>
<vertex x="0.7" y="1.52"/>
<vertex x="1.52" y="1.52"/>
<vertex x="1.52" y="1.19"/>
<vertex x="1" y="1.19"/>
<vertex x="1" y="0.8"/>
<vertex x="1.52" y="0.8"/>
<vertex x="1.52" y="0.7"/>
<vertex x="1" y="0.7"/>
<vertex x="1" y="0.31"/>
<vertex x="1.52" y="0.31"/>
<vertex x="1.52" y="0.2"/>
<vertex x="1" y="0.2"/>
<vertex x="1" y="-0.19"/>
<vertex x="1.52" y="-0.19"/>
<vertex x="1.52" y="-0.31"/>
<vertex x="1" y="-0.31"/>
<vertex x="1" y="-0.7"/>
<vertex x="1.52" y="-0.7"/>
<vertex x="1.52" y="-0.8"/>
<vertex x="1" y="-0.8"/>
<vertex x="1" y="-1.18"/>
<vertex x="1.52" y="-1.18"/>
<vertex x="1.52" y="-1.52"/>
<vertex x="0.7" y="-1.52"/>
<vertex x="0.7" y="-1"/>
<vertex x="0.31" y="-1"/>
<vertex x="0.31" y="-1.52"/>
<vertex x="0.19" y="-1.52"/>
<vertex x="0.19" y="-1"/>
<vertex x="-0.2" y="-1"/>
<vertex x="-0.2" y="-1.52"/>
<vertex x="-0.31" y="-1.52"/>
<vertex x="-0.31" y="-1"/>
<vertex x="-0.7" y="-1"/>
<vertex x="-0.7" y="-1.52"/>
<vertex x="-1.52" y="-1.52"/>
<vertex x="-1.52" y="-1.18"/>
<vertex x="-1" y="-1.18"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="-0.8"/>
<vertex x="-1.53" y="-0.8"/>
<vertex x="-1.53" y="-0.7"/>
<vertex x="-1" y="-0.7"/>
<vertex x="-1" y="-0.31"/>
<vertex x="-1.52" y="-0.31"/>
<vertex x="-1.52" y="-0.19"/>
<vertex x="-1" y="-0.19"/>
<vertex x="-1" y="0.19"/>
<vertex x="-1.52" y="0.19"/>
<vertex x="-1.52" y="0.31"/>
<vertex x="-1" y="0.31"/>
<vertex x="-1" y="0.7"/>
<vertex x="-1.52" y="0.7"/>
<vertex x="-1.52" y="0.8"/>
<vertex x="-1" y="0.8"/>
<vertex x="-1" y="1.19"/>
<vertex x="-1.52" y="1.19"/>
<vertex x="-1.52" y="1.52"/>
<vertex x="-0.7" y="1.52"/>
<vertex x="-0.7" y="1"/>
<vertex x="-0.31" y="1"/>
<vertex x="-0.31" y="1.52"/>
<vertex x="-0.19" y="1.52"/>
<vertex x="-0.19" y="1"/>
<vertex x="0.19" y="1"/>
<vertex x="0.19" y="1.52"/>
<vertex x="0.31" y="1.52"/>
<vertex x="0.31" y="1"/>
<vertex x="0.7" y="1"/>
<vertex x="0.7" y="1.52"/>
<vertex x="1.52" y="1.52"/>
<vertex x="1.52" y="1.19"/>
<vertex x="1" y="1.19"/>
<vertex x="1" y="0.8"/>
<vertex x="1.52" y="0.8"/>
<vertex x="1.52" y="0.7"/>
<vertex x="1" y="0.7"/>
<vertex x="1" y="0.31"/>
<vertex x="1.52" y="0.31"/>
<vertex x="1.52" y="0.2"/>
<vertex x="1" y="0.2"/>
<vertex x="1" y="-0.19"/>
<vertex x="1.52" y="-0.19"/>
<vertex x="1.52" y="-0.31"/>
<vertex x="1" y="-0.31"/>
<vertex x="1" y="-0.7"/>
<vertex x="1.52" y="-0.7"/>
<vertex x="1.52" y="-0.8"/>
<vertex x="1" y="-0.8"/>
<vertex x="1" y="-1.18"/>
<vertex x="1.52" y="-1.18"/>
<vertex x="1.52" y="-1.52"/>
<vertex x="0.7" y="-1.52"/>
<vertex x="0.7" y="-1"/>
<vertex x="0.31" y="-1"/>
<vertex x="0.31" y="-1.52"/>
<vertex x="0.19" y="-1.52"/>
<vertex x="0.19" y="-1"/>
<vertex x="-0.2" y="-1"/>
<vertex x="-0.2" y="-1.52"/>
<vertex x="-0.31" y="-1.52"/>
<vertex x="-0.31" y="-1"/>
<vertex x="-0.7" y="-1"/>
<vertex x="-0.7" y="-1.52"/>
<vertex x="-1.52" y="-1.52"/>
<vertex x="-1.52" y="-1.18"/>
<vertex x="-1" y="-1.18"/>
</polygon>
</package>
<package name="MS5837-30BA" urn="urn:adsk.eagle:footprint:793890/1" library_version="3" library_locally_modified="yes">
<wire x1="-1.65" y1="1.65" x2="1.65" y2="1.65" width="0.127" layer="21"/>
<wire x1="1.65" y1="1.65" x2="1.65" y2="-1.65" width="0.127" layer="21"/>
<wire x1="1.65" y1="-1.65" x2="-1.65" y2="-1.65" width="0.127" layer="21"/>
<wire x1="-1.65" y1="-1.65" x2="-1.65" y2="1.65" width="0.127" layer="21"/>
<smd name="1" x="-1" y="1" dx="1.3" dy="1.3" layer="1"/>
<smd name="2" x="-1" y="-1" dx="1.3" dy="1.3" layer="1"/>
<smd name="3" x="1" y="-1" dx="1.3" dy="1.3" layer="1"/>
<smd name="4" x="1" y="1" dx="1.3" dy="1.3" layer="1"/>
<text x="-2" y="2.5" size="1.27" layer="25">&gt;Name</text>
<text x="-2" y="-4" size="1.27" layer="27">&gt;Value</text>
</package>
<package name="UDFN-8" urn="urn:adsk.eagle:footprint:8841322/3" library_version="12" library_locally_modified="yes">
<smd name="1" x="-2.8" y="1.905" dx="1.1" dy="0.45" layer="1"/>
<smd name="2" x="-2.8" y="0.635" dx="1.1" dy="0.45" layer="1" roundness="75"/>
<smd name="3" x="-2.8" y="-0.635" dx="1.1" dy="0.45" layer="1" roundness="75"/>
<smd name="4" x="-2.8" y="-1.905" dx="1.1" dy="0.45" layer="1" roundness="75"/>
<smd name="5" x="2.8" y="-1.905" dx="1.1" dy="0.45" layer="1" roundness="75"/>
<smd name="6" x="2.8" y="-0.635" dx="1.1" dy="0.45" layer="1" roundness="75"/>
<smd name="7" x="2.8" y="0.635" dx="1.1" dy="0.45" layer="1" roundness="75"/>
<smd name="8" x="2.8" y="1.905" dx="1.1" dy="0.45" layer="1" roundness="75"/>
<smd name="TP1" x="-0.97" y="1.2" dx="1.9" dy="1.6" layer="1" rot="R90"/>
<smd name="TP2" x="0.97" y="1.2" dx="1.9" dy="1.6" layer="1" rot="R90"/>
<smd name="TP3" x="-0.97" y="-1.2" dx="1.9" dy="1.6" layer="1" rot="R90"/>
<smd name="TP4" x="0.97" y="-1.2" dx="1.9" dy="1.6" layer="1" rot="R90"/>
<text x="2.54" y="6.35" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.6" y1="2.8" x2="3.6" y2="2.8" width="0.05" layer="51"/>
<wire x1="3.6" y1="2.8" x2="3.6" y2="-2.8" width="0.05" layer="51"/>
<wire x1="3.6" y1="-2.8" x2="-3.6" y2="-2.8" width="0.05" layer="51"/>
<wire x1="-3.6" y1="-2.8" x2="-3.6" y2="2.8" width="0.05" layer="51"/>
<wire x1="-3" y1="2.5" x2="3" y2="2.5" width="0.1" layer="51"/>
<wire x1="3" y1="2.5" x2="3" y2="-2.5" width="0.1" layer="51"/>
<wire x1="3" y1="-2.5" x2="-3" y2="-2.5" width="0.1" layer="51"/>
<wire x1="-3" y1="-2.5" x2="-3" y2="2.5" width="0.1" layer="51"/>
<wire x1="-3" y1="1" x2="-1.5" y2="2.5" width="0.1" layer="51"/>
<circle x="0.46" y="5.17" radius="0.125" width="0.25" layer="25"/>
<rectangle x1="-1.78" y1="-2.16" x2="1.78" y2="2.16" layer="48"/>
</package>
<package name="C0603" urn="urn:adsk.eagle:footprint:9209756/2" library_version="13" library_locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.5" y1="0.7" x2="1.5" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.5" y1="0.7" x2="1.5" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.5" y1="-0.7" x2="-1.5" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.5" y1="-0.7" x2="-1.5" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="-1.651" y1="0.762" x2="1.651" y2="0.762" width="0.127" layer="21"/>
<wire x1="1.651" y1="0.762" x2="1.651" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.651" y1="-0.762" x2="-1.651" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-1.651" y1="-0.762" x2="-1.651" y2="0.762" width="0.127" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="R603" urn="urn:adsk.eagle:footprint:8849198/4" library_version="13" library_locally_modified="yes">
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.5" y1="-0.7" x2="1.5" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="1.5" y1="-0.7" x2="1.5" y2="0.7" width="0.0508" layer="39"/>
<wire x1="1.5" y1="0.7" x2="-1.5" y2="0.7" width="0.0508" layer="39"/>
<wire x1="-1.5" y1="0.7" x2="-1.5" y2="-0.7" width="0.0508" layer="39"/>
<wire x1="-1.551" y1="0.789" x2="1.551" y2="0.789" width="0.127" layer="21"/>
<wire x1="1.551" y1="0.789" x2="1.551" y2="-0.789" width="0.127" layer="21"/>
<wire x1="1.551" y1="-0.789" x2="-1.551" y2="-0.789" width="0.127" layer="21"/>
<wire x1="-1.551" y1="-0.789" x2="-1.551" y2="0.789" width="0.127" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
</package>
<package name="CRYSTAL" urn="urn:adsk.eagle:footprint:8849199/3" library_version="13" library_locally_modified="yes">
<smd name="1" x="-1.524" y="-0.0508" dx="1.9" dy="1.1" layer="1" rot="R90"/>
<smd name="2" x="1.576" y="-0.0508" dx="1.9" dy="1.1" layer="1" rot="R90"/>
<text x="-3.524" y="2.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.524" y="-3.984" size="1.27" layer="25">&gt;VALUE</text>
<wire x1="-2.024" y1="-0.884" x2="2.076" y2="-0.884" width="0.127" layer="51"/>
<wire x1="2.076" y1="-0.884" x2="2.076" y2="0.716" width="0.127" layer="51"/>
<wire x1="2.076" y1="0.716" x2="-2.024" y2="0.716" width="0.127" layer="51"/>
<wire x1="-2.024" y1="0.716" x2="-2.024" y2="-0.884" width="0.127" layer="51"/>
</package>
<package name="DIOC0603_N" urn="urn:adsk.eagle:footprint:9209762/1" library_version="2" library_locally_modified="yes">
<description>&lt;b&gt;0603&lt;/b&gt; chip &lt;p&gt;

0603 (imperial)&lt;br/&gt;
1608 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<smd name="K" x="-0.8" y="0" dx="1.05" dy="0.9" layer="1"/>
<smd name="A" x="0.8" y="0" dx="1.05" dy="0.9" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<wire x1="-0.8" y1="0.4" x2="-0.3" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.3" y1="0.4" x2="0.1" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.1" y1="0.4" x2="0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.4" x2="0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.4" x2="0.1" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.1" y1="-0.4" x2="-0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.3" y1="-0.4" x2="-0.8" y2="-0.4" width="0.127" layer="51"/>
<wire x1="-0.8" y1="-0.4" x2="-0.8" y2="0.4" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.7" x2="1.6" y2="0.7" width="0.1" layer="39"/>
<wire x1="1.6" y1="0.7" x2="-1.6" y2="0.7" width="0.1" layer="39"/>
<wire x1="-1.6" y1="0.7" x2="-1.6" y2="-0.7" width="0.1" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="1.6" y2="-0.7" width="0.1" layer="39"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.2" layer="21"/>
<wire x1="0.1" y1="0.4" x2="-0.3" y2="0" width="0.127" layer="51"/>
<wire x1="-0.3" y1="0" x2="0.1" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.1" y1="-0.4" x2="0.1" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.3" y1="0.4" x2="-0.3" y2="-0.4" width="0.127" layer="51"/>
</package>
<package name="2-1761603-5" urn="urn:adsk.eagle:footprint:9371632/3" library_version="6" library_locally_modified="yes">
<circle x="0" y="-3.75" radius="0.125" width="0.25" layer="21"/>
<wire x1="-0.64615" y1="-3.2" x2="0.64615" y2="-3.2" width="0.2" layer="21"/>
<wire x1="0" y1="-1.8" x2="0.64615" y2="-3.2" width="0.2" layer="21"/>
<wire x1="-0.64615" y1="-3.2" x2="0" y2="-1.8" width="0.2" layer="21"/>
<wire x1="-3.2" y1="-3.2" x2="-3.2" y2="5.7" width="0.2" layer="21"/>
<wire x1="-3.2" y1="5.7" x2="17.33" y2="5.7" width="0.2" layer="21"/>
<wire x1="17.33" y1="-3.2" x2="17.33" y2="5.7" width="0.2" layer="21"/>
<wire x1="-3.2" y1="-3.2" x2="17.33" y2="-3.2" width="0.2" layer="21"/>
<wire x1="-0.64615" y1="-3.2" x2="0.64615" y2="-3.2" width="0.1" layer="51"/>
<wire x1="0" y1="-1.8" x2="0.64615" y2="-3.2" width="0.1" layer="51"/>
<wire x1="-0.64615" y1="-3.2" x2="0" y2="-1.8" width="0.1" layer="51"/>
<wire x1="-3.2" y1="-3.2" x2="-3.2" y2="5.7" width="0.1" layer="51"/>
<wire x1="-3.2" y1="5.7" x2="17.52" y2="5.7" width="0.1" layer="51"/>
<wire x1="17.52" y1="-3.2" x2="17.52" y2="5.7" width="0.1" layer="51"/>
<wire x1="-3.2" y1="-3.2" x2="17.52" y2="-3.2" width="0.1" layer="51"/>
<wire x1="-3.7" y1="-3.7" x2="-3.7" y2="6.2" width="0.05" layer="51"/>
<wire x1="-3.7" y1="-3.7" x2="17.52" y2="-3.7" width="0.05" layer="51"/>
<wire x1="17.52" y1="-3.7" x2="17.52" y2="6.2" width="0.05" layer="51"/>
<wire x1="-3.7" y1="6.2" x2="17.52" y2="6.2" width="0.05" layer="51"/>
<pad name="10" x="10.16" y="2.54" drill="1.05" diameter="1.75"/>
<pad name="9" x="10.16" y="0" drill="1.05" diameter="1.75"/>
<pad name="8" x="7.62" y="2.54" drill="1.05" diameter="1.75"/>
<pad name="7" x="7.62" y="0" drill="1.05" diameter="1.75"/>
<pad name="6" x="5.08" y="2.54" drill="1.05" diameter="1.75"/>
<pad name="5" x="5.08" y="0" drill="1.05" diameter="1.75"/>
<pad name="4" x="2.54" y="2.54" drill="1.05" diameter="1.75"/>
<pad name="3" x="2.54" y="0" drill="1.05" diameter="1.75"/>
<pad name="2" x="0" y="2.54" drill="1.05" diameter="1.75"/>
<pad name="1" x="0" y="0" drill="1.05" diameter="1.75" shape="square"/>
<pad name="14" x="15.24" y="2.54" drill="1.05" diameter="1.75"/>
<pad name="13" x="15.24" y="0" drill="1.05" diameter="1.75"/>
<pad name="12" x="12.7" y="2.54" drill="1.05" diameter="1.75"/>
<pad name="11" x="12.7" y="0" drill="1.05" diameter="1.75"/>
<text x="1.27" y="6.35" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="0" y="-5.334" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<dimension x1="0" y1="0" x2="15.24" y2="0" x3="7.62" y3="-5.08" textsize="1.27" layer="21"/>
</package>
<package name="VQFN-24" urn="urn:adsk.eagle:footprint:9526560/3" library_version="12" library_locally_modified="yes">
<description>QFN, 24-Leads, Body 4x4mm, Pitch 0.5mm, Thermal Pad 2.1x2.1mm, TI Recommended</description>
<text x="-2.9031" y="-2.994159375" size="1.27575" layer="27" align="top-left">&gt;VALUE</text>
<text x="-2.906359375" y="2.99773125" size="1.27718125" layer="25">&gt;NAME</text>
<circle x="-2.785" y="1.22" radius="0.1" width="0.2" layer="21"/>
<circle x="-2.785" y="1.22" radius="0.1" width="0.2" layer="51"/>
<wire x1="2" y1="-2" x2="-2" y2="-2" width="0.127" layer="51"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.127" layer="51"/>
<wire x1="2" y1="-2" x2="2" y2="2" width="0.127" layer="51"/>
<wire x1="-2" y1="-2" x2="-2" y2="2" width="0.127" layer="51"/>
<wire x1="2" y1="-2" x2="1.65" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="1.65" y2="2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="-1.65" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="2" x2="-1.65" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="2" y2="-1.65" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="2" y2="1.65" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="-2" y2="-1.65" width="0.127" layer="21"/>
<wire x1="-2" y1="2" x2="-2" y2="1.65" width="0.127" layer="21"/>
<wire x1="-2.615" y1="-2.615" x2="2.615" y2="-2.615" width="0.05" layer="39"/>
<wire x1="-2.615" y1="2.615" x2="2.615" y2="2.615" width="0.05" layer="39"/>
<wire x1="-2.615" y1="-2.615" x2="-2.615" y2="2.615" width="0.05" layer="39"/>
<wire x1="2.615" y1="-2.615" x2="2.615" y2="2.615" width="0.05" layer="39"/>
<wire x1="-1.07" y1="0.75" x2="-1.09" y2="0.75" width="0.254" layer="41" style="dashdot"/>
<smd name="7" x="-1.25" y="-1.935" dx="0.88" dy="0.26" layer="1" roundness="75" rot="R90"/>
<smd name="8" x="-0.75" y="-1.935" dx="0.88" dy="0.26" layer="1" roundness="75" rot="R90"/>
<smd name="9" x="-0.25" y="-1.935" dx="0.88" dy="0.26" layer="1" roundness="75" rot="R90"/>
<smd name="10" x="0.25" y="-1.935" dx="0.88" dy="0.26" layer="1" roundness="75" rot="R90"/>
<smd name="11" x="0.75" y="-1.935" dx="0.88" dy="0.26" layer="1" roundness="75" rot="R90"/>
<smd name="12" x="1.25" y="-1.935" dx="0.88" dy="0.26" layer="1" roundness="75" rot="R90"/>
<smd name="19" x="1.25" y="1.935" dx="0.88" dy="0.26" layer="1" roundness="75" rot="R90"/>
<smd name="20" x="0.75" y="1.935" dx="0.88" dy="0.26" layer="1" roundness="75" rot="R90"/>
<smd name="21" x="0.25" y="1.935" dx="0.88" dy="0.26" layer="1" roundness="75" rot="R90"/>
<smd name="22" x="-0.25" y="1.935" dx="0.88" dy="0.26" layer="1" roundness="75" rot="R90"/>
<smd name="23" x="-0.75" y="1.935" dx="0.88" dy="0.26" layer="1" roundness="75" rot="R90"/>
<smd name="24" x="-1.25" y="1.935" dx="0.88" dy="0.26" layer="1" roundness="75" rot="R90"/>
<smd name="1" x="-1.935" y="1.25" dx="0.88" dy="0.26" layer="1"/>
<smd name="2" x="-1.935" y="0.75" dx="0.88" dy="0.26" layer="1" roundness="75"/>
<smd name="3" x="-1.935" y="0.25" dx="0.88" dy="0.26" layer="1" roundness="75"/>
<smd name="4" x="-1.935" y="-0.25" dx="0.88" dy="0.26" layer="1" roundness="75"/>
<smd name="5" x="-1.935" y="-0.75" dx="0.88" dy="0.26" layer="1" roundness="75"/>
<smd name="6" x="-1.935" y="-1.25" dx="0.88" dy="0.26" layer="1" roundness="75"/>
<smd name="13" x="1.935" y="-1.25" dx="0.88" dy="0.26" layer="1" roundness="75"/>
<smd name="14" x="1.935" y="-0.75" dx="0.88" dy="0.26" layer="1" roundness="75"/>
<smd name="15" x="1.935" y="-0.25" dx="0.88" dy="0.26" layer="1" roundness="75"/>
<smd name="16" x="1.935" y="0.25" dx="0.88" dy="0.26" layer="1" roundness="75"/>
<smd name="17" x="1.935" y="0.75" dx="0.88" dy="0.26" layer="1" roundness="75"/>
<smd name="18" x="1.935" y="1.25" dx="0.88" dy="0.26" layer="1" roundness="75"/>
<smd name="TP1" x="-0.65" y="0.65" dx="0.85" dy="0.85" layer="1"/>
<smd name="TP2" x="0.65" y="0.65" dx="0.85" dy="0.85" layer="1"/>
<smd name="TP4" x="0.65" y="-0.65" dx="0.85" dy="0.85" layer="1"/>
<smd name="TP3" x="-0.65" y="-0.65" dx="0.85" dy="0.85" layer="1"/>
<rectangle x1="-1.05" y1="-1.05" x2="1.05" y2="1.05" layer="48"/>
<polygon width="0.05" layer="1">
<vertex x="-0.25" y="0.25"/>
<vertex x="-1.05" y="0.25"/>
<vertex x="-1.05" y="0.74"/>
<vertex x="-0.74" y="1.05"/>
<vertex x="-0.25" y="1.05"/>
</polygon>
</package>
<package name="SOD-523" urn="urn:adsk.eagle:footprint:9209764/1" library_version="2" library_locally_modified="yes">
<description>SOD-523 (Small Outline Diode)</description>
<smd name="C" x="0.85" y="0" dx="0.6" dy="0.8" layer="1"/>
<smd name="A" x="-0.85" y="0" dx="0.6" dy="0.8" layer="1"/>
<wire x1="-0.625" y1="-0.425" x2="0.625" y2="-0.425" width="0.127" layer="21"/>
<wire x1="0.625" y1="0.425" x2="-0.625" y2="0.425" width="0.127" layer="21"/>
<wire x1="-0.6" y1="-0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="-0.4" x2="0.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="-0.4" x2="0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.6" y1="-0.1" x2="0.6" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.1" x2="0.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.4" x2="0.3" y2="0.4" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="-0.6" y2="0.4" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.4" x2="-0.6" y2="0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.1" x2="-0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.1" x2="-0.6" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0.6" y1="0.1" x2="0.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="0.8" y1="0.1" x2="0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.8" y1="-0.1" x2="0.6" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="-0.1" x2="-0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="-0.6" y1="0.1" x2="-0.8" y2="0.1" width="0.127" layer="51"/>
<wire x1="-0.8" y1="0.1" x2="-0.8" y2="-0.1" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.4" x2="0.3" y2="-0.4" width="0.127" layer="51"/>
<wire x1="1.3716" y1="0.3048" x2="1.3716" y2="-0.3048" width="0.2032" layer="21"/>
</package>
<package name="VQFN-16" urn="urn:adsk.eagle:footprint:9209758/5" library_version="17">
<description>RF430CL330HIRGTR</description>
<wire x1="-1.5748" y1="-1.5748" x2="1.5748" y2="-1.5748" width="0.1524" layer="51"/>
<wire x1="-1.5748" y1="1.5748" x2="1.5748" y2="1.5748" width="0.1524" layer="51"/>
<wire x1="1.5748" y1="-1.5748" x2="1.5748" y2="1.5748" width="0.1524" layer="51"/>
<wire x1="-1.5748" y1="-1.5748" x2="-1.5748" y2="1.5748" width="0.1524" layer="51"/>
<wire x1="-2.0574" y1="1.27" x2="-2.4384" y2="1.27" width="0.1016" layer="51" curve="-180"/>
<wire x1="-2.4384" y1="1.27" x2="-2.0574" y2="1.27" width="0.1016" layer="51" curve="-180"/>
<wire x1="-1.7272" y1="-1.7272" x2="-1.524" y2="-1.7272" width="0.2032" layer="21"/>
<wire x1="-1.7272" y1="-1.7272" x2="-1.7272" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="1.524" y1="-1.7272" x2="1.7272" y2="-1.7272" width="0.2032" layer="21"/>
<wire x1="1.7272" y1="-1.7272" x2="1.7272" y2="-1.524" width="0.2032" layer="21"/>
<wire x1="1.7272" y1="1.524" x2="1.7272" y2="1.7272" width="0.2032" layer="21"/>
<wire x1="1.524" y1="1.7272" x2="1.7272" y2="1.7272" width="0.2032" layer="21"/>
<wire x1="-1.7272" y1="1.7272" x2="-1.0922" y2="1.7272" width="0.2032" layer="21"/>
<wire x1="-1.7272" y1="1.0922" x2="-1.7272" y2="1.7272" width="0.2032" layer="21"/>
<smd name="1" x="-1.6" y="0.75" dx="0.8" dy="0.26" layer="1" rot="R180"/>
<smd name="2" x="-1.6" y="0.250003125" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="3" x="-1.6" y="-0.25" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="4" x="-1.6" y="-0.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="5" x="-0.75" y="-1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="6" x="-0.25" y="-1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="7" x="0.250003125" y="-1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="8" x="0.75" y="-1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="9" x="1.6" y="-0.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="10" x="1.6" y="-0.25" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="11" x="1.6" y="0.250003125" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="12" x="1.6" y="0.75" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R180"/>
<smd name="13" x="0.75" y="1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="14" x="0.250003125" y="1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="15" x="-0.25" y="1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="16" x="-0.75" y="1.6" dx="0.8" dy="0.26" layer="1" roundness="100" rot="R90"/>
<smd name="EP" x="0.000003125" y="0" dx="1.7" dy="1.7" layer="1" cream="no"/>
<text x="-3.81" y="-3.81" size="1.778" layer="27">&gt;Value</text>
<text x="-3.81" y="2.54" size="1.778" layer="25">&gt;Name</text>
<polygon width="0.0254" layer="31">
<vertex x="-0.775" y="-0.775"/>
<vertex x="0.775" y="-0.775"/>
<vertex x="0.775" y="0.775"/>
<vertex x="-0.775" y="0.775"/>
</polygon>
</package>
<package name="NFC_ANT" urn="urn:adsk.eagle:footprint:9209763/1" library_version="9" library_locally_modified="yes">
<wire x1="8" y1="-2.6" x2="16" y2="-2.6" width="0.3" layer="16"/>
<wire x1="19.5" y1="0" x2="26.3" y2="0" width="0.3" layer="16"/>
<wire x1="29.8" y1="-2.6" x2="35.15" y2="-2.6" width="0.3" layer="16"/>
<wire x1="8" y1="0" x2="16" y2="0" width="0.3" layer="1"/>
<wire x1="19.4" y1="-2.6" x2="26.8" y2="-2.6" width="0.3" layer="1"/>
<wire x1="30.1" y1="0" x2="30.7" y2="0" width="0.3" layer="1"/>
<wire x1="5.3" y1="-2.3" x2="7.3" y2="-0.3" width="0.3" layer="1"/>
<wire x1="16.7" y1="-0.3" x2="18.7" y2="-2.3" width="0.3" layer="1"/>
<wire x1="27.5" y1="-2.3" x2="29.4" y2="-0.3" width="0.3" layer="1"/>
<wire x1="16.7" y1="-2.3" x2="18.7" y2="-0.3" width="0.3" layer="16"/>
<wire x1="27" y1="-0.3" x2="29" y2="-2.3" width="0.3" layer="16"/>
<wire x1="5.3" y1="-0.3" x2="7.3" y2="-2.3" width="0.3" layer="16"/>
<wire x1="7.3" y1="-0.3" x2="8" y2="0" width="0.3" layer="1" curve="-43.602819"/>
<wire x1="16" y1="0" x2="16.7" y2="-0.3" width="0.3" layer="1" curve="-46.397226"/>
<wire x1="18.7" y1="-2.3" x2="19.4" y2="-2.6" width="0.3" layer="1" curve="43.602819"/>
<wire x1="26.8" y1="-2.6" x2="27.5" y2="-2.3" width="0.3" layer="1" curve="46.397226"/>
<wire x1="29.4" y1="-0.3" x2="30.1" y2="0" width="0.3" layer="1" curve="-46.540546"/>
<wire x1="7.3" y1="-2.3" x2="8" y2="-2.6" width="0.3" layer="16" curve="43.602819"/>
<wire x1="16" y1="-2.6" x2="16.7" y2="-2.3" width="0.3" layer="16" curve="46.397226"/>
<wire x1="18.7" y1="-0.3" x2="19.5" y2="0" width="0.3" layer="16" curve="-48.88791"/>
<wire x1="26.3" y1="0" x2="27" y2="-0.3" width="0.3" layer="16" curve="-46.397226"/>
<wire x1="29" y1="-2.3" x2="29.8" y2="-2.6" width="0.3" layer="16" curve="48.88791"/>
<wire x1="31.15" y1="0.5" x2="31.15" y2="7.6" width="0.3" layer="1"/>
<wire x1="32.15" y1="8.65" x2="54.85" y2="8.65" width="0.3" layer="1"/>
<wire x1="55.85" y1="-10.05" x2="55.85" y2="7.65" width="0.3" layer="1"/>
<wire x1="32.15" y1="-11.05" x2="54.85" y2="-11.05" width="0.3" layer="1"/>
<wire x1="31.15" y1="-10.05" x2="31.15" y2="-2.66" width="0.3" layer="1"/>
<wire x1="32.15" y1="0.25" x2="32.15" y2="6.65" width="0.3" layer="1"/>
<wire x1="33.15" y1="7.65" x2="53.85" y2="7.65" width="0.3" layer="1"/>
<wire x1="54.85" y1="-9.05" x2="54.85" y2="6.65" width="0.3" layer="1"/>
<wire x1="33.15" y1="-10.05" x2="53.85" y2="-10.05" width="0.3" layer="1"/>
<wire x1="32.15" y1="-9.05" x2="32.15" y2="-2.66" width="0.3" layer="1"/>
<wire x1="33.15" y1="0.25" x2="33.15" y2="5.65" width="0.3" layer="1"/>
<wire x1="34.15" y1="6.65" x2="52.85" y2="6.65" width="0.3" layer="1"/>
<wire x1="53.85" y1="-8.05" x2="53.85" y2="5.65" width="0.3" layer="1"/>
<wire x1="34.15" y1="-9.05" x2="52.85" y2="-9.05" width="0.3" layer="1"/>
<wire x1="33.15" y1="-8.05" x2="33.15" y2="-2.66" width="0.3" layer="1"/>
<wire x1="34.15" y1="0.25" x2="34.15" y2="4.65" width="0.3" layer="1"/>
<wire x1="35.15" y1="5.65" x2="51.85" y2="5.65" width="0.3" layer="1"/>
<wire x1="52.85" y1="-7.05" x2="52.85" y2="4.65" width="0.3" layer="1"/>
<wire x1="35.15" y1="-8.05" x2="51.85" y2="-8.05" width="0.3" layer="1"/>
<wire x1="34.15" y1="-7.05" x2="34.15" y2="-2.66" width="0.3" layer="1"/>
<wire x1="35.15" y1="0.25" x2="35.15" y2="3.65" width="0.3" layer="1"/>
<wire x1="36.15" y1="4.65" x2="50.85" y2="4.65" width="0.3" layer="1"/>
<wire x1="51.85" y1="-6.05" x2="51.85" y2="3.65" width="0.3" layer="1"/>
<wire x1="36.15" y1="-7.05" x2="50.85" y2="-7.05" width="0.3" layer="1"/>
<wire x1="35.15" y1="-6.05" x2="35.15" y2="-2.6" width="0.3" layer="1"/>
<wire x1="31.2" y1="-2.3" x2="32.1" y2="-0.1" width="0.3" layer="1"/>
<wire x1="32.2" y1="-2.3" x2="33.1" y2="-0.1" width="0.3" layer="1"/>
<wire x1="33.2" y1="-2.3" x2="34.1" y2="-0.1" width="0.3" layer="1"/>
<wire x1="34.2" y1="-2.3" x2="35.1" y2="-0.1" width="0.3" layer="1"/>
<wire x1="30.7" y1="0" x2="31.15" y2="0.5" width="0.3" layer="1" curve="96.025575"/>
<wire x1="32.1" y1="-0.1" x2="32.15" y2="0.2" width="0.3" layer="1" curve="25.573403"/>
<wire x1="33.1" y1="-0.1" x2="33.15" y2="0.2" width="0.3" layer="1" curve="25.573403"/>
<wire x1="34.1" y1="-0.1" x2="34.15" y2="0.25" width="0.3" layer="1" curve="27.342637"/>
<wire x1="35.1" y1="-0.1" x2="35.15" y2="0.25" width="0.3" layer="1" curve="28.237858"/>
<wire x1="31.2" y1="-2.3" x2="31.15" y2="-2.65" width="0.3" layer="1" curve="28.237858"/>
<wire x1="32.2" y1="-2.3" x2="32.15" y2="-2.65" width="0.3" layer="1" curve="28.237858"/>
<wire x1="33.2" y1="-2.3" x2="33.15" y2="-2.65" width="0.3" layer="1" curve="28.237858"/>
<wire x1="34.2" y1="-2.3" x2="34.15" y2="-2.65" width="0.3" layer="1" curve="28.237858"/>
<wire x1="31.15" y1="7.6" x2="32.15" y2="8.65" width="0.3" layer="1" curve="-87.205638"/>
<wire x1="32.15" y1="6.65" x2="33.15" y2="7.65" width="0.3" layer="1" curve="-90"/>
<wire x1="33.15" y1="5.65" x2="34.15" y2="6.65" width="0.3" layer="1" curve="-90"/>
<wire x1="34.15" y1="4.65" x2="35.15" y2="5.65" width="0.3" layer="1" curve="-90"/>
<wire x1="35.15" y1="3.65" x2="36.15" y2="4.65" width="0.3" layer="1" curve="-90"/>
<wire x1="54.85" y1="8.65" x2="55.85" y2="7.65" width="0.3" layer="1" curve="-90"/>
<wire x1="53.85" y1="7.65" x2="54.85" y2="6.65" width="0.3" layer="1" curve="-90"/>
<wire x1="52.85" y1="6.65" x2="53.85" y2="5.65" width="0.3" layer="1" curve="-90"/>
<wire x1="51.85" y1="5.65" x2="52.85" y2="4.65" width="0.3" layer="1" curve="-90"/>
<wire x1="50.85" y1="4.65" x2="51.85" y2="3.65" width="0.3" layer="1" curve="-90"/>
<wire x1="51.85" y1="-6.05" x2="50.85" y2="-7.05" width="0.3" layer="1" curve="-90"/>
<wire x1="52.85" y1="-7.05" x2="51.85" y2="-8.05" width="0.3" layer="1" curve="-90"/>
<wire x1="53.85" y1="-8.05" x2="52.85" y2="-9.05" width="0.3" layer="1" curve="-90"/>
<wire x1="54.85" y1="-9.05" x2="53.85" y2="-10.05" width="0.3" layer="1" curve="-90"/>
<wire x1="55.85" y1="-10.05" x2="54.85" y2="-11.05" width="0.3" layer="1" curve="-90"/>
<wire x1="32.15" y1="-11.05" x2="31.15" y2="-10.05" width="0.3" layer="1" curve="-90"/>
<wire x1="33.15" y1="-10.05" x2="32.15" y2="-9.05" width="0.3" layer="1" curve="-90"/>
<wire x1="34.15" y1="-9.05" x2="33.15" y2="-8.05" width="0.3" layer="1" curve="-90"/>
<wire x1="35.15" y1="-8.05" x2="34.15" y2="-7.05" width="0.3" layer="1" curve="-90"/>
<wire x1="36.15" y1="-7.05" x2="35.15" y2="-6.05" width="0.3" layer="1" curve="-90"/>
<wire x1="0" y1="-2.6" x2="4.4" y2="-2.6" width="0.3" layer="1"/>
<wire x1="3" y1="0" x2="4.4" y2="0" width="0.3" layer="16"/>
<wire x1="4.4" y1="0" x2="5.3" y2="-0.3" width="0.3" layer="16" curve="-36.869898"/>
<wire x1="4.4" y1="-2.6" x2="5.3" y2="-2.3" width="0.3" layer="1" curve="36.869898"/>
<wire x1="0" y1="0" x2="3" y2="0" width="0.3" layer="1"/>
<pad name="P$1" x="3" y="0" drill="0.3" diameter="0.6"/>
<pad name="P$2" x="35.15" y="-2.6" drill="0.3" diameter="0.6"/>
<smd name="1" x="0" y="0" dx="0.3" dy="0.3" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
<smd name="2" x="0" y="-2.6" dx="0.3" dy="0.3" layer="1" roundness="100" stop="no" thermals="no" cream="no"/>
</package>
<package name="TP_PAD" urn="urn:adsk.eagle:footprint:9209760/1" library_version="2" library_locally_modified="yes">
<text x="-0.635" y="1.016" size="0.3048" layer="25">&gt;NAME</text>
<text x="-0.762" y="-1.397" size="0.3048" layer="27">&gt;VALUE</text>
<smd name="P$1" x="0" y="0" dx="1.27" dy="1.27" layer="1" roundness="70" rot="R90"/>
</package>
<package name="TP_PAD_SOLDER" urn="urn:adsk.eagle:footprint:9209761/1" library_version="2" library_locally_modified="yes">
<text x="-0.635" y="1.016" size="0.3048" layer="25">&gt;NAME</text>
<text x="-0.762" y="-1.397" size="0.3048" layer="27">&gt;VALUE</text>
<smd name="P$1" x="0" y="0" dx="2" dy="2" layer="1" roundness="70" rot="R90"/>
</package>
<package name="SOT25" urn="urn:adsk.eagle:footprint:793903/2" library_version="9" library_locally_modified="yes">
<smd name="1" x="-0.95" y="-1.3032" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3032" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3032" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3032" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.9754" y="1.3032" dx="0.55" dy="1.2" layer="1"/>
<text x="-1.5" y="2" size="1.27" layer="25">&gt;Name</text>
<text x="-1.5" y="-3.5" size="1.27" layer="27">&gt;Value</text>
<circle x="-1.016" y="-0.381" radius="0.127" width="0.1016" layer="25"/>
<wire x1="1.422" y1="0.785" x2="1.422" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.785" x2="-1.422" y2="-0.785" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.785" x2="-1.422" y2="0.785" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.785" x2="1.422" y2="0.785" width="0.1524" layer="51"/>
<wire x1="-0.522" y1="0.785" x2="0.522" y2="0.785" width="0.1524" layer="21"/>
<wire x1="-0.428" y1="-0.785" x2="-0.522" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="0.522" y1="-0.785" x2="0.428" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="-1.328" y1="-0.785" x2="-1.422" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.785" x2="1.328" y2="-0.785" width="0.1524" layer="21"/>
<wire x1="1.328" y1="0.785" x2="1.422" y2="0.785" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.785" x2="-1.328" y2="0.785" width="0.1524" layer="21"/>
<rectangle x1="-1.2" y1="-1.373" x2="-0.7" y2="-0.723" layer="51"/>
<rectangle x1="0.6898" y1="-1.3476" x2="1.1898" y2="-0.6976" layer="51"/>
<rectangle x1="-0.2398" y1="-1.3984" x2="0.2602" y2="-0.7484" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
</package>
<package name="PB-FREE" urn="urn:adsk.eagle:footprint:9339141/1" library_version="9" library_locally_modified="yes">
<circle x="1.27" y="1.27" radius="1.27" width="0.254" layer="22"/>
<text x="2.0828" y="0.8128" size="1.016" layer="22" ratio="15" rot="MR0">Pb</text>
<polygon width="0.127" layer="22">
<vertex x="0.381" y="0.381"/>
<vertex x="1.27" y="1.27"/>
<vertex x="2.159" y="2.159"/>
</polygon>
</package>
<package name="BATTERY" urn="urn:adsk.eagle:footprint:9535779/2" library_version="13" library_locally_modified="yes">
<text x="-0.635" y="1.016" size="0.3048" layer="25">&gt;NAME</text>
<text x="-0.762" y="-1.397" size="0.3048" layer="27">&gt;VALUE</text>
<text x="3.175" y="1.016" size="0.3048" layer="25">&gt;NAME</text>
<smd name="-" x="0" y="0" dx="3" dy="1.4" layer="1" roundness="70" rot="R90"/>
<smd name="+" x="2.54" y="0" dx="3" dy="1.4" layer="1" roundness="70" rot="R90"/>
</package>
<package name="SWITCH" urn="urn:adsk.eagle:footprint:9557606/2" library_version="13" library_locally_modified="yes">
<text x="-3.81" y="2.921" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<smd name="1" x="2" y="1.275" dx="2.55" dy="1" layer="1" rot="R270"/>
<smd name="2" x="0" y="1.275" dx="2.55" dy="1" layer="1" rot="R270"/>
<smd name="3" x="-2" y="1.275" dx="2.55" dy="1" layer="1" rot="R270"/>
<smd name="TP1" x="-4.9" y="-2.1" dx="1.7" dy="1.7" layer="1" rot="R90"/>
<smd name="TP2" x="4.9" y="-2.1" dx="1.7" dy="1.7" layer="1" rot="R90"/>
<wire x1="-4.6" y1="1.5" x2="4.6" y2="1.5" width="0.1524" layer="21"/>
<wire x1="4.6" y1="1.5" x2="4.6" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="4.6" y1="-3.6" x2="-4.6" y2="-3.6" width="0.1524" layer="21"/>
<wire x1="-4.6" y1="-3.6" x2="-4.6" y2="1.5" width="0.1524" layer="21"/>
</package>
<package name="WSON-6" urn="urn:adsk.eagle:footprint:9496070/3" library_version="12" library_locally_modified="yes">
<smd name="1" x="-0.975" y="0.65" dx="0.3" dy="0.45" layer="1" rot="R90"/>
<smd name="2" x="-0.975" y="0" dx="0.3" dy="0.45" layer="1" rot="R90"/>
<smd name="3" x="-0.975" y="-0.65" dx="0.3" dy="0.45" layer="1" rot="R90"/>
<smd name="4" x="0.975" y="-0.65" dx="0.3" dy="0.45" layer="1" rot="R90"/>
<smd name="5" x="0.975" y="0" dx="0.3" dy="0.45" layer="1" rot="R90"/>
<smd name="6" x="0.975" y="0.65" dx="0.3" dy="0.45" layer="1" rot="R90"/>
<smd name="7" x="0" y="0" dx="1" dy="1.6" layer="1"/>
<wire x1="-1.0414" y1="1.1938" x2="-0.2032" y2="1.1938" width="0.2032" layer="21"/>
<wire x1="0.7874" y1="1.1938" x2="1.0414" y2="1.1938" width="0.2032" layer="21"/>
<wire x1="0.7874" y1="-1.1938" x2="1.0414" y2="-1.1938" width="0.2032" layer="21"/>
<wire x1="-1.0414" y1="-1.1938" x2="-0.7874" y2="-1.1938" width="0.2032" layer="21"/>
<wire x1="-1.0414" y1="-1.0414" x2="-1.0414" y2="1.0414" width="0.1524" layer="21"/>
<wire x1="1.0414" y1="-1.0414" x2="1.0414" y2="1.0414" width="0.1524" layer="21"/>
<wire x1="-1.0414" y1="1.0414" x2="1.0414" y2="1.0414" width="0.1524" layer="21"/>
<wire x1="-1.0414" y1="-1.0414" x2="1.0414" y2="-1.0414" width="0.1524" layer="21"/>
<wire x1="-0.4318" y1="0.6096" x2="-0.8128" y2="0.6096" width="0.1016" layer="21" curve="-180"/>
<wire x1="-0.8128" y1="0.6096" x2="-0.4318" y2="0.6096" width="0.1016" layer="21" curve="-180"/>
<text x="-1.0668" y="1.7018" size="1.27" layer="25" ratio="6" rot="SR0">&gt;NAME</text>
<polygon width="0.0254" layer="31">
<vertex x="-0.5" y="0.149"/>
<vertex x="-0.5" y="0.751"/>
<vertex x="-0.499059375" y="0.760559375"/>
<vertex x="-0.49626875" y="0.769753125"/>
<vertex x="-0.491740625" y="0.778221875"/>
<vertex x="-0.485646875" y="0.785646875"/>
<vertex x="-0.478225" y="0.791740625"/>
<vertex x="-0.469753125" y="0.79626875"/>
<vertex x="-0.460559375" y="0.799059375"/>
<vertex x="-0.451" y="0.799996875"/>
<vertex x="0.451" y="0.799996875"/>
<vertex x="0.460559375" y="0.799059375"/>
<vertex x="0.469753125" y="0.79626875"/>
<vertex x="0.478225" y="0.791740625"/>
<vertex x="0.485646875" y="0.785646875"/>
<vertex x="0.491740625" y="0.778221875"/>
<vertex x="0.49626875" y="0.769753125"/>
<vertex x="0.499059375" y="0.760559375"/>
<vertex x="0.5" y="0.751"/>
<vertex x="0.5" y="0.149"/>
<vertex x="0.499059375" y="0.1394375"/>
<vertex x="0.49626875" y="0.130246875"/>
<vertex x="0.491740625" y="0.121775"/>
<vertex x="0.485646875" y="0.11435"/>
<vertex x="0.478225" y="0.10825625"/>
<vertex x="0.469753125" y="0.103728125"/>
<vertex x="0.460559375" y="0.100940625"/>
<vertex x="0.451" y="0.099996875"/>
<vertex x="-0.451" y="0.099996875"/>
<vertex x="-0.460559375" y="0.100940625"/>
<vertex x="-0.469753125" y="0.103728125"/>
<vertex x="-0.478225" y="0.10825625"/>
<vertex x="-0.485646875" y="0.11435"/>
<vertex x="-0.491740625" y="0.121775"/>
<vertex x="-0.49626875" y="0.130246875"/>
<vertex x="-0.499059375" y="0.1394375"/>
</polygon>
<polygon width="0.0254" layer="31">
<vertex x="-0.5" y="-0.751"/>
<vertex x="-0.5" y="-0.149"/>
<vertex x="-0.499059375" y="-0.1394375"/>
<vertex x="-0.49626875" y="-0.130246875"/>
<vertex x="-0.491740625" y="-0.121775"/>
<vertex x="-0.485646875" y="-0.11435"/>
<vertex x="-0.478225" y="-0.10825625"/>
<vertex x="-0.469753125" y="-0.103728125"/>
<vertex x="-0.460559375" y="-0.100940625"/>
<vertex x="-0.451" y="-0.099996875"/>
<vertex x="0.451" y="-0.099996875"/>
<vertex x="0.460559375" y="-0.100940625"/>
<vertex x="0.469753125" y="-0.103728125"/>
<vertex x="0.478225" y="-0.10825625"/>
<vertex x="0.485646875" y="-0.11435"/>
<vertex x="0.491740625" y="-0.121775"/>
<vertex x="0.49626875" y="-0.130246875"/>
<vertex x="0.499059375" y="-0.1394375"/>
<vertex x="0.5" y="-0.149"/>
<vertex x="0.5" y="-0.751"/>
<vertex x="0.499059375" y="-0.760559375"/>
<vertex x="0.49626875" y="-0.769753125"/>
<vertex x="0.491740625" y="-0.778221875"/>
<vertex x="0.485646875" y="-0.785646875"/>
<vertex x="0.478225" y="-0.791740625"/>
<vertex x="0.469753125" y="-0.79626875"/>
<vertex x="0.460559375" y="-0.799059375"/>
<vertex x="0.451" y="-0.799996875"/>
<vertex x="-0.451" y="-0.799996875"/>
<vertex x="-0.460559375" y="-0.799059375"/>
<vertex x="-0.469753125" y="-0.79626875"/>
<vertex x="-0.478225" y="-0.791740625"/>
<vertex x="-0.485646875" y="-0.785646875"/>
<vertex x="-0.491740625" y="-0.778221875"/>
<vertex x="-0.49626875" y="-0.769753125"/>
<vertex x="-0.499059375" y="-0.760559375"/>
</polygon>
</package>
<package name="LAUNCHPAD_TEMPLATE_LIBRARY_3PIN_PWR_5V" urn="urn:adsk.eagle:footprint:9557605/2" library_version="17">
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="4.445" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="5.715" x2="-0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="0.635" y1="6.35" x2="1.27" y2="5.715" width="0.2032" layer="21"/>
<wire x1="1.27" y1="5.715" x2="1.27" y2="4.445" width="0.2032" layer="21"/>
<wire x1="1.27" y1="4.445" x2="0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="1.905" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.905" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="6.35" x2="0.635" y2="6.35" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="4.445" width="0.2032" layer="22"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="5.715" width="0.2032" layer="22"/>
<wire x1="-1.27" y1="5.715" x2="-0.635" y2="6.35" width="0.2032" layer="22"/>
<wire x1="0.635" y1="6.35" x2="1.27" y2="5.715" width="0.2032" layer="22"/>
<wire x1="1.27" y1="5.715" x2="1.27" y2="4.445" width="0.2032" layer="22"/>
<wire x1="1.27" y1="4.445" x2="0.635" y2="3.81" width="0.2032" layer="22"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.905" width="0.2032" layer="22"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="3.175" width="0.2032" layer="22"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.2032" layer="22"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.2032" layer="22"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="1.905" width="0.2032" layer="22"/>
<wire x1="1.27" y1="1.905" x2="0.635" y2="1.27" width="0.2032" layer="22"/>
<wire x1="-0.635" y1="6.35" x2="0.635" y2="6.35" width="0.2032" layer="22"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="1.27" width="0.2032" layer="22"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="22"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.2032" layer="22"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.2032" layer="22"/>
<wire x1="1.27" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="22"/>
<rectangle x1="-0.254" y1="4.826" x2="0.254" y2="5.334" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R90"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796" shape="square" rot="R180"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796" rot="R180"/>
<pad name="3" x="0" y="5.08" drill="1.016" diameter="1.8796" rot="R180"/>
<text x="-0.635" y="6.8326" size="0.7112" layer="21" font="vector" ratio="14">&gt;NAME</text>
</package>
<package name="BUTTON" urn="urn:adsk.eagle:footprint:9658927/1" library_version="13" library_locally_modified="yes">
<smd name="P1" x="-1.7" y="0" dx="1.7" dy="0.8" layer="1" rot="R90"/>
<smd name="P2" x="1.7" y="0" dx="1.7" dy="0.8" layer="1" rot="R90"/>
<wire x1="-1.5" y1="-1.25" x2="-1.5" y2="1.25" width="0.127" layer="21"/>
<wire x1="-1.5" y1="1.25" x2="1.5" y2="1.25" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.25" x2="1.5" y2="-1.25" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.25" x2="-1.5" y2="-1.25" width="0.127" layer="21"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="TP_HOLE" urn="urn:adsk.eagle:footprint:9658926/1" library_version="13" library_locally_modified="yes">
<pad name="P1" x="0" y="0" drill="1.02"/>
</package>
<package name="C0805K" urn="urn:adsk.eagle:footprint:9688573/1" library_version="14" library_locally_modified="yes">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="1" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="2X4_HEADER_SMALL" urn="urn:adsk.eagle:footprint:9688575/1" library_version="14" library_locally_modified="yes">
<pad name="P$1" x="1.27" y="1.905" drill="0.6" diameter="1.016" shape="square"/>
<pad name="P$2" x="-1.27" y="1.905" drill="0.6" diameter="1.016" shape="square"/>
<pad name="P$3" x="1.27" y="0.635" drill="0.6" diameter="1.016"/>
<pad name="P$4" x="-1.27" y="0.635" drill="0.6" diameter="1.016"/>
<pad name="P$5" x="1.27" y="-0.635" drill="0.6" diameter="1.016"/>
<pad name="P$6" x="-1.27" y="-0.635" drill="0.6" diameter="1.016"/>
<pad name="P$7" x="1.27" y="-1.905" drill="0.6" diameter="1.016"/>
<pad name="P$8" x="-1.27" y="-1.905" drill="0.6" diameter="1.016"/>
<wire x1="-2.032" y1="2.667" x2="2.032" y2="2.667" width="0.1524" layer="21"/>
<wire x1="2.032" y1="2.667" x2="2.032" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.032" y1="1.143" x2="1.778" y2="1.143" width="0.1524" layer="21"/>
<wire x1="1.778" y1="1.143" x2="2.032" y2="0.889" width="0.1524" layer="21"/>
<wire x1="2.032" y1="0.889" x2="2.032" y2="0.254" width="0.1524" layer="21"/>
<wire x1="2.032" y1="0.254" x2="1.778" y2="0" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.032" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-0.254" x2="2.032" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="1.778" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-1.27" x2="2.032" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-1.524" x2="2.032" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.032" y1="-2.286" x2="1.651" y2="-2.667" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-2.667" x2="-1.651" y2="-2.667" width="0.1524" layer="21"/>
<wire x1="-1.651" y1="-2.667" x2="-2.032" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-2.286" x2="-2.032" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-1.524" x2="-1.778" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-1.27" x2="-2.032" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="-0.254" x2="-1.778" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0" x2="-2.032" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="0.254" x2="-2.032" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="0.889" x2="-1.778" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="1.143" x2="-2.032" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.032" y1="1.143" x2="-2.032" y2="2.667" width="0.1524" layer="21"/>
</package>
<package name="GQFN-8" urn="urn:adsk.eagle:footprint:9688576/1" library_version="14" library_locally_modified="yes">
<smd name="P1" x="-1.577" y="0.975" dx="0.8" dy="0.4" layer="1" roundness="60"/>
<smd name="P2" x="-1.577" y="0.325" dx="0.8" dy="0.4" layer="1"/>
<smd name="P3" x="-1.577" y="-0.325" dx="0.8" dy="0.4" layer="1"/>
<smd name="P4" x="-1.577" y="-0.975" dx="0.8" dy="0.4" layer="1"/>
<smd name="P5" x="1.54" y="-0.975" dx="0.8" dy="0.4" layer="1"/>
<smd name="P6" x="1.5516" y="-0.325" dx="0.8" dy="0.4" layer="1"/>
<smd name="P7" x="1.5516" y="0.325" dx="0.8" dy="0.4" layer="1"/>
<smd name="P8" x="1.5516" y="0.975" dx="0.8" dy="0.4" layer="1"/>
<wire x1="-1.62" y1="1.64" x2="1.62" y2="1.64" width="0.1524" layer="21"/>
<wire x1="1.62" y1="1.64" x2="1.62" y2="-1.64" width="0.1524" layer="21"/>
<wire x1="1.62" y1="-1.64" x2="-1.62" y2="-1.64" width="0.1524" layer="21"/>
<wire x1="-1.62" y1="-1.64" x2="-1.62" y2="1.64" width="0.1524" layer="21"/>
<wire x1="-1.6002" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.6256" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<text x="-1.778" y="-2.667" size="0.8128" layer="25">&gt;NAME</text>
<text x="-1.778" y="-3.81" size="0.8128" layer="27">&gt;VALUE</text>
</package>
<package name="1X20_1.27+2X10_2.54" urn="urn:adsk.eagle:footprint:9774413/1" library_version="15">
<wire x1="-0.508" y1="4.445" x2="-0.762" y2="4.699" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="4.699" x2="-0.762" y2="5.461" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="5.461" x2="-0.508" y2="5.715" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.445" x2="-0.762" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="6.985" x2="-0.762" y2="7.239" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="5.715" x2="-0.762" y2="5.969" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="5.969" x2="-0.762" y2="6.731" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="6.731" x2="-0.508" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="8.255" x2="-0.762" y2="8.509" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="8.509" x2="-0.762" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="9.271" x2="-0.508" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="8.255" x2="-0.762" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="7.239" x2="-0.762" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="9.525" x2="-0.762" y2="9.779" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="9.779" x2="-0.762" y2="10.541" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="10.541" x2="-0.508" y2="10.795" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="3.429" x2="-0.508" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="3.175" x2="-0.762" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="3.429" x2="-0.762" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0.381" x2="-0.508" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.635" x2="-0.762" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="0.889" x2="-0.762" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="1.651" x2="-0.508" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="1.905" x2="-0.762" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="2.159" x2="-0.762" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="10.795" x2="-0.762" y2="10.795" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="10.795" x2="-0.762" y2="12.192" width="0.1524" layer="21"/>
<wire x1="0.762" y1="4.191" x2="0.762" y2="3.429" width="0.1524" layer="21"/>
<wire x1="0.762" y1="3.429" x2="0.508" y2="3.175" width="0.1524" layer="21"/>
<wire x1="0.508" y1="5.715" x2="0.762" y2="5.461" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.461" x2="0.762" y2="4.699" width="0.1524" layer="21"/>
<wire x1="0.762" y1="4.699" x2="0.508" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.762" y1="4.191" x2="0.508" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.762" y1="8.001" x2="0.762" y2="7.239" width="0.1524" layer="21"/>
<wire x1="0.762" y1="7.239" x2="0.508" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.508" y1="6.985" x2="0.762" y2="6.731" width="0.1524" layer="21"/>
<wire x1="0.762" y1="6.731" x2="0.762" y2="5.969" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.969" x2="0.508" y2="5.715" width="0.1524" layer="21"/>
<wire x1="0.508" y1="9.525" x2="0.762" y2="9.271" width="0.1524" layer="21"/>
<wire x1="0.762" y1="9.271" x2="0.762" y2="8.509" width="0.1524" layer="21"/>
<wire x1="0.762" y1="8.509" x2="0.508" y2="8.255" width="0.1524" layer="21"/>
<wire x1="0.762" y1="8.001" x2="0.508" y2="8.255" width="0.1524" layer="21"/>
<wire x1="0.508" y1="10.795" x2="0.762" y2="10.541" width="0.1524" layer="21"/>
<wire x1="0.762" y1="10.541" x2="0.762" y2="9.779" width="0.1524" layer="21"/>
<wire x1="0.762" y1="9.779" x2="0.508" y2="9.525" width="0.1524" layer="21"/>
<wire x1="0.762" y1="2.921" x2="0.762" y2="2.159" width="0.1524" layer="21"/>
<wire x1="0.762" y1="2.159" x2="0.508" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0.762" y1="2.921" x2="0.508" y2="3.175" width="0.1524" layer="21"/>
<wire x1="0.762" y1="1.651" x2="0.762" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.889" x2="0.508" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.762" y1="1.651" x2="0.508" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="0.762" y1="0.381" x2="0.508" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="12.192" x2="0.762" y2="12.192" width="0.1524" layer="21"/>
<wire x1="0.762" y1="12.192" x2="0.762" y2="10.795" width="0.1524" layer="21"/>
<wire x1="0.762" y1="10.795" x2="0.508" y2="10.795" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-7.239" x2="0.762" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-8.001" x2="0.508" y2="-8.255" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-5.715" x2="0.762" y2="-5.969" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-5.969" x2="0.762" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-6.731" x2="0.508" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-7.239" x2="0.508" y2="-6.985" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-3.429" x2="0.762" y2="-4.191" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-4.191" x2="0.508" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-4.445" x2="0.762" y2="-4.699" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-4.699" x2="0.762" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-5.461" x2="0.508" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-1.905" x2="0.762" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-2.159" x2="0.762" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-2.921" x2="0.508" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-3.429" x2="0.508" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-0.889" x2="0.762" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-1.651" x2="0.508" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-8.509" x2="0.762" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-9.271" x2="0.508" y2="-9.525" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-8.509" x2="0.508" y2="-8.255" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-9.779" x2="0.762" y2="-10.541" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-10.541" x2="0.508" y2="-10.795" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-9.779" x2="0.508" y2="-9.525" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-11.049" x2="0.762" y2="-11.811" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-11.049" x2="0.508" y2="-10.795" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-0.381" x2="0.508" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-0.635" x2="0.762" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-12.319" x2="0.762" y2="-13.081" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-12.319" x2="0.508" y2="-12.065" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-11.811" x2="0.508" y2="-12.065" width="0.1524" layer="21"/>
<wire x1="0.762" y1="-13.081" x2="0.508" y2="-13.335" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-6.985" x2="-0.762" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-6.731" x2="-0.762" y2="-5.969" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-5.969" x2="-0.508" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-6.985" x2="-0.762" y2="-7.239" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-4.445" x2="-0.762" y2="-4.191" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-5.715" x2="-0.762" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-5.461" x2="-0.762" y2="-4.699" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-4.699" x2="-0.508" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-3.175" x2="-0.762" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-2.921" x2="-0.762" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-2.159" x2="-0.508" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-3.175" x2="-0.762" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-4.191" x2="-0.762" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-1.905" x2="-0.762" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-1.651" x2="-0.762" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-8.001" x2="-0.508" y2="-8.255" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-8.255" x2="-0.762" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-8.001" x2="-0.762" y2="-7.239" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-11.811" x2="-0.762" y2="-11.049" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-11.049" x2="-0.508" y2="-10.795" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-10.795" x2="-0.762" y2="-10.541" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-10.541" x2="-0.762" y2="-9.779" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-9.779" x2="-0.508" y2="-9.525" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-9.525" x2="-0.762" y2="-9.271" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-9.271" x2="-0.762" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-0.889" x2="-0.508" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-0.635" x2="-0.762" y2="-0.381" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-13.335" x2="-0.508" y2="-13.335" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-13.335" x2="-0.762" y2="-13.081" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-13.081" x2="-0.762" y2="-12.319" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="-12.319" x2="-0.508" y2="-12.065" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-12.065" x2="-0.762" y2="-11.811" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.905" x2="-6.35" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="4.445" x2="-6.35" y2="5.08" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="6.35" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="6.35" x2="-5.715" y2="6.985" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="4.445" x2="-6.35" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="3.81" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="6.985" x2="-6.35" y2="7.62" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="7.62" x2="-6.35" y2="8.89" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="8.89" x2="-5.715" y2="9.525" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="9.525" x2="-6.35" y2="9.525" width="0.2286" layer="21"/>
<wire x1="-6.35" y1="9.525" x2="-6.35" y2="12.065" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="3.81" x2="-1.27" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.905" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="6.985" x2="-1.27" y2="6.35" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="6.35" x2="-1.27" y2="5.08" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="5.08" x2="-1.905" y2="4.445" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="3.81" x2="-1.905" y2="4.445" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="9.525" x2="-1.27" y2="8.89" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="8.89" x2="-1.27" y2="7.62" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="7.62" x2="-1.905" y2="6.985" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="12.065" x2="-1.27" y2="12.065" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="12.065" x2="-1.27" y2="9.525" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="9.525" x2="-1.905" y2="9.525" width="0.2286" layer="21"/>
<wire x1="-5.715" y1="-13.335" x2="-1.905" y2="-13.335" width="0.2286" layer="21"/>
<wire x1="-1.27" y1="-3.81" x2="-1.27" y2="-5.08" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-5.08" x2="-1.905" y2="-5.715" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-0.635" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.905" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-3.81" x2="-1.905" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.905" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-11.43" x2="-1.27" y2="-12.7" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-12.7" x2="-1.905" y2="-13.335" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-8.255" x2="-1.27" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-8.89" x2="-1.27" y2="-10.16" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-10.16" x2="-1.905" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-11.43" x2="-1.905" y2="-10.795" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-5.715" x2="-1.27" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-7.62" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-7.62" x2="-1.905" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="-5.715" x2="-6.35" y2="-5.08" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="-3.175" x2="-6.35" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="-2.54" x2="-6.35" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="-1.27" x2="-5.715" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="-3.175" x2="-6.35" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="-5.08" x2="-6.35" y2="-3.81" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="-0.635" x2="-6.35" y2="0" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="0" x2="-6.35" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="1.27" x2="-5.715" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="-13.335" x2="-6.35" y2="-12.7" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="-10.795" x2="-6.35" y2="-10.16" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="-10.16" x2="-6.35" y2="-8.89" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="-8.89" x2="-5.715" y2="-8.255" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="-10.795" x2="-6.35" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="-12.7" x2="-6.35" y2="-11.43" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="-8.255" x2="-6.35" y2="-7.62" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="-7.62" x2="-6.35" y2="-6.35" width="0.2032" layer="21"/>
<wire x1="-6.35" y1="-6.35" x2="-5.715" y2="-5.715" width="0.2032" layer="21"/>
<pad name="1" x="0" y="11.43" drill="0.6" diameter="1.016" shape="square"/>
<pad name="2" x="0" y="10.16" drill="0.6" diameter="1.016"/>
<pad name="3" x="0" y="8.89" drill="0.6" diameter="1.016"/>
<pad name="4" x="0" y="7.62" drill="0.6" diameter="1.016"/>
<pad name="5" x="0" y="6.35" drill="0.6" diameter="1.016"/>
<pad name="6" x="0" y="5.08" drill="0.6" diameter="1.016"/>
<pad name="7" x="0" y="3.81" drill="0.6" diameter="1.016"/>
<pad name="8" x="0" y="2.54" drill="0.6" diameter="1.016"/>
<pad name="9" x="0" y="1.27" drill="0.6" diameter="1.016"/>
<pad name="10" x="0" y="0" drill="0.6" diameter="1.016"/>
<pad name="11" x="0" y="-1.27" drill="0.6" diameter="1.016"/>
<pad name="12" x="0" y="-2.54" drill="0.6" diameter="1.016"/>
<pad name="13" x="0" y="-3.81" drill="0.6" diameter="1.016"/>
<pad name="14" x="0" y="-5.08" drill="0.6" diameter="1.016"/>
<pad name="15" x="0" y="-6.35" drill="0.6" diameter="1.016"/>
<pad name="16" x="0" y="-7.62" drill="0.6" diameter="1.016"/>
<pad name="17" x="0" y="-8.89" drill="0.6" diameter="1.016"/>
<pad name="18" x="0" y="-10.16" drill="0.6" diameter="1.016"/>
<pad name="19" x="0" y="-11.43" drill="0.6" diameter="1.016"/>
<pad name="20" x="0" y="-12.7" drill="0.6" diameter="1.016"/>
<pad name="P$1" x="-5.08" y="10.795" drill="0.9" diameter="1.4224" shape="square"/>
<pad name="P$2" x="-2.54" y="10.795" drill="0.9" diameter="1.4224"/>
<pad name="P$3" x="-5.08" y="8.255" drill="0.9" diameter="1.4224"/>
<pad name="P$4" x="-2.54" y="8.255" drill="0.9" diameter="1.4224"/>
<pad name="P$5" x="-5.08" y="5.715" drill="0.9" diameter="1.4224"/>
<pad name="P$6" x="-2.54" y="5.715" drill="0.9" diameter="1.4224"/>
<pad name="P$7" x="-5.08" y="3.175" drill="0.9" diameter="1.4224"/>
<pad name="P$8" x="-2.54" y="3.175" drill="0.9" diameter="1.4224"/>
<pad name="P$9" x="-5.08" y="0.635" drill="0.9" diameter="1.4224"/>
<pad name="P$10" x="-2.54" y="0.635" drill="0.9" diameter="1.4224"/>
<pad name="P$11" x="-5.08" y="-1.905" drill="0.9" diameter="1.4224"/>
<pad name="P$12" x="-2.54" y="-1.905" drill="0.9" diameter="1.4224"/>
<pad name="P$13" x="-5.08" y="-4.445" drill="0.9" diameter="1.4224"/>
<pad name="P$14" x="-2.54" y="-4.445" drill="0.9" diameter="1.4224"/>
<pad name="P$15" x="-5.08" y="-6.985" drill="0.9" diameter="1.4224"/>
<pad name="P$16" x="-2.54" y="-6.985" drill="0.9" diameter="1.4224"/>
<pad name="P$17" x="-5.08" y="-9.525" drill="0.9" diameter="1.4224"/>
<pad name="P$18" x="-2.54" y="-9.525" drill="0.9" diameter="1.4224"/>
<pad name="P$19" x="-5.08" y="-12.065" drill="0.9" diameter="1.4224"/>
<pad name="P$20" x="-2.54" y="-12.065" drill="0.9" diameter="1.4224"/>
<text x="-1.3462" y="13.2588" size="1.27" layer="25" ratio="10">&gt;NAME</text>
</package>
<package name="2X4_HEADER" urn="urn:adsk.eagle:footprint:9658928/2" library_version="14" library_locally_modified="yes">
<pad name="P$1" x="1.27" y="3.81" drill="0.9" diameter="1.4224" shape="square"/>
<pad name="P$2" x="-1.27" y="3.81" drill="0.9" diameter="1.4224" shape="square"/>
<pad name="P$3" x="1.27" y="1.27" drill="0.9" diameter="1.4224"/>
<pad name="P$4" x="-1.27" y="1.27" drill="0.9" diameter="1.4224"/>
<pad name="P$5" x="1.27" y="-1.27" drill="0.9" diameter="1.4224"/>
<pad name="P$6" x="-1.27" y="-1.27" drill="0.9" diameter="1.4224"/>
<pad name="P$7" x="1.27" y="-3.81" drill="0.9" diameter="1.4224"/>
<pad name="P$8" x="-1.27" y="-3.81" drill="0.9" diameter="1.4224"/>
<wire x1="-1.905" y1="-5.08" x2="-2.54" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-2.54" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="0" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-2.54" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-4.445" x2="-2.54" y2="-3.175" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="0" x2="-2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-2.54" y2="2.54" width="0.2286" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="5.08" width="0.2286" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="0" y2="5.08" width="0.2286" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="2.54" y2="-4.445" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-4.445" x2="1.905" y2="-5.08" width="0.2032" layer="21"/>
<wire x1="1.905" y1="0" x2="2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="2.54" y2="-1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="1.905" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.635" x2="1.905" y2="0" width="0.2032" layer="21"/>
<wire x1="0" y1="5.08" x2="2.54" y2="5.08" width="0.2286" layer="21"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="2.54" width="0.2286" layer="21"/>
<wire x1="2.54" y1="2.54" x2="1.905" y2="2.54" width="0.2286" layer="21"/>
<wire x1="-1.905" y1="-5.08" x2="-0.635" y2="-5.08" width="0.2286" layer="21"/>
<wire x1="-0.635" y1="-5.08" x2="0.635" y2="-5.08" width="0.2286" layer="21"/>
<wire x1="-0.635" y1="-5.08" x2="1.905" y2="-5.08" width="0.2286" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="LGA-16" urn="urn:adsk.eagle:package:793905/9" type="model" library_version="12" library_locally_modified="yes">
<description>LGA-16 3x3x1.1</description>
<packageinstances>
<packageinstance name="LGA-16"/>
</packageinstances>
</package3d>
<package3d name="MS5837-30BA" urn="urn:adsk.eagle:package:793906/3" type="box" library_version="17">
<packageinstances>
<packageinstance name="MS5837-30BA"/>
</packageinstances>
</package3d>
<package3d name="UDFN-8" urn="urn:adsk.eagle:package:8841323/4" type="model" library_version="12" library_locally_modified="yes">
<packageinstances>
<packageinstance name="UDFN-8"/>
</packageinstances>
</package3d>
<package3d name="C0603" urn="urn:adsk.eagle:package:9209778/2" type="model" library_version="13" library_locally_modified="yes">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<packageinstances>
<packageinstance name="C0603"/>
</packageinstances>
</package3d>
<package3d name="R603" urn="urn:adsk.eagle:package:8849207/5" type="model" library_version="13" library_locally_modified="yes">
<packageinstances>
<packageinstance name="R603"/>
</packageinstances>
</package3d>
<package3d name="XTAL320X155X90" urn="urn:adsk.eagle:package:8849208/4" type="model" library_version="13" library_locally_modified="yes">
<packageinstances>
<packageinstance name="CRYSTAL"/>
</packageinstances>
</package3d>
<package3d name="DIOC0603_N" urn="urn:adsk.eagle:package:9209769/1" type="box" library_version="2" library_locally_modified="yes">
<description>&lt;b&gt;0603&lt;/b&gt; chip &lt;p&gt;

0603 (imperial)&lt;br/&gt;
1608 (metric)&lt;br/&gt;
IPC Nominal Density</description>
<packageinstances>
<packageinstance name="DIOC0603_N"/>
</packageinstances>
</package3d>
<package3d name="TSS-105-XX-X-D" urn="urn:adsk.eagle:package:9371634/4" type="model" library_version="6" library_locally_modified="yes">
<packageinstances>
<packageinstance name="2-1761603-5"/>
</packageinstances>
</package3d>
<package3d name="VQFN-24" urn="urn:adsk.eagle:package:9526563/3" type="box" library_version="12" library_locally_modified="yes">
<description>QFN, 24-Leads, Body 4x4mm, Pitch 0.5mm, Thermal Pad 2.1x2.1mm, TI Recommended</description>
<packageinstances>
<packageinstance name="VQFN-24"/>
</packageinstances>
</package3d>
<package3d name="SOD-523" urn="urn:adsk.eagle:package:9209767/1" type="box" library_version="2" library_locally_modified="yes">
<description>SOD-523 (Small Outline Diode)</description>
<packageinstances>
<packageinstance name="SOD-523"/>
</packageinstances>
</package3d>
<package3d name="VQFN-16" urn="urn:adsk.eagle:package:9496073/4" type="model" library_version="17">
<description>RF430CL330HIRGTR</description>
<packageinstances>
<packageinstance name="VQFN-16"/>
</packageinstances>
</package3d>
<package3d name="NFC_ANT" urn="urn:adsk.eagle:package:9209768/1" type="box" library_version="9" library_locally_modified="yes">
<packageinstances>
<packageinstance name="NFC_ANT"/>
</packageinstances>
</package3d>
<package3d name="TP_PAD" urn="urn:adsk.eagle:package:9209771/2" type="box" library_version="17">
<packageinstances>
<packageinstance name="TP_PAD"/>
</packageinstances>
</package3d>
<package3d name="SOT95P210X110-5" urn="urn:adsk.eagle:package:793913/4" type="model" library_version="2" library_locally_modified="yes">
<packageinstances>
<packageinstance name="SOT25"/>
</packageinstances>
</package3d>
<package3d name="PB-FREE" urn="urn:adsk.eagle:package:9339155/2" type="box" library_version="17">
<packageinstances>
<packageinstance name="PB-FREE"/>
</packageinstances>
</package3d>
<package3d name="BATTERY" urn="urn:adsk.eagle:package:9535781/2" type="box" library_version="13" library_locally_modified="yes">
<packageinstances>
<packageinstance name="BATTERY"/>
</packageinstances>
</package3d>
<package3d name="316-917" urn="urn:adsk.eagle:package:9557619/2" type="box" library_version="13" library_locally_modified="yes">
<packageinstances>
<packageinstance name="SWITCH"/>
</packageinstances>
</package3d>
<package3d name="WSON-6" urn="urn:adsk.eagle:package:9557612/2" type="box" library_version="12" library_locally_modified="yes">
<packageinstances>
<packageinstance name="WSON-6"/>
</packageinstances>
</package3d>
<package3d name="LAUNCHPAD_TEMPLATE_LIBRARY_3PIN_PWR_5V" urn="urn:adsk.eagle:package:9557613/2" type="box" library_version="17">
<packageinstances>
<packageinstance name="LAUNCHPAD_TEMPLATE_LIBRARY_3PIN_PWR_5V"/>
</packageinstances>
</package3d>
<package3d name="TP_HOLE" urn="urn:adsk.eagle:package:9658929/1" type="box" library_version="13" library_locally_modified="yes">
<packageinstances>
<packageinstance name="TP_HOLE"/>
</packageinstances>
</package3d>
<package3d name="BUTTON" urn="urn:adsk.eagle:package:9658930/1" type="box" library_version="13" library_locally_modified="yes">
<packageinstances>
<packageinstance name="BUTTON"/>
</packageinstances>
</package3d>
<package3d name="C0805K" urn="urn:adsk.eagle:package:9688580/1" type="model" library_version="14" library_locally_modified="yes">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<packageinstances>
<packageinstance name="C0805K"/>
</packageinstances>
</package3d>
<package3d name="2X4_HEADER_SMALL" urn="urn:adsk.eagle:package:9688578/1" type="box" library_version="14" library_locally_modified="yes">
<packageinstances>
<packageinstance name="2X4_HEADER_SMALL"/>
</packageinstances>
</package3d>
<package3d name="2X4_HEADER" urn="urn:adsk.eagle:package:9658931/2" type="box" library_version="14" library_locally_modified="yes">
<packageinstances>
<packageinstance name="2X4_HEADER"/>
</packageinstances>
</package3d>
<package3d name="GQFN-8" urn="urn:adsk.eagle:package:9688579/1" type="box" library_version="14" library_locally_modified="yes">
<packageinstances>
<packageinstance name="GQFN-8"/>
</packageinstances>
</package3d>
<package3d name="1X20_1.27+2X10_2.54" urn="urn:adsk.eagle:package:9774416/1" type="box" library_version="15">
<packageinstances>
<packageinstance name="1X20_1.27+2X10_2.54"/>
</packageinstances>
</package3d>
<package3d name="TP_PAD_SOLDER" urn="urn:adsk.eagle:package:9209770/2" type="box" library_version="17">
<packageinstances>
<packageinstance name="TP_PAD_SOLDER"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="KMX62" urn="urn:adsk.eagle:symbol:9209753/4" library_version="10" library_locally_modified="yes">
<wire x1="-20.32" y1="22.86" x2="22.86" y2="22.86" width="0.254" layer="94"/>
<wire x1="22.86" y1="22.86" x2="22.86" y2="-22.86" width="0.254" layer="94"/>
<wire x1="22.86" y1="-22.86" x2="22.86" y2="-33.02" width="0.254" layer="94"/>
<wire x1="22.86" y1="-33.02" x2="-20.32" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-33.02" x2="-20.32" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-22.86" x2="-20.32" y2="22.86" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-22.86" x2="22.86" y2="-22.86" width="0.254" layer="94"/>
<pin name="ADDR" x="27.94" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="CAP" x="-25.4" y="-17.78" length="middle" direction="nc"/>
<pin name="GND_1" x="27.94" y="-15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="GND_2" x="27.94" y="-17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="GND_3" x="27.94" y="-20.32" length="middle" direction="pwr" rot="R180"/>
<pin name="GPIO1" x="27.94" y="12.7" length="middle" rot="R180"/>
<pin name="GPIO2" x="27.94" y="17.78" length="middle" rot="R180"/>
<pin name="IO_VDD" x="-25.4" y="12.7" length="middle" direction="pwr"/>
<pin name="NC_1" x="-25.4" y="-5.08" length="middle" direction="nc"/>
<pin name="NC_2" x="-25.4" y="-7.62" length="middle" direction="nc"/>
<pin name="NC_3" x="-25.4" y="-10.16" length="middle" direction="nc"/>
<pin name="NC_4" x="-25.4" y="-12.7" length="middle" direction="nc"/>
<pin name="NC_5" x="-25.4" y="-15.24" length="middle" direction="nc"/>
<pin name="SCL" x="27.94" y="-2.54" length="middle" rot="R180"/>
<pin name="SDA" x="27.94" y="2.54" length="middle" rot="R180"/>
<pin name="VDD" x="-25.4" y="17.78" length="middle" direction="pwr"/>
<text x="-20.32" y="22.86" size="3.048" layer="95">&gt;Name</text>
<text x="-20.32" y="-38.1" size="3.048" layer="95">&gt;Value</text>
<text x="-17.78" y="-30.48" size="1.778" layer="94">VDD:              1.7 V to 3.0 V
Size:              3.0 x 3.0  [mm]</text>
</symbol>
<symbol name="MS5837-30BA" urn="urn:adsk.eagle:symbol:9209749/3" library_version="10" library_locally_modified="yes">
<wire x1="-7.62" y1="7.62" x2="15.24" y2="7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="15.24" y2="-2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-2.54" x2="15.24" y2="7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="15.24" y2="-2.54" width="0.254" layer="94"/>
<pin name="GND" x="-12.7" y="0" length="middle" direction="pwr"/>
<pin name="SCL" x="20.32" y="0" length="middle" direction="in" function="clk" rot="R180"/>
<pin name="SDA" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="VDD" x="-12.7" y="5.08" length="middle" direction="pwr"/>
<text x="-7.62" y="10.16" size="3.048" layer="95">&gt;Name</text>
<text x="-7.62" y="-12.7" size="3.048" layer="95">&gt;Value</text>
<text x="-5.08" y="-5.08" size="1.27" layer="94">VDD:    1.5 V to 3.6 V</text>
</symbol>
<symbol name="AT45DB641E" urn="urn:adsk.eagle:symbol:9209755/2" library_version="9" library_locally_modified="yes">
<wire x1="-12.7" y1="12.7" x2="12.7" y2="12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="12.7" x2="12.7" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-25.4" x2="-12.7" y2="12.7" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-25.4" x2="12.7" y2="-25.4" width="0.254" layer="94"/>
<pin name="!CS" x="-17.78" y="-7.62" length="middle"/>
<pin name="!RESET" x="-17.78" y="-12.7" length="middle"/>
<pin name="!WP" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="GND" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="SCK" x="-17.78" y="-2.54" length="middle"/>
<pin name="SI" x="-17.78" y="7.62" length="middle"/>
<pin name="SO" x="-17.78" y="2.54" length="middle"/>
<pin name="VCC" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="TP" x="17.78" y="-12.7" length="middle" rot="R180"/>
<text x="-12.7" y="15.24" size="3.048" layer="95">&gt;Name</text>
<text x="-12.7" y="-30.48" size="3.048" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:9209743/1" library_version="2" library_locally_modified="yes">
<wire x1="-1.905" y1="-2.54" x2="1.905" y2="-2.54" width="0.254" layer="94"/>
<text x="-4.572" y="-8.382" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="0" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="CAPACITOR" urn="urn:adsk.eagle:symbol:9209741/1" library_version="2" library_locally_modified="yes">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:9209744/1" library_version="2" library_locally_modified="yes">
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="RESISTOR" urn="urn:adsk.eagle:symbol:9209751/1" library_version="2" library_locally_modified="yes">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="CRYSTAL" urn="urn:adsk.eagle:symbol:9209746/1" library_version="2" library_locally_modified="yes">
<wire x1="-1.27" y1="2.54" x2="1.397" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.397" y1="2.54" x2="1.397" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.397" y1="-2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="2.3368" y1="2.54" x2="2.3368" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-2.286" y1="2.54" x2="-2.286" y2="-2.54" width="0.4064" layer="94"/>
<text x="-5.08" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="TEST-POINT" urn="urn:adsk.eagle:symbol:9209739/2" library_version="3" library_locally_modified="yes">
<pin name="TP" x="5.08" y="0" visible="off" length="middle" rot="R180"/>
<circle x="0" y="0" radius="0.915809375" width="0.127" layer="94"/>
<text x="0" y="1.524" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="0" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="14P-HEADER" urn="urn:adsk.eagle:symbol:9371628/2" library_version="10" library_locally_modified="yes">
<wire x1="21.59" y1="-10.16" x2="-13.97" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="-13.97" y1="10.16" x2="-13.97" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="21.59" y1="-10.16" x2="21.59" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-13.97" y1="10.16" x2="21.59" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="-11.7475" y2="6.985" width="0.254" layer="94"/>
<wire x1="-12.7" y1="7.62" x2="-11.7475" y2="8.255" width="0.254" layer="94"/>
<wire x1="-12.7" y1="5.08" x2="-11.7475" y2="4.445" width="0.254" layer="94"/>
<wire x1="-12.7" y1="5.08" x2="-11.7475" y2="5.715" width="0.254" layer="94"/>
<wire x1="20.32" y1="5.08" x2="19.3675" y2="5.715" width="0.254" layer="94"/>
<wire x1="20.32" y1="5.08" x2="19.3675" y2="4.445" width="0.254" layer="94"/>
<wire x1="20.32" y1="7.62" x2="19.3675" y2="8.255" width="0.254" layer="94"/>
<wire x1="20.32" y1="7.62" x2="19.3675" y2="6.985" width="0.254" layer="94"/>
<wire x1="-12.7" y1="2.54" x2="-11.7475" y2="1.905" width="0.254" layer="94"/>
<wire x1="-12.7" y1="2.54" x2="-11.7475" y2="3.175" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="19.3675" y2="0.635" width="0.254" layer="94"/>
<wire x1="20.32" y1="0" x2="19.3675" y2="-0.635" width="0.254" layer="94"/>
<wire x1="20.32" y1="2.54" x2="19.3675" y2="3.175" width="0.254" layer="94"/>
<wire x1="20.32" y1="2.54" x2="19.3675" y2="1.905" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-2.54" x2="-11.7475" y2="-3.175" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-2.54" x2="-11.7475" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-11.7475" y2="-5.715" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.08" x2="-11.7475" y2="-4.445" width="0.254" layer="94"/>
<wire x1="20.32" y1="-5.08" x2="19.3675" y2="-4.445" width="0.254" layer="94"/>
<wire x1="20.32" y1="-5.08" x2="19.3675" y2="-5.715" width="0.254" layer="94"/>
<wire x1="20.32" y1="-2.54" x2="19.3675" y2="-1.905" width="0.254" layer="94"/>
<wire x1="20.32" y1="-2.54" x2="19.3675" y2="-3.175" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-7.62" x2="-11.7475" y2="-8.255" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-7.62" x2="-11.7475" y2="-6.985" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="19.3675" y2="-8.255" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="19.3675" y2="-6.985" width="0.254" layer="94"/>
<text x="-13.97" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="-13.97" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<pin name="TDO/TDI" x="-17.78" y="7.62" length="middle" swaplevel="1"/>
<pin name="TDI/VPP" x="-17.78" y="5.08" visible="pad" length="middle" direction="nc" swaplevel="1"/>
<pin name="TMS" x="-17.78" y="2.54" visible="pad" length="middle" direction="nc" swaplevel="1"/>
<pin name="VCC_TOOL" x="25.4" y="7.62" length="middle" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="VCC_TARGET" x="25.4" y="5.08" length="middle" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="N/C" x="25.4" y="2.54" visible="pad" length="middle" direction="nc" swaplevel="1" rot="R180"/>
<pin name="TCK" x="-17.78" y="0" length="middle" function="clk" swaplevel="1"/>
<pin name="GND" x="-17.78" y="-2.54" length="middle" direction="pwr" swaplevel="1"/>
<pin name="TEST/VPP" x="25.4" y="0" length="middle" swaplevel="1" rot="R180"/>
<pin name="I2C_SCL" x="25.4" y="-2.54" visible="pad" length="middle" direction="nc" swaplevel="1" rot="R180"/>
<pin name="RST" x="-17.78" y="-5.08" visible="pad" length="middle" direction="nc" swaplevel="1"/>
<pin name="UART_RTS" x="-17.78" y="-7.62" visible="pad" length="middle" direction="nc" swaplevel="1"/>
<pin name="UART_TXD" x="25.4" y="-5.08" visible="pad" length="middle" direction="nc" swaplevel="1" rot="R180"/>
<pin name="UART_RXD" x="25.4" y="-7.62" visible="pad" length="middle" direction="nc" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="A4-FRAME" urn="urn:adsk.eagle:symbol:9371627/3" library_version="10" library_locally_modified="yes">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="144.145" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="218.44" y1="3.81" x2="218.44" y2="8.89" width="0.1016" layer="94"/>
<wire x1="218.44" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="218.44" y1="8.89" x2="193.04" y2="8.89" width="0.1016" layer="94"/>
<wire x1="193.04" y1="8.89" x2="144.145" y2="8.89" width="0.1016" layer="94"/>
<wire x1="144.145" y1="8.89" x2="144.145" y2="3.81" width="0.1016" layer="94"/>
<wire x1="144.145" y1="8.89" x2="144.145" y2="24.13" width="0.1016" layer="94"/>
<wire x1="193.04" y1="3.81" x2="193.04" y2="8.89" width="0.1016" layer="94"/>
<wire x1="218.5" y1="24.1" x2="218.44" y2="24.1" width="0.1016" layer="94"/>
<wire x1="218.44" y1="24.1" x2="218.44" y2="18.78" width="0.1016" layer="94"/>
<wire x1="218.44" y1="18.78" x2="218.44" y2="13.6" width="0.1016" layer="94"/>
<wire x1="218.44" y1="13.6" x2="218.44" y2="8.89" width="0.1016" layer="94"/>
<wire x1="144.2" y1="13.6" x2="218.44" y2="13.6" width="0.1016" layer="94"/>
<wire x1="144.1" y1="18.8" x2="144.1" y2="18.78" width="0.1016" layer="94"/>
<wire x1="144.1" y1="18.78" x2="218.44" y2="18.78" width="0.1016" layer="94"/>
<text x="158.75" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="146.05" y="5.08" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="238.125" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="219.456" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="194.056" y="4.953" size="2.54" layer="94" font="vector">Rev:</text>
<text x="145.796" y="10.033" size="2.54" layer="94" font="vector">DRAWN:</text>
<text x="146.1896" y="20.0708" size="2.54" layer="94" font="vector">NTNU, Trondheim</text>
<text x="145.796" y="15.113" size="2.54" layer="94" font="vector">TITLE:</text>
<text x="160.02" y="10.16" size="2.54" layer="94">&gt;AUTHOR</text>
<text x="203.2" y="5.08" size="2.54" layer="94">&gt;REV</text>
<frame x1="0" y1="0" x2="260.392340625" y2="179.11234375" columns="6" rows="4" layer="94"/>
<rectangle x1="236.70904375" y1="13.993875" x2="236.83604375" y2="14.03629375" layer="94"/>
<rectangle x1="246.277225" y1="13.993875" x2="246.404225" y2="14.03629375" layer="94"/>
<rectangle x1="249.79004375" y1="13.993875" x2="250.214225" y2="14.03629375" layer="94"/>
<rectangle x1="228.07304375" y1="14.03629375" x2="230.69940625" y2="14.0787125" layer="94"/>
<rectangle x1="236.66840625" y1="14.03629375" x2="236.83604375" y2="14.0787125" layer="94"/>
<rectangle x1="246.23404375" y1="14.03629375" x2="246.404225" y2="14.0787125" layer="94"/>
<rectangle x1="249.28204375" y1="14.03629375" x2="250.63840625" y2="14.0787125" layer="94"/>
<rectangle x1="227.90540625" y1="14.0787125" x2="230.86704375" y2="14.120875" layer="94"/>
<rectangle x1="232.815225" y1="14.0787125" x2="234.38240625" y2="14.120875" layer="94"/>
<rectangle x1="236.66840625" y1="14.0787125" x2="236.83604375" y2="14.120875" layer="94"/>
<rectangle x1="238.82740625" y1="14.0787125" x2="241.02704375" y2="14.120875" layer="94"/>
<rectangle x1="242.38340625" y1="14.0787125" x2="243.90740625" y2="14.120875" layer="94"/>
<rectangle x1="246.23404375" y1="14.0787125" x2="246.404225" y2="14.120875" layer="94"/>
<rectangle x1="249.11440625" y1="14.0787125" x2="250.80604375" y2="14.120875" layer="94"/>
<rectangle x1="227.735225" y1="14.120875" x2="230.99404375" y2="14.16329375" layer="94"/>
<rectangle x1="232.815225" y1="14.120875" x2="234.38240625" y2="14.16329375" layer="94"/>
<rectangle x1="236.625225" y1="14.120875" x2="236.83604375" y2="14.16329375" layer="94"/>
<rectangle x1="238.82740625" y1="14.120875" x2="241.02704375" y2="14.16329375" layer="94"/>
<rectangle x1="242.38340625" y1="14.120875" x2="243.90740625" y2="14.16329375" layer="94"/>
<rectangle x1="246.19340625" y1="14.120875" x2="246.404225" y2="14.16329375" layer="94"/>
<rectangle x1="248.944225" y1="14.120875" x2="250.976225" y2="14.16329375" layer="94"/>
<rectangle x1="227.65140625" y1="14.16329375" x2="231.12104375" y2="14.2057125" layer="94"/>
<rectangle x1="232.815225" y1="14.16329375" x2="234.38240625" y2="14.2057125" layer="94"/>
<rectangle x1="236.58204375" y1="14.16329375" x2="236.83604375" y2="14.2057125" layer="94"/>
<rectangle x1="238.82740625" y1="14.16329375" x2="241.02704375" y2="14.2057125" layer="94"/>
<rectangle x1="242.38340625" y1="14.16329375" x2="243.90740625" y2="14.2057125" layer="94"/>
<rectangle x1="246.150225" y1="14.16329375" x2="246.404225" y2="14.2057125" layer="94"/>
<rectangle x1="248.77404375" y1="14.16329375" x2="251.103225" y2="14.2057125" layer="94"/>
<rectangle x1="227.56504375" y1="14.2057125" x2="231.164225" y2="14.247875" layer="94"/>
<rectangle x1="232.815225" y1="14.2057125" x2="234.38240625" y2="14.247875" layer="94"/>
<rectangle x1="236.54140625" y1="14.2057125" x2="236.83604375" y2="14.247875" layer="94"/>
<rectangle x1="238.82740625" y1="14.2057125" x2="241.02704375" y2="14.247875" layer="94"/>
<rectangle x1="242.38340625" y1="14.2057125" x2="243.90740625" y2="14.247875" layer="94"/>
<rectangle x1="246.10704375" y1="14.2057125" x2="246.404225" y2="14.247875" layer="94"/>
<rectangle x1="248.690225" y1="14.2057125" x2="251.18704375" y2="14.247875" layer="94"/>
<rectangle x1="227.52440625" y1="14.247875" x2="231.24804375" y2="14.29029375" layer="94"/>
<rectangle x1="233.40704375" y1="14.247875" x2="233.62040625" y2="14.29029375" layer="94"/>
<rectangle x1="236.498225" y1="14.247875" x2="236.83604375" y2="14.29029375" layer="94"/>
<rectangle x1="239.546225" y1="14.247875" x2="240.308225" y2="14.29029375" layer="94"/>
<rectangle x1="243.01840625" y1="14.247875" x2="243.229225" y2="14.29029375" layer="94"/>
<rectangle x1="246.10704375" y1="14.247875" x2="246.404225" y2="14.29029375" layer="94"/>
<rectangle x1="248.60640625" y1="14.247875" x2="249.66304375" y2="14.29029375" layer="94"/>
<rectangle x1="250.67904375" y1="14.247875" x2="251.27340625" y2="14.29029375" layer="94"/>
<rectangle x1="227.481225" y1="14.29029375" x2="231.291225" y2="14.3327125" layer="94"/>
<rectangle x1="233.40704375" y1="14.29029375" x2="233.62040625" y2="14.3327125" layer="94"/>
<rectangle x1="236.498225" y1="14.29029375" x2="236.83604375" y2="14.3327125" layer="94"/>
<rectangle x1="239.546225" y1="14.29029375" x2="240.308225" y2="14.3327125" layer="94"/>
<rectangle x1="243.01840625" y1="14.29029375" x2="243.229225" y2="14.3327125" layer="94"/>
<rectangle x1="246.06640625" y1="14.29029375" x2="246.404225" y2="14.3327125" layer="94"/>
<rectangle x1="248.563225" y1="14.29029375" x2="249.53604375" y2="14.3327125" layer="94"/>
<rectangle x1="250.80604375" y1="14.29029375" x2="251.31404375" y2="14.3327125" layer="94"/>
<rectangle x1="227.43804375" y1="14.3327125" x2="231.33440625" y2="14.374875" layer="94"/>
<rectangle x1="233.40704375" y1="14.3327125" x2="233.62040625" y2="14.374875" layer="94"/>
<rectangle x1="236.45504375" y1="14.3327125" x2="236.83604375" y2="14.374875" layer="94"/>
<rectangle x1="239.546225" y1="14.3327125" x2="240.308225" y2="14.374875" layer="94"/>
<rectangle x1="243.01840625" y1="14.3327125" x2="243.229225" y2="14.374875" layer="94"/>
<rectangle x1="246.06640625" y1="14.3327125" x2="246.404225" y2="14.374875" layer="94"/>
<rectangle x1="248.52004375" y1="14.3327125" x2="249.40904375" y2="14.374875" layer="94"/>
<rectangle x1="250.93304375" y1="14.3327125" x2="251.357225" y2="14.374875" layer="94"/>
<rectangle x1="227.354225" y1="14.374875" x2="231.37504375" y2="14.41729375" layer="94"/>
<rectangle x1="233.40704375" y1="14.374875" x2="233.62040625" y2="14.41729375" layer="94"/>
<rectangle x1="236.45504375" y1="14.374875" x2="236.83604375" y2="14.41729375" layer="94"/>
<rectangle x1="239.546225" y1="14.374875" x2="240.308225" y2="14.41729375" layer="94"/>
<rectangle x1="243.01840625" y1="14.374875" x2="243.229225" y2="14.41729375" layer="94"/>
<rectangle x1="246.023225" y1="14.374875" x2="246.404225" y2="14.41729375" layer="94"/>
<rectangle x1="248.47940625" y1="14.374875" x2="249.325225" y2="14.41729375" layer="94"/>
<rectangle x1="251.06004375" y1="14.374875" x2="251.40040625" y2="14.41729375" layer="94"/>
<rectangle x1="227.31104375" y1="14.41729375" x2="231.418225" y2="14.4597125" layer="94"/>
<rectangle x1="233.40704375" y1="14.41729375" x2="233.62040625" y2="14.4597125" layer="94"/>
<rectangle x1="236.41440625" y1="14.41729375" x2="236.83604375" y2="14.4597125" layer="94"/>
<rectangle x1="239.546225" y1="14.41729375" x2="240.308225" y2="14.4597125" layer="94"/>
<rectangle x1="243.01840625" y1="14.41729375" x2="243.229225" y2="14.4597125" layer="94"/>
<rectangle x1="245.98004375" y1="14.41729375" x2="246.404225" y2="14.4597125" layer="94"/>
<rectangle x1="248.436225" y1="14.41729375" x2="249.24140625" y2="14.4597125" layer="94"/>
<rectangle x1="251.14640625" y1="14.41729375" x2="251.44104375" y2="14.4597125" layer="94"/>
<rectangle x1="227.27040625" y1="14.4597125" x2="231.46140625" y2="14.501875" layer="94"/>
<rectangle x1="233.40704375" y1="14.4597125" x2="233.62040625" y2="14.501875" layer="94"/>
<rectangle x1="236.371225" y1="14.4597125" x2="236.83604375" y2="14.501875" layer="94"/>
<rectangle x1="239.546225" y1="14.4597125" x2="240.308225" y2="14.501875" layer="94"/>
<rectangle x1="243.01840625" y1="14.4597125" x2="243.229225" y2="14.501875" layer="94"/>
<rectangle x1="245.93940625" y1="14.4597125" x2="246.404225" y2="14.501875" layer="94"/>
<rectangle x1="248.39304375" y1="14.4597125" x2="249.198225" y2="14.501875" layer="94"/>
<rectangle x1="251.18704375" y1="14.4597125" x2="251.484225" y2="14.501875" layer="94"/>
<rectangle x1="227.227225" y1="14.501875" x2="231.50204375" y2="14.54429375" layer="94"/>
<rectangle x1="233.40704375" y1="14.501875" x2="233.62040625" y2="14.54429375" layer="94"/>
<rectangle x1="236.371225" y1="14.501875" x2="236.83604375" y2="14.54429375" layer="94"/>
<rectangle x1="239.546225" y1="14.501875" x2="240.308225" y2="14.54429375" layer="94"/>
<rectangle x1="243.01840625" y1="14.501875" x2="243.229225" y2="14.54429375" layer="94"/>
<rectangle x1="245.896225" y1="14.501875" x2="246.404225" y2="14.54429375" layer="94"/>
<rectangle x1="248.35240625" y1="14.501875" x2="249.15504375" y2="14.54429375" layer="94"/>
<rectangle x1="251.27340625" y1="14.501875" x2="251.52740625" y2="14.54429375" layer="94"/>
<rectangle x1="227.227225" y1="14.54429375" x2="231.545225" y2="14.5867125" layer="94"/>
<rectangle x1="233.40704375" y1="14.54429375" x2="233.62040625" y2="14.5867125" layer="94"/>
<rectangle x1="236.32804375" y1="14.54429375" x2="236.83604375" y2="14.5867125" layer="94"/>
<rectangle x1="239.546225" y1="14.54429375" x2="240.308225" y2="14.5867125" layer="94"/>
<rectangle x1="243.01840625" y1="14.54429375" x2="243.229225" y2="14.5867125" layer="94"/>
<rectangle x1="245.896225" y1="14.54429375" x2="246.404225" y2="14.5867125" layer="94"/>
<rectangle x1="248.309225" y1="14.54429375" x2="249.11440625" y2="14.5867125" layer="94"/>
<rectangle x1="251.27340625" y1="14.54429375" x2="251.52740625" y2="14.5867125" layer="94"/>
<rectangle x1="227.18404375" y1="14.5867125" x2="231.58840625" y2="14.628875" layer="94"/>
<rectangle x1="233.40704375" y1="14.5867125" x2="233.62040625" y2="14.628875" layer="94"/>
<rectangle x1="236.28740625" y1="14.5867125" x2="236.83604375" y2="14.628875" layer="94"/>
<rectangle x1="239.546225" y1="14.5867125" x2="240.308225" y2="14.628875" layer="94"/>
<rectangle x1="243.01840625" y1="14.5867125" x2="243.229225" y2="14.628875" layer="94"/>
<rectangle x1="245.85304375" y1="14.5867125" x2="246.404225" y2="14.628875" layer="94"/>
<rectangle x1="248.309225" y1="14.5867125" x2="249.071225" y2="14.628875" layer="94"/>
<rectangle x1="251.31404375" y1="14.5867125" x2="251.56804375" y2="14.628875" layer="94"/>
<rectangle x1="227.18404375" y1="14.628875" x2="231.58840625" y2="14.67129375" layer="94"/>
<rectangle x1="233.40704375" y1="14.628875" x2="233.62040625" y2="14.67129375" layer="94"/>
<rectangle x1="236.244225" y1="14.628875" x2="236.83604375" y2="14.67129375" layer="94"/>
<rectangle x1="239.546225" y1="14.628875" x2="240.308225" y2="14.67129375" layer="94"/>
<rectangle x1="243.01840625" y1="14.628875" x2="243.229225" y2="14.67129375" layer="94"/>
<rectangle x1="245.85304375" y1="14.628875" x2="246.404225" y2="14.67129375" layer="94"/>
<rectangle x1="248.26604375" y1="14.628875" x2="249.071225" y2="14.67129375" layer="94"/>
<rectangle x1="251.357225" y1="14.628875" x2="251.56804375" y2="14.67129375" layer="94"/>
<rectangle x1="227.14340625" y1="14.67129375" x2="231.62904375" y2="14.7137125" layer="94"/>
<rectangle x1="233.40704375" y1="14.67129375" x2="233.62040625" y2="14.7137125" layer="94"/>
<rectangle x1="236.244225" y1="14.67129375" x2="236.83604375" y2="14.7137125" layer="94"/>
<rectangle x1="239.546225" y1="14.67129375" x2="240.308225" y2="14.7137125" layer="94"/>
<rectangle x1="243.01840625" y1="14.67129375" x2="243.229225" y2="14.7137125" layer="94"/>
<rectangle x1="245.81240625" y1="14.67129375" x2="246.404225" y2="14.7137125" layer="94"/>
<rectangle x1="248.26604375" y1="14.67129375" x2="249.02804375" y2="14.7137125" layer="94"/>
<rectangle x1="251.357225" y1="14.67129375" x2="251.611225" y2="14.7137125" layer="94"/>
<rectangle x1="227.100225" y1="14.7137125" x2="231.62904375" y2="14.755875" layer="94"/>
<rectangle x1="233.40704375" y1="14.7137125" x2="233.62040625" y2="14.755875" layer="94"/>
<rectangle x1="236.20104375" y1="14.7137125" x2="236.83604375" y2="14.755875" layer="94"/>
<rectangle x1="239.546225" y1="14.7137125" x2="240.308225" y2="14.755875" layer="94"/>
<rectangle x1="243.01840625" y1="14.7137125" x2="243.229225" y2="14.755875" layer="94"/>
<rectangle x1="245.769225" y1="14.7137125" x2="246.404225" y2="14.755875" layer="94"/>
<rectangle x1="248.26604375" y1="14.7137125" x2="248.98740625" y2="14.755875" layer="94"/>
<rectangle x1="251.40040625" y1="14.7137125" x2="251.611225" y2="14.755875" layer="94"/>
<rectangle x1="227.100225" y1="14.755875" x2="231.62904375" y2="14.79829375" layer="94"/>
<rectangle x1="233.40704375" y1="14.755875" x2="233.62040625" y2="14.79829375" layer="94"/>
<rectangle x1="236.20104375" y1="14.755875" x2="236.83604375" y2="14.79829375" layer="94"/>
<rectangle x1="239.546225" y1="14.755875" x2="240.308225" y2="14.79829375" layer="94"/>
<rectangle x1="243.01840625" y1="14.755875" x2="243.229225" y2="14.79829375" layer="94"/>
<rectangle x1="245.769225" y1="14.755875" x2="246.404225" y2="14.79829375" layer="94"/>
<rectangle x1="248.22540625" y1="14.755875" x2="248.98740625" y2="14.79829375" layer="94"/>
<rectangle x1="251.40040625" y1="14.755875" x2="251.611225" y2="14.79829375" layer="94"/>
<rectangle x1="227.100225" y1="14.79829375" x2="231.672225" y2="14.8407125" layer="94"/>
<rectangle x1="233.40704375" y1="14.79829375" x2="233.62040625" y2="14.8407125" layer="94"/>
<rectangle x1="236.16040625" y1="14.79829375" x2="236.83604375" y2="14.8407125" layer="94"/>
<rectangle x1="239.546225" y1="14.79829375" x2="240.308225" y2="14.8407125" layer="94"/>
<rectangle x1="243.01840625" y1="14.79829375" x2="243.229225" y2="14.8407125" layer="94"/>
<rectangle x1="245.72604375" y1="14.79829375" x2="246.404225" y2="14.8407125" layer="94"/>
<rectangle x1="248.22540625" y1="14.79829375" x2="248.98740625" y2="14.8407125" layer="94"/>
<rectangle x1="251.44104375" y1="14.79829375" x2="251.65440625" y2="14.8407125" layer="94"/>
<rectangle x1="227.05704375" y1="14.8407125" x2="231.672225" y2="14.882875" layer="94"/>
<rectangle x1="233.40704375" y1="14.8407125" x2="233.62040625" y2="14.882875" layer="94"/>
<rectangle x1="236.117225" y1="14.8407125" x2="236.83604375" y2="14.882875" layer="94"/>
<rectangle x1="239.546225" y1="14.8407125" x2="240.308225" y2="14.882875" layer="94"/>
<rectangle x1="243.01840625" y1="14.8407125" x2="243.229225" y2="14.882875" layer="94"/>
<rectangle x1="245.68540625" y1="14.8407125" x2="246.404225" y2="14.882875" layer="94"/>
<rectangle x1="248.22540625" y1="14.8407125" x2="248.944225" y2="14.882875" layer="94"/>
<rectangle x1="251.44104375" y1="14.8407125" x2="251.65440625" y2="14.882875" layer="94"/>
<rectangle x1="227.05704375" y1="14.882875" x2="231.71540625" y2="14.92529375" layer="94"/>
<rectangle x1="233.40704375" y1="14.882875" x2="233.62040625" y2="14.92529375" layer="94"/>
<rectangle x1="236.07404375" y1="14.882875" x2="236.83604375" y2="14.92529375" layer="94"/>
<rectangle x1="239.546225" y1="14.882875" x2="240.308225" y2="14.92529375" layer="94"/>
<rectangle x1="243.01840625" y1="14.882875" x2="243.229225" y2="14.92529375" layer="94"/>
<rectangle x1="245.642225" y1="14.882875" x2="246.404225" y2="14.92529375" layer="94"/>
<rectangle x1="248.22540625" y1="14.882875" x2="248.944225" y2="14.92529375" layer="94"/>
<rectangle x1="251.44104375" y1="14.882875" x2="251.69504375" y2="14.92529375" layer="94"/>
<rectangle x1="227.05704375" y1="14.92529375" x2="231.71540625" y2="14.9677125" layer="94"/>
<rectangle x1="233.40704375" y1="14.92529375" x2="233.62040625" y2="14.9677125" layer="94"/>
<rectangle x1="236.03340625" y1="14.92529375" x2="236.83604375" y2="14.9677125" layer="94"/>
<rectangle x1="239.546225" y1="14.92529375" x2="240.308225" y2="14.9677125" layer="94"/>
<rectangle x1="243.01840625" y1="14.92529375" x2="243.229225" y2="14.9677125" layer="94"/>
<rectangle x1="245.642225" y1="14.92529375" x2="246.404225" y2="14.9677125" layer="94"/>
<rectangle x1="248.182225" y1="14.92529375" x2="248.944225" y2="14.9677125" layer="94"/>
<rectangle x1="251.484225" y1="14.92529375" x2="251.69504375" y2="14.9677125" layer="94"/>
<rectangle x1="227.05704375" y1="14.9677125" x2="231.71540625" y2="15.009875" layer="94"/>
<rectangle x1="233.40704375" y1="14.9677125" x2="233.62040625" y2="15.009875" layer="94"/>
<rectangle x1="236.03340625" y1="14.9677125" x2="236.83604375" y2="15.009875" layer="94"/>
<rectangle x1="239.546225" y1="14.9677125" x2="240.308225" y2="15.009875" layer="94"/>
<rectangle x1="243.01840625" y1="14.9677125" x2="243.229225" y2="15.009875" layer="94"/>
<rectangle x1="245.59904375" y1="14.9677125" x2="246.404225" y2="15.009875" layer="94"/>
<rectangle x1="248.182225" y1="14.9677125" x2="248.944225" y2="15.009875" layer="94"/>
<rectangle x1="251.484225" y1="14.9677125" x2="251.69504375" y2="15.009875" layer="94"/>
<rectangle x1="227.01640625" y1="15.009875" x2="231.75604375" y2="15.05229375" layer="94"/>
<rectangle x1="233.40704375" y1="15.009875" x2="233.62040625" y2="15.05229375" layer="94"/>
<rectangle x1="235.990225" y1="15.009875" x2="236.83604375" y2="15.05229375" layer="94"/>
<rectangle x1="239.546225" y1="15.009875" x2="240.308225" y2="15.05229375" layer="94"/>
<rectangle x1="243.01840625" y1="15.009875" x2="243.229225" y2="15.05229375" layer="94"/>
<rectangle x1="245.59904375" y1="15.009875" x2="246.404225" y2="15.05229375" layer="94"/>
<rectangle x1="248.182225" y1="15.009875" x2="248.944225" y2="15.05229375" layer="94"/>
<rectangle x1="251.484225" y1="15.009875" x2="251.69504375" y2="15.05229375" layer="94"/>
<rectangle x1="227.01640625" y1="15.05229375" x2="231.75604375" y2="15.0947125" layer="94"/>
<rectangle x1="233.40704375" y1="15.05229375" x2="233.62040625" y2="15.0947125" layer="94"/>
<rectangle x1="235.990225" y1="15.05229375" x2="236.83604375" y2="15.0947125" layer="94"/>
<rectangle x1="239.546225" y1="15.05229375" x2="240.308225" y2="15.0947125" layer="94"/>
<rectangle x1="243.01840625" y1="15.05229375" x2="243.229225" y2="15.0947125" layer="94"/>
<rectangle x1="245.55840625" y1="15.05229375" x2="246.404225" y2="15.0947125" layer="94"/>
<rectangle x1="248.182225" y1="15.05229375" x2="248.944225" y2="15.0947125" layer="94"/>
<rectangle x1="251.484225" y1="15.05229375" x2="251.69504375" y2="15.0947125" layer="94"/>
<rectangle x1="227.01640625" y1="15.0947125" x2="231.75604375" y2="15.136875" layer="94"/>
<rectangle x1="233.40704375" y1="15.0947125" x2="233.62040625" y2="15.136875" layer="94"/>
<rectangle x1="235.94704375" y1="15.0947125" x2="236.83604375" y2="15.136875" layer="94"/>
<rectangle x1="239.546225" y1="15.0947125" x2="240.308225" y2="15.136875" layer="94"/>
<rectangle x1="243.01840625" y1="15.0947125" x2="243.229225" y2="15.136875" layer="94"/>
<rectangle x1="245.515225" y1="15.0947125" x2="246.404225" y2="15.136875" layer="94"/>
<rectangle x1="248.182225" y1="15.0947125" x2="248.90104375" y2="15.136875" layer="94"/>
<rectangle x1="251.484225" y1="15.0947125" x2="251.69504375" y2="15.136875" layer="94"/>
<rectangle x1="227.01640625" y1="15.136875" x2="231.75604375" y2="15.17929375" layer="94"/>
<rectangle x1="233.40704375" y1="15.136875" x2="233.62040625" y2="15.17929375" layer="94"/>
<rectangle x1="235.90640625" y1="15.136875" x2="236.83604375" y2="15.17929375" layer="94"/>
<rectangle x1="239.546225" y1="15.136875" x2="240.308225" y2="15.17929375" layer="94"/>
<rectangle x1="243.01840625" y1="15.136875" x2="243.229225" y2="15.17929375" layer="94"/>
<rectangle x1="245.47204375" y1="15.136875" x2="246.404225" y2="15.17929375" layer="94"/>
<rectangle x1="248.182225" y1="15.136875" x2="248.90104375" y2="15.17929375" layer="94"/>
<rectangle x1="251.484225" y1="15.136875" x2="251.69504375" y2="15.17929375" layer="94"/>
<rectangle x1="227.01640625" y1="15.17929375" x2="231.75604375" y2="15.2217125" layer="94"/>
<rectangle x1="233.40704375" y1="15.17929375" x2="233.62040625" y2="15.2217125" layer="94"/>
<rectangle x1="235.90640625" y1="15.17929375" x2="236.83604375" y2="15.2217125" layer="94"/>
<rectangle x1="239.546225" y1="15.17929375" x2="240.308225" y2="15.2217125" layer="94"/>
<rectangle x1="243.01840625" y1="15.17929375" x2="243.229225" y2="15.2217125" layer="94"/>
<rectangle x1="245.47204375" y1="15.17929375" x2="246.404225" y2="15.2217125" layer="94"/>
<rectangle x1="248.182225" y1="15.17929375" x2="248.90104375" y2="15.2217125" layer="94"/>
<rectangle x1="251.484225" y1="15.17929375" x2="251.69504375" y2="15.2217125" layer="94"/>
<rectangle x1="227.01640625" y1="15.2217125" x2="231.75604375" y2="15.263875" layer="94"/>
<rectangle x1="233.40704375" y1="15.2217125" x2="233.62040625" y2="15.263875" layer="94"/>
<rectangle x1="235.863225" y1="15.2217125" x2="236.83604375" y2="15.263875" layer="94"/>
<rectangle x1="239.546225" y1="15.2217125" x2="240.308225" y2="15.263875" layer="94"/>
<rectangle x1="243.01840625" y1="15.2217125" x2="243.229225" y2="15.263875" layer="94"/>
<rectangle x1="245.43140625" y1="15.2217125" x2="246.404225" y2="15.263875" layer="94"/>
<rectangle x1="248.182225" y1="15.2217125" x2="248.90104375" y2="15.263875" layer="94"/>
<rectangle x1="251.484225" y1="15.2217125" x2="251.69504375" y2="15.263875" layer="94"/>
<rectangle x1="227.01640625" y1="15.263875" x2="228.20004375" y2="15.30629375" layer="94"/>
<rectangle x1="230.57240625" y1="15.263875" x2="231.75604375" y2="15.30629375" layer="94"/>
<rectangle x1="233.40704375" y1="15.263875" x2="233.62040625" y2="15.30629375" layer="94"/>
<rectangle x1="235.82004375" y1="15.263875" x2="236.83604375" y2="15.30629375" layer="94"/>
<rectangle x1="239.546225" y1="15.263875" x2="240.308225" y2="15.30629375" layer="94"/>
<rectangle x1="243.01840625" y1="15.263875" x2="243.229225" y2="15.30629375" layer="94"/>
<rectangle x1="245.388225" y1="15.263875" x2="246.404225" y2="15.30629375" layer="94"/>
<rectangle x1="248.182225" y1="15.263875" x2="248.90104375" y2="15.30629375" layer="94"/>
<rectangle x1="251.52740625" y1="15.263875" x2="251.738225" y2="15.30629375" layer="94"/>
<rectangle x1="227.01640625" y1="15.30629375" x2="228.20004375" y2="15.3487125" layer="94"/>
<rectangle x1="230.57240625" y1="15.30629375" x2="231.75604375" y2="15.3487125" layer="94"/>
<rectangle x1="233.40704375" y1="15.30629375" x2="233.62040625" y2="15.3487125" layer="94"/>
<rectangle x1="235.77940625" y1="15.30629375" x2="236.83604375" y2="15.3487125" layer="94"/>
<rectangle x1="239.546225" y1="15.30629375" x2="240.308225" y2="15.3487125" layer="94"/>
<rectangle x1="243.01840625" y1="15.30629375" x2="243.229225" y2="15.3487125" layer="94"/>
<rectangle x1="245.388225" y1="15.30629375" x2="246.404225" y2="15.3487125" layer="94"/>
<rectangle x1="248.182225" y1="15.30629375" x2="248.90104375" y2="15.3487125" layer="94"/>
<rectangle x1="251.52740625" y1="15.30629375" x2="251.738225" y2="15.3487125" layer="94"/>
<rectangle x1="227.01640625" y1="15.3487125" x2="228.20004375" y2="15.390875" layer="94"/>
<rectangle x1="230.57240625" y1="15.3487125" x2="231.75604375" y2="15.390875" layer="94"/>
<rectangle x1="233.40704375" y1="15.3487125" x2="233.62040625" y2="15.390875" layer="94"/>
<rectangle x1="235.77940625" y1="15.3487125" x2="236.83604375" y2="15.390875" layer="94"/>
<rectangle x1="239.546225" y1="15.3487125" x2="240.308225" y2="15.390875" layer="94"/>
<rectangle x1="243.01840625" y1="15.3487125" x2="243.229225" y2="15.390875" layer="94"/>
<rectangle x1="245.34504375" y1="15.3487125" x2="246.404225" y2="15.390875" layer="94"/>
<rectangle x1="248.182225" y1="15.3487125" x2="248.90104375" y2="15.390875" layer="94"/>
<rectangle x1="251.52740625" y1="15.3487125" x2="251.738225" y2="15.390875" layer="94"/>
<rectangle x1="227.01640625" y1="15.390875" x2="228.20004375" y2="15.43329375" layer="94"/>
<rectangle x1="230.57240625" y1="15.390875" x2="231.75604375" y2="15.43329375" layer="94"/>
<rectangle x1="233.40704375" y1="15.390875" x2="233.62040625" y2="15.43329375" layer="94"/>
<rectangle x1="235.736225" y1="15.390875" x2="236.83604375" y2="15.43329375" layer="94"/>
<rectangle x1="239.546225" y1="15.390875" x2="240.308225" y2="15.43329375" layer="94"/>
<rectangle x1="243.01840625" y1="15.390875" x2="243.229225" y2="15.43329375" layer="94"/>
<rectangle x1="245.30440625" y1="15.390875" x2="246.404225" y2="15.43329375" layer="94"/>
<rectangle x1="248.182225" y1="15.390875" x2="248.90104375" y2="15.43329375" layer="94"/>
<rectangle x1="251.52740625" y1="15.390875" x2="251.738225" y2="15.43329375" layer="94"/>
<rectangle x1="227.01640625" y1="15.43329375" x2="228.20004375" y2="15.4757125" layer="94"/>
<rectangle x1="230.57240625" y1="15.43329375" x2="231.75604375" y2="15.4757125" layer="94"/>
<rectangle x1="233.40704375" y1="15.43329375" x2="233.62040625" y2="15.4757125" layer="94"/>
<rectangle x1="235.736225" y1="15.43329375" x2="236.83604375" y2="15.4757125" layer="94"/>
<rectangle x1="239.546225" y1="15.43329375" x2="240.308225" y2="15.4757125" layer="94"/>
<rectangle x1="243.01840625" y1="15.43329375" x2="243.229225" y2="15.4757125" layer="94"/>
<rectangle x1="245.30440625" y1="15.43329375" x2="246.404225" y2="15.4757125" layer="94"/>
<rectangle x1="248.182225" y1="15.43329375" x2="248.90104375" y2="15.4757125" layer="94"/>
<rectangle x1="251.52740625" y1="15.43329375" x2="251.738225" y2="15.4757125" layer="94"/>
<rectangle x1="227.01640625" y1="15.4757125" x2="228.20004375" y2="15.517875" layer="94"/>
<rectangle x1="229.132225" y1="15.4757125" x2="229.640225" y2="15.517875" layer="94"/>
<rectangle x1="230.57240625" y1="15.4757125" x2="231.75604375" y2="15.517875" layer="94"/>
<rectangle x1="233.40704375" y1="15.4757125" x2="233.62040625" y2="15.517875" layer="94"/>
<rectangle x1="235.69304375" y1="15.4757125" x2="236.58204375" y2="15.517875" layer="94"/>
<rectangle x1="236.625225" y1="15.4757125" x2="236.83604375" y2="15.517875" layer="94"/>
<rectangle x1="239.546225" y1="15.4757125" x2="240.308225" y2="15.517875" layer="94"/>
<rectangle x1="243.01840625" y1="15.4757125" x2="243.229225" y2="15.517875" layer="94"/>
<rectangle x1="245.261225" y1="15.4757125" x2="246.150225" y2="15.517875" layer="94"/>
<rectangle x1="246.19340625" y1="15.4757125" x2="246.404225" y2="15.517875" layer="94"/>
<rectangle x1="248.182225" y1="15.4757125" x2="248.90104375" y2="15.517875" layer="94"/>
<rectangle x1="251.52740625" y1="15.4757125" x2="251.738225" y2="15.517875" layer="94"/>
<rectangle x1="227.01640625" y1="15.517875" x2="228.20004375" y2="15.56029375" layer="94"/>
<rectangle x1="228.96204375" y1="15.517875" x2="229.767225" y2="15.56029375" layer="94"/>
<rectangle x1="230.57240625" y1="15.517875" x2="231.75604375" y2="15.56029375" layer="94"/>
<rectangle x1="233.40704375" y1="15.517875" x2="233.62040625" y2="15.56029375" layer="94"/>
<rectangle x1="235.65240625" y1="15.517875" x2="236.54140625" y2="15.56029375" layer="94"/>
<rectangle x1="236.625225" y1="15.517875" x2="236.83604375" y2="15.56029375" layer="94"/>
<rectangle x1="239.546225" y1="15.517875" x2="240.308225" y2="15.56029375" layer="94"/>
<rectangle x1="243.01840625" y1="15.517875" x2="243.229225" y2="15.56029375" layer="94"/>
<rectangle x1="245.21804375" y1="15.517875" x2="246.10704375" y2="15.56029375" layer="94"/>
<rectangle x1="246.19340625" y1="15.517875" x2="246.404225" y2="15.56029375" layer="94"/>
<rectangle x1="248.182225" y1="15.517875" x2="248.90104375" y2="15.56029375" layer="94"/>
<rectangle x1="251.52740625" y1="15.517875" x2="251.738225" y2="15.56029375" layer="94"/>
<rectangle x1="227.01640625" y1="15.56029375" x2="228.20004375" y2="15.6027125" layer="94"/>
<rectangle x1="228.878225" y1="15.56029375" x2="229.85104375" y2="15.6027125" layer="94"/>
<rectangle x1="230.57240625" y1="15.56029375" x2="231.75604375" y2="15.6027125" layer="94"/>
<rectangle x1="233.40704375" y1="15.56029375" x2="233.62040625" y2="15.6027125" layer="94"/>
<rectangle x1="235.609225" y1="15.56029375" x2="236.498225" y2="15.6027125" layer="94"/>
<rectangle x1="236.625225" y1="15.56029375" x2="236.83604375" y2="15.6027125" layer="94"/>
<rectangle x1="239.546225" y1="15.56029375" x2="240.308225" y2="15.6027125" layer="94"/>
<rectangle x1="243.01840625" y1="15.56029375" x2="243.229225" y2="15.6027125" layer="94"/>
<rectangle x1="245.17740625" y1="15.56029375" x2="246.10704375" y2="15.6027125" layer="94"/>
<rectangle x1="246.19340625" y1="15.56029375" x2="246.404225" y2="15.6027125" layer="94"/>
<rectangle x1="248.182225" y1="15.56029375" x2="248.90104375" y2="15.6027125" layer="94"/>
<rectangle x1="251.52740625" y1="15.56029375" x2="251.738225" y2="15.6027125" layer="94"/>
<rectangle x1="227.01640625" y1="15.6027125" x2="228.20004375" y2="15.644875" layer="94"/>
<rectangle x1="228.83504375" y1="15.6027125" x2="229.93740625" y2="15.644875" layer="94"/>
<rectangle x1="230.57240625" y1="15.6027125" x2="231.75604375" y2="15.644875" layer="94"/>
<rectangle x1="233.40704375" y1="15.6027125" x2="233.62040625" y2="15.644875" layer="94"/>
<rectangle x1="235.56604375" y1="15.6027125" x2="236.498225" y2="15.644875" layer="94"/>
<rectangle x1="236.625225" y1="15.6027125" x2="236.83604375" y2="15.644875" layer="94"/>
<rectangle x1="239.546225" y1="15.6027125" x2="240.308225" y2="15.644875" layer="94"/>
<rectangle x1="243.01840625" y1="15.6027125" x2="243.229225" y2="15.644875" layer="94"/>
<rectangle x1="245.17740625" y1="15.6027125" x2="246.06640625" y2="15.644875" layer="94"/>
<rectangle x1="246.19340625" y1="15.6027125" x2="246.404225" y2="15.644875" layer="94"/>
<rectangle x1="248.182225" y1="15.6027125" x2="248.90104375" y2="15.644875" layer="94"/>
<rectangle x1="251.52740625" y1="15.6027125" x2="251.738225" y2="15.644875" layer="94"/>
<rectangle x1="231.08121875" y1="13.61246875" x2="232.26485625" y2="13.6548875" layer="94"/>
<rectangle x1="228.751225" y1="15.644875" x2="229.97804375" y2="15.68729375" layer="94"/>
<rectangle x1="230.57240625" y1="15.644875" x2="231.75604375" y2="15.68729375" layer="94"/>
<rectangle x1="233.40704375" y1="15.644875" x2="233.62040625" y2="15.68729375" layer="94"/>
<rectangle x1="235.56604375" y1="15.644875" x2="236.45504375" y2="15.68729375" layer="94"/>
<rectangle x1="236.625225" y1="15.644875" x2="236.83604375" y2="15.68729375" layer="94"/>
<rectangle x1="239.546225" y1="15.644875" x2="240.308225" y2="15.68729375" layer="94"/>
<rectangle x1="243.01840625" y1="15.644875" x2="243.229225" y2="15.68729375" layer="94"/>
<rectangle x1="245.134225" y1="15.644875" x2="246.023225" y2="15.68729375" layer="94"/>
<rectangle x1="246.19340625" y1="15.644875" x2="246.404225" y2="15.68729375" layer="94"/>
<rectangle x1="248.182225" y1="15.644875" x2="248.90104375" y2="15.68729375" layer="94"/>
<rectangle x1="251.52740625" y1="15.644875" x2="251.738225" y2="15.68729375" layer="94"/>
<rectangle x1="227.01640625" y1="15.68729375" x2="228.20004375" y2="15.7297125" layer="94"/>
<rectangle x1="228.70804375" y1="15.68729375" x2="230.021225" y2="15.7297125" layer="94"/>
<rectangle x1="230.57240625" y1="15.68729375" x2="231.75604375" y2="15.7297125" layer="94"/>
<rectangle x1="233.40704375" y1="15.68729375" x2="233.62040625" y2="15.7297125" layer="94"/>
<rectangle x1="235.52540625" y1="15.68729375" x2="236.45504375" y2="15.7297125" layer="94"/>
<rectangle x1="236.625225" y1="15.68729375" x2="236.83604375" y2="15.7297125" layer="94"/>
<rectangle x1="239.546225" y1="15.68729375" x2="240.308225" y2="15.7297125" layer="94"/>
<rectangle x1="243.01840625" y1="15.68729375" x2="243.229225" y2="15.7297125" layer="94"/>
<rectangle x1="245.134225" y1="15.68729375" x2="246.023225" y2="15.7297125" layer="94"/>
<rectangle x1="246.19340625" y1="15.68729375" x2="246.404225" y2="15.7297125" layer="94"/>
<rectangle x1="248.182225" y1="15.68729375" x2="248.90104375" y2="15.7297125" layer="94"/>
<rectangle x1="251.52740625" y1="15.68729375" x2="251.738225" y2="15.7297125" layer="94"/>
<rectangle x1="227.01640625" y1="15.7297125" x2="228.20004375" y2="15.771875" layer="94"/>
<rectangle x1="228.66740625" y1="15.7297125" x2="230.06440625" y2="15.771875" layer="94"/>
<rectangle x1="230.57240625" y1="15.7297125" x2="231.75604375" y2="15.771875" layer="94"/>
<rectangle x1="233.40704375" y1="15.7297125" x2="233.62040625" y2="15.771875" layer="94"/>
<rectangle x1="235.52540625" y1="15.7297125" x2="236.41440625" y2="15.771875" layer="94"/>
<rectangle x1="236.625225" y1="15.7297125" x2="236.83604375" y2="15.771875" layer="94"/>
<rectangle x1="239.546225" y1="15.7297125" x2="240.308225" y2="15.771875" layer="94"/>
<rectangle x1="243.01840625" y1="15.7297125" x2="243.229225" y2="15.771875" layer="94"/>
<rectangle x1="245.09104375" y1="15.7297125" x2="245.98004375" y2="15.771875" layer="94"/>
<rectangle x1="246.19340625" y1="15.7297125" x2="246.404225" y2="15.771875" layer="94"/>
<rectangle x1="248.182225" y1="15.7297125" x2="248.90104375" y2="15.771875" layer="94"/>
<rectangle x1="251.52740625" y1="15.7297125" x2="251.738225" y2="15.771875" layer="94"/>
<rectangle x1="227.01640625" y1="15.771875" x2="228.20004375" y2="15.81429375" layer="94"/>
<rectangle x1="228.624225" y1="15.771875" x2="230.10504375" y2="15.81429375" layer="94"/>
<rectangle x1="230.57240625" y1="15.771875" x2="231.75604375" y2="15.81429375" layer="94"/>
<rectangle x1="233.40704375" y1="15.771875" x2="233.62040625" y2="15.81429375" layer="94"/>
<rectangle x1="235.482225" y1="15.771875" x2="236.371225" y2="15.81429375" layer="94"/>
<rectangle x1="236.625225" y1="15.771875" x2="236.83604375" y2="15.81429375" layer="94"/>
<rectangle x1="239.546225" y1="15.771875" x2="240.308225" y2="15.81429375" layer="94"/>
<rectangle x1="243.01840625" y1="15.771875" x2="243.229225" y2="15.81429375" layer="94"/>
<rectangle x1="245.05040625" y1="15.771875" x2="245.93940625" y2="15.81429375" layer="94"/>
<rectangle x1="246.19340625" y1="15.771875" x2="246.404225" y2="15.81429375" layer="94"/>
<rectangle x1="248.182225" y1="15.771875" x2="248.90104375" y2="15.81429375" layer="94"/>
<rectangle x1="251.52740625" y1="15.771875" x2="251.738225" y2="15.81429375" layer="94"/>
<rectangle x1="227.01640625" y1="15.81429375" x2="228.20004375" y2="15.8567125" layer="94"/>
<rectangle x1="228.58104375" y1="15.81429375" x2="230.148225" y2="15.8567125" layer="94"/>
<rectangle x1="230.57240625" y1="15.81429375" x2="231.75604375" y2="15.8567125" layer="94"/>
<rectangle x1="233.40704375" y1="15.81429375" x2="233.62040625" y2="15.8567125" layer="94"/>
<rectangle x1="235.43904375" y1="15.81429375" x2="236.371225" y2="15.8567125" layer="94"/>
<rectangle x1="236.625225" y1="15.81429375" x2="236.83604375" y2="15.8567125" layer="94"/>
<rectangle x1="239.546225" y1="15.81429375" x2="240.308225" y2="15.8567125" layer="94"/>
<rectangle x1="243.01840625" y1="15.81429375" x2="243.229225" y2="15.8567125" layer="94"/>
<rectangle x1="245.007225" y1="15.81429375" x2="245.896225" y2="15.8567125" layer="94"/>
<rectangle x1="246.19340625" y1="15.81429375" x2="246.404225" y2="15.8567125" layer="94"/>
<rectangle x1="248.182225" y1="15.81429375" x2="248.90104375" y2="15.8567125" layer="94"/>
<rectangle x1="251.52740625" y1="15.81429375" x2="251.738225" y2="15.8567125" layer="94"/>
<rectangle x1="227.01640625" y1="15.8567125" x2="228.20004375" y2="15.898875" layer="94"/>
<rectangle x1="228.58104375" y1="15.8567125" x2="230.19140625" y2="15.898875" layer="94"/>
<rectangle x1="230.57240625" y1="15.8567125" x2="231.75604375" y2="15.898875" layer="94"/>
<rectangle x1="233.40704375" y1="15.8567125" x2="233.62040625" y2="15.898875" layer="94"/>
<rectangle x1="235.39840625" y1="15.8567125" x2="236.32804375" y2="15.898875" layer="94"/>
<rectangle x1="236.625225" y1="15.8567125" x2="236.83604375" y2="15.898875" layer="94"/>
<rectangle x1="239.546225" y1="15.8567125" x2="240.308225" y2="15.898875" layer="94"/>
<rectangle x1="243.01840625" y1="15.8567125" x2="243.229225" y2="15.898875" layer="94"/>
<rectangle x1="245.007225" y1="15.8567125" x2="245.896225" y2="15.898875" layer="94"/>
<rectangle x1="246.19340625" y1="15.8567125" x2="246.404225" y2="15.898875" layer="94"/>
<rectangle x1="248.182225" y1="15.8567125" x2="248.90104375" y2="15.898875" layer="94"/>
<rectangle x1="251.52740625" y1="15.8567125" x2="251.738225" y2="15.898875" layer="94"/>
<rectangle x1="227.01640625" y1="15.898875" x2="228.20004375" y2="15.94129375" layer="94"/>
<rectangle x1="228.54040625" y1="15.898875" x2="230.19140625" y2="15.94129375" layer="94"/>
<rectangle x1="230.57240625" y1="15.898875" x2="231.75604375" y2="15.94129375" layer="94"/>
<rectangle x1="233.40704375" y1="15.898875" x2="233.62040625" y2="15.94129375" layer="94"/>
<rectangle x1="235.39840625" y1="15.898875" x2="236.28740625" y2="15.94129375" layer="94"/>
<rectangle x1="236.625225" y1="15.898875" x2="236.83604375" y2="15.94129375" layer="94"/>
<rectangle x1="239.546225" y1="15.898875" x2="240.308225" y2="15.94129375" layer="94"/>
<rectangle x1="243.01840625" y1="15.898875" x2="243.229225" y2="15.94129375" layer="94"/>
<rectangle x1="244.96404375" y1="15.898875" x2="245.85304375" y2="15.94129375" layer="94"/>
<rectangle x1="246.19340625" y1="15.898875" x2="246.404225" y2="15.94129375" layer="94"/>
<rectangle x1="248.182225" y1="15.898875" x2="248.90104375" y2="15.94129375" layer="94"/>
<rectangle x1="251.52740625" y1="15.898875" x2="251.738225" y2="15.94129375" layer="94"/>
<rectangle x1="227.01640625" y1="15.94129375" x2="228.20004375" y2="15.9837125" layer="94"/>
<rectangle x1="228.54040625" y1="15.94129375" x2="230.23204375" y2="15.9837125" layer="94"/>
<rectangle x1="230.57240625" y1="15.94129375" x2="231.75604375" y2="15.9837125" layer="94"/>
<rectangle x1="233.40704375" y1="15.94129375" x2="233.62040625" y2="15.9837125" layer="94"/>
<rectangle x1="235.355225" y1="15.94129375" x2="236.244225" y2="15.9837125" layer="94"/>
<rectangle x1="236.625225" y1="15.94129375" x2="236.83604375" y2="15.9837125" layer="94"/>
<rectangle x1="239.546225" y1="15.94129375" x2="240.308225" y2="15.9837125" layer="94"/>
<rectangle x1="243.01840625" y1="15.94129375" x2="243.229225" y2="15.9837125" layer="94"/>
<rectangle x1="244.92340625" y1="15.94129375" x2="245.85304375" y2="15.9837125" layer="94"/>
<rectangle x1="246.19340625" y1="15.94129375" x2="246.404225" y2="15.9837125" layer="94"/>
<rectangle x1="248.182225" y1="15.94129375" x2="248.90104375" y2="15.9837125" layer="94"/>
<rectangle x1="251.52740625" y1="15.94129375" x2="251.738225" y2="15.9837125" layer="94"/>
<rectangle x1="227.01640625" y1="15.9837125" x2="228.20004375" y2="16.025875" layer="94"/>
<rectangle x1="228.497225" y1="15.9837125" x2="230.23204375" y2="16.025875" layer="94"/>
<rectangle x1="230.57240625" y1="15.9837125" x2="231.75604375" y2="16.025875" layer="94"/>
<rectangle x1="233.40704375" y1="15.9837125" x2="233.62040625" y2="16.025875" layer="94"/>
<rectangle x1="235.31204375" y1="15.9837125" x2="236.244225" y2="16.025875" layer="94"/>
<rectangle x1="236.625225" y1="15.9837125" x2="236.83604375" y2="16.025875" layer="94"/>
<rectangle x1="239.546225" y1="15.9837125" x2="240.308225" y2="16.025875" layer="94"/>
<rectangle x1="243.01840625" y1="15.9837125" x2="243.229225" y2="16.025875" layer="94"/>
<rectangle x1="244.92340625" y1="15.9837125" x2="245.81240625" y2="16.025875" layer="94"/>
<rectangle x1="246.19340625" y1="15.9837125" x2="246.404225" y2="16.025875" layer="94"/>
<rectangle x1="248.182225" y1="15.9837125" x2="248.90104375" y2="16.025875" layer="94"/>
<rectangle x1="251.52740625" y1="15.9837125" x2="251.738225" y2="16.025875" layer="94"/>
<rectangle x1="227.01640625" y1="16.025875" x2="228.20004375" y2="16.06829375" layer="94"/>
<rectangle x1="228.497225" y1="16.025875" x2="230.275225" y2="16.06829375" layer="94"/>
<rectangle x1="230.57240625" y1="16.025875" x2="231.75604375" y2="16.06829375" layer="94"/>
<rectangle x1="233.40704375" y1="16.025875" x2="233.62040625" y2="16.06829375" layer="94"/>
<rectangle x1="235.31204375" y1="16.025875" x2="236.20104375" y2="16.06829375" layer="94"/>
<rectangle x1="236.625225" y1="16.025875" x2="236.83604375" y2="16.06829375" layer="94"/>
<rectangle x1="239.546225" y1="16.025875" x2="240.308225" y2="16.06829375" layer="94"/>
<rectangle x1="243.01840625" y1="16.025875" x2="243.229225" y2="16.06829375" layer="94"/>
<rectangle x1="244.880225" y1="16.025875" x2="245.769225" y2="16.06829375" layer="94"/>
<rectangle x1="246.19340625" y1="16.025875" x2="246.404225" y2="16.06829375" layer="94"/>
<rectangle x1="248.182225" y1="16.025875" x2="248.90104375" y2="16.06829375" layer="94"/>
<rectangle x1="251.52740625" y1="16.025875" x2="251.738225" y2="16.06829375" layer="94"/>
<rectangle x1="227.01640625" y1="16.06829375" x2="228.20004375" y2="16.1107125" layer="94"/>
<rectangle x1="228.45404375" y1="16.06829375" x2="230.275225" y2="16.1107125" layer="94"/>
<rectangle x1="230.57240625" y1="16.06829375" x2="231.75604375" y2="16.1107125" layer="94"/>
<rectangle x1="233.40704375" y1="16.06829375" x2="233.62040625" y2="16.1107125" layer="94"/>
<rectangle x1="235.27140625" y1="16.06829375" x2="236.16040625" y2="16.1107125" layer="94"/>
<rectangle x1="236.625225" y1="16.06829375" x2="236.83604375" y2="16.1107125" layer="94"/>
<rectangle x1="239.546225" y1="16.06829375" x2="240.308225" y2="16.1107125" layer="94"/>
<rectangle x1="243.01840625" y1="16.06829375" x2="243.229225" y2="16.1107125" layer="94"/>
<rectangle x1="244.83704375" y1="16.06829375" x2="245.769225" y2="16.1107125" layer="94"/>
<rectangle x1="246.19340625" y1="16.06829375" x2="246.404225" y2="16.1107125" layer="94"/>
<rectangle x1="248.182225" y1="16.06829375" x2="248.90104375" y2="16.1107125" layer="94"/>
<rectangle x1="251.52740625" y1="16.06829375" x2="251.738225" y2="16.1107125" layer="94"/>
<rectangle x1="227.01640625" y1="16.1107125" x2="228.20004375" y2="16.152875" layer="94"/>
<rectangle x1="228.45404375" y1="16.1107125" x2="230.31840625" y2="16.152875" layer="94"/>
<rectangle x1="230.57240625" y1="16.1107125" x2="231.75604375" y2="16.152875" layer="94"/>
<rectangle x1="233.40704375" y1="16.1107125" x2="233.62040625" y2="16.152875" layer="94"/>
<rectangle x1="235.228225" y1="16.1107125" x2="236.16040625" y2="16.152875" layer="94"/>
<rectangle x1="236.625225" y1="16.1107125" x2="236.83604375" y2="16.152875" layer="94"/>
<rectangle x1="239.546225" y1="16.1107125" x2="240.308225" y2="16.152875" layer="94"/>
<rectangle x1="243.01840625" y1="16.1107125" x2="243.229225" y2="16.152875" layer="94"/>
<rectangle x1="244.79640625" y1="16.1107125" x2="245.72604375" y2="16.152875" layer="94"/>
<rectangle x1="246.19340625" y1="16.1107125" x2="246.404225" y2="16.152875" layer="94"/>
<rectangle x1="248.182225" y1="16.1107125" x2="248.90104375" y2="16.152875" layer="94"/>
<rectangle x1="251.52740625" y1="16.1107125" x2="251.738225" y2="16.152875" layer="94"/>
<rectangle x1="227.01640625" y1="16.152875" x2="228.20004375" y2="16.19529375" layer="94"/>
<rectangle x1="228.41340625" y1="16.152875" x2="230.31840625" y2="16.19529375" layer="94"/>
<rectangle x1="230.57240625" y1="16.152875" x2="231.75604375" y2="16.19529375" layer="94"/>
<rectangle x1="233.40704375" y1="16.152875" x2="233.62040625" y2="16.19529375" layer="94"/>
<rectangle x1="235.228225" y1="16.152875" x2="236.117225" y2="16.19529375" layer="94"/>
<rectangle x1="236.625225" y1="16.152875" x2="236.83604375" y2="16.19529375" layer="94"/>
<rectangle x1="239.546225" y1="16.152875" x2="240.308225" y2="16.19529375" layer="94"/>
<rectangle x1="243.01840625" y1="16.152875" x2="243.229225" y2="16.19529375" layer="94"/>
<rectangle x1="244.79640625" y1="16.152875" x2="245.68540625" y2="16.19529375" layer="94"/>
<rectangle x1="246.19340625" y1="16.152875" x2="246.404225" y2="16.19529375" layer="94"/>
<rectangle x1="248.182225" y1="16.152875" x2="248.90104375" y2="16.19529375" layer="94"/>
<rectangle x1="251.52740625" y1="16.152875" x2="251.738225" y2="16.19529375" layer="94"/>
<rectangle x1="227.01640625" y1="16.19529375" x2="228.20004375" y2="16.2377125" layer="94"/>
<rectangle x1="228.41340625" y1="16.19529375" x2="230.31840625" y2="16.2377125" layer="94"/>
<rectangle x1="230.57240625" y1="16.19529375" x2="231.75604375" y2="16.2377125" layer="94"/>
<rectangle x1="233.40704375" y1="16.19529375" x2="233.62040625" y2="16.2377125" layer="94"/>
<rectangle x1="235.18504375" y1="16.19529375" x2="236.07404375" y2="16.2377125" layer="94"/>
<rectangle x1="236.625225" y1="16.19529375" x2="236.83604375" y2="16.2377125" layer="94"/>
<rectangle x1="239.546225" y1="16.19529375" x2="240.308225" y2="16.2377125" layer="94"/>
<rectangle x1="243.01840625" y1="16.19529375" x2="243.229225" y2="16.2377125" layer="94"/>
<rectangle x1="244.753225" y1="16.19529375" x2="245.642225" y2="16.2377125" layer="94"/>
<rectangle x1="246.19340625" y1="16.19529375" x2="246.404225" y2="16.2377125" layer="94"/>
<rectangle x1="248.182225" y1="16.19529375" x2="248.90104375" y2="16.2377125" layer="94"/>
<rectangle x1="251.52740625" y1="16.19529375" x2="251.738225" y2="16.2377125" layer="94"/>
<rectangle x1="227.01640625" y1="16.2377125" x2="228.20004375" y2="16.279875" layer="94"/>
<rectangle x1="228.41340625" y1="16.2377125" x2="230.31840625" y2="16.279875" layer="94"/>
<rectangle x1="230.57240625" y1="16.2377125" x2="231.75604375" y2="16.279875" layer="94"/>
<rectangle x1="233.40704375" y1="16.2377125" x2="233.62040625" y2="16.279875" layer="94"/>
<rectangle x1="235.14440625" y1="16.2377125" x2="236.03340625" y2="16.279875" layer="94"/>
<rectangle x1="236.625225" y1="16.2377125" x2="236.83604375" y2="16.279875" layer="94"/>
<rectangle x1="239.546225" y1="16.2377125" x2="240.308225" y2="16.279875" layer="94"/>
<rectangle x1="243.01840625" y1="16.2377125" x2="243.229225" y2="16.279875" layer="94"/>
<rectangle x1="244.71004375" y1="16.2377125" x2="245.642225" y2="16.279875" layer="94"/>
<rectangle x1="246.19340625" y1="16.2377125" x2="246.404225" y2="16.279875" layer="94"/>
<rectangle x1="248.182225" y1="16.2377125" x2="248.90104375" y2="16.279875" layer="94"/>
<rectangle x1="251.52740625" y1="16.2377125" x2="251.738225" y2="16.279875" layer="94"/>
<rectangle x1="227.01640625" y1="16.279875" x2="228.20004375" y2="16.32229375" layer="94"/>
<rectangle x1="228.41340625" y1="16.279875" x2="230.31840625" y2="16.32229375" layer="94"/>
<rectangle x1="230.57240625" y1="16.279875" x2="231.75604375" y2="16.32229375" layer="94"/>
<rectangle x1="233.40704375" y1="16.279875" x2="233.62040625" y2="16.32229375" layer="94"/>
<rectangle x1="235.14440625" y1="16.279875" x2="236.03340625" y2="16.32229375" layer="94"/>
<rectangle x1="236.625225" y1="16.279875" x2="236.83604375" y2="16.32229375" layer="94"/>
<rectangle x1="239.546225" y1="16.279875" x2="240.308225" y2="16.32229375" layer="94"/>
<rectangle x1="243.01840625" y1="16.279875" x2="243.229225" y2="16.32229375" layer="94"/>
<rectangle x1="244.71004375" y1="16.279875" x2="245.59904375" y2="16.32229375" layer="94"/>
<rectangle x1="246.19340625" y1="16.279875" x2="246.404225" y2="16.32229375" layer="94"/>
<rectangle x1="248.182225" y1="16.279875" x2="248.90104375" y2="16.32229375" layer="94"/>
<rectangle x1="251.52740625" y1="16.279875" x2="251.738225" y2="16.32229375" layer="94"/>
<rectangle x1="227.01640625" y1="16.32229375" x2="228.20004375" y2="16.3647125" layer="94"/>
<rectangle x1="228.41340625" y1="16.32229375" x2="230.35904375" y2="16.3647125" layer="94"/>
<rectangle x1="230.57240625" y1="16.32229375" x2="231.75604375" y2="16.3647125" layer="94"/>
<rectangle x1="233.40704375" y1="16.32229375" x2="233.62040625" y2="16.3647125" layer="94"/>
<rectangle x1="235.101225" y1="16.32229375" x2="235.990225" y2="16.3647125" layer="94"/>
<rectangle x1="236.625225" y1="16.32229375" x2="236.83604375" y2="16.3647125" layer="94"/>
<rectangle x1="239.546225" y1="16.32229375" x2="240.308225" y2="16.3647125" layer="94"/>
<rectangle x1="243.01840625" y1="16.32229375" x2="243.229225" y2="16.3647125" layer="94"/>
<rectangle x1="244.66940625" y1="16.32229375" x2="245.55840625" y2="16.3647125" layer="94"/>
<rectangle x1="246.19340625" y1="16.32229375" x2="246.404225" y2="16.3647125" layer="94"/>
<rectangle x1="248.182225" y1="16.32229375" x2="248.90104375" y2="16.3647125" layer="94"/>
<rectangle x1="251.52740625" y1="16.32229375" x2="251.738225" y2="16.3647125" layer="94"/>
<rectangle x1="227.01640625" y1="16.3647125" x2="228.20004375" y2="16.406875" layer="94"/>
<rectangle x1="228.41340625" y1="16.3647125" x2="230.35904375" y2="16.406875" layer="94"/>
<rectangle x1="230.57240625" y1="16.3647125" x2="231.75604375" y2="16.406875" layer="94"/>
<rectangle x1="233.40704375" y1="16.3647125" x2="233.62040625" y2="16.406875" layer="94"/>
<rectangle x1="235.05804375" y1="16.3647125" x2="235.990225" y2="16.406875" layer="94"/>
<rectangle x1="236.625225" y1="16.3647125" x2="236.83604375" y2="16.406875" layer="94"/>
<rectangle x1="239.546225" y1="16.3647125" x2="240.308225" y2="16.406875" layer="94"/>
<rectangle x1="243.01840625" y1="16.3647125" x2="243.229225" y2="16.406875" layer="94"/>
<rectangle x1="244.66940625" y1="16.3647125" x2="245.55840625" y2="16.406875" layer="94"/>
<rectangle x1="246.19340625" y1="16.3647125" x2="246.404225" y2="16.406875" layer="94"/>
<rectangle x1="248.182225" y1="16.3647125" x2="248.90104375" y2="16.406875" layer="94"/>
<rectangle x1="251.52740625" y1="16.3647125" x2="251.738225" y2="16.406875" layer="94"/>
<rectangle x1="227.01640625" y1="16.406875" x2="228.20004375" y2="16.44929375" layer="94"/>
<rectangle x1="228.41340625" y1="16.406875" x2="230.35904375" y2="16.44929375" layer="94"/>
<rectangle x1="230.57240625" y1="16.406875" x2="231.75604375" y2="16.44929375" layer="94"/>
<rectangle x1="233.40704375" y1="16.406875" x2="233.62040625" y2="16.44929375" layer="94"/>
<rectangle x1="235.05804375" y1="16.406875" x2="235.94704375" y2="16.44929375" layer="94"/>
<rectangle x1="236.625225" y1="16.406875" x2="236.83604375" y2="16.44929375" layer="94"/>
<rectangle x1="239.546225" y1="16.406875" x2="240.308225" y2="16.44929375" layer="94"/>
<rectangle x1="243.01840625" y1="16.406875" x2="243.229225" y2="16.44929375" layer="94"/>
<rectangle x1="244.626225" y1="16.406875" x2="245.515225" y2="16.44929375" layer="94"/>
<rectangle x1="246.19340625" y1="16.406875" x2="246.404225" y2="16.44929375" layer="94"/>
<rectangle x1="248.182225" y1="16.406875" x2="248.90104375" y2="16.44929375" layer="94"/>
<rectangle x1="251.52740625" y1="16.406875" x2="251.738225" y2="16.44929375" layer="94"/>
<rectangle x1="227.01640625" y1="16.44929375" x2="228.20004375" y2="16.4917125" layer="94"/>
<rectangle x1="228.41340625" y1="16.44929375" x2="230.35904375" y2="16.4917125" layer="94"/>
<rectangle x1="230.57240625" y1="16.44929375" x2="231.75604375" y2="16.4917125" layer="94"/>
<rectangle x1="233.40704375" y1="16.44929375" x2="233.62040625" y2="16.4917125" layer="94"/>
<rectangle x1="235.01740625" y1="16.44929375" x2="235.90640625" y2="16.4917125" layer="94"/>
<rectangle x1="236.625225" y1="16.44929375" x2="236.83604375" y2="16.4917125" layer="94"/>
<rectangle x1="239.546225" y1="16.44929375" x2="240.308225" y2="16.4917125" layer="94"/>
<rectangle x1="243.01840625" y1="16.44929375" x2="243.229225" y2="16.4917125" layer="94"/>
<rectangle x1="244.58304375" y1="16.44929375" x2="245.47204375" y2="16.4917125" layer="94"/>
<rectangle x1="246.19340625" y1="16.44929375" x2="246.404225" y2="16.4917125" layer="94"/>
<rectangle x1="248.182225" y1="16.44929375" x2="248.90104375" y2="16.4917125" layer="94"/>
<rectangle x1="251.52740625" y1="16.44929375" x2="251.738225" y2="16.4917125" layer="94"/>
<rectangle x1="227.01640625" y1="16.4917125" x2="228.20004375" y2="16.533875" layer="94"/>
<rectangle x1="228.41340625" y1="16.4917125" x2="230.35904375" y2="16.533875" layer="94"/>
<rectangle x1="230.57240625" y1="16.4917125" x2="231.75604375" y2="16.533875" layer="94"/>
<rectangle x1="233.40704375" y1="16.4917125" x2="233.62040625" y2="16.533875" layer="94"/>
<rectangle x1="234.974225" y1="16.4917125" x2="235.90640625" y2="16.533875" layer="94"/>
<rectangle x1="236.625225" y1="16.4917125" x2="236.83604375" y2="16.533875" layer="94"/>
<rectangle x1="239.546225" y1="16.4917125" x2="240.308225" y2="16.533875" layer="94"/>
<rectangle x1="243.01840625" y1="16.4917125" x2="243.229225" y2="16.533875" layer="94"/>
<rectangle x1="244.54240625" y1="16.4917125" x2="245.43140625" y2="16.533875" layer="94"/>
<rectangle x1="246.19340625" y1="16.4917125" x2="246.404225" y2="16.533875" layer="94"/>
<rectangle x1="248.182225" y1="16.4917125" x2="248.90104375" y2="16.533875" layer="94"/>
<rectangle x1="251.52740625" y1="16.4917125" x2="251.738225" y2="16.533875" layer="94"/>
<rectangle x1="227.01640625" y1="16.533875" x2="228.20004375" y2="16.57629375" layer="94"/>
<rectangle x1="228.41340625" y1="16.533875" x2="230.31840625" y2="16.57629375" layer="94"/>
<rectangle x1="230.57240625" y1="16.533875" x2="231.75604375" y2="16.57629375" layer="94"/>
<rectangle x1="233.40704375" y1="16.533875" x2="233.62040625" y2="16.57629375" layer="94"/>
<rectangle x1="234.93104375" y1="16.533875" x2="235.863225" y2="16.57629375" layer="94"/>
<rectangle x1="236.625225" y1="16.533875" x2="236.83604375" y2="16.57629375" layer="94"/>
<rectangle x1="239.546225" y1="16.533875" x2="240.308225" y2="16.57629375" layer="94"/>
<rectangle x1="243.01840625" y1="16.533875" x2="243.229225" y2="16.57629375" layer="94"/>
<rectangle x1="244.54240625" y1="16.533875" x2="245.43140625" y2="16.57629375" layer="94"/>
<rectangle x1="246.19340625" y1="16.533875" x2="246.404225" y2="16.57629375" layer="94"/>
<rectangle x1="248.182225" y1="16.533875" x2="248.90104375" y2="16.57629375" layer="94"/>
<rectangle x1="251.52740625" y1="16.533875" x2="251.738225" y2="16.57629375" layer="94"/>
<rectangle x1="227.01640625" y1="16.57629375" x2="228.20004375" y2="16.6187125" layer="94"/>
<rectangle x1="228.41340625" y1="16.57629375" x2="230.31840625" y2="16.6187125" layer="94"/>
<rectangle x1="230.57240625" y1="16.57629375" x2="231.75604375" y2="16.6187125" layer="94"/>
<rectangle x1="233.40704375" y1="16.57629375" x2="233.62040625" y2="16.6187125" layer="94"/>
<rectangle x1="234.93104375" y1="16.57629375" x2="235.82004375" y2="16.6187125" layer="94"/>
<rectangle x1="236.625225" y1="16.57629375" x2="236.83604375" y2="16.6187125" layer="94"/>
<rectangle x1="239.546225" y1="16.57629375" x2="240.308225" y2="16.6187125" layer="94"/>
<rectangle x1="243.01840625" y1="16.57629375" x2="243.229225" y2="16.6187125" layer="94"/>
<rectangle x1="244.499225" y1="16.57629375" x2="245.388225" y2="16.6187125" layer="94"/>
<rectangle x1="246.19340625" y1="16.57629375" x2="246.404225" y2="16.6187125" layer="94"/>
<rectangle x1="248.182225" y1="16.57629375" x2="248.90104375" y2="16.6187125" layer="94"/>
<rectangle x1="251.52740625" y1="16.57629375" x2="251.738225" y2="16.6187125" layer="94"/>
<rectangle x1="227.01640625" y1="16.6187125" x2="228.20004375" y2="16.660875" layer="94"/>
<rectangle x1="228.41340625" y1="16.6187125" x2="230.31840625" y2="16.660875" layer="94"/>
<rectangle x1="230.57240625" y1="16.6187125" x2="231.75604375" y2="16.660875" layer="94"/>
<rectangle x1="233.40704375" y1="16.6187125" x2="233.62040625" y2="16.660875" layer="94"/>
<rectangle x1="234.89040625" y1="16.6187125" x2="235.77940625" y2="16.660875" layer="94"/>
<rectangle x1="236.625225" y1="16.6187125" x2="236.83604375" y2="16.660875" layer="94"/>
<rectangle x1="239.546225" y1="16.6187125" x2="240.308225" y2="16.660875" layer="94"/>
<rectangle x1="243.01840625" y1="16.6187125" x2="243.229225" y2="16.660875" layer="94"/>
<rectangle x1="244.45604375" y1="16.6187125" x2="245.388225" y2="16.660875" layer="94"/>
<rectangle x1="246.19340625" y1="16.6187125" x2="246.404225" y2="16.660875" layer="94"/>
<rectangle x1="248.182225" y1="16.6187125" x2="248.90104375" y2="16.660875" layer="94"/>
<rectangle x1="251.52740625" y1="16.6187125" x2="251.738225" y2="16.660875" layer="94"/>
<rectangle x1="227.01640625" y1="16.660875" x2="228.20004375" y2="16.70329375" layer="94"/>
<rectangle x1="228.41340625" y1="16.660875" x2="230.31840625" y2="16.70329375" layer="94"/>
<rectangle x1="230.57240625" y1="16.660875" x2="231.75604375" y2="16.70329375" layer="94"/>
<rectangle x1="233.40704375" y1="16.660875" x2="233.62040625" y2="16.70329375" layer="94"/>
<rectangle x1="234.847225" y1="16.660875" x2="235.77940625" y2="16.70329375" layer="94"/>
<rectangle x1="236.625225" y1="16.660875" x2="236.83604375" y2="16.70329375" layer="94"/>
<rectangle x1="239.546225" y1="16.660875" x2="240.308225" y2="16.70329375" layer="94"/>
<rectangle x1="243.01840625" y1="16.660875" x2="243.229225" y2="16.70329375" layer="94"/>
<rectangle x1="244.45604375" y1="16.660875" x2="245.34504375" y2="16.70329375" layer="94"/>
<rectangle x1="246.19340625" y1="16.660875" x2="246.404225" y2="16.70329375" layer="94"/>
<rectangle x1="248.182225" y1="16.660875" x2="248.90104375" y2="16.70329375" layer="94"/>
<rectangle x1="251.52740625" y1="16.660875" x2="251.738225" y2="16.70329375" layer="94"/>
<rectangle x1="227.01640625" y1="16.70329375" x2="228.20004375" y2="16.7457125" layer="94"/>
<rectangle x1="228.45404375" y1="16.70329375" x2="230.31840625" y2="16.7457125" layer="94"/>
<rectangle x1="230.57240625" y1="16.70329375" x2="231.75604375" y2="16.7457125" layer="94"/>
<rectangle x1="233.40704375" y1="16.70329375" x2="233.62040625" y2="16.7457125" layer="94"/>
<rectangle x1="234.847225" y1="16.70329375" x2="235.736225" y2="16.7457125" layer="94"/>
<rectangle x1="236.625225" y1="16.70329375" x2="236.83604375" y2="16.7457125" layer="94"/>
<rectangle x1="239.546225" y1="16.70329375" x2="240.308225" y2="16.7457125" layer="94"/>
<rectangle x1="243.01840625" y1="16.70329375" x2="243.229225" y2="16.7457125" layer="94"/>
<rectangle x1="244.41540625" y1="16.70329375" x2="245.30440625" y2="16.7457125" layer="94"/>
<rectangle x1="246.19340625" y1="16.70329375" x2="246.404225" y2="16.7457125" layer="94"/>
<rectangle x1="248.182225" y1="16.70329375" x2="248.90104375" y2="16.7457125" layer="94"/>
<rectangle x1="251.52740625" y1="16.70329375" x2="251.738225" y2="16.7457125" layer="94"/>
<rectangle x1="227.01640625" y1="16.7457125" x2="228.20004375" y2="16.787875" layer="94"/>
<rectangle x1="228.45404375" y1="16.7457125" x2="230.275225" y2="16.787875" layer="94"/>
<rectangle x1="230.57240625" y1="16.7457125" x2="231.75604375" y2="16.787875" layer="94"/>
<rectangle x1="233.40704375" y1="16.7457125" x2="233.62040625" y2="16.787875" layer="94"/>
<rectangle x1="234.80404375" y1="16.7457125" x2="235.69304375" y2="16.787875" layer="94"/>
<rectangle x1="236.625225" y1="16.7457125" x2="236.83604375" y2="16.787875" layer="94"/>
<rectangle x1="239.546225" y1="16.7457125" x2="240.308225" y2="16.787875" layer="94"/>
<rectangle x1="243.01840625" y1="16.7457125" x2="243.229225" y2="16.787875" layer="94"/>
<rectangle x1="244.372225" y1="16.7457125" x2="245.261225" y2="16.787875" layer="94"/>
<rectangle x1="246.19340625" y1="16.7457125" x2="246.404225" y2="16.787875" layer="94"/>
<rectangle x1="248.182225" y1="16.7457125" x2="248.90104375" y2="16.787875" layer="94"/>
<rectangle x1="251.52740625" y1="16.7457125" x2="251.738225" y2="16.787875" layer="94"/>
<rectangle x1="227.01640625" y1="16.787875" x2="228.20004375" y2="16.83029375" layer="94"/>
<rectangle x1="228.497225" y1="16.787875" x2="230.275225" y2="16.83029375" layer="94"/>
<rectangle x1="230.57240625" y1="16.787875" x2="231.75604375" y2="16.83029375" layer="94"/>
<rectangle x1="233.40704375" y1="16.787875" x2="233.62040625" y2="16.83029375" layer="94"/>
<rectangle x1="234.76340625" y1="16.787875" x2="235.69304375" y2="16.83029375" layer="94"/>
<rectangle x1="236.625225" y1="16.787875" x2="236.83604375" y2="16.83029375" layer="94"/>
<rectangle x1="239.546225" y1="16.787875" x2="240.308225" y2="16.83029375" layer="94"/>
<rectangle x1="243.01840625" y1="16.787875" x2="243.229225" y2="16.83029375" layer="94"/>
<rectangle x1="244.32904375" y1="16.787875" x2="245.261225" y2="16.83029375" layer="94"/>
<rectangle x1="246.19340625" y1="16.787875" x2="246.404225" y2="16.83029375" layer="94"/>
<rectangle x1="248.182225" y1="16.787875" x2="248.90104375" y2="16.83029375" layer="94"/>
<rectangle x1="251.52740625" y1="16.787875" x2="251.738225" y2="16.83029375" layer="94"/>
<rectangle x1="227.01640625" y1="16.83029375" x2="228.20004375" y2="16.8727125" layer="94"/>
<rectangle x1="228.497225" y1="16.83029375" x2="230.275225" y2="16.8727125" layer="94"/>
<rectangle x1="230.57240625" y1="16.83029375" x2="231.75604375" y2="16.8727125" layer="94"/>
<rectangle x1="233.40704375" y1="16.83029375" x2="233.62040625" y2="16.8727125" layer="94"/>
<rectangle x1="234.76340625" y1="16.83029375" x2="235.65240625" y2="16.8727125" layer="94"/>
<rectangle x1="236.625225" y1="16.83029375" x2="236.83604375" y2="16.8727125" layer="94"/>
<rectangle x1="239.546225" y1="16.83029375" x2="240.308225" y2="16.8727125" layer="94"/>
<rectangle x1="243.01840625" y1="16.83029375" x2="243.229225" y2="16.8727125" layer="94"/>
<rectangle x1="244.32904375" y1="16.83029375" x2="245.21804375" y2="16.8727125" layer="94"/>
<rectangle x1="246.19340625" y1="16.83029375" x2="246.404225" y2="16.8727125" layer="94"/>
<rectangle x1="248.182225" y1="16.83029375" x2="248.90104375" y2="16.8727125" layer="94"/>
<rectangle x1="251.52740625" y1="16.83029375" x2="251.738225" y2="16.8727125" layer="94"/>
<rectangle x1="227.01640625" y1="16.8727125" x2="228.20004375" y2="16.914875" layer="94"/>
<rectangle x1="228.54040625" y1="16.8727125" x2="230.23204375" y2="16.914875" layer="94"/>
<rectangle x1="230.57240625" y1="16.8727125" x2="231.75604375" y2="16.914875" layer="94"/>
<rectangle x1="233.40704375" y1="16.8727125" x2="233.62040625" y2="16.914875" layer="94"/>
<rectangle x1="234.720225" y1="16.8727125" x2="235.609225" y2="16.914875" layer="94"/>
<rectangle x1="236.625225" y1="16.8727125" x2="236.83604375" y2="16.914875" layer="94"/>
<rectangle x1="239.546225" y1="16.8727125" x2="240.308225" y2="16.914875" layer="94"/>
<rectangle x1="243.01840625" y1="16.8727125" x2="243.229225" y2="16.914875" layer="94"/>
<rectangle x1="244.28840625" y1="16.8727125" x2="245.17740625" y2="16.914875" layer="94"/>
<rectangle x1="246.19340625" y1="16.8727125" x2="246.404225" y2="16.914875" layer="94"/>
<rectangle x1="248.182225" y1="16.8727125" x2="248.90104375" y2="16.914875" layer="94"/>
<rectangle x1="251.52740625" y1="16.8727125" x2="251.738225" y2="16.914875" layer="94"/>
<rectangle x1="227.01640625" y1="16.914875" x2="228.20004375" y2="16.95729375" layer="94"/>
<rectangle x1="228.54040625" y1="16.914875" x2="230.19140625" y2="16.95729375" layer="94"/>
<rectangle x1="230.57240625" y1="16.914875" x2="231.75604375" y2="16.95729375" layer="94"/>
<rectangle x1="233.40704375" y1="16.914875" x2="233.62040625" y2="16.95729375" layer="94"/>
<rectangle x1="234.67704375" y1="16.914875" x2="235.56604375" y2="16.95729375" layer="94"/>
<rectangle x1="236.625225" y1="16.914875" x2="236.83604375" y2="16.95729375" layer="94"/>
<rectangle x1="239.546225" y1="16.914875" x2="240.308225" y2="16.95729375" layer="94"/>
<rectangle x1="243.01840625" y1="16.914875" x2="243.229225" y2="16.95729375" layer="94"/>
<rectangle x1="244.28840625" y1="16.914875" x2="245.17740625" y2="16.95729375" layer="94"/>
<rectangle x1="246.19340625" y1="16.914875" x2="246.404225" y2="16.95729375" layer="94"/>
<rectangle x1="248.182225" y1="16.914875" x2="248.90104375" y2="16.95729375" layer="94"/>
<rectangle x1="251.52740625" y1="16.914875" x2="251.738225" y2="16.95729375" layer="94"/>
<rectangle x1="227.01640625" y1="16.95729375" x2="228.20004375" y2="16.9997125" layer="94"/>
<rectangle x1="228.58104375" y1="16.95729375" x2="230.148225" y2="16.9997125" layer="94"/>
<rectangle x1="230.57240625" y1="16.95729375" x2="231.75604375" y2="16.9997125" layer="94"/>
<rectangle x1="233.40704375" y1="16.95729375" x2="233.62040625" y2="16.9997125" layer="94"/>
<rectangle x1="234.67704375" y1="16.95729375" x2="235.56604375" y2="16.9997125" layer="94"/>
<rectangle x1="236.625225" y1="16.95729375" x2="236.83604375" y2="16.9997125" layer="94"/>
<rectangle x1="239.546225" y1="16.95729375" x2="240.308225" y2="16.9997125" layer="94"/>
<rectangle x1="243.01840625" y1="16.95729375" x2="243.229225" y2="16.9997125" layer="94"/>
<rectangle x1="244.245225" y1="16.95729375" x2="245.134225" y2="16.9997125" layer="94"/>
<rectangle x1="246.19340625" y1="16.95729375" x2="246.404225" y2="16.9997125" layer="94"/>
<rectangle x1="248.182225" y1="16.95729375" x2="248.90104375" y2="16.9997125" layer="94"/>
<rectangle x1="251.52740625" y1="16.95729375" x2="251.738225" y2="16.9997125" layer="94"/>
<rectangle x1="227.01640625" y1="16.9997125" x2="228.20004375" y2="17.041875" layer="94"/>
<rectangle x1="228.624225" y1="16.9997125" x2="230.148225" y2="17.041875" layer="94"/>
<rectangle x1="230.57240625" y1="16.9997125" x2="231.75604375" y2="17.041875" layer="94"/>
<rectangle x1="233.40704375" y1="16.9997125" x2="233.62040625" y2="17.041875" layer="94"/>
<rectangle x1="234.63640625" y1="16.9997125" x2="235.52540625" y2="17.041875" layer="94"/>
<rectangle x1="236.625225" y1="16.9997125" x2="236.83604375" y2="17.041875" layer="94"/>
<rectangle x1="239.546225" y1="16.9997125" x2="240.308225" y2="17.041875" layer="94"/>
<rectangle x1="243.01840625" y1="16.9997125" x2="243.229225" y2="17.041875" layer="94"/>
<rectangle x1="244.20204375" y1="16.9997125" x2="245.09104375" y2="17.041875" layer="94"/>
<rectangle x1="246.19340625" y1="16.9997125" x2="246.404225" y2="17.041875" layer="94"/>
<rectangle x1="248.182225" y1="16.9997125" x2="248.90104375" y2="17.041875" layer="94"/>
<rectangle x1="251.52740625" y1="16.9997125" x2="251.738225" y2="17.041875" layer="94"/>
<rectangle x1="227.01640625" y1="17.041875" x2="228.20004375" y2="17.08429375" layer="94"/>
<rectangle x1="228.624225" y1="17.041875" x2="230.10504375" y2="17.08429375" layer="94"/>
<rectangle x1="230.57240625" y1="17.041875" x2="231.75604375" y2="17.08429375" layer="94"/>
<rectangle x1="233.40704375" y1="17.041875" x2="233.62040625" y2="17.08429375" layer="94"/>
<rectangle x1="234.593225" y1="17.041875" x2="235.52540625" y2="17.08429375" layer="94"/>
<rectangle x1="236.625225" y1="17.041875" x2="236.83604375" y2="17.08429375" layer="94"/>
<rectangle x1="239.546225" y1="17.041875" x2="240.308225" y2="17.08429375" layer="94"/>
<rectangle x1="243.01840625" y1="17.041875" x2="243.229225" y2="17.08429375" layer="94"/>
<rectangle x1="244.16140625" y1="17.041875" x2="245.09104375" y2="17.08429375" layer="94"/>
<rectangle x1="246.19340625" y1="17.041875" x2="246.404225" y2="17.08429375" layer="94"/>
<rectangle x1="248.182225" y1="17.041875" x2="248.90104375" y2="17.08429375" layer="94"/>
<rectangle x1="251.52740625" y1="17.041875" x2="251.738225" y2="17.08429375" layer="94"/>
<rectangle x1="227.01640625" y1="17.08429375" x2="228.20004375" y2="17.1267125" layer="94"/>
<rectangle x1="228.66740625" y1="17.08429375" x2="230.06440625" y2="17.1267125" layer="94"/>
<rectangle x1="230.57240625" y1="17.08429375" x2="231.75604375" y2="17.1267125" layer="94"/>
<rectangle x1="233.40704375" y1="17.08429375" x2="233.62040625" y2="17.1267125" layer="94"/>
<rectangle x1="234.593225" y1="17.08429375" x2="235.482225" y2="17.1267125" layer="94"/>
<rectangle x1="236.625225" y1="17.08429375" x2="236.83604375" y2="17.1267125" layer="94"/>
<rectangle x1="239.546225" y1="17.08429375" x2="240.308225" y2="17.1267125" layer="94"/>
<rectangle x1="243.01840625" y1="17.08429375" x2="243.229225" y2="17.1267125" layer="94"/>
<rectangle x1="244.16140625" y1="17.08429375" x2="245.05040625" y2="17.1267125" layer="94"/>
<rectangle x1="246.19340625" y1="17.08429375" x2="246.404225" y2="17.1267125" layer="94"/>
<rectangle x1="248.182225" y1="17.08429375" x2="248.90104375" y2="17.1267125" layer="94"/>
<rectangle x1="251.52740625" y1="17.08429375" x2="251.738225" y2="17.1267125" layer="94"/>
<rectangle x1="227.01640625" y1="17.1267125" x2="228.20004375" y2="17.168875" layer="94"/>
<rectangle x1="228.70804375" y1="17.1267125" x2="230.021225" y2="17.168875" layer="94"/>
<rectangle x1="230.57240625" y1="17.1267125" x2="231.75604375" y2="17.168875" layer="94"/>
<rectangle x1="233.40704375" y1="17.1267125" x2="233.62040625" y2="17.168875" layer="94"/>
<rectangle x1="234.55004375" y1="17.1267125" x2="235.43904375" y2="17.168875" layer="94"/>
<rectangle x1="236.625225" y1="17.1267125" x2="236.83604375" y2="17.168875" layer="94"/>
<rectangle x1="239.546225" y1="17.1267125" x2="240.308225" y2="17.168875" layer="94"/>
<rectangle x1="243.01840625" y1="17.1267125" x2="243.229225" y2="17.168875" layer="94"/>
<rectangle x1="244.118225" y1="17.1267125" x2="245.007225" y2="17.168875" layer="94"/>
<rectangle x1="246.19340625" y1="17.1267125" x2="246.404225" y2="17.168875" layer="94"/>
<rectangle x1="248.182225" y1="17.1267125" x2="248.90104375" y2="17.168875" layer="94"/>
<rectangle x1="251.52740625" y1="17.1267125" x2="251.738225" y2="17.168875" layer="94"/>
<rectangle x1="227.01640625" y1="17.168875" x2="228.20004375" y2="17.21129375" layer="94"/>
<rectangle x1="228.79440625" y1="17.168875" x2="229.97804375" y2="17.21129375" layer="94"/>
<rectangle x1="230.57240625" y1="17.168875" x2="231.75604375" y2="17.21129375" layer="94"/>
<rectangle x1="233.40704375" y1="17.168875" x2="233.62040625" y2="17.21129375" layer="94"/>
<rectangle x1="234.50940625" y1="17.168875" x2="235.39840625" y2="17.21129375" layer="94"/>
<rectangle x1="236.625225" y1="17.168875" x2="236.83604375" y2="17.21129375" layer="94"/>
<rectangle x1="239.546225" y1="17.168875" x2="240.308225" y2="17.21129375" layer="94"/>
<rectangle x1="243.01840625" y1="17.168875" x2="243.229225" y2="17.21129375" layer="94"/>
<rectangle x1="244.07504375" y1="17.168875" x2="245.007225" y2="17.21129375" layer="94"/>
<rectangle x1="246.19340625" y1="17.168875" x2="246.404225" y2="17.21129375" layer="94"/>
<rectangle x1="248.182225" y1="17.168875" x2="248.90104375" y2="17.21129375" layer="94"/>
<rectangle x1="251.52740625" y1="17.168875" x2="251.738225" y2="17.21129375" layer="94"/>
<rectangle x1="227.01640625" y1="17.21129375" x2="228.20004375" y2="17.2537125" layer="94"/>
<rectangle x1="228.83504375" y1="17.21129375" x2="229.894225" y2="17.2537125" layer="94"/>
<rectangle x1="230.57240625" y1="17.21129375" x2="231.75604375" y2="17.2537125" layer="94"/>
<rectangle x1="233.40704375" y1="17.21129375" x2="233.62040625" y2="17.2537125" layer="94"/>
<rectangle x1="234.466225" y1="17.21129375" x2="235.39840625" y2="17.2537125" layer="94"/>
<rectangle x1="236.625225" y1="17.21129375" x2="236.83604375" y2="17.2537125" layer="94"/>
<rectangle x1="239.546225" y1="17.21129375" x2="240.308225" y2="17.2537125" layer="94"/>
<rectangle x1="243.01840625" y1="17.21129375" x2="243.229225" y2="17.2537125" layer="94"/>
<rectangle x1="244.07504375" y1="17.21129375" x2="244.96404375" y2="17.2537125" layer="94"/>
<rectangle x1="246.19340625" y1="17.21129375" x2="246.404225" y2="17.2537125" layer="94"/>
<rectangle x1="248.182225" y1="17.21129375" x2="248.90104375" y2="17.2537125" layer="94"/>
<rectangle x1="251.52740625" y1="17.21129375" x2="251.738225" y2="17.2537125" layer="94"/>
<rectangle x1="227.01640625" y1="17.2537125" x2="228.20004375" y2="17.295875" layer="94"/>
<rectangle x1="228.92140625" y1="17.2537125" x2="229.85104375" y2="17.295875" layer="94"/>
<rectangle x1="230.57240625" y1="17.2537125" x2="231.75604375" y2="17.295875" layer="94"/>
<rectangle x1="233.40704375" y1="17.2537125" x2="233.62040625" y2="17.295875" layer="94"/>
<rectangle x1="234.466225" y1="17.2537125" x2="235.355225" y2="17.295875" layer="94"/>
<rectangle x1="236.625225" y1="17.2537125" x2="236.83604375" y2="17.295875" layer="94"/>
<rectangle x1="237.81140625" y1="17.2537125" x2="238.022225" y2="17.295875" layer="94"/>
<rectangle x1="239.546225" y1="17.2537125" x2="240.308225" y2="17.295875" layer="94"/>
<rectangle x1="241.78904375" y1="17.2537125" x2="242.00240625" y2="17.295875" layer="94"/>
<rectangle x1="243.01840625" y1="17.2537125" x2="243.229225" y2="17.295875" layer="94"/>
<rectangle x1="244.03440625" y1="17.2537125" x2="244.92340625" y2="17.295875" layer="94"/>
<rectangle x1="246.19340625" y1="17.2537125" x2="246.404225" y2="17.295875" layer="94"/>
<rectangle x1="248.182225" y1="17.2537125" x2="248.90104375" y2="17.295875" layer="94"/>
<rectangle x1="251.52740625" y1="17.2537125" x2="251.738225" y2="17.295875" layer="94"/>
<rectangle x1="227.01640625" y1="17.295875" x2="228.20004375" y2="17.33829375" layer="94"/>
<rectangle x1="228.96204375" y1="17.295875" x2="229.767225" y2="17.33829375" layer="94"/>
<rectangle x1="230.57240625" y1="17.295875" x2="231.75604375" y2="17.33829375" layer="94"/>
<rectangle x1="233.40704375" y1="17.295875" x2="233.62040625" y2="17.33829375" layer="94"/>
<rectangle x1="234.42304375" y1="17.295875" x2="235.31204375" y2="17.33829375" layer="94"/>
<rectangle x1="236.625225" y1="17.295875" x2="236.83604375" y2="17.33829375" layer="94"/>
<rectangle x1="237.81140625" y1="17.295875" x2="238.022225" y2="17.33829375" layer="94"/>
<rectangle x1="239.546225" y1="17.295875" x2="240.308225" y2="17.33829375" layer="94"/>
<rectangle x1="241.78904375" y1="17.295875" x2="242.00240625" y2="17.33829375" layer="94"/>
<rectangle x1="243.01840625" y1="17.295875" x2="243.229225" y2="17.33829375" layer="94"/>
<rectangle x1="243.991225" y1="17.295875" x2="244.92340625" y2="17.33829375" layer="94"/>
<rectangle x1="246.19340625" y1="17.295875" x2="246.404225" y2="17.33829375" layer="94"/>
<rectangle x1="248.182225" y1="17.295875" x2="248.90104375" y2="17.33829375" layer="94"/>
<rectangle x1="251.52740625" y1="17.295875" x2="251.738225" y2="17.33829375" layer="94"/>
<rectangle x1="227.01640625" y1="17.33829375" x2="228.20004375" y2="17.3807125" layer="94"/>
<rectangle x1="229.08904375" y1="17.33829375" x2="229.640225" y2="17.3807125" layer="94"/>
<rectangle x1="230.57240625" y1="17.33829375" x2="231.75604375" y2="17.3807125" layer="94"/>
<rectangle x1="233.40704375" y1="17.33829375" x2="233.62040625" y2="17.3807125" layer="94"/>
<rectangle x1="234.38240625" y1="17.33829375" x2="235.31204375" y2="17.3807125" layer="94"/>
<rectangle x1="236.625225" y1="17.33829375" x2="236.83604375" y2="17.3807125" layer="94"/>
<rectangle x1="237.81140625" y1="17.33829375" x2="238.022225" y2="17.3807125" layer="94"/>
<rectangle x1="239.546225" y1="17.33829375" x2="240.308225" y2="17.3807125" layer="94"/>
<rectangle x1="241.78904375" y1="17.33829375" x2="242.00240625" y2="17.3807125" layer="94"/>
<rectangle x1="243.01840625" y1="17.33829375" x2="243.229225" y2="17.3807125" layer="94"/>
<rectangle x1="243.94804375" y1="17.33829375" x2="244.880225" y2="17.3807125" layer="94"/>
<rectangle x1="246.19340625" y1="17.33829375" x2="246.404225" y2="17.3807125" layer="94"/>
<rectangle x1="248.182225" y1="17.33829375" x2="248.90104375" y2="17.3807125" layer="94"/>
<rectangle x1="251.52740625" y1="17.33829375" x2="251.738225" y2="17.3807125" layer="94"/>
<rectangle x1="227.01640625" y1="17.3807125" x2="228.20004375" y2="17.422875" layer="94"/>
<rectangle x1="230.57240625" y1="17.3807125" x2="231.75604375" y2="17.422875" layer="94"/>
<rectangle x1="233.40704375" y1="17.3807125" x2="233.62040625" y2="17.422875" layer="94"/>
<rectangle x1="234.38240625" y1="17.3807125" x2="235.27140625" y2="17.422875" layer="94"/>
<rectangle x1="236.625225" y1="17.3807125" x2="236.83604375" y2="17.422875" layer="94"/>
<rectangle x1="237.81140625" y1="17.3807125" x2="238.022225" y2="17.422875" layer="94"/>
<rectangle x1="239.546225" y1="17.3807125" x2="240.308225" y2="17.422875" layer="94"/>
<rectangle x1="241.78904375" y1="17.3807125" x2="242.00240625" y2="17.422875" layer="94"/>
<rectangle x1="243.01840625" y1="17.3807125" x2="243.229225" y2="17.422875" layer="94"/>
<rectangle x1="243.94804375" y1="17.3807125" x2="244.83704375" y2="17.422875" layer="94"/>
<rectangle x1="246.19340625" y1="17.3807125" x2="246.404225" y2="17.422875" layer="94"/>
<rectangle x1="248.182225" y1="17.3807125" x2="248.90104375" y2="17.422875" layer="94"/>
<rectangle x1="251.52740625" y1="17.3807125" x2="251.738225" y2="17.422875" layer="94"/>
<rectangle x1="227.01640625" y1="17.422875" x2="228.20004375" y2="17.46529375" layer="94"/>
<rectangle x1="230.57240625" y1="17.422875" x2="231.75604375" y2="17.46529375" layer="94"/>
<rectangle x1="233.40704375" y1="17.422875" x2="233.62040625" y2="17.46529375" layer="94"/>
<rectangle x1="234.339225" y1="17.422875" x2="235.228225" y2="17.46529375" layer="94"/>
<rectangle x1="236.625225" y1="17.422875" x2="236.83604375" y2="17.46529375" layer="94"/>
<rectangle x1="237.81140625" y1="17.422875" x2="238.022225" y2="17.46529375" layer="94"/>
<rectangle x1="239.546225" y1="17.422875" x2="240.308225" y2="17.46529375" layer="94"/>
<rectangle x1="241.78904375" y1="17.422875" x2="242.00240625" y2="17.46529375" layer="94"/>
<rectangle x1="243.01840625" y1="17.422875" x2="243.229225" y2="17.46529375" layer="94"/>
<rectangle x1="243.90740625" y1="17.422875" x2="244.79640625" y2="17.46529375" layer="94"/>
<rectangle x1="246.19340625" y1="17.422875" x2="246.404225" y2="17.46529375" layer="94"/>
<rectangle x1="248.182225" y1="17.422875" x2="248.90104375" y2="17.46529375" layer="94"/>
<rectangle x1="251.52740625" y1="17.422875" x2="251.738225" y2="17.46529375" layer="94"/>
<rectangle x1="227.01640625" y1="17.46529375" x2="228.20004375" y2="17.5077125" layer="94"/>
<rectangle x1="230.57240625" y1="17.46529375" x2="231.75604375" y2="17.5077125" layer="94"/>
<rectangle x1="233.40704375" y1="17.46529375" x2="233.62040625" y2="17.5077125" layer="94"/>
<rectangle x1="234.29604375" y1="17.46529375" x2="235.18504375" y2="17.5077125" layer="94"/>
<rectangle x1="236.625225" y1="17.46529375" x2="236.83604375" y2="17.5077125" layer="94"/>
<rectangle x1="237.81140625" y1="17.46529375" x2="238.06540625" y2="17.5077125" layer="94"/>
<rectangle x1="239.546225" y1="17.46529375" x2="240.308225" y2="17.5077125" layer="94"/>
<rectangle x1="241.78904375" y1="17.46529375" x2="242.00240625" y2="17.5077125" layer="94"/>
<rectangle x1="243.01840625" y1="17.46529375" x2="243.229225" y2="17.5077125" layer="94"/>
<rectangle x1="243.864225" y1="17.46529375" x2="244.79640625" y2="17.5077125" layer="94"/>
<rectangle x1="246.19340625" y1="17.46529375" x2="246.404225" y2="17.5077125" layer="94"/>
<rectangle x1="248.182225" y1="17.46529375" x2="248.90104375" y2="17.5077125" layer="94"/>
<rectangle x1="251.52740625" y1="17.46529375" x2="251.738225" y2="17.5077125" layer="94"/>
<rectangle x1="227.01640625" y1="17.5077125" x2="228.20004375" y2="17.549875" layer="94"/>
<rectangle x1="230.57240625" y1="17.5077125" x2="231.75604375" y2="17.549875" layer="94"/>
<rectangle x1="233.40704375" y1="17.5077125" x2="233.62040625" y2="17.549875" layer="94"/>
<rectangle x1="234.29604375" y1="17.5077125" x2="235.18504375" y2="17.549875" layer="94"/>
<rectangle x1="236.625225" y1="17.5077125" x2="236.83604375" y2="17.549875" layer="94"/>
<rectangle x1="237.81140625" y1="17.5077125" x2="238.06540625" y2="17.549875" layer="94"/>
<rectangle x1="239.546225" y1="17.5077125" x2="240.308225" y2="17.549875" layer="94"/>
<rectangle x1="241.74840625" y1="17.5077125" x2="242.00240625" y2="17.549875" layer="94"/>
<rectangle x1="243.01840625" y1="17.5077125" x2="243.229225" y2="17.549875" layer="94"/>
<rectangle x1="243.864225" y1="17.5077125" x2="244.753225" y2="17.549875" layer="94"/>
<rectangle x1="246.19340625" y1="17.5077125" x2="246.404225" y2="17.549875" layer="94"/>
<rectangle x1="248.182225" y1="17.5077125" x2="248.90104375" y2="17.549875" layer="94"/>
<rectangle x1="251.52740625" y1="17.5077125" x2="251.738225" y2="17.549875" layer="94"/>
<rectangle x1="227.01640625" y1="17.549875" x2="228.20004375" y2="17.59229375" layer="94"/>
<rectangle x1="230.57240625" y1="17.549875" x2="231.75604375" y2="17.59229375" layer="94"/>
<rectangle x1="233.40704375" y1="17.549875" x2="233.62040625" y2="17.59229375" layer="94"/>
<rectangle x1="234.25540625" y1="17.549875" x2="235.14440625" y2="17.59229375" layer="94"/>
<rectangle x1="236.625225" y1="17.549875" x2="236.83604375" y2="17.59229375" layer="94"/>
<rectangle x1="237.81140625" y1="17.549875" x2="238.06540625" y2="17.59229375" layer="94"/>
<rectangle x1="239.546225" y1="17.549875" x2="240.308225" y2="17.59229375" layer="94"/>
<rectangle x1="241.74840625" y1="17.549875" x2="242.00240625" y2="17.59229375" layer="94"/>
<rectangle x1="243.01840625" y1="17.549875" x2="243.229225" y2="17.59229375" layer="94"/>
<rectangle x1="243.82104375" y1="17.549875" x2="244.71004375" y2="17.59229375" layer="94"/>
<rectangle x1="246.19340625" y1="17.549875" x2="246.404225" y2="17.59229375" layer="94"/>
<rectangle x1="248.182225" y1="17.549875" x2="248.90104375" y2="17.59229375" layer="94"/>
<rectangle x1="251.52740625" y1="17.549875" x2="251.738225" y2="17.59229375" layer="94"/>
<rectangle x1="227.01640625" y1="17.59229375" x2="231.75604375" y2="17.6347125" layer="94"/>
<rectangle x1="233.40704375" y1="17.59229375" x2="233.62040625" y2="17.6347125" layer="94"/>
<rectangle x1="234.212225" y1="17.59229375" x2="235.101225" y2="17.6347125" layer="94"/>
<rectangle x1="236.625225" y1="17.59229375" x2="236.83604375" y2="17.6347125" layer="94"/>
<rectangle x1="237.81140625" y1="17.59229375" x2="238.06540625" y2="17.6347125" layer="94"/>
<rectangle x1="239.546225" y1="17.59229375" x2="240.308225" y2="17.6347125" layer="94"/>
<rectangle x1="241.74840625" y1="17.59229375" x2="242.00240625" y2="17.6347125" layer="94"/>
<rectangle x1="243.01840625" y1="17.59229375" x2="243.229225" y2="17.6347125" layer="94"/>
<rectangle x1="243.82104375" y1="17.59229375" x2="244.71004375" y2="17.6347125" layer="94"/>
<rectangle x1="246.19340625" y1="17.59229375" x2="246.404225" y2="17.6347125" layer="94"/>
<rectangle x1="248.182225" y1="17.59229375" x2="248.90104375" y2="17.6347125" layer="94"/>
<rectangle x1="251.52740625" y1="17.59229375" x2="251.738225" y2="17.6347125" layer="94"/>
<rectangle x1="227.01640625" y1="17.6347125" x2="231.75604375" y2="17.676875" layer="94"/>
<rectangle x1="233.40704375" y1="17.6347125" x2="233.62040625" y2="17.676875" layer="94"/>
<rectangle x1="234.212225" y1="17.6347125" x2="235.101225" y2="17.676875" layer="94"/>
<rectangle x1="236.625225" y1="17.6347125" x2="236.83604375" y2="17.676875" layer="94"/>
<rectangle x1="237.81140625" y1="17.6347125" x2="238.10604375" y2="17.676875" layer="94"/>
<rectangle x1="239.546225" y1="17.6347125" x2="240.308225" y2="17.676875" layer="94"/>
<rectangle x1="241.74840625" y1="17.6347125" x2="242.00240625" y2="17.676875" layer="94"/>
<rectangle x1="243.01840625" y1="17.6347125" x2="243.229225" y2="17.676875" layer="94"/>
<rectangle x1="243.78040625" y1="17.6347125" x2="244.66940625" y2="17.676875" layer="94"/>
<rectangle x1="246.19340625" y1="17.6347125" x2="246.404225" y2="17.676875" layer="94"/>
<rectangle x1="248.182225" y1="17.6347125" x2="248.90104375" y2="17.676875" layer="94"/>
<rectangle x1="251.52740625" y1="17.6347125" x2="251.738225" y2="17.676875" layer="94"/>
<rectangle x1="227.01640625" y1="17.676875" x2="231.75604375" y2="17.71929375" layer="94"/>
<rectangle x1="233.40704375" y1="17.676875" x2="233.62040625" y2="17.71929375" layer="94"/>
<rectangle x1="234.16904375" y1="17.676875" x2="235.05804375" y2="17.71929375" layer="94"/>
<rectangle x1="236.625225" y1="17.676875" x2="236.83604375" y2="17.71929375" layer="94"/>
<rectangle x1="237.81140625" y1="17.676875" x2="238.10604375" y2="17.71929375" layer="94"/>
<rectangle x1="239.546225" y1="17.676875" x2="240.308225" y2="17.71929375" layer="94"/>
<rectangle x1="241.74840625" y1="17.676875" x2="242.00240625" y2="17.71929375" layer="94"/>
<rectangle x1="243.01840625" y1="17.676875" x2="243.229225" y2="17.71929375" layer="94"/>
<rectangle x1="243.737225" y1="17.676875" x2="244.626225" y2="17.71929375" layer="94"/>
<rectangle x1="246.19340625" y1="17.676875" x2="246.404225" y2="17.71929375" layer="94"/>
<rectangle x1="248.182225" y1="17.676875" x2="248.90104375" y2="17.71929375" layer="94"/>
<rectangle x1="251.52740625" y1="17.676875" x2="251.738225" y2="17.71929375" layer="94"/>
<rectangle x1="227.01640625" y1="17.71929375" x2="231.75604375" y2="17.7617125" layer="94"/>
<rectangle x1="233.40704375" y1="17.71929375" x2="233.62040625" y2="17.7617125" layer="94"/>
<rectangle x1="234.12840625" y1="17.71929375" x2="235.01740625" y2="17.7617125" layer="94"/>
<rectangle x1="236.625225" y1="17.71929375" x2="236.83604375" y2="17.7617125" layer="94"/>
<rectangle x1="237.81140625" y1="17.71929375" x2="238.10604375" y2="17.7617125" layer="94"/>
<rectangle x1="239.546225" y1="17.71929375" x2="240.308225" y2="17.7617125" layer="94"/>
<rectangle x1="241.705225" y1="17.71929375" x2="242.00240625" y2="17.7617125" layer="94"/>
<rectangle x1="243.01840625" y1="17.71929375" x2="243.229225" y2="17.7617125" layer="94"/>
<rectangle x1="243.69404375" y1="17.71929375" x2="244.58304375" y2="17.7617125" layer="94"/>
<rectangle x1="246.19340625" y1="17.71929375" x2="246.404225" y2="17.7617125" layer="94"/>
<rectangle x1="248.182225" y1="17.71929375" x2="248.90104375" y2="17.7617125" layer="94"/>
<rectangle x1="251.52740625" y1="17.71929375" x2="251.738225" y2="17.7617125" layer="94"/>
<rectangle x1="227.01640625" y1="17.7617125" x2="231.75604375" y2="17.803875" layer="94"/>
<rectangle x1="233.40704375" y1="17.7617125" x2="233.62040625" y2="17.803875" layer="94"/>
<rectangle x1="234.085225" y1="17.7617125" x2="235.01740625" y2="17.803875" layer="94"/>
<rectangle x1="236.625225" y1="17.7617125" x2="236.83604375" y2="17.803875" layer="94"/>
<rectangle x1="237.81140625" y1="17.7617125" x2="238.10604375" y2="17.803875" layer="94"/>
<rectangle x1="239.546225" y1="17.7617125" x2="240.308225" y2="17.803875" layer="94"/>
<rectangle x1="241.705225" y1="17.7617125" x2="242.00240625" y2="17.803875" layer="94"/>
<rectangle x1="243.01840625" y1="17.7617125" x2="243.229225" y2="17.803875" layer="94"/>
<rectangle x1="243.69404375" y1="17.7617125" x2="244.58304375" y2="17.803875" layer="94"/>
<rectangle x1="246.19340625" y1="17.7617125" x2="246.404225" y2="17.803875" layer="94"/>
<rectangle x1="248.182225" y1="17.7617125" x2="248.90104375" y2="17.803875" layer="94"/>
<rectangle x1="251.52740625" y1="17.7617125" x2="251.738225" y2="17.803875" layer="94"/>
<rectangle x1="227.01640625" y1="17.803875" x2="231.71540625" y2="17.84629375" layer="94"/>
<rectangle x1="233.40704375" y1="17.803875" x2="233.62040625" y2="17.84629375" layer="94"/>
<rectangle x1="234.085225" y1="17.803875" x2="234.974225" y2="17.84629375" layer="94"/>
<rectangle x1="236.625225" y1="17.803875" x2="236.83604375" y2="17.84629375" layer="94"/>
<rectangle x1="237.81140625" y1="17.803875" x2="238.149225" y2="17.84629375" layer="94"/>
<rectangle x1="239.546225" y1="17.803875" x2="240.308225" y2="17.84629375" layer="94"/>
<rectangle x1="241.705225" y1="17.803875" x2="242.00240625" y2="17.84629375" layer="94"/>
<rectangle x1="243.01840625" y1="17.803875" x2="243.229225" y2="17.84629375" layer="94"/>
<rectangle x1="243.65340625" y1="17.803875" x2="244.54240625" y2="17.84629375" layer="94"/>
<rectangle x1="246.19340625" y1="17.803875" x2="246.404225" y2="17.84629375" layer="94"/>
<rectangle x1="248.182225" y1="17.803875" x2="248.90104375" y2="17.84629375" layer="94"/>
<rectangle x1="251.52740625" y1="17.803875" x2="251.738225" y2="17.84629375" layer="94"/>
<rectangle x1="227.05704375" y1="17.84629375" x2="231.71540625" y2="17.8887125" layer="94"/>
<rectangle x1="233.40704375" y1="17.84629375" x2="233.62040625" y2="17.8887125" layer="94"/>
<rectangle x1="234.04204375" y1="17.84629375" x2="234.93104375" y2="17.8887125" layer="94"/>
<rectangle x1="236.625225" y1="17.84629375" x2="236.83604375" y2="17.8887125" layer="94"/>
<rectangle x1="237.81140625" y1="17.84629375" x2="238.149225" y2="17.8887125" layer="94"/>
<rectangle x1="239.546225" y1="17.84629375" x2="240.308225" y2="17.8887125" layer="94"/>
<rectangle x1="241.66204375" y1="17.84629375" x2="242.00240625" y2="17.8887125" layer="94"/>
<rectangle x1="243.01840625" y1="17.84629375" x2="243.229225" y2="17.8887125" layer="94"/>
<rectangle x1="243.610225" y1="17.84629375" x2="244.54240625" y2="17.8887125" layer="94"/>
<rectangle x1="246.19340625" y1="17.84629375" x2="246.404225" y2="17.8887125" layer="94"/>
<rectangle x1="248.182225" y1="17.84629375" x2="248.90104375" y2="17.8887125" layer="94"/>
<rectangle x1="251.52740625" y1="17.84629375" x2="251.738225" y2="17.8887125" layer="94"/>
<rectangle x1="227.05704375" y1="17.8887125" x2="231.71540625" y2="17.930875" layer="94"/>
<rectangle x1="233.40704375" y1="17.8887125" x2="233.62040625" y2="17.930875" layer="94"/>
<rectangle x1="234.00140625" y1="17.8887125" x2="234.93104375" y2="17.930875" layer="94"/>
<rectangle x1="236.625225" y1="17.8887125" x2="236.83604375" y2="17.930875" layer="94"/>
<rectangle x1="237.81140625" y1="17.8887125" x2="238.149225" y2="17.930875" layer="94"/>
<rectangle x1="239.546225" y1="17.8887125" x2="240.308225" y2="17.930875" layer="94"/>
<rectangle x1="241.66204375" y1="17.8887125" x2="242.00240625" y2="17.930875" layer="94"/>
<rectangle x1="243.01840625" y1="17.8887125" x2="243.229225" y2="17.930875" layer="94"/>
<rectangle x1="243.610225" y1="17.8887125" x2="244.499225" y2="17.930875" layer="94"/>
<rectangle x1="246.19340625" y1="17.8887125" x2="246.404225" y2="17.930875" layer="94"/>
<rectangle x1="248.182225" y1="17.8887125" x2="248.90104375" y2="17.930875" layer="94"/>
<rectangle x1="251.52740625" y1="17.8887125" x2="251.738225" y2="17.930875" layer="94"/>
<rectangle x1="227.05704375" y1="17.930875" x2="231.672225" y2="17.97329375" layer="94"/>
<rectangle x1="233.40704375" y1="17.930875" x2="233.62040625" y2="17.97329375" layer="94"/>
<rectangle x1="234.00140625" y1="17.930875" x2="234.89040625" y2="17.97329375" layer="94"/>
<rectangle x1="236.625225" y1="17.930875" x2="236.83604375" y2="17.97329375" layer="94"/>
<rectangle x1="237.81140625" y1="17.930875" x2="238.19240625" y2="17.97329375" layer="94"/>
<rectangle x1="239.546225" y1="17.930875" x2="240.308225" y2="17.97329375" layer="94"/>
<rectangle x1="241.66204375" y1="17.930875" x2="242.00240625" y2="17.97329375" layer="94"/>
<rectangle x1="243.01840625" y1="17.930875" x2="243.229225" y2="17.97329375" layer="94"/>
<rectangle x1="243.56704375" y1="17.930875" x2="244.45604375" y2="17.97329375" layer="94"/>
<rectangle x1="246.19340625" y1="17.930875" x2="246.404225" y2="17.97329375" layer="94"/>
<rectangle x1="248.182225" y1="17.930875" x2="248.90104375" y2="17.97329375" layer="94"/>
<rectangle x1="251.52740625" y1="17.930875" x2="251.738225" y2="17.97329375" layer="94"/>
<rectangle x1="227.05704375" y1="17.97329375" x2="231.672225" y2="18.0157125" layer="94"/>
<rectangle x1="233.40704375" y1="17.97329375" x2="233.62040625" y2="18.0157125" layer="94"/>
<rectangle x1="233.958225" y1="17.97329375" x2="234.847225" y2="18.0157125" layer="94"/>
<rectangle x1="236.625225" y1="17.97329375" x2="236.83604375" y2="18.0157125" layer="94"/>
<rectangle x1="237.81140625" y1="17.97329375" x2="238.19240625" y2="18.0157125" layer="94"/>
<rectangle x1="239.546225" y1="17.97329375" x2="240.308225" y2="18.0157125" layer="94"/>
<rectangle x1="241.62140625" y1="17.97329375" x2="242.00240625" y2="18.0157125" layer="94"/>
<rectangle x1="243.01840625" y1="17.97329375" x2="243.229225" y2="18.0157125" layer="94"/>
<rectangle x1="243.52640625" y1="17.97329375" x2="244.45604375" y2="18.0157125" layer="94"/>
<rectangle x1="246.19340625" y1="17.97329375" x2="246.404225" y2="18.0157125" layer="94"/>
<rectangle x1="248.182225" y1="17.97329375" x2="248.90104375" y2="18.0157125" layer="94"/>
<rectangle x1="251.52740625" y1="17.97329375" x2="251.738225" y2="18.0157125" layer="94"/>
<rectangle x1="227.100225" y1="18.0157125" x2="231.672225" y2="18.057875" layer="94"/>
<rectangle x1="233.40704375" y1="18.0157125" x2="233.62040625" y2="18.057875" layer="94"/>
<rectangle x1="233.958225" y1="18.0157125" x2="234.847225" y2="18.057875" layer="94"/>
<rectangle x1="236.625225" y1="18.0157125" x2="236.83604375" y2="18.057875" layer="94"/>
<rectangle x1="237.81140625" y1="18.0157125" x2="238.23304375" y2="18.057875" layer="94"/>
<rectangle x1="239.546225" y1="18.0157125" x2="240.308225" y2="18.057875" layer="94"/>
<rectangle x1="241.62140625" y1="18.0157125" x2="242.00240625" y2="18.057875" layer="94"/>
<rectangle x1="243.01840625" y1="18.0157125" x2="243.229225" y2="18.057875" layer="94"/>
<rectangle x1="243.483225" y1="18.0157125" x2="244.41540625" y2="18.057875" layer="94"/>
<rectangle x1="246.19340625" y1="18.0157125" x2="246.404225" y2="18.057875" layer="94"/>
<rectangle x1="248.182225" y1="18.0157125" x2="248.90104375" y2="18.057875" layer="94"/>
<rectangle x1="251.52740625" y1="18.0157125" x2="251.738225" y2="18.057875" layer="94"/>
<rectangle x1="227.100225" y1="18.057875" x2="231.62904375" y2="18.10029375" layer="94"/>
<rectangle x1="233.40704375" y1="18.057875" x2="233.62040625" y2="18.10029375" layer="94"/>
<rectangle x1="233.91504375" y1="18.057875" x2="234.80404375" y2="18.10029375" layer="94"/>
<rectangle x1="236.625225" y1="18.057875" x2="236.83604375" y2="18.10029375" layer="94"/>
<rectangle x1="237.81140625" y1="18.057875" x2="238.23304375" y2="18.10029375" layer="94"/>
<rectangle x1="239.546225" y1="18.057875" x2="240.308225" y2="18.10029375" layer="94"/>
<rectangle x1="241.578225" y1="18.057875" x2="242.00240625" y2="18.10029375" layer="94"/>
<rectangle x1="243.01840625" y1="18.057875" x2="243.229225" y2="18.10029375" layer="94"/>
<rectangle x1="243.483225" y1="18.057875" x2="244.372225" y2="18.10029375" layer="94"/>
<rectangle x1="246.19340625" y1="18.057875" x2="246.404225" y2="18.10029375" layer="94"/>
<rectangle x1="248.182225" y1="18.057875" x2="248.90104375" y2="18.10029375" layer="94"/>
<rectangle x1="251.52740625" y1="18.057875" x2="251.738225" y2="18.10029375" layer="94"/>
<rectangle x1="227.14340625" y1="18.10029375" x2="231.62904375" y2="18.1427125" layer="94"/>
<rectangle x1="233.40704375" y1="18.10029375" x2="233.62040625" y2="18.1427125" layer="94"/>
<rectangle x1="233.87440625" y1="18.10029375" x2="234.76340625" y2="18.1427125" layer="94"/>
<rectangle x1="236.625225" y1="18.10029375" x2="236.83604375" y2="18.1427125" layer="94"/>
<rectangle x1="237.81140625" y1="18.10029375" x2="238.276225" y2="18.1427125" layer="94"/>
<rectangle x1="239.546225" y1="18.10029375" x2="240.308225" y2="18.1427125" layer="94"/>
<rectangle x1="241.53504375" y1="18.10029375" x2="242.00240625" y2="18.1427125" layer="94"/>
<rectangle x1="243.01840625" y1="18.10029375" x2="243.229225" y2="18.1427125" layer="94"/>
<rectangle x1="243.44004375" y1="18.10029375" x2="244.32904375" y2="18.1427125" layer="94"/>
<rectangle x1="246.19340625" y1="18.10029375" x2="246.404225" y2="18.1427125" layer="94"/>
<rectangle x1="248.182225" y1="18.10029375" x2="248.90104375" y2="18.1427125" layer="94"/>
<rectangle x1="251.52740625" y1="18.10029375" x2="251.738225" y2="18.1427125" layer="94"/>
<rectangle x1="227.18404375" y1="18.1427125" x2="231.58840625" y2="18.184875" layer="94"/>
<rectangle x1="233.40704375" y1="18.1427125" x2="233.62040625" y2="18.184875" layer="94"/>
<rectangle x1="233.831225" y1="18.1427125" x2="234.720225" y2="18.184875" layer="94"/>
<rectangle x1="236.625225" y1="18.1427125" x2="236.83604375" y2="18.184875" layer="94"/>
<rectangle x1="237.81140625" y1="18.1427125" x2="238.31940625" y2="18.184875" layer="94"/>
<rectangle x1="239.546225" y1="18.1427125" x2="240.308225" y2="18.184875" layer="94"/>
<rectangle x1="241.53504375" y1="18.1427125" x2="242.00240625" y2="18.184875" layer="94"/>
<rectangle x1="243.01840625" y1="18.1427125" x2="243.229225" y2="18.184875" layer="94"/>
<rectangle x1="243.39940625" y1="18.1427125" x2="244.32904375" y2="18.184875" layer="94"/>
<rectangle x1="246.19340625" y1="18.1427125" x2="246.404225" y2="18.184875" layer="94"/>
<rectangle x1="248.182225" y1="18.1427125" x2="248.90104375" y2="18.184875" layer="94"/>
<rectangle x1="251.52740625" y1="18.1427125" x2="251.738225" y2="18.184875" layer="94"/>
<rectangle x1="227.18404375" y1="18.184875" x2="231.58840625" y2="18.22729375" layer="94"/>
<rectangle x1="233.40704375" y1="18.184875" x2="233.62040625" y2="18.22729375" layer="94"/>
<rectangle x1="233.831225" y1="18.184875" x2="234.720225" y2="18.22729375" layer="94"/>
<rectangle x1="236.625225" y1="18.184875" x2="236.83604375" y2="18.22729375" layer="94"/>
<rectangle x1="237.81140625" y1="18.184875" x2="238.31940625" y2="18.22729375" layer="94"/>
<rectangle x1="239.546225" y1="18.184875" x2="240.308225" y2="18.22729375" layer="94"/>
<rectangle x1="241.49440625" y1="18.184875" x2="242.00240625" y2="18.22729375" layer="94"/>
<rectangle x1="243.01840625" y1="18.184875" x2="243.229225" y2="18.22729375" layer="94"/>
<rectangle x1="243.39940625" y1="18.184875" x2="244.28840625" y2="18.22729375" layer="94"/>
<rectangle x1="246.19340625" y1="18.184875" x2="246.404225" y2="18.22729375" layer="94"/>
<rectangle x1="248.182225" y1="18.184875" x2="248.90104375" y2="18.22729375" layer="94"/>
<rectangle x1="251.52740625" y1="18.184875" x2="251.738225" y2="18.22729375" layer="94"/>
<rectangle x1="227.227225" y1="18.22729375" x2="231.545225" y2="18.2697125" layer="94"/>
<rectangle x1="233.40704375" y1="18.22729375" x2="233.62040625" y2="18.2697125" layer="94"/>
<rectangle x1="233.78804375" y1="18.22729375" x2="234.67704375" y2="18.2697125" layer="94"/>
<rectangle x1="236.625225" y1="18.22729375" x2="236.83604375" y2="18.2697125" layer="94"/>
<rectangle x1="237.81140625" y1="18.22729375" x2="238.36004375" y2="18.2697125" layer="94"/>
<rectangle x1="239.546225" y1="18.22729375" x2="240.308225" y2="18.2697125" layer="94"/>
<rectangle x1="241.49440625" y1="18.22729375" x2="242.00240625" y2="18.2697125" layer="94"/>
<rectangle x1="243.01840625" y1="18.22729375" x2="243.229225" y2="18.2697125" layer="94"/>
<rectangle x1="243.356225" y1="18.22729375" x2="244.245225" y2="18.2697125" layer="94"/>
<rectangle x1="246.19340625" y1="18.22729375" x2="246.404225" y2="18.2697125" layer="94"/>
<rectangle x1="248.182225" y1="18.22729375" x2="248.90104375" y2="18.2697125" layer="94"/>
<rectangle x1="251.52740625" y1="18.22729375" x2="251.738225" y2="18.2697125" layer="94"/>
<rectangle x1="227.227225" y1="18.2697125" x2="231.545225" y2="18.311875" layer="94"/>
<rectangle x1="233.40704375" y1="18.2697125" x2="233.62040625" y2="18.311875" layer="94"/>
<rectangle x1="233.74740625" y1="18.2697125" x2="234.67704375" y2="18.311875" layer="94"/>
<rectangle x1="236.625225" y1="18.2697125" x2="236.83604375" y2="18.311875" layer="94"/>
<rectangle x1="237.81140625" y1="18.2697125" x2="238.403225" y2="18.311875" layer="94"/>
<rectangle x1="239.546225" y1="18.2697125" x2="240.308225" y2="18.311875" layer="94"/>
<rectangle x1="241.451225" y1="18.2697125" x2="242.00240625" y2="18.311875" layer="94"/>
<rectangle x1="243.01840625" y1="18.2697125" x2="243.229225" y2="18.311875" layer="94"/>
<rectangle x1="243.356225" y1="18.2697125" x2="244.245225" y2="18.311875" layer="94"/>
<rectangle x1="246.19340625" y1="18.2697125" x2="246.404225" y2="18.311875" layer="94"/>
<rectangle x1="248.182225" y1="18.2697125" x2="248.90104375" y2="18.311875" layer="94"/>
<rectangle x1="251.52740625" y1="18.2697125" x2="251.738225" y2="18.311875" layer="94"/>
<rectangle x1="227.27040625" y1="18.311875" x2="231.50204375" y2="18.35429375" layer="94"/>
<rectangle x1="233.40704375" y1="18.311875" x2="233.62040625" y2="18.35429375" layer="94"/>
<rectangle x1="233.74740625" y1="18.311875" x2="234.63640625" y2="18.35429375" layer="94"/>
<rectangle x1="236.625225" y1="18.311875" x2="236.83604375" y2="18.35429375" layer="94"/>
<rectangle x1="237.81140625" y1="18.311875" x2="238.44640625" y2="18.35429375" layer="94"/>
<rectangle x1="239.546225" y1="18.311875" x2="240.308225" y2="18.35429375" layer="94"/>
<rectangle x1="241.40804375" y1="18.311875" x2="242.00240625" y2="18.35429375" layer="94"/>
<rectangle x1="243.01840625" y1="18.311875" x2="243.229225" y2="18.35429375" layer="94"/>
<rectangle x1="243.31304375" y1="18.311875" x2="244.20204375" y2="18.35429375" layer="94"/>
<rectangle x1="246.19340625" y1="18.311875" x2="246.404225" y2="18.35429375" layer="94"/>
<rectangle x1="248.182225" y1="18.311875" x2="248.90104375" y2="18.35429375" layer="94"/>
<rectangle x1="251.52740625" y1="18.311875" x2="251.738225" y2="18.35429375" layer="94"/>
<rectangle x1="227.31104375" y1="18.35429375" x2="231.46140625" y2="18.3967125" layer="94"/>
<rectangle x1="233.40704375" y1="18.35429375" x2="233.62040625" y2="18.3967125" layer="94"/>
<rectangle x1="233.704225" y1="18.35429375" x2="234.593225" y2="18.3967125" layer="94"/>
<rectangle x1="236.625225" y1="18.35429375" x2="236.83604375" y2="18.3967125" layer="94"/>
<rectangle x1="237.81140625" y1="18.35429375" x2="238.48704375" y2="18.3967125" layer="94"/>
<rectangle x1="239.546225" y1="18.35429375" x2="240.308225" y2="18.3967125" layer="94"/>
<rectangle x1="241.324225" y1="18.35429375" x2="242.00240625" y2="18.3967125" layer="94"/>
<rectangle x1="243.01840625" y1="18.35429375" x2="243.229225" y2="18.3967125" layer="94"/>
<rectangle x1="243.27240625" y1="18.35429375" x2="244.16140625" y2="18.3967125" layer="94"/>
<rectangle x1="246.19340625" y1="18.35429375" x2="246.404225" y2="18.3967125" layer="94"/>
<rectangle x1="248.182225" y1="18.35429375" x2="248.90104375" y2="18.3967125" layer="94"/>
<rectangle x1="251.52740625" y1="18.35429375" x2="251.738225" y2="18.3967125" layer="94"/>
<rectangle x1="227.354225" y1="18.3967125" x2="231.418225" y2="18.438875" layer="94"/>
<rectangle x1="233.40704375" y1="18.3967125" x2="233.62040625" y2="18.438875" layer="94"/>
<rectangle x1="233.66104375" y1="18.3967125" x2="234.55004375" y2="18.438875" layer="94"/>
<rectangle x1="236.625225" y1="18.3967125" x2="236.83604375" y2="18.438875" layer="94"/>
<rectangle x1="237.81140625" y1="18.3967125" x2="238.57340625" y2="18.438875" layer="94"/>
<rectangle x1="239.546225" y1="18.3967125" x2="240.308225" y2="18.438875" layer="94"/>
<rectangle x1="241.28104375" y1="18.3967125" x2="242.00240625" y2="18.438875" layer="94"/>
<rectangle x1="243.01840625" y1="18.3967125" x2="244.118225" y2="18.438875" layer="94"/>
<rectangle x1="246.19340625" y1="18.3967125" x2="246.404225" y2="18.438875" layer="94"/>
<rectangle x1="248.182225" y1="18.3967125" x2="248.90104375" y2="18.438875" layer="94"/>
<rectangle x1="251.52740625" y1="18.3967125" x2="251.738225" y2="18.438875" layer="94"/>
<rectangle x1="227.39740625" y1="18.438875" x2="231.37504375" y2="18.48129375" layer="94"/>
<rectangle x1="233.40704375" y1="18.438875" x2="234.55004375" y2="18.48129375" layer="94"/>
<rectangle x1="236.625225" y1="18.438875" x2="236.83604375" y2="18.48129375" layer="94"/>
<rectangle x1="237.81140625" y1="18.438875" x2="238.61404375" y2="18.48129375" layer="94"/>
<rectangle x1="239.546225" y1="18.438875" x2="240.308225" y2="18.48129375" layer="94"/>
<rectangle x1="241.24040625" y1="18.438875" x2="242.00240625" y2="18.48129375" layer="94"/>
<rectangle x1="243.01840625" y1="18.438875" x2="244.118225" y2="18.48129375" layer="94"/>
<rectangle x1="246.19340625" y1="18.438875" x2="246.404225" y2="18.48129375" layer="94"/>
<rectangle x1="248.13904375" y1="18.438875" x2="248.90104375" y2="18.48129375" layer="94"/>
<rectangle x1="251.52740625" y1="18.438875" x2="251.738225" y2="18.48129375" layer="94"/>
<rectangle x1="227.43804375" y1="18.48129375" x2="231.33440625" y2="18.5237125" layer="94"/>
<rectangle x1="233.40704375" y1="18.48129375" x2="234.50940625" y2="18.5237125" layer="94"/>
<rectangle x1="236.625225" y1="18.48129375" x2="236.83604375" y2="18.5237125" layer="94"/>
<rectangle x1="237.81140625" y1="18.48129375" x2="238.657225" y2="18.5237125" layer="94"/>
<rectangle x1="239.546225" y1="18.48129375" x2="240.308225" y2="18.5237125" layer="94"/>
<rectangle x1="241.15404375" y1="18.48129375" x2="242.00240625" y2="18.5237125" layer="94"/>
<rectangle x1="243.01840625" y1="18.48129375" x2="244.07504375" y2="18.5237125" layer="94"/>
<rectangle x1="246.19340625" y1="18.48129375" x2="246.404225" y2="18.5237125" layer="94"/>
<rectangle x1="248.13904375" y1="18.48129375" x2="248.90104375" y2="18.5237125" layer="94"/>
<rectangle x1="251.52740625" y1="18.48129375" x2="251.738225" y2="18.5237125" layer="94"/>
<rectangle x1="227.481225" y1="18.5237125" x2="231.24804375" y2="18.565875" layer="94"/>
<rectangle x1="233.40704375" y1="18.5237125" x2="234.466225" y2="18.565875" layer="94"/>
<rectangle x1="236.625225" y1="18.5237125" x2="236.83604375" y2="18.565875" layer="94"/>
<rectangle x1="237.81140625" y1="18.5237125" x2="238.82740625" y2="18.565875" layer="94"/>
<rectangle x1="239.546225" y1="18.5237125" x2="240.308225" y2="18.565875" layer="94"/>
<rectangle x1="240.98640625" y1="18.5237125" x2="242.00240625" y2="18.565875" layer="94"/>
<rectangle x1="243.01840625" y1="18.5237125" x2="244.07504375" y2="18.565875" layer="94"/>
<rectangle x1="246.19340625" y1="18.5237125" x2="246.404225" y2="18.565875" layer="94"/>
<rectangle x1="248.13904375" y1="18.5237125" x2="248.90104375" y2="18.565875" layer="94"/>
<rectangle x1="251.484225" y1="18.5237125" x2="251.738225" y2="18.565875" layer="94"/>
<rectangle x1="227.56504375" y1="18.565875" x2="231.20740625" y2="18.60829375" layer="94"/>
<rectangle x1="233.40704375" y1="18.565875" x2="234.466225" y2="18.60829375" layer="94"/>
<rectangle x1="236.625225" y1="18.565875" x2="236.83604375" y2="18.60829375" layer="94"/>
<rectangle x1="237.81140625" y1="18.565875" x2="242.00240625" y2="18.60829375" layer="94"/>
<rectangle x1="243.01840625" y1="18.565875" x2="244.03440625" y2="18.60829375" layer="94"/>
<rectangle x1="246.19340625" y1="18.565875" x2="246.404225" y2="18.60829375" layer="94"/>
<rectangle x1="248.13904375" y1="18.565875" x2="248.90104375" y2="18.60829375" layer="94"/>
<rectangle x1="251.484225" y1="18.565875" x2="251.738225" y2="18.60829375" layer="94"/>
<rectangle x1="227.608225" y1="18.60829375" x2="231.12104375" y2="18.6507125" layer="94"/>
<rectangle x1="232.815225" y1="18.60829375" x2="234.42304375" y2="18.6507125" layer="94"/>
<rectangle x1="235.90640625" y1="18.60829375" x2="237.387225" y2="18.6507125" layer="94"/>
<rectangle x1="237.81140625" y1="18.60829375" x2="242.00240625" y2="18.6507125" layer="94"/>
<rectangle x1="242.38340625" y1="18.60829375" x2="243.991225" y2="18.6507125" layer="94"/>
<rectangle x1="245.47204375" y1="18.60829375" x2="246.99604375" y2="18.6507125" layer="94"/>
<rectangle x1="247.59040625" y1="18.60829375" x2="249.579225" y2="18.6507125" layer="94"/>
<rectangle x1="250.849225" y1="18.60829375" x2="252.33004375" y2="18.6507125" layer="94"/>
<rectangle x1="227.69204375" y1="18.6507125" x2="231.08040625" y2="18.692875" layer="94"/>
<rectangle x1="232.815225" y1="18.6507125" x2="234.38240625" y2="18.692875" layer="94"/>
<rectangle x1="235.90640625" y1="18.6507125" x2="237.387225" y2="18.692875" layer="94"/>
<rectangle x1="237.81140625" y1="18.6507125" x2="242.00240625" y2="18.692875" layer="94"/>
<rectangle x1="242.38340625" y1="18.6507125" x2="243.94804375" y2="18.692875" layer="94"/>
<rectangle x1="245.47204375" y1="18.6507125" x2="246.99604375" y2="18.692875" layer="94"/>
<rectangle x1="247.59040625" y1="18.6507125" x2="249.579225" y2="18.692875" layer="94"/>
<rectangle x1="250.849225" y1="18.6507125" x2="252.33004375" y2="18.692875" layer="94"/>
<rectangle x1="227.81904375" y1="18.692875" x2="230.95340625" y2="18.73529375" layer="94"/>
<rectangle x1="232.815225" y1="18.692875" x2="234.38240625" y2="18.73529375" layer="94"/>
<rectangle x1="235.90640625" y1="18.692875" x2="237.387225" y2="18.73529375" layer="94"/>
<rectangle x1="237.81140625" y1="18.692875" x2="242.00240625" y2="18.73529375" layer="94"/>
<rectangle x1="242.38340625" y1="18.692875" x2="243.94804375" y2="18.73529375" layer="94"/>
<rectangle x1="245.47204375" y1="18.692875" x2="246.99604375" y2="18.73529375" layer="94"/>
<rectangle x1="247.59040625" y1="18.692875" x2="249.579225" y2="18.73529375" layer="94"/>
<rectangle x1="250.849225" y1="18.692875" x2="252.33004375" y2="18.73529375" layer="94"/>
<rectangle x1="227.94604375" y1="18.73529375" x2="230.82640625" y2="18.7777125" layer="94"/>
<rectangle x1="232.815225" y1="18.73529375" x2="234.339225" y2="18.7777125" layer="94"/>
<rectangle x1="235.90640625" y1="18.73529375" x2="237.387225" y2="18.7777125" layer="94"/>
<rectangle x1="237.81140625" y1="18.73529375" x2="242.00240625" y2="18.7777125" layer="94"/>
<rectangle x1="242.38340625" y1="18.73529375" x2="243.90740625" y2="18.7777125" layer="94"/>
<rectangle x1="245.47204375" y1="18.73529375" x2="246.99604375" y2="18.7777125" layer="94"/>
<rectangle x1="247.59040625" y1="18.73529375" x2="249.579225" y2="18.7777125" layer="94"/>
<rectangle x1="250.849225" y1="18.73529375" x2="252.33004375" y2="18.7777125" layer="94"/>
</symbol>
<symbol name="MSP430FR5738RGE" urn="urn:adsk.eagle:symbol:9376775/3" library_version="10" library_locally_modified="yes">
<wire x1="-35.56" y1="5.08" x2="40.64" y2="5.08" width="0.254" layer="94"/>
<wire x1="40.64" y1="-63.5" x2="40.64" y2="-55.88" width="0.254" layer="94"/>
<wire x1="40.64" y1="-55.88" x2="40.64" y2="5.08" width="0.254" layer="94"/>
<wire x1="40.64" y1="-63.5" x2="-35.56" y2="-63.5" width="0.254" layer="94"/>
<wire x1="-35.56" y1="5.08" x2="-35.56" y2="-55.88" width="0.254" layer="94"/>
<wire x1="-35.56" y1="-55.88" x2="-35.56" y2="-63.5" width="0.254" layer="94"/>
<wire x1="-35.56" y1="-55.88" x2="40.64" y2="-55.88" width="0.254" layer="94"/>
<text x="-34.29" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="-36.83" y="-66.04" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<text x="-12.7" y="-61.976" size="1.778" layer="94">VCC:       2.0 V to 3.6 V
Size:       4.00 x 4.00 [mm]</text>
<pin name="P1.0/TA0.1/DMAE0/RTCCLK/A0*/CD0/VEREF-*" x="45.72" y="0" length="middle" direction="out" rot="R180"/>
<pin name="P1.1/TA0.2/TA1CLK/CDOUT/A1*/CD1/VEREF+*" x="45.72" y="-2.54" length="middle" rot="R180"/>
<pin name="P1.2/TA1.1/TA0CLK/CDOUT/A2*/CD2" x="45.72" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="P1.3/TA1.2/UCB0STE/A3*/CD3" x="45.72" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="P1.4/TB0.1/UCA0STE/A4*/CD4" x="-40.64" y="-20.32" length="middle" direction="out"/>
<pin name="P1.5/TB0.2/UCA0CLK/A5*/CD5" x="-40.64" y="-27.94" length="middle" direction="out" function="clk"/>
<pin name="PJ.0/TDO/TB0OUTH/SMCLK/CD6" x="45.72" y="-17.78" length="middle" rot="R180"/>
<pin name="PJ.1/TDI/TCLK/MCLK/CD7" x="45.72" y="-25.4" length="middle" rot="R180"/>
<pin name="PJ.2/TMS/ACLK/CD8" x="45.72" y="-30.48" length="middle" rot="R180"/>
<pin name="PJ.3/TCK/CD9" x="45.72" y="-35.56" length="middle" rot="R180"/>
<pin name="TEST/SBWTCK" x="-40.64" y="-5.08" length="middle" direction="in"/>
<pin name="RST/NMI/SBWTDIO" x="-40.64" y="0" length="middle" direction="in"/>
<pin name="VCORE" x="45.72" y="-53.34" length="middle" direction="pwr" rot="R180"/>
<pin name="P1.7/UCB0SOMI/UCB0SCL/TA1.0" x="-40.64" y="-15.24" length="middle" direction="out" function="clk"/>
<pin name="P1.6/UCB0SIMO/UCB0SDA/TA0.0" x="-40.64" y="-10.16" length="middle"/>
<pin name="P2.2/UCB0CLK" x="45.72" y="-12.7" length="middle" rot="R180"/>
<pin name="P2.1/UCA0RXD/UCA0SOMI/TB0.0" x="-40.64" y="-22.86" length="middle"/>
<pin name="P2.0/UCA0TXD/UCA0SIMO/TB0CLK/ACLK" x="-40.64" y="-25.4" length="middle"/>
<pin name="EP" x="-40.64" y="-53.34" visible="pin" length="middle" direction="pwr"/>
<pin name="AVCC" x="45.72" y="-48.26" length="middle" direction="pwr" rot="R180"/>
<pin name="AVSS" x="-40.64" y="-48.26" length="middle" direction="pwr"/>
<pin name="PJ.5/XOUT" x="-40.64" y="-40.64" length="middle"/>
<pin name="PJ.4/XIN" x="-40.64" y="-35.56" length="middle" direction="in" function="clk"/>
<pin name="DVCC" x="45.72" y="-50.8" length="middle" direction="pwr" rot="R180"/>
<pin name="DVSS" x="-40.64" y="-45.72" length="middle" direction="pwr"/>
</symbol>
<symbol name="SCHOTTKY-DIODE" urn="urn:adsk.eagle:symbol:9209735/2" library_version="3" library_locally_modified="yes">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.27" x2="1.905" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.016" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="-2.286" y="1.905" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
</symbol>
<symbol name="RF430CL330HRGT" urn="urn:adsk.eagle:symbol:9209748/5" library_version="10" library_locally_modified="yes">
<wire x1="-15.24" y1="-27.94" x2="15.24" y2="-27.94" width="0.254" layer="94"/>
<wire x1="15.24" y1="-27.94" x2="15.24" y2="20.32" width="0.254" layer="94"/>
<wire x1="15.24" y1="20.32" x2="-15.24" y2="20.32" width="0.254" layer="94"/>
<wire x1="-15.24" y1="20.32" x2="-15.24" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-27.94" x2="-15.24" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-38.1" x2="15.24" y2="-38.1" width="0.254" layer="94"/>
<wire x1="15.24" y1="-38.1" x2="15.24" y2="-27.94" width="0.254" layer="94"/>
<pin name="ANT1" x="20.32" y="17.78" length="middle" direction="in" rot="R180"/>
<pin name="ANT2" x="20.32" y="7.62" length="middle" direction="in" rot="R180"/>
<pin name="E0(TMS)" x="-20.32" y="-20.32" length="middle" direction="in"/>
<pin name="E1(TDO)" x="-20.32" y="-22.86" length="middle"/>
<pin name="E2(TDI)" x="-20.32" y="-25.4" length="middle" direction="in"/>
<pin name="INTO" x="20.32" y="-15.24" length="middle" direction="out" rot="R180"/>
<pin name="NC" x="20.32" y="-2.54" length="middle" direction="nc" rot="R180"/>
<pin name="NC_2" x="20.32" y="0" length="middle" direction="nc" rot="R180"/>
<pin name="!RST!" x="20.32" y="-10.16" length="middle" direction="in" rot="R180"/>
<pin name="SCK" x="-20.32" y="-12.7" length="middle" direction="in"/>
<pin name="!CS!/SCMS" x="-20.32" y="-15.24" length="middle" direction="in"/>
<pin name="SDA/SI" x="-20.32" y="12.7" length="middle" direction="out"/>
<pin name="SCL/SO" x="-20.32" y="17.78" length="middle" direction="in" function="clk"/>
<pin name="EP" x="20.32" y="-25.4" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="VCC" x="-20.32" y="-7.62" length="middle" direction="pwr"/>
<pin name="VCORE" x="-20.32" y="5.08" length="middle" direction="pwr"/>
<pin name="VSS" x="20.32" y="-22.86" length="middle" direction="pwr" rot="R180"/>
<text x="-14.8844" y="21.8186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-15.5194" y="-44.2214" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<text x="-10.16" y="-35.56" size="1.778" layer="94">VDD:   2.0 V to 3.0 V
Size:    3.0 x 3.0 [mm]</text>
</symbol>
<symbol name="NFC" urn="urn:adsk.eagle:symbol:9209737/1" library_version="9" library_locally_modified="yes">
<pin name="P$1" x="-12.7" y="5.08" visible="off" length="middle"/>
<pin name="P$2" x="-12.7" y="2.54" visible="off" length="middle"/>
<wire x1="-7.62" y1="5.08" x2="-6.35" y2="5.08" width="0.254" layer="94"/>
<wire x1="-6.35" y1="5.08" x2="-5.08" y2="6.35" width="0.254" layer="94" curve="90"/>
<wire x1="-5.08" y1="6.35" x2="-3.81" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="-3.81" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="3.81" y2="7.62" width="0.254" layer="94"/>
<wire x1="3.81" y1="7.62" x2="5.08" y2="6.35" width="0.254" layer="94" curve="-90"/>
<wire x1="5.08" y1="6.35" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="3.81" y2="0" width="0.254" layer="94" curve="-90"/>
<wire x1="3.81" y1="0" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-5.08" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="-5.08" y1="1.27" x2="-6.35" y2="2.54" width="0.254" layer="94" curve="90"/>
<wire x1="-6.35" y1="2.54" x2="-7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.048" y1="1.016" x2="3.048" y2="1.016" width="0.254" layer="94"/>
<wire x1="-3.048" y1="6.604" x2="3.048" y2="6.604" width="0.254" layer="94"/>
<wire x1="4.064" y1="5.588" x2="4.064" y2="2.032" width="0.254" layer="94"/>
<wire x1="-4.064" y1="2.032" x2="-4.064" y2="5.588" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.588" x2="-2.54" y2="5.588" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.032" x2="-2.54" y2="2.032" width="0.254" layer="94"/>
<wire x1="3.048" y1="2.54" x2="3.048" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="5.08" width="0.254" layer="94"/>
<wire x1="3.048" y1="6.604" x2="4.064" y2="5.588" width="0.254" layer="94" curve="-90"/>
<wire x1="4.064" y1="2.032" x2="3.048" y2="1.016" width="0.254" layer="94" curve="-90"/>
<wire x1="-3.048" y1="1.016" x2="-4.064" y2="2.032" width="0.254" layer="94" curve="-90"/>
<wire x1="-4.064" y1="5.588" x2="-3.048" y2="6.604" width="0.254" layer="94" curve="-90"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="2.032" width="0.254" layer="94" curve="-90"/>
<wire x1="-2.54" y1="2.032" x2="-3.048" y2="2.54" width="0.254" layer="94" curve="-90"/>
<wire x1="-2.54" y1="5.588" x2="-3.048" y2="5.08" width="0.254" layer="94" curve="90"/>
<wire x1="2.54" y1="5.588" x2="3.048" y2="5.08" width="0.254" layer="94" curve="-90"/>
<text x="2.54" y="-0.508" size="1.27" layer="95" rot="R180">&gt;NAME</text>
<text x="2.54" y="9.398" size="1.27" layer="96" rot="R180">&gt;VALUE</text>
</symbol>
<symbol name="+12V" urn="urn:adsk.eagle:symbol:9535777/1" library_version="10" library_locally_modified="yes">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+12V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="LDO" urn="urn:adsk.eagle:symbol:9209752/4" library_version="10" library_locally_modified="yes">
<wire x1="17.78" y1="-20.32" x2="-15.24" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-20.32" x2="-15.24" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-10.16" x2="-15.24" y2="12.7" width="0.254" layer="94"/>
<wire x1="-15.24" y1="12.7" x2="17.78" y2="12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="-20.32" x2="17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="-10.16" x2="17.78" y2="12.7" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-10.16" x2="17.78" y2="-10.16" width="0.254" layer="94"/>
<pin name="CE" x="-20.32" y="-7.62" length="middle"/>
<pin name="NC" x="22.86" y="0" length="middle" direction="nc" rot="R180"/>
<pin name="VIN" x="-20.32" y="5.08" length="middle" direction="pwr"/>
<pin name="VOUT" x="22.86" y="7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="VSS" x="22.86" y="-7.62" length="middle" direction="pwr" rot="R180"/>
<text x="-12.7" y="15.24" size="3.048" layer="95">&gt;Name</text>
<text x="-15.24" y="-25.4" size="3.048" layer="95">&gt;Value</text>
<text x="-12.7" y="-17.78" size="1.778" layer="94">VIN:      1.4 V to 6.0 V
VOUT:   2V (150 mA MAX)</text>
</symbol>
<symbol name="PB-FREE" urn="urn:adsk.eagle:symbol:9371630/1" library_version="9" library_locally_modified="yes">
<circle x="5.08" y="2.54" radius="3.5921" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<text x="3.302" y="1.27" size="2.54" layer="94">Pb</text>
</symbol>
<symbol name="NTNU" urn="urn:adsk.eagle:symbol:9376774/1" library_version="9" library_locally_modified="yes">
<rectangle x1="11.6078" y1="0.0254" x2="11.7602" y2="0.0762" layer="94"/>
<rectangle x1="23.0886" y1="0.0254" x2="23.241" y2="0.0762" layer="94"/>
<rectangle x1="27.305" y1="0.0254" x2="27.813" y2="0.0762" layer="94"/>
<rectangle x1="1.2446" y1="0.0762" x2="4.3942" y2="0.127" layer="94"/>
<rectangle x1="11.557" y1="0.0762" x2="11.7602" y2="0.127" layer="94"/>
<rectangle x1="23.0378" y1="0.0762" x2="23.241" y2="0.127" layer="94"/>
<rectangle x1="26.6954" y1="0.0762" x2="28.321" y2="0.127" layer="94"/>
<rectangle x1="1.0414" y1="0.127" x2="4.5974" y2="0.1778" layer="94"/>
<rectangle x1="6.9342" y1="0.127" x2="8.8138" y2="0.1778" layer="94"/>
<rectangle x1="11.557" y1="0.127" x2="11.7602" y2="0.1778" layer="94"/>
<rectangle x1="14.1478" y1="0.127" x2="16.7894" y2="0.1778" layer="94"/>
<rectangle x1="18.415" y1="0.127" x2="20.2438" y2="0.1778" layer="94"/>
<rectangle x1="23.0378" y1="0.127" x2="23.241" y2="0.1778" layer="94"/>
<rectangle x1="26.4922" y1="0.127" x2="28.5242" y2="0.1778" layer="94"/>
<rectangle x1="0.8382" y1="0.1778" x2="4.7498" y2="0.2286" layer="94"/>
<rectangle x1="6.9342" y1="0.1778" x2="8.8138" y2="0.2286" layer="94"/>
<rectangle x1="11.5062" y1="0.1778" x2="11.7602" y2="0.2286" layer="94"/>
<rectangle x1="14.1478" y1="0.1778" x2="16.7894" y2="0.2286" layer="94"/>
<rectangle x1="18.415" y1="0.1778" x2="20.2438" y2="0.2286" layer="94"/>
<rectangle x1="22.987" y1="0.1778" x2="23.241" y2="0.2286" layer="94"/>
<rectangle x1="26.289" y1="0.1778" x2="28.7274" y2="0.2286" layer="94"/>
<rectangle x1="0.7366" y1="0.2286" x2="4.9022" y2="0.2794" layer="94"/>
<rectangle x1="6.9342" y1="0.2286" x2="8.8138" y2="0.2794" layer="94"/>
<rectangle x1="11.4554" y1="0.2286" x2="11.7602" y2="0.2794" layer="94"/>
<rectangle x1="14.1478" y1="0.2286" x2="16.7894" y2="0.2794" layer="94"/>
<rectangle x1="18.415" y1="0.2286" x2="20.2438" y2="0.2794" layer="94"/>
<rectangle x1="22.9362" y1="0.2286" x2="23.241" y2="0.2794" layer="94"/>
<rectangle x1="26.0858" y1="0.2286" x2="28.8798" y2="0.2794" layer="94"/>
<rectangle x1="0.635" y1="0.2794" x2="4.953" y2="0.3302" layer="94"/>
<rectangle x1="6.9342" y1="0.2794" x2="8.8138" y2="0.3302" layer="94"/>
<rectangle x1="11.4046" y1="0.2794" x2="11.7602" y2="0.3302" layer="94"/>
<rectangle x1="14.1478" y1="0.2794" x2="16.7894" y2="0.3302" layer="94"/>
<rectangle x1="18.415" y1="0.2794" x2="20.2438" y2="0.3302" layer="94"/>
<rectangle x1="22.8854" y1="0.2794" x2="23.241" y2="0.3302" layer="94"/>
<rectangle x1="25.9842" y1="0.2794" x2="28.9814" y2="0.3302" layer="94"/>
<rectangle x1="0.5842" y1="0.3302" x2="5.0546" y2="0.381" layer="94"/>
<rectangle x1="7.6454" y1="0.3302" x2="7.8994" y2="0.381" layer="94"/>
<rectangle x1="11.3538" y1="0.3302" x2="11.7602" y2="0.381" layer="94"/>
<rectangle x1="15.0114" y1="0.3302" x2="15.9258" y2="0.381" layer="94"/>
<rectangle x1="19.177" y1="0.3302" x2="19.431" y2="0.381" layer="94"/>
<rectangle x1="22.8854" y1="0.3302" x2="23.241" y2="0.381" layer="94"/>
<rectangle x1="25.8826" y1="0.3302" x2="27.1526" y2="0.381" layer="94"/>
<rectangle x1="28.3718" y1="0.3302" x2="29.083" y2="0.381" layer="94"/>
<rectangle x1="0.5334" y1="0.381" x2="5.1054" y2="0.4318" layer="94"/>
<rectangle x1="7.6454" y1="0.381" x2="7.8994" y2="0.4318" layer="94"/>
<rectangle x1="11.3538" y1="0.381" x2="11.7602" y2="0.4318" layer="94"/>
<rectangle x1="15.0114" y1="0.381" x2="15.9258" y2="0.4318" layer="94"/>
<rectangle x1="19.177" y1="0.381" x2="19.431" y2="0.4318" layer="94"/>
<rectangle x1="22.8346" y1="0.381" x2="23.241" y2="0.4318" layer="94"/>
<rectangle x1="25.8318" y1="0.381" x2="27.0002" y2="0.4318" layer="94"/>
<rectangle x1="28.5242" y1="0.381" x2="29.1338" y2="0.4318" layer="94"/>
<rectangle x1="0.4826" y1="0.4318" x2="5.1562" y2="0.4826" layer="94"/>
<rectangle x1="7.6454" y1="0.4318" x2="7.8994" y2="0.4826" layer="94"/>
<rectangle x1="11.303" y1="0.4318" x2="11.7602" y2="0.4826" layer="94"/>
<rectangle x1="15.0114" y1="0.4318" x2="15.9258" y2="0.4826" layer="94"/>
<rectangle x1="19.177" y1="0.4318" x2="19.431" y2="0.4826" layer="94"/>
<rectangle x1="22.8346" y1="0.4318" x2="23.241" y2="0.4826" layer="94"/>
<rectangle x1="25.781" y1="0.4318" x2="26.8478" y2="0.4826" layer="94"/>
<rectangle x1="28.6766" y1="0.4318" x2="29.1846" y2="0.4826" layer="94"/>
<rectangle x1="0.381" y1="0.4826" x2="5.207" y2="0.5334" layer="94"/>
<rectangle x1="7.6454" y1="0.4826" x2="7.8994" y2="0.5334" layer="94"/>
<rectangle x1="11.303" y1="0.4826" x2="11.7602" y2="0.5334" layer="94"/>
<rectangle x1="15.0114" y1="0.4826" x2="15.9258" y2="0.5334" layer="94"/>
<rectangle x1="19.177" y1="0.4826" x2="19.431" y2="0.5334" layer="94"/>
<rectangle x1="22.7838" y1="0.4826" x2="23.241" y2="0.5334" layer="94"/>
<rectangle x1="25.7302" y1="0.4826" x2="26.7462" y2="0.5334" layer="94"/>
<rectangle x1="28.829" y1="0.4826" x2="29.2354" y2="0.5334" layer="94"/>
<rectangle x1="0.3302" y1="0.5334" x2="5.2578" y2="0.5842" layer="94"/>
<rectangle x1="7.6454" y1="0.5334" x2="7.8994" y2="0.5842" layer="94"/>
<rectangle x1="11.2522" y1="0.5334" x2="11.7602" y2="0.5842" layer="94"/>
<rectangle x1="15.0114" y1="0.5334" x2="15.9258" y2="0.5842" layer="94"/>
<rectangle x1="19.177" y1="0.5334" x2="19.431" y2="0.5842" layer="94"/>
<rectangle x1="22.733" y1="0.5334" x2="23.241" y2="0.5842" layer="94"/>
<rectangle x1="25.6794" y1="0.5334" x2="26.6446" y2="0.5842" layer="94"/>
<rectangle x1="28.9306" y1="0.5334" x2="29.2862" y2="0.5842" layer="94"/>
<rectangle x1="0.2794" y1="0.5842" x2="5.3086" y2="0.635" layer="94"/>
<rectangle x1="7.6454" y1="0.5842" x2="7.8994" y2="0.635" layer="94"/>
<rectangle x1="11.2014" y1="0.5842" x2="11.7602" y2="0.635" layer="94"/>
<rectangle x1="15.0114" y1="0.5842" x2="15.9258" y2="0.635" layer="94"/>
<rectangle x1="19.177" y1="0.5842" x2="19.431" y2="0.635" layer="94"/>
<rectangle x1="22.6822" y1="0.5842" x2="23.241" y2="0.635" layer="94"/>
<rectangle x1="25.6286" y1="0.5842" x2="26.5938" y2="0.635" layer="94"/>
<rectangle x1="28.9814" y1="0.5842" x2="29.337" y2="0.635" layer="94"/>
<rectangle x1="0.2286" y1="0.635" x2="5.3594" y2="0.6858" layer="94"/>
<rectangle x1="7.6454" y1="0.635" x2="7.8994" y2="0.6858" layer="94"/>
<rectangle x1="11.2014" y1="0.635" x2="11.7602" y2="0.6858" layer="94"/>
<rectangle x1="15.0114" y1="0.635" x2="15.9258" y2="0.6858" layer="94"/>
<rectangle x1="19.177" y1="0.635" x2="19.431" y2="0.6858" layer="94"/>
<rectangle x1="22.6314" y1="0.635" x2="23.241" y2="0.6858" layer="94"/>
<rectangle x1="25.5778" y1="0.635" x2="26.543" y2="0.6858" layer="94"/>
<rectangle x1="29.083" y1="0.635" x2="29.3878" y2="0.6858" layer="94"/>
<rectangle x1="0.2286" y1="0.6858" x2="5.4102" y2="0.7366" layer="94"/>
<rectangle x1="7.6454" y1="0.6858" x2="7.8994" y2="0.7366" layer="94"/>
<rectangle x1="11.1506" y1="0.6858" x2="11.7602" y2="0.7366" layer="94"/>
<rectangle x1="15.0114" y1="0.6858" x2="15.9258" y2="0.7366" layer="94"/>
<rectangle x1="19.177" y1="0.6858" x2="19.431" y2="0.7366" layer="94"/>
<rectangle x1="22.6314" y1="0.6858" x2="23.241" y2="0.7366" layer="94"/>
<rectangle x1="25.527" y1="0.6858" x2="26.4922" y2="0.7366" layer="94"/>
<rectangle x1="29.083" y1="0.6858" x2="29.3878" y2="0.7366" layer="94"/>
<rectangle x1="0.1778" y1="0.7366" x2="5.461" y2="0.7874" layer="94"/>
<rectangle x1="7.6454" y1="0.7366" x2="7.8994" y2="0.7874" layer="94"/>
<rectangle x1="11.0998" y1="0.7366" x2="11.7602" y2="0.7874" layer="94"/>
<rectangle x1="15.0114" y1="0.7366" x2="15.9258" y2="0.7874" layer="94"/>
<rectangle x1="19.177" y1="0.7366" x2="19.431" y2="0.7874" layer="94"/>
<rectangle x1="22.5806" y1="0.7366" x2="23.241" y2="0.7874" layer="94"/>
<rectangle x1="25.527" y1="0.7366" x2="26.4414" y2="0.7874" layer="94"/>
<rectangle x1="29.1338" y1="0.7366" x2="29.4386" y2="0.7874" layer="94"/>
<rectangle x1="0.1778" y1="0.7874" x2="5.461" y2="0.8382" layer="94"/>
<rectangle x1="7.6454" y1="0.7874" x2="7.8994" y2="0.8382" layer="94"/>
<rectangle x1="11.049" y1="0.7874" x2="11.7602" y2="0.8382" layer="94"/>
<rectangle x1="15.0114" y1="0.7874" x2="15.9258" y2="0.8382" layer="94"/>
<rectangle x1="19.177" y1="0.7874" x2="19.431" y2="0.8382" layer="94"/>
<rectangle x1="22.5806" y1="0.7874" x2="23.241" y2="0.8382" layer="94"/>
<rectangle x1="25.4762" y1="0.7874" x2="26.4414" y2="0.8382" layer="94"/>
<rectangle x1="29.1846" y1="0.7874" x2="29.4386" y2="0.8382" layer="94"/>
<rectangle x1="0.127" y1="0.8382" x2="5.5118" y2="0.889" layer="94"/>
<rectangle x1="7.6454" y1="0.8382" x2="7.8994" y2="0.889" layer="94"/>
<rectangle x1="11.049" y1="0.8382" x2="11.7602" y2="0.889" layer="94"/>
<rectangle x1="15.0114" y1="0.8382" x2="15.9258" y2="0.889" layer="94"/>
<rectangle x1="19.177" y1="0.8382" x2="19.431" y2="0.889" layer="94"/>
<rectangle x1="22.5298" y1="0.8382" x2="23.241" y2="0.889" layer="94"/>
<rectangle x1="25.4762" y1="0.8382" x2="26.3906" y2="0.889" layer="94"/>
<rectangle x1="29.1846" y1="0.8382" x2="29.4894" y2="0.889" layer="94"/>
<rectangle x1="0.0762" y1="0.889" x2="5.5118" y2="0.9398" layer="94"/>
<rectangle x1="7.6454" y1="0.889" x2="7.8994" y2="0.9398" layer="94"/>
<rectangle x1="10.9982" y1="0.889" x2="11.7602" y2="0.9398" layer="94"/>
<rectangle x1="15.0114" y1="0.889" x2="15.9258" y2="0.9398" layer="94"/>
<rectangle x1="19.177" y1="0.889" x2="19.431" y2="0.9398" layer="94"/>
<rectangle x1="22.479" y1="0.889" x2="23.241" y2="0.9398" layer="94"/>
<rectangle x1="25.4762" y1="0.889" x2="26.3398" y2="0.9398" layer="94"/>
<rectangle x1="29.2354" y1="0.889" x2="29.4894" y2="0.9398" layer="94"/>
<rectangle x1="0.0762" y1="0.9398" x2="5.5118" y2="0.9906" layer="94"/>
<rectangle x1="7.6454" y1="0.9398" x2="7.8994" y2="0.9906" layer="94"/>
<rectangle x1="10.9982" y1="0.9398" x2="11.7602" y2="0.9906" layer="94"/>
<rectangle x1="15.0114" y1="0.9398" x2="15.9258" y2="0.9906" layer="94"/>
<rectangle x1="19.177" y1="0.9398" x2="19.431" y2="0.9906" layer="94"/>
<rectangle x1="22.479" y1="0.9398" x2="23.241" y2="0.9906" layer="94"/>
<rectangle x1="25.4254" y1="0.9398" x2="26.3398" y2="0.9906" layer="94"/>
<rectangle x1="29.2354" y1="0.9398" x2="29.4894" y2="0.9906" layer="94"/>
<rectangle x1="0.0762" y1="0.9906" x2="5.5626" y2="1.0414" layer="94"/>
<rectangle x1="7.6454" y1="0.9906" x2="7.8994" y2="1.0414" layer="94"/>
<rectangle x1="10.9474" y1="0.9906" x2="11.7602" y2="1.0414" layer="94"/>
<rectangle x1="15.0114" y1="0.9906" x2="15.9258" y2="1.0414" layer="94"/>
<rectangle x1="19.177" y1="0.9906" x2="19.431" y2="1.0414" layer="94"/>
<rectangle x1="22.4282" y1="0.9906" x2="23.241" y2="1.0414" layer="94"/>
<rectangle x1="25.4254" y1="0.9906" x2="26.3398" y2="1.0414" layer="94"/>
<rectangle x1="29.2862" y1="0.9906" x2="29.5402" y2="1.0414" layer="94"/>
<rectangle x1="0.0254" y1="1.0414" x2="5.5626" y2="1.0922" layer="94"/>
<rectangle x1="7.6454" y1="1.0414" x2="7.8994" y2="1.0922" layer="94"/>
<rectangle x1="10.8966" y1="1.0414" x2="11.7602" y2="1.0922" layer="94"/>
<rectangle x1="15.0114" y1="1.0414" x2="15.9258" y2="1.0922" layer="94"/>
<rectangle x1="19.177" y1="1.0414" x2="19.431" y2="1.0922" layer="94"/>
<rectangle x1="22.3774" y1="1.0414" x2="23.241" y2="1.0922" layer="94"/>
<rectangle x1="25.4254" y1="1.0414" x2="26.289" y2="1.0922" layer="94"/>
<rectangle x1="29.2862" y1="1.0414" x2="29.5402" y2="1.0922" layer="94"/>
<rectangle x1="0.0254" y1="1.0922" x2="5.6134" y2="1.143" layer="94"/>
<rectangle x1="7.6454" y1="1.0922" x2="7.8994" y2="1.143" layer="94"/>
<rectangle x1="10.8458" y1="1.0922" x2="11.7602" y2="1.143" layer="94"/>
<rectangle x1="15.0114" y1="1.0922" x2="15.9258" y2="1.143" layer="94"/>
<rectangle x1="19.177" y1="1.0922" x2="19.431" y2="1.143" layer="94"/>
<rectangle x1="22.3266" y1="1.0922" x2="23.241" y2="1.143" layer="94"/>
<rectangle x1="25.4254" y1="1.0922" x2="26.289" y2="1.143" layer="94"/>
<rectangle x1="29.2862" y1="1.0922" x2="29.591" y2="1.143" layer="94"/>
<rectangle x1="0.0254" y1="1.143" x2="5.6134" y2="1.1938" layer="94"/>
<rectangle x1="7.6454" y1="1.143" x2="7.8994" y2="1.1938" layer="94"/>
<rectangle x1="10.795" y1="1.143" x2="11.7602" y2="1.1938" layer="94"/>
<rectangle x1="15.0114" y1="1.143" x2="15.9258" y2="1.1938" layer="94"/>
<rectangle x1="19.177" y1="1.143" x2="19.431" y2="1.1938" layer="94"/>
<rectangle x1="22.3266" y1="1.143" x2="23.241" y2="1.1938" layer="94"/>
<rectangle x1="25.3746" y1="1.143" x2="26.289" y2="1.1938" layer="94"/>
<rectangle x1="29.337" y1="1.143" x2="29.591" y2="1.1938" layer="94"/>
<rectangle x1="0.0254" y1="1.1938" x2="5.6134" y2="1.2446" layer="94"/>
<rectangle x1="7.6454" y1="1.1938" x2="7.8994" y2="1.2446" layer="94"/>
<rectangle x1="10.795" y1="1.1938" x2="11.7602" y2="1.2446" layer="94"/>
<rectangle x1="15.0114" y1="1.1938" x2="15.9258" y2="1.2446" layer="94"/>
<rectangle x1="19.177" y1="1.1938" x2="19.431" y2="1.2446" layer="94"/>
<rectangle x1="22.2758" y1="1.1938" x2="23.241" y2="1.2446" layer="94"/>
<rectangle x1="25.3746" y1="1.1938" x2="26.289" y2="1.2446" layer="94"/>
<rectangle x1="29.337" y1="1.1938" x2="29.591" y2="1.2446" layer="94"/>
<rectangle x1="-0.0254" y1="1.2446" x2="5.6642" y2="1.2954" layer="94"/>
<rectangle x1="7.6454" y1="1.2446" x2="7.8994" y2="1.2954" layer="94"/>
<rectangle x1="10.7442" y1="1.2446" x2="11.7602" y2="1.2954" layer="94"/>
<rectangle x1="15.0114" y1="1.2446" x2="15.9258" y2="1.2954" layer="94"/>
<rectangle x1="19.177" y1="1.2446" x2="19.431" y2="1.2954" layer="94"/>
<rectangle x1="22.2758" y1="1.2446" x2="23.241" y2="1.2954" layer="94"/>
<rectangle x1="25.3746" y1="1.2446" x2="26.289" y2="1.2954" layer="94"/>
<rectangle x1="29.337" y1="1.2446" x2="29.591" y2="1.2954" layer="94"/>
<rectangle x1="-0.0254" y1="1.2954" x2="5.6642" y2="1.3462" layer="94"/>
<rectangle x1="7.6454" y1="1.2954" x2="7.8994" y2="1.3462" layer="94"/>
<rectangle x1="10.7442" y1="1.2954" x2="11.7602" y2="1.3462" layer="94"/>
<rectangle x1="15.0114" y1="1.2954" x2="15.9258" y2="1.3462" layer="94"/>
<rectangle x1="19.177" y1="1.2954" x2="19.431" y2="1.3462" layer="94"/>
<rectangle x1="22.225" y1="1.2954" x2="23.241" y2="1.3462" layer="94"/>
<rectangle x1="25.3746" y1="1.2954" x2="26.289" y2="1.3462" layer="94"/>
<rectangle x1="29.337" y1="1.2954" x2="29.591" y2="1.3462" layer="94"/>
<rectangle x1="-0.0254" y1="1.3462" x2="5.6642" y2="1.397" layer="94"/>
<rectangle x1="7.6454" y1="1.3462" x2="7.8994" y2="1.397" layer="94"/>
<rectangle x1="10.6934" y1="1.3462" x2="11.7602" y2="1.397" layer="94"/>
<rectangle x1="15.0114" y1="1.3462" x2="15.9258" y2="1.397" layer="94"/>
<rectangle x1="19.177" y1="1.3462" x2="19.431" y2="1.397" layer="94"/>
<rectangle x1="22.1742" y1="1.3462" x2="23.241" y2="1.397" layer="94"/>
<rectangle x1="25.3746" y1="1.3462" x2="26.2382" y2="1.397" layer="94"/>
<rectangle x1="29.337" y1="1.3462" x2="29.591" y2="1.397" layer="94"/>
<rectangle x1="-0.0254" y1="1.397" x2="5.6642" y2="1.4478" layer="94"/>
<rectangle x1="7.6454" y1="1.397" x2="7.8994" y2="1.4478" layer="94"/>
<rectangle x1="10.6426" y1="1.397" x2="11.7602" y2="1.4478" layer="94"/>
<rectangle x1="15.0114" y1="1.397" x2="15.9258" y2="1.4478" layer="94"/>
<rectangle x1="19.177" y1="1.397" x2="19.431" y2="1.4478" layer="94"/>
<rectangle x1="22.1234" y1="1.397" x2="23.241" y2="1.4478" layer="94"/>
<rectangle x1="25.3746" y1="1.397" x2="26.2382" y2="1.4478" layer="94"/>
<rectangle x1="29.337" y1="1.397" x2="29.591" y2="1.4478" layer="94"/>
<rectangle x1="-0.0254" y1="1.4478" x2="5.6642" y2="1.4986" layer="94"/>
<rectangle x1="7.6454" y1="1.4478" x2="7.8994" y2="1.4986" layer="94"/>
<rectangle x1="10.6426" y1="1.4478" x2="11.7602" y2="1.4986" layer="94"/>
<rectangle x1="15.0114" y1="1.4478" x2="15.9258" y2="1.4986" layer="94"/>
<rectangle x1="19.177" y1="1.4478" x2="19.431" y2="1.4986" layer="94"/>
<rectangle x1="22.1234" y1="1.4478" x2="23.241" y2="1.4986" layer="94"/>
<rectangle x1="25.3746" y1="1.4478" x2="26.2382" y2="1.4986" layer="94"/>
<rectangle x1="29.337" y1="1.4478" x2="29.591" y2="1.4986" layer="94"/>
<rectangle x1="-0.0254" y1="1.4986" x2="5.6642" y2="1.5494" layer="94"/>
<rectangle x1="7.6454" y1="1.4986" x2="7.8994" y2="1.5494" layer="94"/>
<rectangle x1="10.5918" y1="1.4986" x2="11.7602" y2="1.5494" layer="94"/>
<rectangle x1="15.0114" y1="1.4986" x2="15.9258" y2="1.5494" layer="94"/>
<rectangle x1="19.177" y1="1.4986" x2="19.431" y2="1.5494" layer="94"/>
<rectangle x1="22.0726" y1="1.4986" x2="23.241" y2="1.5494" layer="94"/>
<rectangle x1="25.3746" y1="1.4986" x2="26.2382" y2="1.5494" layer="94"/>
<rectangle x1="29.337" y1="1.4986" x2="29.591" y2="1.5494" layer="94"/>
<rectangle x1="-0.0254" y1="1.5494" x2="1.397" y2="1.6002" layer="94"/>
<rectangle x1="4.2418" y1="1.5494" x2="5.6642" y2="1.6002" layer="94"/>
<rectangle x1="7.6454" y1="1.5494" x2="7.8994" y2="1.6002" layer="94"/>
<rectangle x1="10.541" y1="1.5494" x2="11.7602" y2="1.6002" layer="94"/>
<rectangle x1="15.0114" y1="1.5494" x2="15.9258" y2="1.6002" layer="94"/>
<rectangle x1="19.177" y1="1.5494" x2="19.431" y2="1.6002" layer="94"/>
<rectangle x1="22.0218" y1="1.5494" x2="23.241" y2="1.6002" layer="94"/>
<rectangle x1="25.3746" y1="1.5494" x2="26.2382" y2="1.6002" layer="94"/>
<rectangle x1="29.3878" y1="1.5494" x2="29.6418" y2="1.6002" layer="94"/>
<rectangle x1="-0.0254" y1="1.6002" x2="1.397" y2="1.651" layer="94"/>
<rectangle x1="4.2418" y1="1.6002" x2="5.6642" y2="1.651" layer="94"/>
<rectangle x1="7.6454" y1="1.6002" x2="7.8994" y2="1.651" layer="94"/>
<rectangle x1="10.4902" y1="1.6002" x2="11.7602" y2="1.651" layer="94"/>
<rectangle x1="15.0114" y1="1.6002" x2="15.9258" y2="1.651" layer="94"/>
<rectangle x1="19.177" y1="1.6002" x2="19.431" y2="1.651" layer="94"/>
<rectangle x1="22.0218" y1="1.6002" x2="23.241" y2="1.651" layer="94"/>
<rectangle x1="25.3746" y1="1.6002" x2="26.2382" y2="1.651" layer="94"/>
<rectangle x1="29.3878" y1="1.6002" x2="29.6418" y2="1.651" layer="94"/>
<rectangle x1="-0.0254" y1="1.651" x2="1.397" y2="1.7018" layer="94"/>
<rectangle x1="4.2418" y1="1.651" x2="5.6642" y2="1.7018" layer="94"/>
<rectangle x1="7.6454" y1="1.651" x2="7.8994" y2="1.7018" layer="94"/>
<rectangle x1="10.4902" y1="1.651" x2="11.7602" y2="1.7018" layer="94"/>
<rectangle x1="15.0114" y1="1.651" x2="15.9258" y2="1.7018" layer="94"/>
<rectangle x1="19.177" y1="1.651" x2="19.431" y2="1.7018" layer="94"/>
<rectangle x1="21.971" y1="1.651" x2="23.241" y2="1.7018" layer="94"/>
<rectangle x1="25.3746" y1="1.651" x2="26.2382" y2="1.7018" layer="94"/>
<rectangle x1="29.3878" y1="1.651" x2="29.6418" y2="1.7018" layer="94"/>
<rectangle x1="-0.0254" y1="1.7018" x2="1.397" y2="1.7526" layer="94"/>
<rectangle x1="4.2418" y1="1.7018" x2="5.6642" y2="1.7526" layer="94"/>
<rectangle x1="7.6454" y1="1.7018" x2="7.8994" y2="1.7526" layer="94"/>
<rectangle x1="10.4394" y1="1.7018" x2="11.7602" y2="1.7526" layer="94"/>
<rectangle x1="15.0114" y1="1.7018" x2="15.9258" y2="1.7526" layer="94"/>
<rectangle x1="19.177" y1="1.7018" x2="19.431" y2="1.7526" layer="94"/>
<rectangle x1="21.9202" y1="1.7018" x2="23.241" y2="1.7526" layer="94"/>
<rectangle x1="25.3746" y1="1.7018" x2="26.2382" y2="1.7526" layer="94"/>
<rectangle x1="29.3878" y1="1.7018" x2="29.6418" y2="1.7526" layer="94"/>
<rectangle x1="-0.0254" y1="1.7526" x2="1.397" y2="1.8034" layer="94"/>
<rectangle x1="4.2418" y1="1.7526" x2="5.6642" y2="1.8034" layer="94"/>
<rectangle x1="7.6454" y1="1.7526" x2="7.8994" y2="1.8034" layer="94"/>
<rectangle x1="10.4394" y1="1.7526" x2="11.7602" y2="1.8034" layer="94"/>
<rectangle x1="15.0114" y1="1.7526" x2="15.9258" y2="1.8034" layer="94"/>
<rectangle x1="19.177" y1="1.7526" x2="19.431" y2="1.8034" layer="94"/>
<rectangle x1="21.9202" y1="1.7526" x2="23.241" y2="1.8034" layer="94"/>
<rectangle x1="25.3746" y1="1.7526" x2="26.2382" y2="1.8034" layer="94"/>
<rectangle x1="29.3878" y1="1.7526" x2="29.6418" y2="1.8034" layer="94"/>
<rectangle x1="-0.0254" y1="1.8034" x2="1.397" y2="1.8542" layer="94"/>
<rectangle x1="2.5146" y1="1.8034" x2="3.1242" y2="1.8542" layer="94"/>
<rectangle x1="4.2418" y1="1.8034" x2="5.6642" y2="1.8542" layer="94"/>
<rectangle x1="7.6454" y1="1.8034" x2="7.8994" y2="1.8542" layer="94"/>
<rectangle x1="10.3886" y1="1.8034" x2="11.4554" y2="1.8542" layer="94"/>
<rectangle x1="11.5062" y1="1.8034" x2="11.7602" y2="1.8542" layer="94"/>
<rectangle x1="15.0114" y1="1.8034" x2="15.9258" y2="1.8542" layer="94"/>
<rectangle x1="19.177" y1="1.8034" x2="19.431" y2="1.8542" layer="94"/>
<rectangle x1="21.8694" y1="1.8034" x2="22.9362" y2="1.8542" layer="94"/>
<rectangle x1="22.987" y1="1.8034" x2="23.241" y2="1.8542" layer="94"/>
<rectangle x1="25.3746" y1="1.8034" x2="26.2382" y2="1.8542" layer="94"/>
<rectangle x1="29.3878" y1="1.8034" x2="29.6418" y2="1.8542" layer="94"/>
<rectangle x1="-0.0254" y1="1.8542" x2="1.397" y2="1.905" layer="94"/>
<rectangle x1="2.3114" y1="1.8542" x2="3.2766" y2="1.905" layer="94"/>
<rectangle x1="4.2418" y1="1.8542" x2="5.6642" y2="1.905" layer="94"/>
<rectangle x1="7.6454" y1="1.8542" x2="7.8994" y2="1.905" layer="94"/>
<rectangle x1="10.3378" y1="1.8542" x2="11.4046" y2="1.905" layer="94"/>
<rectangle x1="11.5062" y1="1.8542" x2="11.7602" y2="1.905" layer="94"/>
<rectangle x1="15.0114" y1="1.8542" x2="15.9258" y2="1.905" layer="94"/>
<rectangle x1="19.177" y1="1.8542" x2="19.431" y2="1.905" layer="94"/>
<rectangle x1="21.8186" y1="1.8542" x2="22.8854" y2="1.905" layer="94"/>
<rectangle x1="22.987" y1="1.8542" x2="23.241" y2="1.905" layer="94"/>
<rectangle x1="25.3746" y1="1.8542" x2="26.2382" y2="1.905" layer="94"/>
<rectangle x1="29.3878" y1="1.8542" x2="29.6418" y2="1.905" layer="94"/>
<rectangle x1="-0.0254" y1="1.905" x2="1.397" y2="1.9558" layer="94"/>
<rectangle x1="2.2098" y1="1.905" x2="3.3782" y2="1.9558" layer="94"/>
<rectangle x1="4.2418" y1="1.905" x2="5.6642" y2="1.9558" layer="94"/>
<rectangle x1="7.6454" y1="1.905" x2="7.8994" y2="1.9558" layer="94"/>
<rectangle x1="10.287" y1="1.905" x2="11.3538" y2="1.9558" layer="94"/>
<rectangle x1="11.5062" y1="1.905" x2="11.7602" y2="1.9558" layer="94"/>
<rectangle x1="15.0114" y1="1.905" x2="15.9258" y2="1.9558" layer="94"/>
<rectangle x1="19.177" y1="1.905" x2="19.431" y2="1.9558" layer="94"/>
<rectangle x1="21.7678" y1="1.905" x2="22.8854" y2="1.9558" layer="94"/>
<rectangle x1="22.987" y1="1.905" x2="23.241" y2="1.9558" layer="94"/>
<rectangle x1="25.3746" y1="1.905" x2="26.2382" y2="1.9558" layer="94"/>
<rectangle x1="29.3878" y1="1.905" x2="29.6418" y2="1.9558" layer="94"/>
<rectangle x1="-0.0254" y1="1.9558" x2="1.397" y2="2.0066" layer="94"/>
<rectangle x1="2.159" y1="1.9558" x2="3.4798" y2="2.0066" layer="94"/>
<rectangle x1="4.2418" y1="1.9558" x2="5.6642" y2="2.0066" layer="94"/>
<rectangle x1="7.6454" y1="1.9558" x2="7.8994" y2="2.0066" layer="94"/>
<rectangle x1="10.2362" y1="1.9558" x2="11.3538" y2="2.0066" layer="94"/>
<rectangle x1="11.5062" y1="1.9558" x2="11.7602" y2="2.0066" layer="94"/>
<rectangle x1="15.0114" y1="1.9558" x2="15.9258" y2="2.0066" layer="94"/>
<rectangle x1="19.177" y1="1.9558" x2="19.431" y2="2.0066" layer="94"/>
<rectangle x1="21.7678" y1="1.9558" x2="22.8346" y2="2.0066" layer="94"/>
<rectangle x1="22.987" y1="1.9558" x2="23.241" y2="2.0066" layer="94"/>
<rectangle x1="25.3746" y1="1.9558" x2="26.2382" y2="2.0066" layer="94"/>
<rectangle x1="29.3878" y1="1.9558" x2="29.6418" y2="2.0066" layer="94"/>
<rectangle x1="-0.0254" y1="2.0066" x2="1.397" y2="2.0574" layer="94"/>
<rectangle x1="2.0574" y1="2.0066" x2="3.5306" y2="2.0574" layer="94"/>
<rectangle x1="4.2418" y1="2.0066" x2="5.6642" y2="2.0574" layer="94"/>
<rectangle x1="7.6454" y1="2.0066" x2="7.8994" y2="2.0574" layer="94"/>
<rectangle x1="10.2362" y1="2.0066" x2="11.303" y2="2.0574" layer="94"/>
<rectangle x1="11.5062" y1="2.0066" x2="11.7602" y2="2.0574" layer="94"/>
<rectangle x1="15.0114" y1="2.0066" x2="15.9258" y2="2.0574" layer="94"/>
<rectangle x1="19.177" y1="2.0066" x2="19.431" y2="2.0574" layer="94"/>
<rectangle x1="21.717" y1="2.0066" x2="22.7838" y2="2.0574" layer="94"/>
<rectangle x1="22.987" y1="2.0066" x2="23.241" y2="2.0574" layer="94"/>
<rectangle x1="25.3746" y1="2.0066" x2="26.2382" y2="2.0574" layer="94"/>
<rectangle x1="29.3878" y1="2.0066" x2="29.6418" y2="2.0574" layer="94"/>
<rectangle x1="-0.0254" y1="2.0574" x2="1.397" y2="2.1082" layer="94"/>
<rectangle x1="2.0066" y1="2.0574" x2="3.5814" y2="2.1082" layer="94"/>
<rectangle x1="4.2418" y1="2.0574" x2="5.6642" y2="2.1082" layer="94"/>
<rectangle x1="7.6454" y1="2.0574" x2="7.8994" y2="2.1082" layer="94"/>
<rectangle x1="10.1854" y1="2.0574" x2="11.303" y2="2.1082" layer="94"/>
<rectangle x1="11.5062" y1="2.0574" x2="11.7602" y2="2.1082" layer="94"/>
<rectangle x1="15.0114" y1="2.0574" x2="15.9258" y2="2.1082" layer="94"/>
<rectangle x1="19.177" y1="2.0574" x2="19.431" y2="2.1082" layer="94"/>
<rectangle x1="21.717" y1="2.0574" x2="22.7838" y2="2.1082" layer="94"/>
<rectangle x1="22.987" y1="2.0574" x2="23.241" y2="2.1082" layer="94"/>
<rectangle x1="25.3746" y1="2.0574" x2="26.2382" y2="2.1082" layer="94"/>
<rectangle x1="29.3878" y1="2.0574" x2="29.6418" y2="2.1082" layer="94"/>
<rectangle x1="-0.0254" y1="2.1082" x2="1.397" y2="2.159" layer="94"/>
<rectangle x1="1.9558" y1="2.1082" x2="3.6322" y2="2.159" layer="94"/>
<rectangle x1="4.2418" y1="2.1082" x2="5.6642" y2="2.159" layer="94"/>
<rectangle x1="7.6454" y1="2.1082" x2="7.8994" y2="2.159" layer="94"/>
<rectangle x1="10.1854" y1="2.1082" x2="11.2522" y2="2.159" layer="94"/>
<rectangle x1="11.5062" y1="2.1082" x2="11.7602" y2="2.159" layer="94"/>
<rectangle x1="15.0114" y1="2.1082" x2="15.9258" y2="2.159" layer="94"/>
<rectangle x1="19.177" y1="2.1082" x2="19.431" y2="2.159" layer="94"/>
<rectangle x1="21.6662" y1="2.1082" x2="22.733" y2="2.159" layer="94"/>
<rectangle x1="22.987" y1="2.1082" x2="23.241" y2="2.159" layer="94"/>
<rectangle x1="25.3746" y1="2.1082" x2="26.2382" y2="2.159" layer="94"/>
<rectangle x1="29.3878" y1="2.1082" x2="29.6418" y2="2.159" layer="94"/>
<rectangle x1="-0.0254" y1="2.159" x2="1.397" y2="2.2098" layer="94"/>
<rectangle x1="1.905" y1="2.159" x2="3.683" y2="2.2098" layer="94"/>
<rectangle x1="4.2418" y1="2.159" x2="5.6642" y2="2.2098" layer="94"/>
<rectangle x1="7.6454" y1="2.159" x2="7.8994" y2="2.2098" layer="94"/>
<rectangle x1="10.1346" y1="2.159" x2="11.2014" y2="2.2098" layer="94"/>
<rectangle x1="11.5062" y1="2.159" x2="11.7602" y2="2.2098" layer="94"/>
<rectangle x1="15.0114" y1="2.159" x2="15.9258" y2="2.2098" layer="94"/>
<rectangle x1="19.177" y1="2.159" x2="19.431" y2="2.2098" layer="94"/>
<rectangle x1="21.6154" y1="2.159" x2="22.6822" y2="2.2098" layer="94"/>
<rectangle x1="22.987" y1="2.159" x2="23.241" y2="2.2098" layer="94"/>
<rectangle x1="25.3746" y1="2.159" x2="26.2382" y2="2.2098" layer="94"/>
<rectangle x1="29.3878" y1="2.159" x2="29.6418" y2="2.2098" layer="94"/>
<rectangle x1="-0.0254" y1="2.2098" x2="1.397" y2="2.2606" layer="94"/>
<rectangle x1="1.8542" y1="2.2098" x2="3.7338" y2="2.2606" layer="94"/>
<rectangle x1="4.2418" y1="2.2098" x2="5.6642" y2="2.2606" layer="94"/>
<rectangle x1="7.6454" y1="2.2098" x2="7.8994" y2="2.2606" layer="94"/>
<rectangle x1="10.0838" y1="2.2098" x2="11.2014" y2="2.2606" layer="94"/>
<rectangle x1="11.5062" y1="2.2098" x2="11.7602" y2="2.2606" layer="94"/>
<rectangle x1="15.0114" y1="2.2098" x2="15.9258" y2="2.2606" layer="94"/>
<rectangle x1="19.177" y1="2.2098" x2="19.431" y2="2.2606" layer="94"/>
<rectangle x1="21.5646" y1="2.2098" x2="22.6314" y2="2.2606" layer="94"/>
<rectangle x1="22.987" y1="2.2098" x2="23.241" y2="2.2606" layer="94"/>
<rectangle x1="25.3746" y1="2.2098" x2="26.2382" y2="2.2606" layer="94"/>
<rectangle x1="29.3878" y1="2.2098" x2="29.6418" y2="2.2606" layer="94"/>
<rectangle x1="-0.0254" y1="2.2606" x2="1.397" y2="2.3114" layer="94"/>
<rectangle x1="1.8542" y1="2.2606" x2="3.7846" y2="2.3114" layer="94"/>
<rectangle x1="4.2418" y1="2.2606" x2="5.6642" y2="2.3114" layer="94"/>
<rectangle x1="7.6454" y1="2.2606" x2="7.8994" y2="2.3114" layer="94"/>
<rectangle x1="10.033" y1="2.2606" x2="11.1506" y2="2.3114" layer="94"/>
<rectangle x1="11.5062" y1="2.2606" x2="11.7602" y2="2.3114" layer="94"/>
<rectangle x1="15.0114" y1="2.2606" x2="15.9258" y2="2.3114" layer="94"/>
<rectangle x1="19.177" y1="2.2606" x2="19.431" y2="2.3114" layer="94"/>
<rectangle x1="21.5646" y1="2.2606" x2="22.6314" y2="2.3114" layer="94"/>
<rectangle x1="22.987" y1="2.2606" x2="23.241" y2="2.3114" layer="94"/>
<rectangle x1="25.3746" y1="2.2606" x2="26.2382" y2="2.3114" layer="94"/>
<rectangle x1="29.3878" y1="2.2606" x2="29.6418" y2="2.3114" layer="94"/>
<rectangle x1="-0.0254" y1="2.3114" x2="1.397" y2="2.3622" layer="94"/>
<rectangle x1="1.8034" y1="2.3114" x2="3.7846" y2="2.3622" layer="94"/>
<rectangle x1="4.2418" y1="2.3114" x2="5.6642" y2="2.3622" layer="94"/>
<rectangle x1="7.6454" y1="2.3114" x2="7.8994" y2="2.3622" layer="94"/>
<rectangle x1="10.033" y1="2.3114" x2="11.0998" y2="2.3622" layer="94"/>
<rectangle x1="11.5062" y1="2.3114" x2="11.7602" y2="2.3622" layer="94"/>
<rectangle x1="15.0114" y1="2.3114" x2="15.9258" y2="2.3622" layer="94"/>
<rectangle x1="19.177" y1="2.3114" x2="19.431" y2="2.3622" layer="94"/>
<rectangle x1="21.5138" y1="2.3114" x2="22.5806" y2="2.3622" layer="94"/>
<rectangle x1="22.987" y1="2.3114" x2="23.241" y2="2.3622" layer="94"/>
<rectangle x1="25.3746" y1="2.3114" x2="26.2382" y2="2.3622" layer="94"/>
<rectangle x1="29.3878" y1="2.3114" x2="29.6418" y2="2.3622" layer="94"/>
<rectangle x1="-0.0254" y1="2.3622" x2="1.397" y2="2.413" layer="94"/>
<rectangle x1="1.8034" y1="2.3622" x2="3.8354" y2="2.413" layer="94"/>
<rectangle x1="4.2418" y1="2.3622" x2="5.6642" y2="2.413" layer="94"/>
<rectangle x1="7.6454" y1="2.3622" x2="7.8994" y2="2.413" layer="94"/>
<rectangle x1="9.9822" y1="2.3622" x2="11.049" y2="2.413" layer="94"/>
<rectangle x1="11.5062" y1="2.3622" x2="11.7602" y2="2.413" layer="94"/>
<rectangle x1="15.0114" y1="2.3622" x2="15.9258" y2="2.413" layer="94"/>
<rectangle x1="19.177" y1="2.3622" x2="19.431" y2="2.413" layer="94"/>
<rectangle x1="21.463" y1="2.3622" x2="22.5806" y2="2.413" layer="94"/>
<rectangle x1="22.987" y1="2.3622" x2="23.241" y2="2.413" layer="94"/>
<rectangle x1="25.3746" y1="2.3622" x2="26.2382" y2="2.413" layer="94"/>
<rectangle x1="29.3878" y1="2.3622" x2="29.6418" y2="2.413" layer="94"/>
<rectangle x1="-0.0254" y1="2.413" x2="1.397" y2="2.4638" layer="94"/>
<rectangle x1="1.7526" y1="2.413" x2="3.8354" y2="2.4638" layer="94"/>
<rectangle x1="4.2418" y1="2.413" x2="5.6642" y2="2.4638" layer="94"/>
<rectangle x1="7.6454" y1="2.413" x2="7.8994" y2="2.4638" layer="94"/>
<rectangle x1="9.9314" y1="2.413" x2="11.049" y2="2.4638" layer="94"/>
<rectangle x1="11.5062" y1="2.413" x2="11.7602" y2="2.4638" layer="94"/>
<rectangle x1="15.0114" y1="2.413" x2="15.9258" y2="2.4638" layer="94"/>
<rectangle x1="19.177" y1="2.413" x2="19.431" y2="2.4638" layer="94"/>
<rectangle x1="21.463" y1="2.413" x2="22.5298" y2="2.4638" layer="94"/>
<rectangle x1="22.987" y1="2.413" x2="23.241" y2="2.4638" layer="94"/>
<rectangle x1="25.3746" y1="2.413" x2="26.2382" y2="2.4638" layer="94"/>
<rectangle x1="29.3878" y1="2.413" x2="29.6418" y2="2.4638" layer="94"/>
<rectangle x1="-0.0254" y1="2.4638" x2="1.397" y2="2.5146" layer="94"/>
<rectangle x1="1.7526" y1="2.4638" x2="3.8862" y2="2.5146" layer="94"/>
<rectangle x1="4.2418" y1="2.4638" x2="5.6642" y2="2.5146" layer="94"/>
<rectangle x1="7.6454" y1="2.4638" x2="7.8994" y2="2.5146" layer="94"/>
<rectangle x1="9.9314" y1="2.4638" x2="10.9982" y2="2.5146" layer="94"/>
<rectangle x1="11.5062" y1="2.4638" x2="11.7602" y2="2.5146" layer="94"/>
<rectangle x1="15.0114" y1="2.4638" x2="15.9258" y2="2.5146" layer="94"/>
<rectangle x1="19.177" y1="2.4638" x2="19.431" y2="2.5146" layer="94"/>
<rectangle x1="21.4122" y1="2.4638" x2="22.479" y2="2.5146" layer="94"/>
<rectangle x1="22.987" y1="2.4638" x2="23.241" y2="2.5146" layer="94"/>
<rectangle x1="25.3746" y1="2.4638" x2="26.2382" y2="2.5146" layer="94"/>
<rectangle x1="29.3878" y1="2.4638" x2="29.6418" y2="2.5146" layer="94"/>
<rectangle x1="-0.0254" y1="2.5146" x2="1.397" y2="2.5654" layer="94"/>
<rectangle x1="1.7018" y1="2.5146" x2="3.8862" y2="2.5654" layer="94"/>
<rectangle x1="4.2418" y1="2.5146" x2="5.6642" y2="2.5654" layer="94"/>
<rectangle x1="7.6454" y1="2.5146" x2="7.8994" y2="2.5654" layer="94"/>
<rectangle x1="9.8806" y1="2.5146" x2="10.9474" y2="2.5654" layer="94"/>
<rectangle x1="11.5062" y1="2.5146" x2="11.7602" y2="2.5654" layer="94"/>
<rectangle x1="15.0114" y1="2.5146" x2="15.9258" y2="2.5654" layer="94"/>
<rectangle x1="19.177" y1="2.5146" x2="19.431" y2="2.5654" layer="94"/>
<rectangle x1="21.3614" y1="2.5146" x2="22.479" y2="2.5654" layer="94"/>
<rectangle x1="22.987" y1="2.5146" x2="23.241" y2="2.5654" layer="94"/>
<rectangle x1="25.3746" y1="2.5146" x2="26.2382" y2="2.5654" layer="94"/>
<rectangle x1="29.3878" y1="2.5146" x2="29.6418" y2="2.5654" layer="94"/>
<rectangle x1="-0.0254" y1="2.5654" x2="1.397" y2="2.6162" layer="94"/>
<rectangle x1="1.7018" y1="2.5654" x2="3.937" y2="2.6162" layer="94"/>
<rectangle x1="4.2418" y1="2.5654" x2="5.6642" y2="2.6162" layer="94"/>
<rectangle x1="7.6454" y1="2.5654" x2="7.8994" y2="2.6162" layer="94"/>
<rectangle x1="9.8298" y1="2.5654" x2="10.9474" y2="2.6162" layer="94"/>
<rectangle x1="11.5062" y1="2.5654" x2="11.7602" y2="2.6162" layer="94"/>
<rectangle x1="15.0114" y1="2.5654" x2="15.9258" y2="2.6162" layer="94"/>
<rectangle x1="19.177" y1="2.5654" x2="19.431" y2="2.6162" layer="94"/>
<rectangle x1="21.3106" y1="2.5654" x2="22.4282" y2="2.6162" layer="94"/>
<rectangle x1="22.987" y1="2.5654" x2="23.241" y2="2.6162" layer="94"/>
<rectangle x1="25.3746" y1="2.5654" x2="26.2382" y2="2.6162" layer="94"/>
<rectangle x1="29.3878" y1="2.5654" x2="29.6418" y2="2.6162" layer="94"/>
<rectangle x1="-0.0254" y1="2.6162" x2="1.397" y2="2.667" layer="94"/>
<rectangle x1="1.651" y1="2.6162" x2="3.937" y2="2.667" layer="94"/>
<rectangle x1="4.2418" y1="2.6162" x2="5.6642" y2="2.667" layer="94"/>
<rectangle x1="7.6454" y1="2.6162" x2="7.8994" y2="2.667" layer="94"/>
<rectangle x1="9.8298" y1="2.6162" x2="10.8966" y2="2.667" layer="94"/>
<rectangle x1="11.5062" y1="2.6162" x2="11.7602" y2="2.667" layer="94"/>
<rectangle x1="15.0114" y1="2.6162" x2="15.9258" y2="2.667" layer="94"/>
<rectangle x1="19.177" y1="2.6162" x2="19.431" y2="2.667" layer="94"/>
<rectangle x1="21.3106" y1="2.6162" x2="22.3774" y2="2.667" layer="94"/>
<rectangle x1="22.987" y1="2.6162" x2="23.241" y2="2.667" layer="94"/>
<rectangle x1="25.3746" y1="2.6162" x2="26.2382" y2="2.667" layer="94"/>
<rectangle x1="29.3878" y1="2.6162" x2="29.6418" y2="2.667" layer="94"/>
<rectangle x1="-0.0254" y1="2.667" x2="1.397" y2="2.7178" layer="94"/>
<rectangle x1="1.651" y1="2.667" x2="3.937" y2="2.7178" layer="94"/>
<rectangle x1="4.2418" y1="2.667" x2="5.6642" y2="2.7178" layer="94"/>
<rectangle x1="7.6454" y1="2.667" x2="7.8994" y2="2.7178" layer="94"/>
<rectangle x1="9.779" y1="2.667" x2="10.8458" y2="2.7178" layer="94"/>
<rectangle x1="11.5062" y1="2.667" x2="11.7602" y2="2.7178" layer="94"/>
<rectangle x1="15.0114" y1="2.667" x2="15.9258" y2="2.7178" layer="94"/>
<rectangle x1="19.177" y1="2.667" x2="19.431" y2="2.7178" layer="94"/>
<rectangle x1="21.2598" y1="2.667" x2="22.3266" y2="2.7178" layer="94"/>
<rectangle x1="22.987" y1="2.667" x2="23.241" y2="2.7178" layer="94"/>
<rectangle x1="25.3746" y1="2.667" x2="26.2382" y2="2.7178" layer="94"/>
<rectangle x1="29.3878" y1="2.667" x2="29.6418" y2="2.7178" layer="94"/>
<rectangle x1="-0.0254" y1="2.7178" x2="1.397" y2="2.7686" layer="94"/>
<rectangle x1="1.651" y1="2.7178" x2="3.937" y2="2.7686" layer="94"/>
<rectangle x1="4.2418" y1="2.7178" x2="5.6642" y2="2.7686" layer="94"/>
<rectangle x1="7.6454" y1="2.7178" x2="7.8994" y2="2.7686" layer="94"/>
<rectangle x1="9.7282" y1="2.7178" x2="10.795" y2="2.7686" layer="94"/>
<rectangle x1="11.5062" y1="2.7178" x2="11.7602" y2="2.7686" layer="94"/>
<rectangle x1="15.0114" y1="2.7178" x2="15.9258" y2="2.7686" layer="94"/>
<rectangle x1="19.177" y1="2.7178" x2="19.431" y2="2.7686" layer="94"/>
<rectangle x1="21.209" y1="2.7178" x2="22.3266" y2="2.7686" layer="94"/>
<rectangle x1="22.987" y1="2.7178" x2="23.241" y2="2.7686" layer="94"/>
<rectangle x1="25.3746" y1="2.7178" x2="26.2382" y2="2.7686" layer="94"/>
<rectangle x1="29.3878" y1="2.7178" x2="29.6418" y2="2.7686" layer="94"/>
<rectangle x1="-0.0254" y1="2.7686" x2="1.397" y2="2.8194" layer="94"/>
<rectangle x1="1.651" y1="2.7686" x2="3.937" y2="2.8194" layer="94"/>
<rectangle x1="4.2418" y1="2.7686" x2="5.6642" y2="2.8194" layer="94"/>
<rectangle x1="7.6454" y1="2.7686" x2="7.8994" y2="2.8194" layer="94"/>
<rectangle x1="9.7282" y1="2.7686" x2="10.795" y2="2.8194" layer="94"/>
<rectangle x1="11.5062" y1="2.7686" x2="11.7602" y2="2.8194" layer="94"/>
<rectangle x1="15.0114" y1="2.7686" x2="15.9258" y2="2.8194" layer="94"/>
<rectangle x1="19.177" y1="2.7686" x2="19.431" y2="2.8194" layer="94"/>
<rectangle x1="21.209" y1="2.7686" x2="22.2758" y2="2.8194" layer="94"/>
<rectangle x1="22.987" y1="2.7686" x2="23.241" y2="2.8194" layer="94"/>
<rectangle x1="25.3746" y1="2.7686" x2="26.2382" y2="2.8194" layer="94"/>
<rectangle x1="29.3878" y1="2.7686" x2="29.6418" y2="2.8194" layer="94"/>
<rectangle x1="-0.0254" y1="2.8194" x2="1.397" y2="2.8702" layer="94"/>
<rectangle x1="1.651" y1="2.8194" x2="3.9878" y2="2.8702" layer="94"/>
<rectangle x1="4.2418" y1="2.8194" x2="5.6642" y2="2.8702" layer="94"/>
<rectangle x1="7.6454" y1="2.8194" x2="7.8994" y2="2.8702" layer="94"/>
<rectangle x1="9.6774" y1="2.8194" x2="10.7442" y2="2.8702" layer="94"/>
<rectangle x1="11.5062" y1="2.8194" x2="11.7602" y2="2.8702" layer="94"/>
<rectangle x1="15.0114" y1="2.8194" x2="15.9258" y2="2.8702" layer="94"/>
<rectangle x1="19.177" y1="2.8194" x2="19.431" y2="2.8702" layer="94"/>
<rectangle x1="21.1582" y1="2.8194" x2="22.225" y2="2.8702" layer="94"/>
<rectangle x1="22.987" y1="2.8194" x2="23.241" y2="2.8702" layer="94"/>
<rectangle x1="25.3746" y1="2.8194" x2="26.2382" y2="2.8702" layer="94"/>
<rectangle x1="29.3878" y1="2.8194" x2="29.6418" y2="2.8702" layer="94"/>
<rectangle x1="-0.0254" y1="2.8702" x2="1.397" y2="2.921" layer="94"/>
<rectangle x1="1.651" y1="2.8702" x2="3.9878" y2="2.921" layer="94"/>
<rectangle x1="4.2418" y1="2.8702" x2="5.6642" y2="2.921" layer="94"/>
<rectangle x1="7.6454" y1="2.8702" x2="7.8994" y2="2.921" layer="94"/>
<rectangle x1="9.6266" y1="2.8702" x2="10.7442" y2="2.921" layer="94"/>
<rectangle x1="11.5062" y1="2.8702" x2="11.7602" y2="2.921" layer="94"/>
<rectangle x1="15.0114" y1="2.8702" x2="15.9258" y2="2.921" layer="94"/>
<rectangle x1="19.177" y1="2.8702" x2="19.431" y2="2.921" layer="94"/>
<rectangle x1="21.1582" y1="2.8702" x2="22.225" y2="2.921" layer="94"/>
<rectangle x1="22.987" y1="2.8702" x2="23.241" y2="2.921" layer="94"/>
<rectangle x1="25.3746" y1="2.8702" x2="26.2382" y2="2.921" layer="94"/>
<rectangle x1="29.3878" y1="2.8702" x2="29.6418" y2="2.921" layer="94"/>
<rectangle x1="-0.0254" y1="2.921" x2="1.397" y2="2.9718" layer="94"/>
<rectangle x1="1.651" y1="2.921" x2="3.9878" y2="2.9718" layer="94"/>
<rectangle x1="4.2418" y1="2.921" x2="5.6642" y2="2.9718" layer="94"/>
<rectangle x1="7.6454" y1="2.921" x2="7.8994" y2="2.9718" layer="94"/>
<rectangle x1="9.6266" y1="2.921" x2="10.6934" y2="2.9718" layer="94"/>
<rectangle x1="11.5062" y1="2.921" x2="11.7602" y2="2.9718" layer="94"/>
<rectangle x1="15.0114" y1="2.921" x2="15.9258" y2="2.9718" layer="94"/>
<rectangle x1="19.177" y1="2.921" x2="19.431" y2="2.9718" layer="94"/>
<rectangle x1="21.1074" y1="2.921" x2="22.1742" y2="2.9718" layer="94"/>
<rectangle x1="22.987" y1="2.921" x2="23.241" y2="2.9718" layer="94"/>
<rectangle x1="25.3746" y1="2.921" x2="26.2382" y2="2.9718" layer="94"/>
<rectangle x1="29.3878" y1="2.921" x2="29.6418" y2="2.9718" layer="94"/>
<rectangle x1="-0.0254" y1="2.9718" x2="1.397" y2="3.0226" layer="94"/>
<rectangle x1="1.651" y1="2.9718" x2="3.9878" y2="3.0226" layer="94"/>
<rectangle x1="4.2418" y1="2.9718" x2="5.6642" y2="3.0226" layer="94"/>
<rectangle x1="7.6454" y1="2.9718" x2="7.8994" y2="3.0226" layer="94"/>
<rectangle x1="9.5758" y1="2.9718" x2="10.6426" y2="3.0226" layer="94"/>
<rectangle x1="11.5062" y1="2.9718" x2="11.7602" y2="3.0226" layer="94"/>
<rectangle x1="15.0114" y1="2.9718" x2="15.9258" y2="3.0226" layer="94"/>
<rectangle x1="19.177" y1="2.9718" x2="19.431" y2="3.0226" layer="94"/>
<rectangle x1="21.0566" y1="2.9718" x2="22.1234" y2="3.0226" layer="94"/>
<rectangle x1="22.987" y1="2.9718" x2="23.241" y2="3.0226" layer="94"/>
<rectangle x1="25.3746" y1="2.9718" x2="26.2382" y2="3.0226" layer="94"/>
<rectangle x1="29.3878" y1="2.9718" x2="29.6418" y2="3.0226" layer="94"/>
<rectangle x1="-0.0254" y1="3.0226" x2="1.397" y2="3.0734" layer="94"/>
<rectangle x1="1.651" y1="3.0226" x2="3.9878" y2="3.0734" layer="94"/>
<rectangle x1="4.2418" y1="3.0226" x2="5.6642" y2="3.0734" layer="94"/>
<rectangle x1="7.6454" y1="3.0226" x2="7.8994" y2="3.0734" layer="94"/>
<rectangle x1="9.525" y1="3.0226" x2="10.6426" y2="3.0734" layer="94"/>
<rectangle x1="11.5062" y1="3.0226" x2="11.7602" y2="3.0734" layer="94"/>
<rectangle x1="15.0114" y1="3.0226" x2="15.9258" y2="3.0734" layer="94"/>
<rectangle x1="19.177" y1="3.0226" x2="19.431" y2="3.0734" layer="94"/>
<rectangle x1="21.0058" y1="3.0226" x2="22.0726" y2="3.0734" layer="94"/>
<rectangle x1="22.987" y1="3.0226" x2="23.241" y2="3.0734" layer="94"/>
<rectangle x1="25.3746" y1="3.0226" x2="26.2382" y2="3.0734" layer="94"/>
<rectangle x1="29.3878" y1="3.0226" x2="29.6418" y2="3.0734" layer="94"/>
<rectangle x1="-0.0254" y1="3.0734" x2="1.397" y2="3.1242" layer="94"/>
<rectangle x1="1.651" y1="3.0734" x2="3.937" y2="3.1242" layer="94"/>
<rectangle x1="4.2418" y1="3.0734" x2="5.6642" y2="3.1242" layer="94"/>
<rectangle x1="7.6454" y1="3.0734" x2="7.8994" y2="3.1242" layer="94"/>
<rectangle x1="9.4742" y1="3.0734" x2="10.5918" y2="3.1242" layer="94"/>
<rectangle x1="11.5062" y1="3.0734" x2="11.7602" y2="3.1242" layer="94"/>
<rectangle x1="15.0114" y1="3.0734" x2="15.9258" y2="3.1242" layer="94"/>
<rectangle x1="19.177" y1="3.0734" x2="19.431" y2="3.1242" layer="94"/>
<rectangle x1="21.0058" y1="3.0734" x2="22.0726" y2="3.1242" layer="94"/>
<rectangle x1="22.987" y1="3.0734" x2="23.241" y2="3.1242" layer="94"/>
<rectangle x1="25.3746" y1="3.0734" x2="26.2382" y2="3.1242" layer="94"/>
<rectangle x1="29.3878" y1="3.0734" x2="29.6418" y2="3.1242" layer="94"/>
<rectangle x1="-0.0254" y1="3.1242" x2="1.397" y2="3.175" layer="94"/>
<rectangle x1="1.651" y1="3.1242" x2="3.937" y2="3.175" layer="94"/>
<rectangle x1="4.2418" y1="3.1242" x2="5.6642" y2="3.175" layer="94"/>
<rectangle x1="7.6454" y1="3.1242" x2="7.8994" y2="3.175" layer="94"/>
<rectangle x1="9.4742" y1="3.1242" x2="10.541" y2="3.175" layer="94"/>
<rectangle x1="11.5062" y1="3.1242" x2="11.7602" y2="3.175" layer="94"/>
<rectangle x1="15.0114" y1="3.1242" x2="15.9258" y2="3.175" layer="94"/>
<rectangle x1="19.177" y1="3.1242" x2="19.431" y2="3.175" layer="94"/>
<rectangle x1="20.955" y1="3.1242" x2="22.0218" y2="3.175" layer="94"/>
<rectangle x1="22.987" y1="3.1242" x2="23.241" y2="3.175" layer="94"/>
<rectangle x1="25.3746" y1="3.1242" x2="26.2382" y2="3.175" layer="94"/>
<rectangle x1="29.3878" y1="3.1242" x2="29.6418" y2="3.175" layer="94"/>
<rectangle x1="-0.0254" y1="3.175" x2="1.397" y2="3.2258" layer="94"/>
<rectangle x1="1.651" y1="3.175" x2="3.937" y2="3.2258" layer="94"/>
<rectangle x1="4.2418" y1="3.175" x2="5.6642" y2="3.2258" layer="94"/>
<rectangle x1="7.6454" y1="3.175" x2="7.8994" y2="3.2258" layer="94"/>
<rectangle x1="9.4234" y1="3.175" x2="10.4902" y2="3.2258" layer="94"/>
<rectangle x1="11.5062" y1="3.175" x2="11.7602" y2="3.2258" layer="94"/>
<rectangle x1="15.0114" y1="3.175" x2="15.9258" y2="3.2258" layer="94"/>
<rectangle x1="19.177" y1="3.175" x2="19.431" y2="3.2258" layer="94"/>
<rectangle x1="20.9042" y1="3.175" x2="22.0218" y2="3.2258" layer="94"/>
<rectangle x1="22.987" y1="3.175" x2="23.241" y2="3.2258" layer="94"/>
<rectangle x1="25.3746" y1="3.175" x2="26.2382" y2="3.2258" layer="94"/>
<rectangle x1="29.3878" y1="3.175" x2="29.6418" y2="3.2258" layer="94"/>
<rectangle x1="-0.0254" y1="3.2258" x2="1.397" y2="3.2766" layer="94"/>
<rectangle x1="1.651" y1="3.2258" x2="3.937" y2="3.2766" layer="94"/>
<rectangle x1="4.2418" y1="3.2258" x2="5.6642" y2="3.2766" layer="94"/>
<rectangle x1="7.6454" y1="3.2258" x2="7.8994" y2="3.2766" layer="94"/>
<rectangle x1="9.3726" y1="3.2258" x2="10.4902" y2="3.2766" layer="94"/>
<rectangle x1="11.5062" y1="3.2258" x2="11.7602" y2="3.2766" layer="94"/>
<rectangle x1="15.0114" y1="3.2258" x2="15.9258" y2="3.2766" layer="94"/>
<rectangle x1="19.177" y1="3.2258" x2="19.431" y2="3.2766" layer="94"/>
<rectangle x1="20.9042" y1="3.2258" x2="21.971" y2="3.2766" layer="94"/>
<rectangle x1="22.987" y1="3.2258" x2="23.241" y2="3.2766" layer="94"/>
<rectangle x1="25.3746" y1="3.2258" x2="26.2382" y2="3.2766" layer="94"/>
<rectangle x1="29.3878" y1="3.2258" x2="29.6418" y2="3.2766" layer="94"/>
<rectangle x1="-0.0254" y1="3.2766" x2="1.397" y2="3.3274" layer="94"/>
<rectangle x1="1.7018" y1="3.2766" x2="3.937" y2="3.3274" layer="94"/>
<rectangle x1="4.2418" y1="3.2766" x2="5.6642" y2="3.3274" layer="94"/>
<rectangle x1="7.6454" y1="3.2766" x2="7.8994" y2="3.3274" layer="94"/>
<rectangle x1="9.3726" y1="3.2766" x2="10.4394" y2="3.3274" layer="94"/>
<rectangle x1="11.5062" y1="3.2766" x2="11.7602" y2="3.3274" layer="94"/>
<rectangle x1="15.0114" y1="3.2766" x2="15.9258" y2="3.3274" layer="94"/>
<rectangle x1="19.177" y1="3.2766" x2="19.431" y2="3.3274" layer="94"/>
<rectangle x1="20.8534" y1="3.2766" x2="21.9202" y2="3.3274" layer="94"/>
<rectangle x1="22.987" y1="3.2766" x2="23.241" y2="3.3274" layer="94"/>
<rectangle x1="25.3746" y1="3.2766" x2="26.2382" y2="3.3274" layer="94"/>
<rectangle x1="29.3878" y1="3.2766" x2="29.6418" y2="3.3274" layer="94"/>
<rectangle x1="-0.0254" y1="3.3274" x2="1.397" y2="3.3782" layer="94"/>
<rectangle x1="1.7018" y1="3.3274" x2="3.8862" y2="3.3782" layer="94"/>
<rectangle x1="4.2418" y1="3.3274" x2="5.6642" y2="3.3782" layer="94"/>
<rectangle x1="7.6454" y1="3.3274" x2="7.8994" y2="3.3782" layer="94"/>
<rectangle x1="9.3218" y1="3.3274" x2="10.3886" y2="3.3782" layer="94"/>
<rectangle x1="11.5062" y1="3.3274" x2="11.7602" y2="3.3782" layer="94"/>
<rectangle x1="15.0114" y1="3.3274" x2="15.9258" y2="3.3782" layer="94"/>
<rectangle x1="19.177" y1="3.3274" x2="19.431" y2="3.3782" layer="94"/>
<rectangle x1="20.8026" y1="3.3274" x2="21.8694" y2="3.3782" layer="94"/>
<rectangle x1="22.987" y1="3.3274" x2="23.241" y2="3.3782" layer="94"/>
<rectangle x1="25.3746" y1="3.3274" x2="26.2382" y2="3.3782" layer="94"/>
<rectangle x1="29.3878" y1="3.3274" x2="29.6418" y2="3.3782" layer="94"/>
<rectangle x1="-0.0254" y1="3.3782" x2="1.397" y2="3.429" layer="94"/>
<rectangle x1="1.7526" y1="3.3782" x2="3.8862" y2="3.429" layer="94"/>
<rectangle x1="4.2418" y1="3.3782" x2="5.6642" y2="3.429" layer="94"/>
<rectangle x1="7.6454" y1="3.3782" x2="7.8994" y2="3.429" layer="94"/>
<rectangle x1="9.271" y1="3.3782" x2="10.3886" y2="3.429" layer="94"/>
<rectangle x1="11.5062" y1="3.3782" x2="11.7602" y2="3.429" layer="94"/>
<rectangle x1="15.0114" y1="3.3782" x2="15.9258" y2="3.429" layer="94"/>
<rectangle x1="19.177" y1="3.3782" x2="19.431" y2="3.429" layer="94"/>
<rectangle x1="20.7518" y1="3.3782" x2="21.8694" y2="3.429" layer="94"/>
<rectangle x1="22.987" y1="3.3782" x2="23.241" y2="3.429" layer="94"/>
<rectangle x1="25.3746" y1="3.3782" x2="26.2382" y2="3.429" layer="94"/>
<rectangle x1="29.3878" y1="3.3782" x2="29.6418" y2="3.429" layer="94"/>
<rectangle x1="-0.0254" y1="3.429" x2="1.397" y2="3.4798" layer="94"/>
<rectangle x1="1.7526" y1="3.429" x2="3.8862" y2="3.4798" layer="94"/>
<rectangle x1="4.2418" y1="3.429" x2="5.6642" y2="3.4798" layer="94"/>
<rectangle x1="7.6454" y1="3.429" x2="7.8994" y2="3.4798" layer="94"/>
<rectangle x1="9.271" y1="3.429" x2="10.3378" y2="3.4798" layer="94"/>
<rectangle x1="11.5062" y1="3.429" x2="11.7602" y2="3.4798" layer="94"/>
<rectangle x1="15.0114" y1="3.429" x2="15.9258" y2="3.4798" layer="94"/>
<rectangle x1="19.177" y1="3.429" x2="19.431" y2="3.4798" layer="94"/>
<rectangle x1="20.7518" y1="3.429" x2="21.8186" y2="3.4798" layer="94"/>
<rectangle x1="22.987" y1="3.429" x2="23.241" y2="3.4798" layer="94"/>
<rectangle x1="25.3746" y1="3.429" x2="26.2382" y2="3.4798" layer="94"/>
<rectangle x1="29.3878" y1="3.429" x2="29.6418" y2="3.4798" layer="94"/>
<rectangle x1="-0.0254" y1="3.4798" x2="1.397" y2="3.5306" layer="94"/>
<rectangle x1="1.8034" y1="3.4798" x2="3.8354" y2="3.5306" layer="94"/>
<rectangle x1="4.2418" y1="3.4798" x2="5.6642" y2="3.5306" layer="94"/>
<rectangle x1="7.6454" y1="3.4798" x2="7.8994" y2="3.5306" layer="94"/>
<rectangle x1="9.2202" y1="3.4798" x2="10.287" y2="3.5306" layer="94"/>
<rectangle x1="11.5062" y1="3.4798" x2="11.7602" y2="3.5306" layer="94"/>
<rectangle x1="15.0114" y1="3.4798" x2="15.9258" y2="3.5306" layer="94"/>
<rectangle x1="19.177" y1="3.4798" x2="19.431" y2="3.5306" layer="94"/>
<rectangle x1="20.701" y1="3.4798" x2="21.7678" y2="3.5306" layer="94"/>
<rectangle x1="22.987" y1="3.4798" x2="23.241" y2="3.5306" layer="94"/>
<rectangle x1="25.3746" y1="3.4798" x2="26.2382" y2="3.5306" layer="94"/>
<rectangle x1="29.3878" y1="3.4798" x2="29.6418" y2="3.5306" layer="94"/>
<rectangle x1="-0.0254" y1="3.5306" x2="1.397" y2="3.5814" layer="94"/>
<rectangle x1="1.8034" y1="3.5306" x2="3.7846" y2="3.5814" layer="94"/>
<rectangle x1="4.2418" y1="3.5306" x2="5.6642" y2="3.5814" layer="94"/>
<rectangle x1="7.6454" y1="3.5306" x2="7.8994" y2="3.5814" layer="94"/>
<rectangle x1="9.1694" y1="3.5306" x2="10.2362" y2="3.5814" layer="94"/>
<rectangle x1="11.5062" y1="3.5306" x2="11.7602" y2="3.5814" layer="94"/>
<rectangle x1="15.0114" y1="3.5306" x2="15.9258" y2="3.5814" layer="94"/>
<rectangle x1="19.177" y1="3.5306" x2="19.431" y2="3.5814" layer="94"/>
<rectangle x1="20.701" y1="3.5306" x2="21.7678" y2="3.5814" layer="94"/>
<rectangle x1="22.987" y1="3.5306" x2="23.241" y2="3.5814" layer="94"/>
<rectangle x1="25.3746" y1="3.5306" x2="26.2382" y2="3.5814" layer="94"/>
<rectangle x1="29.3878" y1="3.5306" x2="29.6418" y2="3.5814" layer="94"/>
<rectangle x1="-0.0254" y1="3.5814" x2="1.397" y2="3.6322" layer="94"/>
<rectangle x1="1.8542" y1="3.5814" x2="3.7338" y2="3.6322" layer="94"/>
<rectangle x1="4.2418" y1="3.5814" x2="5.6642" y2="3.6322" layer="94"/>
<rectangle x1="7.6454" y1="3.5814" x2="7.8994" y2="3.6322" layer="94"/>
<rectangle x1="9.1694" y1="3.5814" x2="10.2362" y2="3.6322" layer="94"/>
<rectangle x1="11.5062" y1="3.5814" x2="11.7602" y2="3.6322" layer="94"/>
<rectangle x1="15.0114" y1="3.5814" x2="15.9258" y2="3.6322" layer="94"/>
<rectangle x1="19.177" y1="3.5814" x2="19.431" y2="3.6322" layer="94"/>
<rectangle x1="20.6502" y1="3.5814" x2="21.717" y2="3.6322" layer="94"/>
<rectangle x1="22.987" y1="3.5814" x2="23.241" y2="3.6322" layer="94"/>
<rectangle x1="25.3746" y1="3.5814" x2="26.2382" y2="3.6322" layer="94"/>
<rectangle x1="29.3878" y1="3.5814" x2="29.6418" y2="3.6322" layer="94"/>
<rectangle x1="-0.0254" y1="3.6322" x2="1.397" y2="3.683" layer="94"/>
<rectangle x1="1.905" y1="3.6322" x2="3.7338" y2="3.683" layer="94"/>
<rectangle x1="4.2418" y1="3.6322" x2="5.6642" y2="3.683" layer="94"/>
<rectangle x1="7.6454" y1="3.6322" x2="7.8994" y2="3.683" layer="94"/>
<rectangle x1="9.1186" y1="3.6322" x2="10.1854" y2="3.683" layer="94"/>
<rectangle x1="11.5062" y1="3.6322" x2="11.7602" y2="3.683" layer="94"/>
<rectangle x1="15.0114" y1="3.6322" x2="15.9258" y2="3.683" layer="94"/>
<rectangle x1="19.177" y1="3.6322" x2="19.431" y2="3.683" layer="94"/>
<rectangle x1="20.5994" y1="3.6322" x2="21.6662" y2="3.683" layer="94"/>
<rectangle x1="22.987" y1="3.6322" x2="23.241" y2="3.683" layer="94"/>
<rectangle x1="25.3746" y1="3.6322" x2="26.2382" y2="3.683" layer="94"/>
<rectangle x1="29.3878" y1="3.6322" x2="29.6418" y2="3.683" layer="94"/>
<rectangle x1="-0.0254" y1="3.683" x2="1.397" y2="3.7338" layer="94"/>
<rectangle x1="1.905" y1="3.683" x2="3.683" y2="3.7338" layer="94"/>
<rectangle x1="4.2418" y1="3.683" x2="5.6642" y2="3.7338" layer="94"/>
<rectangle x1="7.6454" y1="3.683" x2="7.8994" y2="3.7338" layer="94"/>
<rectangle x1="9.0678" y1="3.683" x2="10.1854" y2="3.7338" layer="94"/>
<rectangle x1="11.5062" y1="3.683" x2="11.7602" y2="3.7338" layer="94"/>
<rectangle x1="15.0114" y1="3.683" x2="15.9258" y2="3.7338" layer="94"/>
<rectangle x1="19.177" y1="3.683" x2="19.431" y2="3.7338" layer="94"/>
<rectangle x1="20.5486" y1="3.683" x2="21.6662" y2="3.7338" layer="94"/>
<rectangle x1="22.987" y1="3.683" x2="23.241" y2="3.7338" layer="94"/>
<rectangle x1="25.3746" y1="3.683" x2="26.2382" y2="3.7338" layer="94"/>
<rectangle x1="29.3878" y1="3.683" x2="29.6418" y2="3.7338" layer="94"/>
<rectangle x1="-0.0254" y1="3.7338" x2="1.397" y2="3.7846" layer="94"/>
<rectangle x1="1.9558" y1="3.7338" x2="3.6322" y2="3.7846" layer="94"/>
<rectangle x1="4.2418" y1="3.7338" x2="5.6642" y2="3.7846" layer="94"/>
<rectangle x1="7.6454" y1="3.7338" x2="7.8994" y2="3.7846" layer="94"/>
<rectangle x1="9.0678" y1="3.7338" x2="10.1346" y2="3.7846" layer="94"/>
<rectangle x1="11.5062" y1="3.7338" x2="11.7602" y2="3.7846" layer="94"/>
<rectangle x1="15.0114" y1="3.7338" x2="15.9258" y2="3.7846" layer="94"/>
<rectangle x1="19.177" y1="3.7338" x2="19.431" y2="3.7846" layer="94"/>
<rectangle x1="20.5486" y1="3.7338" x2="21.6154" y2="3.7846" layer="94"/>
<rectangle x1="22.987" y1="3.7338" x2="23.241" y2="3.7846" layer="94"/>
<rectangle x1="25.3746" y1="3.7338" x2="26.2382" y2="3.7846" layer="94"/>
<rectangle x1="29.3878" y1="3.7338" x2="29.6418" y2="3.7846" layer="94"/>
<rectangle x1="-0.0254" y1="3.7846" x2="1.397" y2="3.8354" layer="94"/>
<rectangle x1="2.0066" y1="3.7846" x2="3.5814" y2="3.8354" layer="94"/>
<rectangle x1="4.2418" y1="3.7846" x2="5.6642" y2="3.8354" layer="94"/>
<rectangle x1="7.6454" y1="3.7846" x2="7.8994" y2="3.8354" layer="94"/>
<rectangle x1="9.017" y1="3.7846" x2="10.0838" y2="3.8354" layer="94"/>
<rectangle x1="11.5062" y1="3.7846" x2="11.7602" y2="3.8354" layer="94"/>
<rectangle x1="15.0114" y1="3.7846" x2="15.9258" y2="3.8354" layer="94"/>
<rectangle x1="19.177" y1="3.7846" x2="19.431" y2="3.8354" layer="94"/>
<rectangle x1="20.4978" y1="3.7846" x2="21.5646" y2="3.8354" layer="94"/>
<rectangle x1="22.987" y1="3.7846" x2="23.241" y2="3.8354" layer="94"/>
<rectangle x1="25.3746" y1="3.7846" x2="26.2382" y2="3.8354" layer="94"/>
<rectangle x1="29.3878" y1="3.7846" x2="29.6418" y2="3.8354" layer="94"/>
<rectangle x1="-0.0254" y1="3.8354" x2="1.397" y2="3.8862" layer="94"/>
<rectangle x1="2.1082" y1="3.8354" x2="3.5306" y2="3.8862" layer="94"/>
<rectangle x1="4.2418" y1="3.8354" x2="5.6642" y2="3.8862" layer="94"/>
<rectangle x1="7.6454" y1="3.8354" x2="7.8994" y2="3.8862" layer="94"/>
<rectangle x1="8.9662" y1="3.8354" x2="10.033" y2="3.8862" layer="94"/>
<rectangle x1="11.5062" y1="3.8354" x2="11.7602" y2="3.8862" layer="94"/>
<rectangle x1="15.0114" y1="3.8354" x2="15.9258" y2="3.8862" layer="94"/>
<rectangle x1="19.177" y1="3.8354" x2="19.431" y2="3.8862" layer="94"/>
<rectangle x1="20.447" y1="3.8354" x2="21.5646" y2="3.8862" layer="94"/>
<rectangle x1="22.987" y1="3.8354" x2="23.241" y2="3.8862" layer="94"/>
<rectangle x1="25.3746" y1="3.8354" x2="26.2382" y2="3.8862" layer="94"/>
<rectangle x1="29.3878" y1="3.8354" x2="29.6418" y2="3.8862" layer="94"/>
<rectangle x1="-0.0254" y1="3.8862" x2="1.397" y2="3.937" layer="94"/>
<rectangle x1="2.159" y1="3.8862" x2="3.429" y2="3.937" layer="94"/>
<rectangle x1="4.2418" y1="3.8862" x2="5.6642" y2="3.937" layer="94"/>
<rectangle x1="7.6454" y1="3.8862" x2="7.8994" y2="3.937" layer="94"/>
<rectangle x1="8.9154" y1="3.8862" x2="10.033" y2="3.937" layer="94"/>
<rectangle x1="11.5062" y1="3.8862" x2="11.7602" y2="3.937" layer="94"/>
<rectangle x1="15.0114" y1="3.8862" x2="15.9258" y2="3.937" layer="94"/>
<rectangle x1="19.177" y1="3.8862" x2="19.431" y2="3.937" layer="94"/>
<rectangle x1="20.447" y1="3.8862" x2="21.5138" y2="3.937" layer="94"/>
<rectangle x1="22.987" y1="3.8862" x2="23.241" y2="3.937" layer="94"/>
<rectangle x1="25.3746" y1="3.8862" x2="26.2382" y2="3.937" layer="94"/>
<rectangle x1="29.3878" y1="3.8862" x2="29.6418" y2="3.937" layer="94"/>
<rectangle x1="-0.0254" y1="3.937" x2="1.397" y2="3.9878" layer="94"/>
<rectangle x1="2.2606" y1="3.937" x2="3.3782" y2="3.9878" layer="94"/>
<rectangle x1="4.2418" y1="3.937" x2="5.6642" y2="3.9878" layer="94"/>
<rectangle x1="7.6454" y1="3.937" x2="7.8994" y2="3.9878" layer="94"/>
<rectangle x1="8.9154" y1="3.937" x2="9.9822" y2="3.9878" layer="94"/>
<rectangle x1="11.5062" y1="3.937" x2="11.7602" y2="3.9878" layer="94"/>
<rectangle x1="12.9286" y1="3.937" x2="13.1826" y2="3.9878" layer="94"/>
<rectangle x1="15.0114" y1="3.937" x2="15.9258" y2="3.9878" layer="94"/>
<rectangle x1="17.7038" y1="3.937" x2="17.9578" y2="3.9878" layer="94"/>
<rectangle x1="19.177" y1="3.937" x2="19.431" y2="3.9878" layer="94"/>
<rectangle x1="20.3962" y1="3.937" x2="21.463" y2="3.9878" layer="94"/>
<rectangle x1="22.987" y1="3.937" x2="23.241" y2="3.9878" layer="94"/>
<rectangle x1="25.3746" y1="3.937" x2="26.2382" y2="3.9878" layer="94"/>
<rectangle x1="29.3878" y1="3.937" x2="29.6418" y2="3.9878" layer="94"/>
<rectangle x1="-0.0254" y1="3.9878" x2="1.397" y2="4.0386" layer="94"/>
<rectangle x1="2.3114" y1="3.9878" x2="3.2766" y2="4.0386" layer="94"/>
<rectangle x1="4.2418" y1="3.9878" x2="5.6642" y2="4.0386" layer="94"/>
<rectangle x1="7.6454" y1="3.9878" x2="7.8994" y2="4.0386" layer="94"/>
<rectangle x1="8.8646" y1="3.9878" x2="9.9314" y2="4.0386" layer="94"/>
<rectangle x1="11.5062" y1="3.9878" x2="11.7602" y2="4.0386" layer="94"/>
<rectangle x1="12.9286" y1="3.9878" x2="13.1826" y2="4.0386" layer="94"/>
<rectangle x1="15.0114" y1="3.9878" x2="15.9258" y2="4.0386" layer="94"/>
<rectangle x1="17.7038" y1="3.9878" x2="17.9578" y2="4.0386" layer="94"/>
<rectangle x1="19.177" y1="3.9878" x2="19.431" y2="4.0386" layer="94"/>
<rectangle x1="20.3454" y1="3.9878" x2="21.463" y2="4.0386" layer="94"/>
<rectangle x1="22.987" y1="3.9878" x2="23.241" y2="4.0386" layer="94"/>
<rectangle x1="25.3746" y1="3.9878" x2="26.2382" y2="4.0386" layer="94"/>
<rectangle x1="29.3878" y1="3.9878" x2="29.6418" y2="4.0386" layer="94"/>
<rectangle x1="-0.0254" y1="4.0386" x2="1.397" y2="4.0894" layer="94"/>
<rectangle x1="2.4638" y1="4.0386" x2="3.1242" y2="4.0894" layer="94"/>
<rectangle x1="4.2418" y1="4.0386" x2="5.6642" y2="4.0894" layer="94"/>
<rectangle x1="7.6454" y1="4.0386" x2="7.8994" y2="4.0894" layer="94"/>
<rectangle x1="8.8138" y1="4.0386" x2="9.9314" y2="4.0894" layer="94"/>
<rectangle x1="11.5062" y1="4.0386" x2="11.7602" y2="4.0894" layer="94"/>
<rectangle x1="12.9286" y1="4.0386" x2="13.1826" y2="4.0894" layer="94"/>
<rectangle x1="15.0114" y1="4.0386" x2="15.9258" y2="4.0894" layer="94"/>
<rectangle x1="17.7038" y1="4.0386" x2="17.9578" y2="4.0894" layer="94"/>
<rectangle x1="19.177" y1="4.0386" x2="19.431" y2="4.0894" layer="94"/>
<rectangle x1="20.2946" y1="4.0386" x2="21.4122" y2="4.0894" layer="94"/>
<rectangle x1="22.987" y1="4.0386" x2="23.241" y2="4.0894" layer="94"/>
<rectangle x1="25.3746" y1="4.0386" x2="26.2382" y2="4.0894" layer="94"/>
<rectangle x1="29.3878" y1="4.0386" x2="29.6418" y2="4.0894" layer="94"/>
<rectangle x1="-0.0254" y1="4.0894" x2="1.397" y2="4.1402" layer="94"/>
<rectangle x1="4.2418" y1="4.0894" x2="5.6642" y2="4.1402" layer="94"/>
<rectangle x1="7.6454" y1="4.0894" x2="7.8994" y2="4.1402" layer="94"/>
<rectangle x1="8.8138" y1="4.0894" x2="9.8806" y2="4.1402" layer="94"/>
<rectangle x1="11.5062" y1="4.0894" x2="11.7602" y2="4.1402" layer="94"/>
<rectangle x1="12.9286" y1="4.0894" x2="13.1826" y2="4.1402" layer="94"/>
<rectangle x1="15.0114" y1="4.0894" x2="15.9258" y2="4.1402" layer="94"/>
<rectangle x1="17.7038" y1="4.0894" x2="17.9578" y2="4.1402" layer="94"/>
<rectangle x1="19.177" y1="4.0894" x2="19.431" y2="4.1402" layer="94"/>
<rectangle x1="20.2946" y1="4.0894" x2="21.3614" y2="4.1402" layer="94"/>
<rectangle x1="22.987" y1="4.0894" x2="23.241" y2="4.1402" layer="94"/>
<rectangle x1="25.3746" y1="4.0894" x2="26.2382" y2="4.1402" layer="94"/>
<rectangle x1="29.3878" y1="4.0894" x2="29.6418" y2="4.1402" layer="94"/>
<rectangle x1="-0.0254" y1="4.1402" x2="1.397" y2="4.191" layer="94"/>
<rectangle x1="4.2418" y1="4.1402" x2="5.6642" y2="4.191" layer="94"/>
<rectangle x1="7.6454" y1="4.1402" x2="7.8994" y2="4.191" layer="94"/>
<rectangle x1="8.763" y1="4.1402" x2="9.8298" y2="4.191" layer="94"/>
<rectangle x1="11.5062" y1="4.1402" x2="11.7602" y2="4.191" layer="94"/>
<rectangle x1="12.9286" y1="4.1402" x2="13.1826" y2="4.191" layer="94"/>
<rectangle x1="15.0114" y1="4.1402" x2="15.9258" y2="4.191" layer="94"/>
<rectangle x1="17.7038" y1="4.1402" x2="17.9578" y2="4.191" layer="94"/>
<rectangle x1="19.177" y1="4.1402" x2="19.431" y2="4.191" layer="94"/>
<rectangle x1="20.2438" y1="4.1402" x2="21.3106" y2="4.191" layer="94"/>
<rectangle x1="22.987" y1="4.1402" x2="23.241" y2="4.191" layer="94"/>
<rectangle x1="25.3746" y1="4.1402" x2="26.2382" y2="4.191" layer="94"/>
<rectangle x1="29.3878" y1="4.1402" x2="29.6418" y2="4.191" layer="94"/>
<rectangle x1="-0.0254" y1="4.191" x2="1.397" y2="4.2418" layer="94"/>
<rectangle x1="4.2418" y1="4.191" x2="5.6642" y2="4.2418" layer="94"/>
<rectangle x1="7.6454" y1="4.191" x2="7.8994" y2="4.2418" layer="94"/>
<rectangle x1="8.7122" y1="4.191" x2="9.779" y2="4.2418" layer="94"/>
<rectangle x1="11.5062" y1="4.191" x2="11.7602" y2="4.2418" layer="94"/>
<rectangle x1="12.9286" y1="4.191" x2="13.2334" y2="4.2418" layer="94"/>
<rectangle x1="15.0114" y1="4.191" x2="15.9258" y2="4.2418" layer="94"/>
<rectangle x1="17.7038" y1="4.191" x2="17.9578" y2="4.2418" layer="94"/>
<rectangle x1="19.177" y1="4.191" x2="19.431" y2="4.2418" layer="94"/>
<rectangle x1="20.193" y1="4.191" x2="21.3106" y2="4.2418" layer="94"/>
<rectangle x1="22.987" y1="4.191" x2="23.241" y2="4.2418" layer="94"/>
<rectangle x1="25.3746" y1="4.191" x2="26.2382" y2="4.2418" layer="94"/>
<rectangle x1="29.3878" y1="4.191" x2="29.6418" y2="4.2418" layer="94"/>
<rectangle x1="-0.0254" y1="4.2418" x2="1.397" y2="4.2926" layer="94"/>
<rectangle x1="4.2418" y1="4.2418" x2="5.6642" y2="4.2926" layer="94"/>
<rectangle x1="7.6454" y1="4.2418" x2="7.8994" y2="4.2926" layer="94"/>
<rectangle x1="8.7122" y1="4.2418" x2="9.779" y2="4.2926" layer="94"/>
<rectangle x1="11.5062" y1="4.2418" x2="11.7602" y2="4.2926" layer="94"/>
<rectangle x1="12.9286" y1="4.2418" x2="13.2334" y2="4.2926" layer="94"/>
<rectangle x1="15.0114" y1="4.2418" x2="15.9258" y2="4.2926" layer="94"/>
<rectangle x1="17.653" y1="4.2418" x2="17.9578" y2="4.2926" layer="94"/>
<rectangle x1="19.177" y1="4.2418" x2="19.431" y2="4.2926" layer="94"/>
<rectangle x1="20.193" y1="4.2418" x2="21.2598" y2="4.2926" layer="94"/>
<rectangle x1="22.987" y1="4.2418" x2="23.241" y2="4.2926" layer="94"/>
<rectangle x1="25.3746" y1="4.2418" x2="26.2382" y2="4.2926" layer="94"/>
<rectangle x1="29.3878" y1="4.2418" x2="29.6418" y2="4.2926" layer="94"/>
<rectangle x1="-0.0254" y1="4.2926" x2="1.397" y2="4.3434" layer="94"/>
<rectangle x1="4.2418" y1="4.2926" x2="5.6642" y2="4.3434" layer="94"/>
<rectangle x1="7.6454" y1="4.2926" x2="7.8994" y2="4.3434" layer="94"/>
<rectangle x1="8.6614" y1="4.2926" x2="9.7282" y2="4.3434" layer="94"/>
<rectangle x1="11.5062" y1="4.2926" x2="11.7602" y2="4.3434" layer="94"/>
<rectangle x1="12.9286" y1="4.2926" x2="13.2334" y2="4.3434" layer="94"/>
<rectangle x1="15.0114" y1="4.2926" x2="15.9258" y2="4.3434" layer="94"/>
<rectangle x1="17.653" y1="4.2926" x2="17.9578" y2="4.3434" layer="94"/>
<rectangle x1="19.177" y1="4.2926" x2="19.431" y2="4.3434" layer="94"/>
<rectangle x1="20.1422" y1="4.2926" x2="21.209" y2="4.3434" layer="94"/>
<rectangle x1="22.987" y1="4.2926" x2="23.241" y2="4.3434" layer="94"/>
<rectangle x1="25.3746" y1="4.2926" x2="26.2382" y2="4.3434" layer="94"/>
<rectangle x1="29.3878" y1="4.2926" x2="29.6418" y2="4.3434" layer="94"/>
<rectangle x1="-0.0254" y1="4.3434" x2="5.6642" y2="4.3942" layer="94"/>
<rectangle x1="7.6454" y1="4.3434" x2="7.8994" y2="4.3942" layer="94"/>
<rectangle x1="8.6106" y1="4.3434" x2="9.6774" y2="4.3942" layer="94"/>
<rectangle x1="11.5062" y1="4.3434" x2="11.7602" y2="4.3942" layer="94"/>
<rectangle x1="12.9286" y1="4.3434" x2="13.2334" y2="4.3942" layer="94"/>
<rectangle x1="15.0114" y1="4.3434" x2="15.9258" y2="4.3942" layer="94"/>
<rectangle x1="17.653" y1="4.3434" x2="17.9578" y2="4.3942" layer="94"/>
<rectangle x1="19.177" y1="4.3434" x2="19.431" y2="4.3942" layer="94"/>
<rectangle x1="20.1422" y1="4.3434" x2="21.209" y2="4.3942" layer="94"/>
<rectangle x1="22.987" y1="4.3434" x2="23.241" y2="4.3942" layer="94"/>
<rectangle x1="25.3746" y1="4.3434" x2="26.2382" y2="4.3942" layer="94"/>
<rectangle x1="29.3878" y1="4.3434" x2="29.6418" y2="4.3942" layer="94"/>
<rectangle x1="-0.0254" y1="4.3942" x2="5.6642" y2="4.445" layer="94"/>
<rectangle x1="7.6454" y1="4.3942" x2="7.8994" y2="4.445" layer="94"/>
<rectangle x1="8.6106" y1="4.3942" x2="9.6774" y2="4.445" layer="94"/>
<rectangle x1="11.5062" y1="4.3942" x2="11.7602" y2="4.445" layer="94"/>
<rectangle x1="12.9286" y1="4.3942" x2="13.2842" y2="4.445" layer="94"/>
<rectangle x1="15.0114" y1="4.3942" x2="15.9258" y2="4.445" layer="94"/>
<rectangle x1="17.653" y1="4.3942" x2="17.9578" y2="4.445" layer="94"/>
<rectangle x1="19.177" y1="4.3942" x2="19.431" y2="4.445" layer="94"/>
<rectangle x1="20.0914" y1="4.3942" x2="21.1582" y2="4.445" layer="94"/>
<rectangle x1="22.987" y1="4.3942" x2="23.241" y2="4.445" layer="94"/>
<rectangle x1="25.3746" y1="4.3942" x2="26.2382" y2="4.445" layer="94"/>
<rectangle x1="29.3878" y1="4.3942" x2="29.6418" y2="4.445" layer="94"/>
<rectangle x1="-0.0254" y1="4.445" x2="5.6642" y2="4.4958" layer="94"/>
<rectangle x1="7.6454" y1="4.445" x2="7.8994" y2="4.4958" layer="94"/>
<rectangle x1="8.5598" y1="4.445" x2="9.6266" y2="4.4958" layer="94"/>
<rectangle x1="11.5062" y1="4.445" x2="11.7602" y2="4.4958" layer="94"/>
<rectangle x1="12.9286" y1="4.445" x2="13.2842" y2="4.4958" layer="94"/>
<rectangle x1="15.0114" y1="4.445" x2="15.9258" y2="4.4958" layer="94"/>
<rectangle x1="17.653" y1="4.445" x2="17.9578" y2="4.4958" layer="94"/>
<rectangle x1="19.177" y1="4.445" x2="19.431" y2="4.4958" layer="94"/>
<rectangle x1="20.0406" y1="4.445" x2="21.1074" y2="4.4958" layer="94"/>
<rectangle x1="22.987" y1="4.445" x2="23.241" y2="4.4958" layer="94"/>
<rectangle x1="25.3746" y1="4.445" x2="26.2382" y2="4.4958" layer="94"/>
<rectangle x1="29.3878" y1="4.445" x2="29.6418" y2="4.4958" layer="94"/>
<rectangle x1="-0.0254" y1="4.4958" x2="5.6642" y2="4.5466" layer="94"/>
<rectangle x1="7.6454" y1="4.4958" x2="7.8994" y2="4.5466" layer="94"/>
<rectangle x1="8.509" y1="4.4958" x2="9.5758" y2="4.5466" layer="94"/>
<rectangle x1="11.5062" y1="4.4958" x2="11.7602" y2="4.5466" layer="94"/>
<rectangle x1="12.9286" y1="4.4958" x2="13.2842" y2="4.5466" layer="94"/>
<rectangle x1="15.0114" y1="4.4958" x2="15.9258" y2="4.5466" layer="94"/>
<rectangle x1="17.6022" y1="4.4958" x2="17.9578" y2="4.5466" layer="94"/>
<rectangle x1="19.177" y1="4.4958" x2="19.431" y2="4.5466" layer="94"/>
<rectangle x1="19.9898" y1="4.4958" x2="21.0566" y2="4.5466" layer="94"/>
<rectangle x1="22.987" y1="4.4958" x2="23.241" y2="4.5466" layer="94"/>
<rectangle x1="25.3746" y1="4.4958" x2="26.2382" y2="4.5466" layer="94"/>
<rectangle x1="29.3878" y1="4.4958" x2="29.6418" y2="4.5466" layer="94"/>
<rectangle x1="-0.0254" y1="4.5466" x2="5.6642" y2="4.5974" layer="94"/>
<rectangle x1="7.6454" y1="4.5466" x2="7.8994" y2="4.5974" layer="94"/>
<rectangle x1="8.4582" y1="4.5466" x2="9.5758" y2="4.5974" layer="94"/>
<rectangle x1="11.5062" y1="4.5466" x2="11.7602" y2="4.5974" layer="94"/>
<rectangle x1="12.9286" y1="4.5466" x2="13.2842" y2="4.5974" layer="94"/>
<rectangle x1="15.0114" y1="4.5466" x2="15.9258" y2="4.5974" layer="94"/>
<rectangle x1="17.6022" y1="4.5466" x2="17.9578" y2="4.5974" layer="94"/>
<rectangle x1="19.177" y1="4.5466" x2="19.431" y2="4.5974" layer="94"/>
<rectangle x1="19.9898" y1="4.5466" x2="21.0566" y2="4.5974" layer="94"/>
<rectangle x1="22.987" y1="4.5466" x2="23.241" y2="4.5974" layer="94"/>
<rectangle x1="25.3746" y1="4.5466" x2="26.2382" y2="4.5974" layer="94"/>
<rectangle x1="29.3878" y1="4.5466" x2="29.6418" y2="4.5974" layer="94"/>
<rectangle x1="-0.0254" y1="4.5974" x2="5.6134" y2="4.6482" layer="94"/>
<rectangle x1="7.6454" y1="4.5974" x2="7.8994" y2="4.6482" layer="94"/>
<rectangle x1="8.4582" y1="4.5974" x2="9.525" y2="4.6482" layer="94"/>
<rectangle x1="11.5062" y1="4.5974" x2="11.7602" y2="4.6482" layer="94"/>
<rectangle x1="12.9286" y1="4.5974" x2="13.335" y2="4.6482" layer="94"/>
<rectangle x1="15.0114" y1="4.5974" x2="15.9258" y2="4.6482" layer="94"/>
<rectangle x1="17.6022" y1="4.5974" x2="17.9578" y2="4.6482" layer="94"/>
<rectangle x1="19.177" y1="4.5974" x2="19.431" y2="4.6482" layer="94"/>
<rectangle x1="19.939" y1="4.5974" x2="21.0058" y2="4.6482" layer="94"/>
<rectangle x1="22.987" y1="4.5974" x2="23.241" y2="4.6482" layer="94"/>
<rectangle x1="25.3746" y1="4.5974" x2="26.2382" y2="4.6482" layer="94"/>
<rectangle x1="29.3878" y1="4.5974" x2="29.6418" y2="4.6482" layer="94"/>
<rectangle x1="0.0254" y1="4.6482" x2="5.6134" y2="4.699" layer="94"/>
<rectangle x1="7.6454" y1="4.6482" x2="7.8994" y2="4.699" layer="94"/>
<rectangle x1="8.4074" y1="4.6482" x2="9.4742" y2="4.699" layer="94"/>
<rectangle x1="11.5062" y1="4.6482" x2="11.7602" y2="4.699" layer="94"/>
<rectangle x1="12.9286" y1="4.6482" x2="13.335" y2="4.699" layer="94"/>
<rectangle x1="15.0114" y1="4.6482" x2="15.9258" y2="4.699" layer="94"/>
<rectangle x1="17.5514" y1="4.6482" x2="17.9578" y2="4.699" layer="94"/>
<rectangle x1="19.177" y1="4.6482" x2="19.431" y2="4.699" layer="94"/>
<rectangle x1="19.8882" y1="4.6482" x2="21.0058" y2="4.699" layer="94"/>
<rectangle x1="22.987" y1="4.6482" x2="23.241" y2="4.699" layer="94"/>
<rectangle x1="25.3746" y1="4.6482" x2="26.2382" y2="4.699" layer="94"/>
<rectangle x1="29.3878" y1="4.6482" x2="29.6418" y2="4.699" layer="94"/>
<rectangle x1="0.0254" y1="4.699" x2="5.6134" y2="4.7498" layer="94"/>
<rectangle x1="7.6454" y1="4.699" x2="7.8994" y2="4.7498" layer="94"/>
<rectangle x1="8.3566" y1="4.699" x2="9.4742" y2="4.7498" layer="94"/>
<rectangle x1="11.5062" y1="4.699" x2="11.7602" y2="4.7498" layer="94"/>
<rectangle x1="12.9286" y1="4.699" x2="13.335" y2="4.7498" layer="94"/>
<rectangle x1="15.0114" y1="4.699" x2="15.9258" y2="4.7498" layer="94"/>
<rectangle x1="17.5514" y1="4.699" x2="17.9578" y2="4.7498" layer="94"/>
<rectangle x1="19.177" y1="4.699" x2="19.431" y2="4.7498" layer="94"/>
<rectangle x1="19.8882" y1="4.699" x2="20.955" y2="4.7498" layer="94"/>
<rectangle x1="22.987" y1="4.699" x2="23.241" y2="4.7498" layer="94"/>
<rectangle x1="25.3746" y1="4.699" x2="26.2382" y2="4.7498" layer="94"/>
<rectangle x1="29.3878" y1="4.699" x2="29.6418" y2="4.7498" layer="94"/>
<rectangle x1="0.0254" y1="4.7498" x2="5.5626" y2="4.8006" layer="94"/>
<rectangle x1="7.6454" y1="4.7498" x2="7.8994" y2="4.8006" layer="94"/>
<rectangle x1="8.3566" y1="4.7498" x2="9.4234" y2="4.8006" layer="94"/>
<rectangle x1="11.5062" y1="4.7498" x2="11.7602" y2="4.8006" layer="94"/>
<rectangle x1="12.9286" y1="4.7498" x2="13.3858" y2="4.8006" layer="94"/>
<rectangle x1="15.0114" y1="4.7498" x2="15.9258" y2="4.8006" layer="94"/>
<rectangle x1="17.5514" y1="4.7498" x2="17.9578" y2="4.8006" layer="94"/>
<rectangle x1="19.177" y1="4.7498" x2="19.431" y2="4.8006" layer="94"/>
<rectangle x1="19.8374" y1="4.7498" x2="20.9042" y2="4.8006" layer="94"/>
<rectangle x1="22.987" y1="4.7498" x2="23.241" y2="4.8006" layer="94"/>
<rectangle x1="25.3746" y1="4.7498" x2="26.2382" y2="4.8006" layer="94"/>
<rectangle x1="29.3878" y1="4.7498" x2="29.6418" y2="4.8006" layer="94"/>
<rectangle x1="0.0254" y1="4.8006" x2="5.5626" y2="4.8514" layer="94"/>
<rectangle x1="7.6454" y1="4.8006" x2="7.8994" y2="4.8514" layer="94"/>
<rectangle x1="8.3058" y1="4.8006" x2="9.3726" y2="4.8514" layer="94"/>
<rectangle x1="11.5062" y1="4.8006" x2="11.7602" y2="4.8514" layer="94"/>
<rectangle x1="12.9286" y1="4.8006" x2="13.3858" y2="4.8514" layer="94"/>
<rectangle x1="15.0114" y1="4.8006" x2="15.9258" y2="4.8514" layer="94"/>
<rectangle x1="17.5006" y1="4.8006" x2="17.9578" y2="4.8514" layer="94"/>
<rectangle x1="19.177" y1="4.8006" x2="19.431" y2="4.8514" layer="94"/>
<rectangle x1="19.7866" y1="4.8006" x2="20.9042" y2="4.8514" layer="94"/>
<rectangle x1="22.987" y1="4.8006" x2="23.241" y2="4.8514" layer="94"/>
<rectangle x1="25.3746" y1="4.8006" x2="26.2382" y2="4.8514" layer="94"/>
<rectangle x1="29.3878" y1="4.8006" x2="29.6418" y2="4.8514" layer="94"/>
<rectangle x1="0.0762" y1="4.8514" x2="5.5626" y2="4.9022" layer="94"/>
<rectangle x1="7.6454" y1="4.8514" x2="7.8994" y2="4.9022" layer="94"/>
<rectangle x1="8.3058" y1="4.8514" x2="9.3726" y2="4.9022" layer="94"/>
<rectangle x1="11.5062" y1="4.8514" x2="11.7602" y2="4.9022" layer="94"/>
<rectangle x1="12.9286" y1="4.8514" x2="13.4366" y2="4.9022" layer="94"/>
<rectangle x1="15.0114" y1="4.8514" x2="15.9258" y2="4.9022" layer="94"/>
<rectangle x1="17.5006" y1="4.8514" x2="17.9578" y2="4.9022" layer="94"/>
<rectangle x1="19.177" y1="4.8514" x2="19.431" y2="4.9022" layer="94"/>
<rectangle x1="19.7358" y1="4.8514" x2="20.8534" y2="4.9022" layer="94"/>
<rectangle x1="22.987" y1="4.8514" x2="23.241" y2="4.9022" layer="94"/>
<rectangle x1="25.3746" y1="4.8514" x2="26.2382" y2="4.9022" layer="94"/>
<rectangle x1="29.3878" y1="4.8514" x2="29.6418" y2="4.9022" layer="94"/>
<rectangle x1="0.0762" y1="4.9022" x2="5.5118" y2="4.953" layer="94"/>
<rectangle x1="7.6454" y1="4.9022" x2="7.8994" y2="4.953" layer="94"/>
<rectangle x1="8.255" y1="4.9022" x2="9.3218" y2="4.953" layer="94"/>
<rectangle x1="11.5062" y1="4.9022" x2="11.7602" y2="4.953" layer="94"/>
<rectangle x1="12.9286" y1="4.9022" x2="13.4366" y2="4.953" layer="94"/>
<rectangle x1="15.0114" y1="4.9022" x2="15.9258" y2="4.953" layer="94"/>
<rectangle x1="17.4498" y1="4.9022" x2="17.9578" y2="4.953" layer="94"/>
<rectangle x1="19.177" y1="4.9022" x2="19.431" y2="4.953" layer="94"/>
<rectangle x1="19.7358" y1="4.9022" x2="20.8026" y2="4.953" layer="94"/>
<rectangle x1="22.987" y1="4.9022" x2="23.241" y2="4.953" layer="94"/>
<rectangle x1="25.3746" y1="4.9022" x2="26.2382" y2="4.953" layer="94"/>
<rectangle x1="29.3878" y1="4.9022" x2="29.6418" y2="4.953" layer="94"/>
<rectangle x1="0.127" y1="4.953" x2="5.5118" y2="5.0038" layer="94"/>
<rectangle x1="7.6454" y1="4.953" x2="7.8994" y2="5.0038" layer="94"/>
<rectangle x1="8.2042" y1="4.953" x2="9.271" y2="5.0038" layer="94"/>
<rectangle x1="11.5062" y1="4.953" x2="11.7602" y2="5.0038" layer="94"/>
<rectangle x1="12.9286" y1="4.953" x2="13.4874" y2="5.0038" layer="94"/>
<rectangle x1="15.0114" y1="4.953" x2="15.9258" y2="5.0038" layer="94"/>
<rectangle x1="17.399" y1="4.953" x2="17.9578" y2="5.0038" layer="94"/>
<rectangle x1="19.177" y1="4.953" x2="19.431" y2="5.0038" layer="94"/>
<rectangle x1="19.685" y1="4.953" x2="20.7518" y2="5.0038" layer="94"/>
<rectangle x1="22.987" y1="4.953" x2="23.241" y2="5.0038" layer="94"/>
<rectangle x1="25.3746" y1="4.953" x2="26.2382" y2="5.0038" layer="94"/>
<rectangle x1="29.3878" y1="4.953" x2="29.6418" y2="5.0038" layer="94"/>
<rectangle x1="0.1778" y1="5.0038" x2="5.461" y2="5.0546" layer="94"/>
<rectangle x1="7.6454" y1="5.0038" x2="7.8994" y2="5.0546" layer="94"/>
<rectangle x1="8.1534" y1="5.0038" x2="9.2202" y2="5.0546" layer="94"/>
<rectangle x1="11.5062" y1="5.0038" x2="11.7602" y2="5.0546" layer="94"/>
<rectangle x1="12.9286" y1="5.0038" x2="13.5382" y2="5.0546" layer="94"/>
<rectangle x1="15.0114" y1="5.0038" x2="15.9258" y2="5.0546" layer="94"/>
<rectangle x1="17.399" y1="5.0038" x2="17.9578" y2="5.0546" layer="94"/>
<rectangle x1="19.177" y1="5.0038" x2="19.431" y2="5.0546" layer="94"/>
<rectangle x1="19.6342" y1="5.0038" x2="20.7518" y2="5.0546" layer="94"/>
<rectangle x1="22.987" y1="5.0038" x2="23.241" y2="5.0546" layer="94"/>
<rectangle x1="25.3746" y1="5.0038" x2="26.2382" y2="5.0546" layer="94"/>
<rectangle x1="29.3878" y1="5.0038" x2="29.6418" y2="5.0546" layer="94"/>
<rectangle x1="0.1778" y1="5.0546" x2="5.461" y2="5.1054" layer="94"/>
<rectangle x1="7.6454" y1="5.0546" x2="7.8994" y2="5.1054" layer="94"/>
<rectangle x1="8.1534" y1="5.0546" x2="9.2202" y2="5.1054" layer="94"/>
<rectangle x1="11.5062" y1="5.0546" x2="11.7602" y2="5.1054" layer="94"/>
<rectangle x1="12.9286" y1="5.0546" x2="13.5382" y2="5.1054" layer="94"/>
<rectangle x1="15.0114" y1="5.0546" x2="15.9258" y2="5.1054" layer="94"/>
<rectangle x1="17.3482" y1="5.0546" x2="17.9578" y2="5.1054" layer="94"/>
<rectangle x1="19.177" y1="5.0546" x2="19.431" y2="5.1054" layer="94"/>
<rectangle x1="19.6342" y1="5.0546" x2="20.701" y2="5.1054" layer="94"/>
<rectangle x1="22.987" y1="5.0546" x2="23.241" y2="5.1054" layer="94"/>
<rectangle x1="25.3746" y1="5.0546" x2="26.2382" y2="5.1054" layer="94"/>
<rectangle x1="29.3878" y1="5.0546" x2="29.6418" y2="5.1054" layer="94"/>
<rectangle x1="0.2286" y1="5.1054" x2="5.4102" y2="5.1562" layer="94"/>
<rectangle x1="7.6454" y1="5.1054" x2="7.8994" y2="5.1562" layer="94"/>
<rectangle x1="8.1026" y1="5.1054" x2="9.1694" y2="5.1562" layer="94"/>
<rectangle x1="11.5062" y1="5.1054" x2="11.7602" y2="5.1562" layer="94"/>
<rectangle x1="12.9286" y1="5.1054" x2="13.589" y2="5.1562" layer="94"/>
<rectangle x1="15.0114" y1="5.1054" x2="15.9258" y2="5.1562" layer="94"/>
<rectangle x1="17.3482" y1="5.1054" x2="17.9578" y2="5.1562" layer="94"/>
<rectangle x1="19.177" y1="5.1054" x2="19.431" y2="5.1562" layer="94"/>
<rectangle x1="19.5834" y1="5.1054" x2="20.6502" y2="5.1562" layer="94"/>
<rectangle x1="22.987" y1="5.1054" x2="23.241" y2="5.1562" layer="94"/>
<rectangle x1="25.3746" y1="5.1054" x2="26.2382" y2="5.1562" layer="94"/>
<rectangle x1="29.3878" y1="5.1054" x2="29.6418" y2="5.1562" layer="94"/>
<rectangle x1="0.2286" y1="5.1562" x2="5.4102" y2="5.207" layer="94"/>
<rectangle x1="7.6454" y1="5.1562" x2="7.8994" y2="5.207" layer="94"/>
<rectangle x1="8.0518" y1="5.1562" x2="9.1694" y2="5.207" layer="94"/>
<rectangle x1="11.5062" y1="5.1562" x2="11.7602" y2="5.207" layer="94"/>
<rectangle x1="12.9286" y1="5.1562" x2="13.6398" y2="5.207" layer="94"/>
<rectangle x1="15.0114" y1="5.1562" x2="15.9258" y2="5.207" layer="94"/>
<rectangle x1="17.2974" y1="5.1562" x2="17.9578" y2="5.207" layer="94"/>
<rectangle x1="19.177" y1="5.1562" x2="19.431" y2="5.207" layer="94"/>
<rectangle x1="19.5834" y1="5.1562" x2="20.6502" y2="5.207" layer="94"/>
<rectangle x1="22.987" y1="5.1562" x2="23.241" y2="5.207" layer="94"/>
<rectangle x1="25.3746" y1="5.1562" x2="26.2382" y2="5.207" layer="94"/>
<rectangle x1="29.3878" y1="5.1562" x2="29.6418" y2="5.207" layer="94"/>
<rectangle x1="0.2794" y1="5.207" x2="5.3594" y2="5.2578" layer="94"/>
<rectangle x1="7.6454" y1="5.207" x2="7.8994" y2="5.2578" layer="94"/>
<rectangle x1="8.0518" y1="5.207" x2="9.1186" y2="5.2578" layer="94"/>
<rectangle x1="11.5062" y1="5.207" x2="11.7602" y2="5.2578" layer="94"/>
<rectangle x1="12.9286" y1="5.207" x2="13.6906" y2="5.2578" layer="94"/>
<rectangle x1="15.0114" y1="5.207" x2="15.9258" y2="5.2578" layer="94"/>
<rectangle x1="17.2466" y1="5.207" x2="17.9578" y2="5.2578" layer="94"/>
<rectangle x1="19.177" y1="5.207" x2="19.431" y2="5.2578" layer="94"/>
<rectangle x1="19.5326" y1="5.207" x2="20.5994" y2="5.2578" layer="94"/>
<rectangle x1="22.987" y1="5.207" x2="23.241" y2="5.2578" layer="94"/>
<rectangle x1="25.3746" y1="5.207" x2="26.2382" y2="5.2578" layer="94"/>
<rectangle x1="29.3878" y1="5.207" x2="29.6418" y2="5.2578" layer="94"/>
<rectangle x1="0.3302" y1="5.2578" x2="5.3086" y2="5.3086" layer="94"/>
<rectangle x1="7.6454" y1="5.2578" x2="7.8994" y2="5.3086" layer="94"/>
<rectangle x1="8.001" y1="5.2578" x2="9.0678" y2="5.3086" layer="94"/>
<rectangle x1="11.5062" y1="5.2578" x2="11.7602" y2="5.3086" layer="94"/>
<rectangle x1="12.9286" y1="5.2578" x2="13.7414" y2="5.3086" layer="94"/>
<rectangle x1="15.0114" y1="5.2578" x2="15.9258" y2="5.3086" layer="94"/>
<rectangle x1="17.145" y1="5.2578" x2="17.9578" y2="5.3086" layer="94"/>
<rectangle x1="19.177" y1="5.2578" x2="19.431" y2="5.3086" layer="94"/>
<rectangle x1="19.4818" y1="5.2578" x2="20.5486" y2="5.3086" layer="94"/>
<rectangle x1="22.987" y1="5.2578" x2="23.241" y2="5.3086" layer="94"/>
<rectangle x1="25.3746" y1="5.2578" x2="26.2382" y2="5.3086" layer="94"/>
<rectangle x1="29.3878" y1="5.2578" x2="29.6418" y2="5.3086" layer="94"/>
<rectangle x1="0.381" y1="5.3086" x2="5.2578" y2="5.3594" layer="94"/>
<rectangle x1="7.6454" y1="5.3086" x2="7.8994" y2="5.3594" layer="94"/>
<rectangle x1="7.9502" y1="5.3086" x2="9.017" y2="5.3594" layer="94"/>
<rectangle x1="11.5062" y1="5.3086" x2="11.7602" y2="5.3594" layer="94"/>
<rectangle x1="12.9286" y1="5.3086" x2="13.843" y2="5.3594" layer="94"/>
<rectangle x1="15.0114" y1="5.3086" x2="15.9258" y2="5.3594" layer="94"/>
<rectangle x1="17.0942" y1="5.3086" x2="17.9578" y2="5.3594" layer="94"/>
<rectangle x1="19.177" y1="5.3086" x2="20.4978" y2="5.3594" layer="94"/>
<rectangle x1="22.987" y1="5.3086" x2="23.241" y2="5.3594" layer="94"/>
<rectangle x1="25.3746" y1="5.3086" x2="26.2382" y2="5.3594" layer="94"/>
<rectangle x1="29.3878" y1="5.3086" x2="29.6418" y2="5.3594" layer="94"/>
<rectangle x1="0.4318" y1="5.3594" x2="5.207" y2="5.4102" layer="94"/>
<rectangle x1="7.6454" y1="5.3594" x2="9.017" y2="5.4102" layer="94"/>
<rectangle x1="11.5062" y1="5.3594" x2="11.7602" y2="5.4102" layer="94"/>
<rectangle x1="12.9286" y1="5.3594" x2="13.8938" y2="5.4102" layer="94"/>
<rectangle x1="15.0114" y1="5.3594" x2="15.9258" y2="5.4102" layer="94"/>
<rectangle x1="17.0434" y1="5.3594" x2="17.9578" y2="5.4102" layer="94"/>
<rectangle x1="19.177" y1="5.3594" x2="20.4978" y2="5.4102" layer="94"/>
<rectangle x1="22.987" y1="5.3594" x2="23.241" y2="5.4102" layer="94"/>
<rectangle x1="25.3238" y1="5.3594" x2="26.2382" y2="5.4102" layer="94"/>
<rectangle x1="29.3878" y1="5.3594" x2="29.6418" y2="5.4102" layer="94"/>
<rectangle x1="0.4826" y1="5.4102" x2="5.1562" y2="5.461" layer="94"/>
<rectangle x1="7.6454" y1="5.4102" x2="8.9662" y2="5.461" layer="94"/>
<rectangle x1="11.5062" y1="5.4102" x2="11.7602" y2="5.461" layer="94"/>
<rectangle x1="12.9286" y1="5.4102" x2="13.9446" y2="5.461" layer="94"/>
<rectangle x1="15.0114" y1="5.4102" x2="15.9258" y2="5.461" layer="94"/>
<rectangle x1="16.9418" y1="5.4102" x2="17.9578" y2="5.461" layer="94"/>
<rectangle x1="19.177" y1="5.4102" x2="20.447" y2="5.461" layer="94"/>
<rectangle x1="22.987" y1="5.4102" x2="23.241" y2="5.461" layer="94"/>
<rectangle x1="25.3238" y1="5.4102" x2="26.2382" y2="5.461" layer="94"/>
<rectangle x1="29.3878" y1="5.4102" x2="29.6418" y2="5.461" layer="94"/>
<rectangle x1="0.5334" y1="5.461" x2="5.0546" y2="5.5118" layer="94"/>
<rectangle x1="7.6454" y1="5.461" x2="8.9154" y2="5.5118" layer="94"/>
<rectangle x1="11.5062" y1="5.461" x2="11.7602" y2="5.5118" layer="94"/>
<rectangle x1="12.9286" y1="5.461" x2="14.1478" y2="5.5118" layer="94"/>
<rectangle x1="15.0114" y1="5.461" x2="15.9258" y2="5.5118" layer="94"/>
<rectangle x1="16.7386" y1="5.461" x2="17.9578" y2="5.5118" layer="94"/>
<rectangle x1="19.177" y1="5.461" x2="20.447" y2="5.5118" layer="94"/>
<rectangle x1="22.987" y1="5.461" x2="23.241" y2="5.5118" layer="94"/>
<rectangle x1="25.3238" y1="5.461" x2="26.2382" y2="5.5118" layer="94"/>
<rectangle x1="29.337" y1="5.461" x2="29.6418" y2="5.5118" layer="94"/>
<rectangle x1="0.635" y1="5.5118" x2="5.0038" y2="5.5626" layer="94"/>
<rectangle x1="7.6454" y1="5.5118" x2="8.9154" y2="5.5626" layer="94"/>
<rectangle x1="11.5062" y1="5.5118" x2="11.7602" y2="5.5626" layer="94"/>
<rectangle x1="12.9286" y1="5.5118" x2="17.9578" y2="5.5626" layer="94"/>
<rectangle x1="19.177" y1="5.5118" x2="20.3962" y2="5.5626" layer="94"/>
<rectangle x1="22.987" y1="5.5118" x2="23.241" y2="5.5626" layer="94"/>
<rectangle x1="25.3238" y1="5.5118" x2="26.2382" y2="5.5626" layer="94"/>
<rectangle x1="29.337" y1="5.5118" x2="29.6418" y2="5.5626" layer="94"/>
<rectangle x1="0.6858" y1="5.5626" x2="4.9022" y2="5.6134" layer="94"/>
<rectangle x1="6.9342" y1="5.5626" x2="8.8646" y2="5.6134" layer="94"/>
<rectangle x1="10.6426" y1="5.5626" x2="12.4206" y2="5.6134" layer="94"/>
<rectangle x1="12.9286" y1="5.5626" x2="17.9578" y2="5.6134" layer="94"/>
<rectangle x1="18.415" y1="5.5626" x2="20.3454" y2="5.6134" layer="94"/>
<rectangle x1="22.1234" y1="5.5626" x2="23.9522" y2="5.6134" layer="94"/>
<rectangle x1="24.6634" y1="5.5626" x2="27.051" y2="5.6134" layer="94"/>
<rectangle x1="28.575" y1="5.5626" x2="30.353" y2="5.6134" layer="94"/>
<rectangle x1="0.7874" y1="5.6134" x2="4.8514" y2="5.6642" layer="94"/>
<rectangle x1="6.9342" y1="5.6134" x2="8.8138" y2="5.6642" layer="94"/>
<rectangle x1="10.6426" y1="5.6134" x2="12.4206" y2="5.6642" layer="94"/>
<rectangle x1="12.9286" y1="5.6134" x2="17.9578" y2="5.6642" layer="94"/>
<rectangle x1="18.415" y1="5.6134" x2="20.2946" y2="5.6642" layer="94"/>
<rectangle x1="22.1234" y1="5.6134" x2="23.9522" y2="5.6642" layer="94"/>
<rectangle x1="24.6634" y1="5.6134" x2="27.051" y2="5.6642" layer="94"/>
<rectangle x1="28.575" y1="5.6134" x2="30.353" y2="5.6642" layer="94"/>
<rectangle x1="0.9398" y1="5.6642" x2="4.699" y2="5.715" layer="94"/>
<rectangle x1="6.9342" y1="5.6642" x2="8.8138" y2="5.715" layer="94"/>
<rectangle x1="10.6426" y1="5.6642" x2="12.4206" y2="5.715" layer="94"/>
<rectangle x1="12.9286" y1="5.6642" x2="17.9578" y2="5.715" layer="94"/>
<rectangle x1="18.415" y1="5.6642" x2="20.2946" y2="5.715" layer="94"/>
<rectangle x1="22.1234" y1="5.6642" x2="23.9522" y2="5.715" layer="94"/>
<rectangle x1="24.6634" y1="5.6642" x2="27.051" y2="5.715" layer="94"/>
<rectangle x1="28.575" y1="5.6642" x2="30.353" y2="5.715" layer="94"/>
<rectangle x1="1.0922" y1="5.715" x2="4.5466" y2="5.7658" layer="94"/>
<rectangle x1="6.9342" y1="5.715" x2="8.763" y2="5.7658" layer="94"/>
<rectangle x1="10.6426" y1="5.715" x2="12.4206" y2="5.7658" layer="94"/>
<rectangle x1="12.9286" y1="5.715" x2="17.9578" y2="5.7658" layer="94"/>
<rectangle x1="18.415" y1="5.715" x2="20.2438" y2="5.7658" layer="94"/>
<rectangle x1="22.1234" y1="5.715" x2="23.9522" y2="5.7658" layer="94"/>
<rectangle x1="24.6634" y1="5.715" x2="27.051" y2="5.7658" layer="94"/>
<rectangle x1="28.575" y1="5.715" x2="30.353" y2="5.7658" layer="94"/>
</symbol>
<symbol name="BAT" urn="urn:adsk.eagle:symbol:9535776/1" library_version="10">
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="0" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="-0.635" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="2.54" x2="-0.635" y2="0" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-2.54" width="0.4064" layer="94"/>
<text x="-2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+" x="2.54" y="0" visible="pad" length="short" direction="pwr" rot="R180"/>
<pin name="-" x="-5.08" y="0" visible="pad" length="short" direction="pwr"/>
</symbol>
<symbol name="SWITCH-ON-OFF-ON" urn="urn:adsk.eagle:symbol:9557600/1" library_version="11" library_locally_modified="yes">
<wire x1="-3.81" y1="1.905" x2="-2.54" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.762" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="0.254" y1="0" x2="0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.905" x2="0.635" y2="3.175" width="0.254" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="3.175" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.254" y2="-0.762" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-0.762" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<text x="-3.81" y="2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<text x="-6.35" y="-1.905" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<pin name="3" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="2" x="10.16" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
</symbol>
<symbol name="TMP117" urn="urn:adsk.eagle:symbol:9496069/3" library_version="11" library_locally_modified="yes">
<pin name="SCL" x="-17.78" y="-2.54" length="middle" direction="in" function="clk"/>
<pin name="GND" x="17.78" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="ALERT" x="-17.78" y="-7.62" length="middle" direction="nc"/>
<pin name="ADD0" x="17.78" y="2.54" length="middle" direction="in" rot="R180"/>
<pin name="V+" x="-17.78" y="7.62" length="middle" direction="pwr"/>
<pin name="SDA" x="-17.78" y="2.54" length="middle"/>
<pin name="PAD" x="17.78" y="-7.62" visible="pin" length="middle" direction="pas" rot="R180"/>
<wire x1="-12.7" y1="-20.32" x2="12.7" y2="-20.32" width="0.2032" layer="94"/>
<wire x1="12.7" y1="-20.32" x2="12.7" y2="-12.7" width="0.2032" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="10.16" width="0.2032" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="15.24" width="0.2032" layer="94"/>
<wire x1="12.7" y1="15.24" x2="-12.7" y2="15.24" width="0.2032" layer="94"/>
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="10.16" width="0.2032" layer="94"/>
<wire x1="-12.7" y1="10.16" x2="-12.7" y2="-12.7" width="0.2032" layer="94"/>
<wire x1="-12.7" y1="-12.7" x2="-12.7" y2="-20.32" width="0.2032" layer="94"/>
<wire x1="-12.7" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-12.7" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<text x="-12.3444" y="16.7386" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="-12.9794" y="-23.9014" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
<text x="-10.16" y="-17.78" size="1.27" layer="94">V+ :        1.8 V to 5.5 V
Size:       2.00 x 2.00 [mm]</text>
<text x="-7.62" y="12.7" size="1.27" layer="94">Temperature Sensor</text>
</symbol>
<symbol name="LAUNCHPAD_TEMPLATE_LIBRARY_BP_HEADERS_PWR_5V" urn="urn:adsk.eagle:symbol:9557599/1" library_version="11" library_locally_modified="yes">
<wire x1="-7.62" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<pin name="1" x="7.62" y="2.54" visible="pad" length="middle" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" rot="R180"/>
<text x="-2.54" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="0" y="3.302" size="1.778" layer="94" rot="R180">+5V</text>
<text x="0" y="0.762" size="1.778" layer="94" rot="R180">GND</text>
<text x="0" y="-1.778" size="1.778" layer="94" rot="R180">GND</text>
</symbol>
<symbol name="TS" urn="urn:adsk.eagle:symbol:9658924/1" library_version="13" library_locally_modified="yes">
<wire x1="0" y1="-3.175" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="3.175" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-0.635" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<text x="-6.35" y="-1.905" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="2*4HEADER" urn="urn:adsk.eagle:symbol:9658925/2" library_version="14" library_locally_modified="yes">
<wire x1="0" y1="5.08" x2="-7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="5.08" width="0.1524" layer="94"/>
<pin name="P1" x="5.08" y="2.54" length="middle" rot="R180"/>
<pin name="P2" x="5.08" y="0" length="middle" rot="R180"/>
<pin name="P3" x="5.08" y="-2.54" length="middle" rot="R180"/>
<pin name="P4" x="5.08" y="-5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="CY15B108" urn="urn:adsk.eagle:symbol:9688572/1" library_version="14" library_locally_modified="yes">
<pin name="!CS" x="-22.86" y="10.16" length="middle"/>
<pin name="SO" x="-22.86" y="5.08" length="middle"/>
<pin name="!WP" x="-22.86" y="0" length="middle"/>
<pin name="VSS" x="-22.86" y="-5.08" length="middle"/>
<pin name="SI" x="5.08" y="-5.08" length="middle" rot="R180"/>
<pin name="SCK" x="5.08" y="0" length="middle" rot="R180"/>
<pin name="DNU" x="5.08" y="5.08" length="middle" rot="R180"/>
<pin name="VDD" x="5.08" y="10.16" length="middle" rot="R180"/>
<wire x1="-17.78" y1="-7.62" x2="-17.78" y2="12.7" width="0.1524" layer="94"/>
<wire x1="-17.78" y1="12.7" x2="0" y2="12.7" width="0.1524" layer="94"/>
<wire x1="0" y1="12.7" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="-17.78" y2="-7.62" width="0.1524" layer="94"/>
<text x="-12.7" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="HEADER2*10" urn="urn:adsk.eagle:symbol:9774410/1" library_version="15">
<wire x1="-12.7" y1="15.24" x2="-12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-12.7" y1="15.24" x2="7.62" y2="15.24" width="0.254" layer="94"/>
<wire x1="7.62" y1="15.24" x2="7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="-12.7" y2="-12.7" width="0.254" layer="94"/>
<pin name="1" x="-15.24" y="12.7" visible="pad" length="short"/>
<pin name="2" x="-15.24" y="10.16" visible="pad" length="short"/>
<pin name="3" x="-15.24" y="7.62" visible="pad" length="short"/>
<pin name="4" x="-15.24" y="5.08" visible="pad" length="short"/>
<pin name="5" x="-15.24" y="2.54" visible="pad" length="short"/>
<pin name="6" x="-15.24" y="0" visible="pad" length="short"/>
<pin name="7" x="-15.24" y="-2.54" visible="pad" length="short"/>
<pin name="8" x="-15.24" y="-5.08" visible="pad" length="short"/>
<pin name="9" x="-15.24" y="-7.62" visible="pad" length="short"/>
<pin name="10" x="-15.24" y="-10.16" visible="pad" length="short"/>
<text x="0" y="17.78" size="1.778" layer="95" font="vector" rot="R180">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="95" font="vector">&gt;VALUE</text>
<text x="5.08" y="12.7" size="1.27" layer="97">1</text>
<text x="5.08" y="-10.16" size="1.27" layer="97">10</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="KMX62-1031-SR" urn="urn:adsk.eagle:component:9209795/6" prefix="U" library_version="17">
<description>&lt;b&gt;Tri-Axis Magnetometer/ Tri-Axis Accelerometer&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="KMX62" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LGA-16">
<connects>
<connect gate="G$1" pin="ADDR" pad="7"/>
<connect gate="G$1" pin="CAP" pad="2"/>
<connect gate="G$1" pin="GND_1" pad="3"/>
<connect gate="G$1" pin="GND_2" pad="5"/>
<connect gate="G$1" pin="GND_3" pad="12"/>
<connect gate="G$1" pin="GPIO1" pad="11"/>
<connect gate="G$1" pin="GPIO2" pad="9"/>
<connect gate="G$1" pin="IO_VDD" pad="1"/>
<connect gate="G$1" pin="NC_1" pad="8"/>
<connect gate="G$1" pin="NC_2" pad="10"/>
<connect gate="G$1" pin="NC_3" pad="13"/>
<connect gate="G$1" pin="NC_4" pad="15"/>
<connect gate="G$1" pin="NC_5" pad="16"/>
<connect gate="G$1" pin="SCL" pad="4"/>
<connect gate="G$1" pin="SDA" pad="6"/>
<connect gate="G$1" pin="VDD" pad="14"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:793905/9"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MS5837-30BA" urn="urn:adsk.eagle:component:9209792/5" prefix="U" library_version="17">
<description>&lt;b&gt;Pressure sensor&lt;/b&gt;&lt;p&gt;
 High resolution pressure sensors with I2C bus interface</description>
<gates>
<gate name="G$1" symbol="MS5837-30BA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MS5837-30BA">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="SCL" pad="3"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="VDD" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:793906/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AT45DB641E-UDFN" urn="urn:adsk.eagle:component:9209788/4" prefix="U" library_version="17">
<description>&lt;b&gt;ADESTO TECHNOLOGIES - AT45DB641E-MHN-Y - MEMORY, SERIAL FLASH, 64MBIT, UDFN-8&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.mouser.com/ds/2/590/DS-45DB641E-027-534005.pdf"&gt; Datasheet &lt;/a&gt;
&lt;p&gt; Created By Ali Aljumaili  - 2019 &lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="AT45DB641E" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UDFN-8">
<connects>
<connect gate="G$1" pin="!CS" pad="4"/>
<connect gate="G$1" pin="!RESET" pad="3"/>
<connect gate="G$1" pin="!WP" pad="5"/>
<connect gate="G$1" pin="GND" pad="7"/>
<connect gate="G$1" pin="SCK" pad="2"/>
<connect gate="G$1" pin="SI" pad="1"/>
<connect gate="G$1" pin="SO" pad="8"/>
<connect gate="G$1" pin="TP" pad="TP1 TP2 TP3 TP4"/>
<connect gate="G$1" pin="VCC" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8841323/4"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="ADESTO TECHNOLOGIES - AT45DB641E-MHN-Y - MEMORY, SERIAL FLASH, 64MBIT, UDFN-8" constant="no"/>
<attribute name="HEIGHT" value="0.6mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Adesto Technologies" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="AT45DB641E-MHN-Y" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:9209798/2" prefix="GND" library_version="17">
<description>&lt;b&gt;GROUND SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPACITOR" urn="urn:adsk.eagle:component:9209785/4" prefix="C" library_version="14" library_locally_modified="yes">
<gates>
<gate name="C$1" symbol="CAPACITOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0603">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9209778/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="C0805K">
<connects>
<connect gate="C$1" pin="1" pad="1"/>
<connect gate="C$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9688580/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:9209799/2" prefix="VCC" library_version="17">
<description>&lt;b&gt;VCC POWER SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R603" urn="urn:adsk.eagle:component:9209786/4" prefix="R" library_version="17">
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8849207/5"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CM315D" urn="urn:adsk.eagle:component:9209790/3" prefix="Y" library_version="17">
<gates>
<gate name="Y$1" symbol="CRYSTAL" x="-5.08" y="0"/>
</gates>
<devices>
<device name="" package="CRYSTAL">
<connects>
<connect gate="Y$1" pin="1" pad="1"/>
<connect gate="Y$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8849208/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="14P-HEADER" urn="urn:adsk.eagle:component:9371637/4" prefix="J" library_version="10" library_locally_modified="yes">
<description>&lt;b&gt;FEMALE HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="14P-HEADER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2-1761603-5">
<connects>
<connect gate="G$1" pin="GND" pad="9"/>
<connect gate="G$1" pin="I2C_SCL" pad="10"/>
<connect gate="G$1" pin="N/C" pad="6"/>
<connect gate="G$1" pin="RST" pad="11"/>
<connect gate="G$1" pin="TCK" pad="7"/>
<connect gate="G$1" pin="TDI/VPP" pad="3"/>
<connect gate="G$1" pin="TDO/TDI" pad="1"/>
<connect gate="G$1" pin="TEST/VPP" pad="8"/>
<connect gate="G$1" pin="TMS" pad="5"/>
<connect gate="G$1" pin="UART_RTS" pad="13"/>
<connect gate="G$1" pin="UART_RXD" pad="14"/>
<connect gate="G$1" pin="UART_TXD" pad="12"/>
<connect gate="G$1" pin="VCC_TARGET" pad="4"/>
<connect gate="G$1" pin="VCC_TOOL" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9371634/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="A4-FRAME" urn="urn:adsk.eagle:component:9371636/3" prefix="FRAME" uservalue="yes" library_version="10" library_locally_modified="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4-FRAME" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MSP430FR5738RGE" urn="urn:adsk.eagle:component:9376780/4" prefix="U" library_version="12" library_locally_modified="yes">
<description>24 MHz Mixed Signal Microcontroller, 1024 B SRAM and 17 GPIOs, -40 to 85 degC</description>
<gates>
<gate name="G$1" symbol="MSP430FR5738RGE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="VQFN-24">
<connects>
<connect gate="G$1" pin="AVCC" pad="24"/>
<connect gate="G$1" pin="AVSS" pad="23"/>
<connect gate="G$1" pin="DVCC" pad="20"/>
<connect gate="G$1" pin="DVSS" pad="19"/>
<connect gate="G$1" pin="EP" pad="TP1 TP2 TP3 TP4"/>
<connect gate="G$1" pin="P1.0/TA0.1/DMAE0/RTCCLK/A0*/CD0/VEREF-*" pad="1"/>
<connect gate="G$1" pin="P1.1/TA0.2/TA1CLK/CDOUT/A1*/CD1/VEREF+*" pad="2"/>
<connect gate="G$1" pin="P1.2/TA1.1/TA0CLK/CDOUT/A2*/CD2" pad="3"/>
<connect gate="G$1" pin="P1.3/TA1.2/UCB0STE/A3*/CD3" pad="4"/>
<connect gate="G$1" pin="P1.4/TB0.1/UCA0STE/A4*/CD4" pad="5"/>
<connect gate="G$1" pin="P1.5/TB0.2/UCA0CLK/A5*/CD5" pad="6"/>
<connect gate="G$1" pin="P1.6/UCB0SIMO/UCB0SDA/TA0.0" pad="16"/>
<connect gate="G$1" pin="P1.7/UCB0SOMI/UCB0SCL/TA1.0" pad="17"/>
<connect gate="G$1" pin="P2.0/UCA0TXD/UCA0SIMO/TB0CLK/ACLK" pad="13"/>
<connect gate="G$1" pin="P2.1/UCA0RXD/UCA0SOMI/TB0.0" pad="14"/>
<connect gate="G$1" pin="P2.2/UCB0CLK" pad="15"/>
<connect gate="G$1" pin="PJ.0/TDO/TB0OUTH/SMCLK/CD6" pad="7"/>
<connect gate="G$1" pin="PJ.1/TDI/TCLK/MCLK/CD7" pad="8"/>
<connect gate="G$1" pin="PJ.2/TMS/ACLK/CD8" pad="9"/>
<connect gate="G$1" pin="PJ.3/TCK/CD9" pad="10"/>
<connect gate="G$1" pin="PJ.4/XIN" pad="21"/>
<connect gate="G$1" pin="PJ.5/XOUT" pad="22"/>
<connect gate="G$1" pin="RST/NMI/SBWTDIO" pad="12"/>
<connect gate="G$1" pin="TEST/SBWTCK" pad="11"/>
<connect gate="G$1" pin="VCORE" pad="18"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9526563/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CDBU0130L" urn="urn:adsk.eagle:component:9209780/3" prefix="D" library_version="17">
<gates>
<gate name="G$1" symbol="SCHOTTKY-DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOD-523">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9209767/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="DIOC0603_N">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9209769/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RF430CL330HIRGTR" urn="urn:adsk.eagle:component:9209791/7" prefix="U" library_version="17">
<description>&lt;b&gt;Texas Instruments - RF430CL330H -  Dynamic Dual Interface NFC Transponder
 - VQFN-16&lt;/b&gt;&lt;p&gt;

&lt;p&gt;
&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt; &lt;TD align=center&gt;Name&lt;/TD&gt; &lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;ANT1&lt;/TD&gt;&lt;TD&gt;Antenna input1&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;ANT2&lt;/TD&gt;&lt;TD&gt;Antenna input2&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;!RST!&lt;/TD&gt;&lt;TD&gt;Reset input (active low)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;E0 (TMS)&lt;/TD&gt;&lt;TD&gt;I2C address select0&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;E1 (TDO)&lt;/TD&gt;&lt;TD&gt;I2C address select1&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;E2 (TDI)&lt;/TD&gt;&lt;TD&gt;I2C address select2&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;7&lt;/TD&gt;&lt;TD&gt;INTO (TCK)&lt;/TD&gt;&lt;TD&gt;Interrupt output&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;8&lt;/TD&gt;&lt;TD&gt;!CS!&lt;/TD&gt;&lt;TD&gt;Chip select (SPI mode)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;9&lt;/TD&gt;&lt;TD&gt;SCK&lt;/TD&gt;&lt;TD&gt;SPI clock input (SPI mode)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;10&lt;/TD&gt;&lt;TD&gt;SCL&lt;/TD&gt;&lt;TD&gt;I2C clock (I2C mode)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;11&lt;/TD&gt;&lt;TD&gt;SDA&lt;/TD&gt;&lt;TD&gt;I2C data (I2C mode)&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;12&lt;/TD&gt;&lt;TD&gt;Vcore&lt;/TD&gt;&lt;TD&gt;Regulated core supply voltage&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;13&lt;/TD&gt;&lt;TD&gt;VSS&lt;/TD&gt;&lt;TD&gt;Ground supply&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;15&lt;/TD&gt;&lt;TD&gt;VCC&lt;/TD&gt;&lt;TD&gt;3.3V power supply&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;14,16&lt;/TD&gt;&lt;TD&gt;NC&lt;/TD&gt;&lt;TD&gt;Leave open, no connection&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;EP&lt;/TD&gt;&lt;TD&gt;EP&lt;/TD&gt;&lt;TD&gt;Exposed Thermal Pad&lt;/TD&gt;&lt;/TR&gt;
&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;

&lt;a href="http://www.ti.com/lit/gpn/rf430cl330h "&gt;Datasheet&lt;/a&gt;&lt;BR&gt;

©2019 - modified and updated by Ali Aljumaili.</description>
<gates>
<gate name="A" symbol="RF430CL330HRGT" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="VQFN-16">
<connects>
<connect gate="A" pin="!CS!/SCMS" pad="8"/>
<connect gate="A" pin="!RST!" pad="3"/>
<connect gate="A" pin="ANT1" pad="1"/>
<connect gate="A" pin="ANT2" pad="2"/>
<connect gate="A" pin="E0(TMS)" pad="4"/>
<connect gate="A" pin="E1(TDO)" pad="5"/>
<connect gate="A" pin="E2(TDI)" pad="6"/>
<connect gate="A" pin="EP" pad="EP"/>
<connect gate="A" pin="INTO" pad="7"/>
<connect gate="A" pin="NC" pad="14"/>
<connect gate="A" pin="NC_2" pad="16"/>
<connect gate="A" pin="SCK" pad="9"/>
<connect gate="A" pin="SCL/SO" pad="10"/>
<connect gate="A" pin="SDA/SI" pad="11"/>
<connect gate="A" pin="VCC" pad="15"/>
<connect gate="A" pin="VCORE" pad="12"/>
<connect gate="A" pin="VSS" pad="13"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9496073/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ANTENNA" urn="urn:adsk.eagle:component:9209781/2" prefix="ANT" library_version="17">
<gates>
<gate name="G$1" symbol="NFC" x="12.7" y="-2.54"/>
</gates>
<devices>
<device name="" package="NFC_ANT">
<connects>
<connect gate="G$1" pin="P$1" pad="1 P$1"/>
<connect gate="G$1" pin="P$2" pad="2 P$2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9209768/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RF_PWR" urn="urn:adsk.eagle:component:9535783/1" prefix="P+" library_version="10" library_locally_modified="yes">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+12V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TP" urn="urn:adsk.eagle:component:9209783/5" prefix="TP" library_version="17">
<description>Test Point</description>
<gates>
<gate name="G$1" symbol="TEST-POINT" x="0" y="0"/>
</gates>
<devices>
<device name="TEST_PAD" package="TP_PAD">
<connects>
<connect gate="G$1" pin="TP" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9209771/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOLDER_PAD" package="TP_PAD_SOLDER">
<connects>
<connect gate="G$1" pin="TP" pad="P$1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9209770/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="TP_HOLE">
<connects>
<connect gate="G$1" pin="TP" pad="P1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9658929/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="XC6504A251MR-G" urn="urn:adsk.eagle:component:9209794/5" prefix="U" library_version="17">
<description>&lt;b&gt;LDO&lt;/b&gt;&lt;p&gt;

Low Quiescent Current Voltage Regulator with ON/OFF Control</description>
<gates>
<gate name="G$1" symbol="LDO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT25">
<connects>
<connect gate="G$1" pin="CE" pad="3"/>
<connect gate="G$1" pin="NC" pad="4"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
<connect gate="G$1" pin="VSS" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:793913/4"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PB-FREE" urn="urn:adsk.eagle:component:9371639/3" prefix="LOGO" library_version="17">
<gates>
<gate name="G$1" symbol="PB-FREE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PB-FREE">
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9339155/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NTNU_LOGO_BOTTOM_SILK" urn="urn:adsk.eagle:component:9376781/2" prefix="LOGO" library_version="17">
<gates>
<gate name="G$1" symbol="NTNU" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BATTERY" urn="urn:adsk.eagle:component:9535782/4" prefix="BAT" library_version="17">
<gates>
<gate name="G$1" symbol="BAT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BATTERY">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9535781/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SWITCH" urn="urn:adsk.eagle:component:9557622/2" prefix="S" uservalue="yes" library_version="13" library_locally_modified="yes">
<description>&lt;b&gt;TOGGLE SWITCH&lt;/b&gt;&lt;p&gt;
Mors, distributor RS Components</description>
<gates>
<gate name="1" symbol="SWITCH-ON-OFF-ON" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SWITCH">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="3"/>
<connect gate="1" pin="3" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9557619/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TMP117" urn="urn:adsk.eagle:component:9496077/4" prefix="U" library_version="17">
<description>&lt;b&gt;Texas Instruments - TMP117x - High accuracy, low-power digital temperature sensor- WSON-6&lt;/b&gt;&lt;p&gt;

&lt;p&gt;
&lt;TABLE cellspacing=0 cellpadding=0 border=0&gt;
&lt;TR&gt;&lt;TD width=20&gt;&lt;/TD&gt;&lt;TD&gt;
&lt;TABLE cellspacing=0 cellpadding=1 border=1&gt;
&lt;TR bgcolor=silver&gt;&lt;TD align=center&gt;PAD&lt;/TD&gt; &lt;TD align=center&gt;Name&lt;/TD&gt; &lt;TD align=center&gt;Description&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;1&lt;/TD&gt;&lt;TD&gt;SCL&lt;/TD&gt;&lt;TD&gt;Serial Clock&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;2&lt;/TD&gt;&lt;TD&gt;GND&lt;/TD&gt;&lt;TD&gt;Ground&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;3&lt;/TD&gt;&lt;TD&gt;ALERT&lt;/TD&gt;&lt;TD&gt;Overtemperaturealertor data-readysignal.Open-drain output; requires a pullupresistor&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;4&lt;/TD&gt;&lt;TD&gt;ADD0&lt;/TD&gt;&lt;TD&gt;Address select. Connect to GND,V+, SDA,or SCL&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;5&lt;/TD&gt;&lt;TD&gt;V+&lt;/TD&gt;&lt;TD&gt;Supply voltage&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;&lt;TD align=center&gt;6&lt;/TD&gt;&lt;TD&gt;SDA&lt;/TD&gt;&lt;TD&gt;Serial data input&lt;/TD&gt;&lt;/TR&gt;

&lt;/TABLE&gt;
&lt;/TD&gt;&lt;/TR&gt;&lt;/TABLE&gt;&lt;BR&gt;&lt;BR&gt;

&lt;a href="http://www.ti.com/lit/ds/symlink/tmp117.pdf "&gt;Datasheet&lt;/a&gt;&lt;BR&gt;

©2019 - modified and updated by Ali Aljumaili.</description>
<gates>
<gate name="G$1" symbol="TMP117" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WSON-6">
<connects>
<connect gate="G$1" pin="ADD0" pad="4"/>
<connect gate="G$1" pin="ALERT" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="PAD" pad="7"/>
<connect gate="G$1" pin="SCL" pad="1"/>
<connect gate="G$1" pin="SDA" pad="6"/>
<connect gate="G$1" pin="V+" pad="5"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9557612/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PWR_HEADER" urn="urn:adsk.eagle:component:9557621/2" prefix="J" library_version="17">
<gates>
<gate name="G$1" symbol="LAUNCHPAD_TEMPLATE_LIBRARY_BP_HEADERS_PWR_5V" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LAUNCHPAD_TEMPLATE_LIBRARY_3PIN_PWR_5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9557613/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SW_BUTTON" urn="urn:adsk.eagle:component:9658932/1" prefix="S" uservalue="yes" library_version="13" library_locally_modified="yes">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="TS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BUTTON">
<connects>
<connect gate="1" pin="P" pad="P1"/>
<connect gate="1" pin="S" pad="P2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9658930/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2*4_HEADER" urn="urn:adsk.eagle:component:9658933/3" prefix="XS" library_version="17">
<gates>
<gate name="G$1" symbol="2*4HEADER" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="2X4_HEADER">
<connects>
<connect gate="G$1" pin="P1" pad="P$1 P$2"/>
<connect gate="G$1" pin="P2" pad="P$3 P$4"/>
<connect gate="G$1" pin="P3" pad="P$5 P$6"/>
<connect gate="G$1" pin="P4" pad="P$7 P$8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9658931/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMALLER" package="2X4_HEADER_SMALL">
<connects>
<connect gate="G$1" pin="P1" pad="P$1 P$2"/>
<connect gate="G$1" pin="P2" pad="P$3 P$4"/>
<connect gate="G$1" pin="P3" pad="P$5 P$6"/>
<connect gate="G$1" pin="P4" pad="P$7 P$8"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9688578/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CY15B108" urn="urn:adsk.eagle:component:9688581/2" prefix="U" library_version="17">
<gates>
<gate name="G$1" symbol="CY15B108" x="7.62" y="-2.54"/>
</gates>
<devices>
<device name="" package="GQFN-8">
<connects>
<connect gate="G$1" pin="!CS" pad="P1"/>
<connect gate="G$1" pin="!WP" pad="P3"/>
<connect gate="G$1" pin="DNU" pad="P7"/>
<connect gate="G$1" pin="SCK" pad="P6"/>
<connect gate="G$1" pin="SI" pad="P5"/>
<connect gate="G$1" pin="SO" pad="P2"/>
<connect gate="G$1" pin="VDD" pad="P8"/>
<connect gate="G$1" pin="VSS" pad="P4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9688579/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2*10_HEADER" urn="urn:adsk.eagle:component:9774417/2" prefix="XS" library_version="17">
<gates>
<gate name="G$1" symbol="HEADER2*10" x="-12.7" y="5.08"/>
<gate name="G$2" symbol="HEADER2*10" x="30.48" y="5.08"/>
</gates>
<devices>
<device name="" package="1X20_1.27+2X10_2.54">
<connects>
<connect gate="G$1" pin="1" pad="1 P$1"/>
<connect gate="G$1" pin="10" pad="19 P$19"/>
<connect gate="G$1" pin="2" pad="3 P$3"/>
<connect gate="G$1" pin="3" pad="5 P$5"/>
<connect gate="G$1" pin="4" pad="7 P$7"/>
<connect gate="G$1" pin="5" pad="9 P$9"/>
<connect gate="G$1" pin="6" pad="11 P$11"/>
<connect gate="G$1" pin="7" pad="13 P$13"/>
<connect gate="G$1" pin="8" pad="15 P$15"/>
<connect gate="G$1" pin="9" pad="17 P$17"/>
<connect gate="G$2" pin="1" pad="2 P$2"/>
<connect gate="G$2" pin="10" pad="20 P$20"/>
<connect gate="G$2" pin="2" pad="4 P$4"/>
<connect gate="G$2" pin="3" pad="6 P$6"/>
<connect gate="G$2" pin="4" pad="8 P$8"/>
<connect gate="G$2" pin="5" pad="10 P$10"/>
<connect gate="G$2" pin="6" pad="12 P$12"/>
<connect gate="G$2" pin="7" pad="14 P$14"/>
<connect gate="G$2" pin="8" pad="16 P$16"/>
<connect gate="G$2" pin="9" pad="18 P$18"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:9774416/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="led" urn="urn:adsk.eagle:library:259">
<description>&lt;b&gt;LEDs&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;br&gt;
Extended by Federico Battaglin &lt;author&gt;&amp;lt;federico.rd@fdpinternational.com&amp;gt;&lt;/author&gt; with DUOLED</description>
<packages>
<package name="SML1206" urn="urn:adsk.eagle:footprint:15684/1" library_version="1">
<description>&lt;b&gt;SML10XXKH-TR (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;SML10R3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10E3KH-TR&lt;/td&gt;&lt;td&gt;SUPER REDSUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10O3KH-TR&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PY3KH-TR&lt;/td&gt;&lt;td&gt;PURE YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10OY3KH-TR&lt;/td&gt;&lt;td&gt;ULTRA YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10AG3KH-TR&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10BG3KH-TR&lt;/td&gt;&lt;td&gt;BLUE GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10PB1KH-TR&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;SML10CW1KH-TR&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;

Source: http://www.ledtronics.com/ds/smd-1206/dstr0094.PDF</description>
<wire x1="-1.5" y1="0.5" x2="-1.5" y2="-0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="1.5" y1="-0.5" x2="1.5" y2="0.5" width="0.2032" layer="51" curve="-180"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<circle x="-0.725" y="0.525" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="1.75" y="0" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="0.4" x2="-1.15" y2="0.8" layer="51"/>
<rectangle x1="-1.6" y1="-0.8" x2="-1.15" y2="-0.4" layer="51"/>
<rectangle x1="-1.175" y1="-0.6" x2="-1" y2="-0.275" layer="51"/>
<rectangle x1="1.15" y1="-0.8" x2="1.6" y2="-0.4" layer="51" rot="R180"/>
<rectangle x1="1.15" y1="0.4" x2="1.6" y2="0.8" layer="51" rot="R180"/>
<rectangle x1="1" y1="0.275" x2="1.175" y2="0.6" layer="51" rot="R180"/>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
</package>
<package name="1206" urn="urn:adsk.eagle:footprint:15651/1" library_version="1">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260" urn="urn:adsk.eagle:footprint:15652/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5" urn="urn:adsk.eagle:footprint:15653/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED3MM" urn="urn:adsk.eagle:footprint:15654/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM" urn="urn:adsk.eagle:footprint:15655/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260" urn="urn:adsk.eagle:footprint:15656/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181" urn="urn:adsk.eagle:footprint:15657/1" library_version="1">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152" urn="urn:adsk.eagle:footprint:15658/1" library_version="1">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-0.254" x2="-2.9718" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.1082" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153" urn="urn:adsk.eagle:footprint:15659/1" library_version="1">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-0.254" x2="-5.5118" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-4.6482" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.2129" y1="0.0539" x2="-0.0539" y2="2.2129" width="0.1524" layer="51" curve="-90.010616"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155" urn="urn:adsk.eagle:footprint:15660/1" library_version="1">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156" urn="urn:adsk.eagle:footprint:15661/1" library_version="1">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90"/>
<wire x1="0.0539" y1="-2.2129" x2="2.2129" y2="-0.0539" width="0.1524" layer="51" curve="90.005308"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480" urn="urn:adsk.eagle:footprint:15662/1" library_version="1">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482" urn="urn:adsk.eagle:footprint:15650/1" library_version="1">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90"/>
<wire x1="0.0539" y1="-1.0699" x2="1.0699" y2="-0.0539" width="0.1524" layer="51" curve="90"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32" urn="urn:adsk.eagle:footprint:15640/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A" urn="urn:adsk.eagle:footprint:15663/1" library_version="1">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2" urn="urn:adsk.eagle:footprint:15664/1" library_version="1">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED" urn="urn:adsk.eagle:footprint:15665/1" library_version="1">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="29"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="29"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED" urn="urn:adsk.eagle:footprint:15666/1" library_version="1">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED" urn="urn:adsk.eagle:footprint:15667/1" library_version="1">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG" urn="urn:adsk.eagle:footprint:15668/1" library_version="1">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED" urn="urn:adsk.eagle:footprint:15669/1" library_version="1">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4" urn="urn:adsk.eagle:footprint:15670/1" library_version="1">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603" urn="urn:adsk.eagle:footprint:15671/1" library_version="1">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805" urn="urn:adsk.eagle:footprint:15672/1" library_version="1">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA" urn="urn:adsk.eagle:footprint:15673/1" library_version="1">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805" urn="urn:adsk.eagle:footprint:15674/1" library_version="1">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="0.925" x2="0.35" y2="0.925" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="-0.35" y1="-0.925" x2="0.35" y2="-0.925" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.3" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206" urn="urn:adsk.eagle:footprint:15675/1" library_version="1">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.6" x2="0.4" y2="1.6" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED_0603" urn="urn:adsk.eagle:footprint:15676/1" library_version="1">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW" urn="urn:adsk.eagle:footprint:15677/1" library_version="1">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.8" x2="0.3" y2="0.8" width="0.1016" layer="51" curve="170.055574"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW" urn="urn:adsk.eagle:footprint:15678/1" library_version="1">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.15" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="LUMILED+" urn="urn:adsk.eagle:footprint:15679/1" library_version="1">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; with cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="1">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LUMILED" urn="urn:adsk.eagle:footprint:15680/1" library_version="1">
<description>&lt;b&gt;Lumileds Lighting. LUXEON®&lt;/b&gt; without cool pad&lt;p&gt;
Source: K2.pdf</description>
<wire x1="-3.575" y1="2.3375" x2="-2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="3.575" x2="2.3375" y2="3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="2.3375" x2="3.575" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="3.575" y1="-3.575" x2="-2.3375" y2="-3.575" width="0.2032" layer="21"/>
<wire x1="-2.3375" y1="-3.575" x2="-2.5" y2="-3.4125" width="0.2032" layer="21"/>
<wire x1="-2.5" y1="-3.4125" x2="-3.4125" y2="-2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="-3.4125" y1="-2.5" x2="-3.575" y2="-2.3375" width="0.2032" layer="21"/>
<wire x1="-3.575" y1="-2.3375" x2="-3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="2.3375" y1="3.575" x2="2.5" y2="3.4125" width="0.2032" layer="21"/>
<wire x1="2.5" y1="3.4125" x2="3.4125" y2="2.5" width="0.2032" layer="21" curve="167.429893"/>
<wire x1="3.4125" y1="2.5" x2="3.575" y2="2.3375" width="0.2032" layer="21"/>
<wire x1="-1.725" y1="2.225" x2="-1.0625" y2="2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<wire x1="1.725" y1="-2.225" x2="1.0625" y2="-2.5625" width="0.2032" layer="21" curve="-255.44999"/>
<circle x="0" y="0" radius="2.725" width="0.2032" layer="51"/>
<smd name="1NC" x="-5.2" y="1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="2+" x="-5.2" y="-1.15" dx="2.9" dy="1.7" layer="1"/>
<smd name="3NC" x="5.2" y="-1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<smd name="4-" x="5.2" y="1.15" dx="2.9" dy="1.7" layer="1" rot="R180"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-5.975" y1="0.575" x2="-3.625" y2="1.6" layer="51"/>
<rectangle x1="-5.975" y1="-1.6" x2="-3.625" y2="-0.575" layer="51"/>
<rectangle x1="3.625" y1="-1.6" x2="5.975" y2="-0.575" layer="51" rot="R180"/>
<rectangle x1="3.625" y1="0.575" x2="5.975" y2="1.6" layer="51" rot="R180"/>
<polygon width="0.4064" layer="29">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
<polygon width="0.4064" layer="31">
<vertex x="2.3383" y="1.35"/>
<vertex x="0" y="2.7"/>
<vertex x="-2.3383" y="1.35"/>
<vertex x="-2.3383" y="-1.35"/>
<vertex x="0" y="-2.7"/>
<vertex x="2.3383" y="-1.35"/>
</polygon>
</package>
<package name="LED10MM" urn="urn:adsk.eagle:footprint:15681/1" library_version="1">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
10 mm, round</description>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.254" layer="21" curve="-306.869898"/>
<wire x1="4.445" y1="0" x2="0" y2="-4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="3.81" y1="0" x2="0" y2="-3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="3.175" y1="0" x2="0" y2="-3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="2.54" y1="0" x2="0" y2="-2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="-4.445" y1="0" x2="0" y2="4.445" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.81" y1="0" x2="0" y2="3.81" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="0" x2="0" y2="3.175" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="0" y2="2.54" width="0.127" layer="21" curve="-90"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="21"/>
<circle x="0" y="0" radius="5.08" width="0.127" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="square"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="6.35" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="6.35" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="KA-3528ASYC" urn="urn:adsk.eagle:footprint:15682/1" library_version="1">
<description>&lt;b&gt;SURFACE MOUNT LED LAMP&lt;/b&gt; 3.5x2.8mm&lt;p&gt;
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<wire x1="-1.55" y1="1.35" x2="1.55" y2="1.35" width="0.1016" layer="21"/>
<wire x1="1.55" y1="1.35" x2="1.55" y2="-1.35" width="0.1016" layer="51"/>
<wire x1="1.55" y1="-1.35" x2="-1.55" y2="-1.35" width="0.1016" layer="21"/>
<wire x1="-1.55" y1="-1.35" x2="-1.55" y2="1.35" width="0.1016" layer="51"/>
<wire x1="-0.65" y1="0.95" x2="0.65" y2="0.95" width="0.1016" layer="21" curve="-68.40813"/>
<wire x1="0.65" y1="-0.95" x2="-0.65" y2="-0.95" width="0.1016" layer="21" curve="-68.40813"/>
<circle x="0" y="0" radius="1.15" width="0.1016" layer="51"/>
<smd name="A" x="-1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<smd name="C" x="1.55" y="0" dx="1.5" dy="2.2" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.75" y1="0.6" x2="-1.6" y2="1.1" layer="51"/>
<rectangle x1="-1.75" y1="-1.1" x2="-1.6" y2="-0.6" layer="51"/>
<rectangle x1="1.6" y1="-1.1" x2="1.75" y2="-0.6" layer="51" rot="R180"/>
<rectangle x1="1.6" y1="0.6" x2="1.75" y2="1.1" layer="51" rot="R180"/>
<polygon width="0.1016" layer="51">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-0.625"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
<polygon width="0.1016" layer="21">
<vertex x="1.55" y="-1.35"/>
<vertex x="1.55" y="-1.175"/>
<vertex x="1" y="-1.175"/>
<vertex x="0.825" y="-1.35"/>
</polygon>
</package>
<package name="SML0805" urn="urn:adsk.eagle:footprint:15683/1" library_version="1">
<description>&lt;b&gt;SML0805-2CW-TR (0805 PROFILE)&lt;/b&gt; COOL WHITE&lt;p&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0093.pdf</description>
<wire x1="-0.95" y1="-0.55" x2="0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="-0.55" x2="0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="0.95" y1="0.55" x2="-0.95" y2="0.55" width="0.1016" layer="51"/>
<wire x1="-0.95" y1="0.55" x2="-0.95" y2="-0.55" width="0.1016" layer="51"/>
<wire x1="-0.175" y1="-0.025" x2="0" y2="0.15" width="0.0634" layer="21"/>
<wire x1="0" y1="0.15" x2="0.15" y2="0" width="0.0634" layer="21"/>
<wire x1="0.15" y1="0" x2="-0.025" y2="-0.175" width="0.0634" layer="21"/>
<wire x1="-0.025" y1="-0.175" x2="-0.175" y2="-0.025" width="0.0634" layer="21"/>
<circle x="-0.275" y="0.4" radius="0.125" width="0" layer="21"/>
<smd name="C" x="-1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="1.05" y="0" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.5" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.5" y="-2" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SML0603" urn="urn:adsk.eagle:footprint:15685/1" library_version="1">
<description>&lt;b&gt;SML0603-XXX (HIGH INTENSITY) LED&lt;/b&gt;&lt;p&gt;
&lt;table&gt;
&lt;tr&gt;&lt;td&gt;AG3K&lt;/td&gt;&lt;td&gt;AQUA GREEN&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;B1K&lt;/td&gt;&lt;td&gt;SUPER BLUE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R1K&lt;/td&gt;&lt;td&gt;SUPER RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;R3K&lt;/td&gt;&lt;td&gt;ULTRA RED&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3K&lt;/td&gt;&lt;td&gt;SUPER ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;O3KH&lt;/td&gt;&lt;td&gt;SOFT ORANGE&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3KH&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;Y3K&lt;/td&gt;&lt;td&gt;SUPER YELLOW&lt;/td&gt;&lt;/tr&gt;
&lt;tr&gt;&lt;td&gt;2CW&lt;/td&gt;&lt;td&gt;WHITE&lt;/td&gt;&lt;/tr&gt;
&lt;/table&gt;
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0092.pdf</description>
<wire x1="-0.75" y1="0.35" x2="0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="0.35" x2="0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.75" y1="-0.35" x2="-0.75" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="-0.35" x2="-0.75" y2="0.35" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.3" x2="-0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="0.45" y1="0.3" x2="0.45" y2="-0.3" width="0.1016" layer="51"/>
<wire x1="-0.2" y1="0.35" x2="0.2" y2="0.35" width="0.1016" layer="21"/>
<wire x1="-0.2" y1="-0.35" x2="0.2" y2="-0.35" width="0.1016" layer="21"/>
<smd name="C" x="-0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0.75" y="0" dx="0.8" dy="0.8" layer="1"/>
<text x="-1" y="1" size="1.27" layer="25">&gt;NAME</text>
<text x="-1" y="-2" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.4" y1="0.175" x2="0" y2="0.4" layer="51"/>
<rectangle x1="-0.25" y1="0.175" x2="0" y2="0.4" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="SML0603" urn="urn:adsk.eagle:package:15832/1" type="box" library_version="4">
<description>SML0603-XXX (HIGH INTENSITY) LED

AG3KAQUA GREEN
B1KSUPER BLUE
R1KSUPER RED
R3KULTRA RED
O3KSUPER ORANGE
O3KHSOFT ORANGE
Y3KHSUPER YELLOW
Y3KSUPER YELLOW
2CWWHITE

Source: http://www.ledtronics.com/ds/smd-0603/Dstr0092.pdf</description>
<packageinstances>
<packageinstance name="SML0603"/>
</packageinstances>
</package3d>
<package3d name="1206" urn="urn:adsk.eagle:package:15796/2" type="model" library_version="4">
<description>CHICAGO MINIATURE LAMP, INC.
7022X Series SMT LEDs 1206 Package Size</description>
<packageinstances>
<packageinstance name="1206"/>
</packageinstances>
</package3d>
<package3d name="LD260" urn="urn:adsk.eagle:package:15794/1" type="box" library_version="4">
<description>LED
5 mm, square, Siemens</description>
<packageinstances>
<packageinstance name="LD260"/>
</packageinstances>
</package3d>
<package3d name="LED2X5" urn="urn:adsk.eagle:package:15800/1" type="box" library_version="4">
<description>LED
2 x 5 mm, rectangle</description>
<packageinstances>
<packageinstance name="LED2X5"/>
</packageinstances>
</package3d>
<package3d name="LED3MM" urn="urn:adsk.eagle:package:15797/1" type="box" library_version="4">
<description>LED
3 mm, round</description>
<packageinstances>
<packageinstance name="LED3MM"/>
</packageinstances>
</package3d>
<package3d name="LED5MM" urn="urn:adsk.eagle:package:15799/2" type="model" library_version="4">
<description>LED
5 mm, round</description>
<packageinstances>
<packageinstance name="LED5MM"/>
</packageinstances>
</package3d>
<package3d name="LSU260" urn="urn:adsk.eagle:package:15805/1" type="box" library_version="4">
<description>LED
1 mm, round, Siemens</description>
<packageinstances>
<packageinstance name="LSU260"/>
</packageinstances>
</package3d>
<package3d name="LZR181" urn="urn:adsk.eagle:package:15808/1" type="box" library_version="4">
<description>LED BLOCK
1 LED, Siemens</description>
<packageinstances>
<packageinstance name="LZR181"/>
</packageinstances>
</package3d>
<package3d name="Q62902-B152" urn="urn:adsk.eagle:package:15803/1" type="box" library_version="4">
<description>LED HOLDER
Siemens</description>
<packageinstances>
<packageinstance name="Q62902-B152"/>
</packageinstances>
</package3d>
<package3d name="Q62902-B153" urn="urn:adsk.eagle:package:15804/1" type="box" library_version="4">
<description>LED HOLDER
Siemens</description>
<packageinstances>
<packageinstance name="Q62902-B153"/>
</packageinstances>
</package3d>
<package3d name="Q62902-B155" urn="urn:adsk.eagle:package:15807/1" type="box" library_version="4">
<description>LED HOLDER
Siemens</description>
<packageinstances>
<packageinstance name="Q62902-B155"/>
</packageinstances>
</package3d>
<package3d name="Q62902-B156" urn="urn:adsk.eagle:package:15806/1" type="box" library_version="4">
<description>LED HOLDER
Siemens</description>
<packageinstances>
<packageinstance name="Q62902-B156"/>
</packageinstances>
</package3d>
<package3d name="SFH480" urn="urn:adsk.eagle:package:15809/1" type="box" library_version="4">
<description>IR LED
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking
Inifineon</description>
<packageinstances>
<packageinstance name="SFH480"/>
</packageinstances>
</package3d>
<package3d name="SFH482" urn="urn:adsk.eagle:package:15795/1" type="box" library_version="4">
<description>IR LED
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking
Inifineon</description>
<packageinstances>
<packageinstance name="SFH482"/>
</packageinstances>
</package3d>
<package3d name="U57X32" urn="urn:adsk.eagle:package:15789/1" type="box" library_version="4">
<description>LED
rectangle, 5.7 x 3.2 mm</description>
<packageinstances>
<packageinstance name="U57X32"/>
</packageinstances>
</package3d>
<package3d name="IRL80A" urn="urn:adsk.eagle:package:15810/1" type="box" library_version="4">
<description>IR LED
IR transmitter Siemens</description>
<packageinstances>
<packageinstance name="IRL80A"/>
</packageinstances>
</package3d>
<package3d name="P-LCC-2" urn="urn:adsk.eagle:package:15817/1" type="box" library_version="4">
<description>TOPLED® High-optical Power LED (HOP)
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<packageinstances>
<packageinstance name="P-LCC-2"/>
</packageinstances>
</package3d>
<package3d name="OSRAM-MINI-TOP-LED" urn="urn:adsk.eagle:package:15811/1" type="box" library_version="4">
<description>BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<packageinstances>
<packageinstance name="OSRAM-MINI-TOP-LED"/>
</packageinstances>
</package3d>
<package3d name="OSRAM-SIDELED" urn="urn:adsk.eagle:package:15812/1" type="box" library_version="4">
<description>Super SIDELED® High-Current LED
LG A672, LP A672 
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<packageinstances>
<packageinstance name="OSRAM-SIDELED"/>
</packageinstances>
</package3d>
<package3d name="SMART-LED" urn="urn:adsk.eagle:package:15814/1" type="box" library_version="4">
<description>SmartLEDTM Hyper-Bright LED
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<packageinstances>
<packageinstance name="SMART-LED"/>
</packageinstances>
</package3d>
<package3d name="P-LCC-2-TOPLED-RG" urn="urn:adsk.eagle:package:15813/1" type="box" library_version="4">
<description>Hyper TOPLED® RG Hyper-Bright LED
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<packageinstances>
<packageinstance name="P-LCC-2-TOPLED-RG"/>
</packageinstances>
</package3d>
<package3d name="MICRO-SIDELED" urn="urn:adsk.eagle:package:15815/1" type="box" library_version="4">
<description>Hyper Micro SIDELED®
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<packageinstances>
<packageinstance name="MICRO-SIDELED"/>
</packageinstances>
</package3d>
<package3d name="P-LCC-4" urn="urn:adsk.eagle:package:15816/1" type="box" library_version="4">
<description>Power TOPLED®
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<packageinstances>
<packageinstance name="P-LCC-4"/>
</packageinstances>
</package3d>
<package3d name="CHIP-LED0603" urn="urn:adsk.eagle:package:15819/3" type="model" library_version="4">
<description>Hyper CHIPLED Hyper-Bright LED
LB Q993
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<packageinstances>
<packageinstance name="CHIP-LED0603"/>
</packageinstances>
</package3d>
<package3d name="CHIP-LED0805" urn="urn:adsk.eagle:package:15818/2" type="model" library_version="4">
<description>Hyper CHIPLED Hyper-Bright LED
LB R99A
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<packageinstances>
<packageinstance name="CHIP-LED0805"/>
</packageinstances>
</package3d>
<package3d name="MINI-TOPLED-SANTANA" urn="urn:adsk.eagle:package:15820/1" type="box" library_version="4">
<description>Mini TOPLED Santana®
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<packageinstances>
<packageinstance name="MINI-TOPLED-SANTANA"/>
</packageinstances>
</package3d>
<package3d name="CHIPLED_0805" urn="urn:adsk.eagle:package:15821/2" type="model" library_version="4">
<description>CHIPLED
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<packageinstances>
<packageinstance name="CHIPLED_0805"/>
</packageinstances>
</package3d>
<package3d name="CHIPLED_1206" urn="urn:adsk.eagle:package:15823/2" type="model" library_version="4">
<description>CHIPLED
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<packageinstances>
<packageinstance name="CHIPLED_1206"/>
</packageinstances>
</package3d>
<package3d name="CHIPLED_0603" urn="urn:adsk.eagle:package:15822/2" type="model" library_version="4">
<description>CHIPLED
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<packageinstances>
<packageinstance name="CHIPLED_0603"/>
</packageinstances>
</package3d>
<package3d name="CHIPLED-0603-TTW" urn="urn:adsk.eagle:package:15824/1" type="box" library_version="4">
<description>CHIPLED-0603
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603
Package able to withstand TTW-soldering heat
Package suitable for TTW-soldering
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<packageinstances>
<packageinstance name="CHIPLED-0603-TTW"/>
</packageinstances>
</package3d>
<package3d name="SMARTLED-TTW" urn="urn:adsk.eagle:package:15825/1" type="box" library_version="4">
<description>SmartLED TTW
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603
Package able to withstand TTW-soldering heat
Package suitable for TTW-soldering
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<packageinstances>
<packageinstance name="SMARTLED-TTW"/>
</packageinstances>
</package3d>
<package3d name="LUMILED+" urn="urn:adsk.eagle:package:15826/1" type="box" library_version="4">
<description>Lumileds Lighting. LUXEON® with cool pad
Source: K2.pdf</description>
<packageinstances>
<packageinstance name="LUMILED+"/>
</packageinstances>
</package3d>
<package3d name="LUMILED" urn="urn:adsk.eagle:package:15827/1" type="box" library_version="4">
<description>Lumileds Lighting. LUXEON® without cool pad
Source: K2.pdf</description>
<packageinstances>
<packageinstance name="LUMILED"/>
</packageinstances>
</package3d>
<package3d name="LED10MM" urn="urn:adsk.eagle:package:15828/1" type="box" library_version="4">
<description>LED
10 mm, round</description>
<packageinstances>
<packageinstance name="LED10MM"/>
</packageinstances>
</package3d>
<package3d name="KA-3528ASYC" urn="urn:adsk.eagle:package:15831/1" type="box" library_version="4">
<description>SURFACE MOUNT LED LAMP 3.5x2.8mm
Source: http://www.kingbright.com/manager/upload/pdf/KA-3528ASYC(Ver1189474662.1)</description>
<packageinstances>
<packageinstance name="KA-3528ASYC"/>
</packageinstances>
</package3d>
<package3d name="SML0805" urn="urn:adsk.eagle:package:15830/1" type="box" library_version="4">
<description>SML0805-2CW-TR (0805 PROFILE) COOL WHITE
Source: http://www.ledtronics.com/ds/smd-0603/Dstr0093.pdf</description>
<packageinstances>
<packageinstance name="SML0805"/>
</packageinstances>
</package3d>
<package3d name="SML1206" urn="urn:adsk.eagle:package:15829/1" type="box" library_version="4">
<description>SML10XXKH-TR (HIGH INTENSITY) LED

SML10R3KH-TRULTRA RED
SML10E3KH-TRSUPER REDSUPER BLUE
SML10O3KH-TRSUPER ORANGE
SML10PY3KH-TRPURE YELLOW
SML10OY3KH-TRULTRA YELLOW
SML10AG3KH-TRAQUA GREEN
SML10BG3KH-TRBLUE GREEN
SML10PB1KH-TRSUPER BLUE
SML10CW1KH-TRWHITE


Source: http://www.ledtronics.com/ds/smd-1206/dstr0094.PDF</description>
<packageinstances>
<packageinstance name="SML1206"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LED" urn="urn:adsk.eagle:symbol:15639/2" library_version="4">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" urn="urn:adsk.eagle:component:15916/9" prefix="LED" uservalue="yes" library_version="4">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;

- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K
&lt;p&gt;
Source: http://www.osram.convergy.de&lt;p&gt;

&lt;u&gt;LUXEON:&lt;/u&gt;&lt;br&gt;
- &lt;u&gt;LUMILED®&lt;/u&gt;&lt;br&gt;
LXK2-PW12-R00, LXK2-PW12-S00, LXK2-PW14-U00, LXK2-PW14-V00&lt;br&gt;
LXK2-PM12-R00, LXK2-PM12-S00, LXK2-PM14-U00&lt;br&gt;
LXK2-PE12-Q00, LXK2-PE12-R00, LXK2-PE12-S00, LXK2-PE14-T00, LXK2-PE14-U00&lt;br&gt;
LXK2-PB12-K00, LXK2-PB12-L00, LXK2-PB12-M00, LXK2-PB14-N00, LXK2-PB14-P00, LXK2-PB14-Q00&lt;br&gt;
LXK2-PR12-L00, LXK2-PR12-M00, LXK2-PR14-Q00, LXK2-PR14-R00&lt;br&gt;
LXK2-PD12-Q00, LXK2-PD12-R00, LXK2-PD12-S00&lt;br&gt;
LXK2-PH12-R00, LXK2-PH12-S00&lt;br&gt;
LXK2-PL12-P00, LXK2-PL12-Q00, LXK2-PL12-R00
&lt;p&gt;
Source: www.luxeon.com&lt;p&gt;

&lt;u&gt;KINGBRIGHT:&lt;/U&gt;&lt;p&gt;
KA-3528ASYC&lt;br&gt;
Source: www.kingbright.com</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15796/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15794/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15800/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15797/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15799/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15805/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15808/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15803/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15804/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15807/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15806/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15809/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15795/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15789/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15810/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15817/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15811/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15812/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15814/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15813/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15815/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15816/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15819/3"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15818/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15820/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15821/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15823/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15822/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15824/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15825/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED+" package="LUMILED+">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15826/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-LUMILED" package="LUMILED">
<connects>
<connect gate="G$1" pin="A" pad="2+"/>
<connect gate="G$1" pin="C" pad="4-"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15827/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10MM" package="LED10MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15828/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KA-3528ASYC" package="KA-3528ASYC">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15831/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0805" package="SML0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15830/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML1206" package="SML1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15829/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SML0603" package="SML0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15832/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
<attribute name="AUTHOR" value="Ali Aljumaili"/>
<attribute name="REV" value="1.2.0"/>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="R2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/5" value="10k"/>
<part name="R1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/5" value="10k"/>
<part name="U3" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="KMX62-1031-SR" device="" package3d_urn="urn:adsk.eagle:package:793905/9">
<attribute name="MF" value=""/>
<attribute name="MPN" value=""/>
<attribute name="OC_NEWARK" value="unknown"/>
</part>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="MS5837-30BA" device="" package3d_urn="urn:adsk.eagle:package:793906/3">
<attribute name="MF" value=""/>
<attribute name="MPN" value=""/>
<attribute name="OC_NEWARK" value="unknown"/>
</part>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C7" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="100nF"/>
<part name="C8" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="100nF"/>
<part name="C9" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="100nF"/>
<part name="C21" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="2200 pF"/>
<part name="GND27" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="GND" device=""/>
<part name="VCC5" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="VCC4" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="U4" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="AT45DB641E-UDFN" device="" package3d_urn="urn:adsk.eagle:package:8841323/4">
<attribute name="SPICEPREFIX" value="X"/>
</part>
<part name="GND8" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="GND" device=""/>
<part name="VCC9" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="C11" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="100nF"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="VCC1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="R3" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/5" value="0"/>
<part name="LED1" library="led" library_urn="urn:adsk.eagle:library:259" deviceset="LED" device="SML0603" package3d_urn="urn:adsk.eagle:package:15832/1" value="RED"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device="">
<attribute name="SPICEGROUND" value=""/>
</part>
<part name="GND2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="GND" device=""/>
<part name="GND3" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="GND" device=""/>
<part name="R8" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/5" value="47k"/>
<part name="VCC12" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="J1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="14P-HEADER" device="" package3d_urn="urn:adsk.eagle:package:9371634/4"/>
<part name="R9" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/5" value="330"/>
<part name="GND28" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="FRAME3" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="A4-FRAME" device=""/>
<part name="FRAME2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="A4-FRAME" device=""/>
<part name="FRAME1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="A4-FRAME" device=""/>
<part name="FRAME5" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="A4-FRAME" device=""/>
<part name="U1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="MSP430FR5738RGE" device="" package3d_urn="urn:adsk.eagle:package:9526563/3"/>
<part name="Y1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CM315D" device="" package3d_urn="urn:adsk.eagle:package:8849208/4" value="32.768 KHz"/>
<part name="C1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="10 pF"/>
<part name="C2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="10 pF"/>
<part name="C3" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="100 nF"/>
<part name="C4" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="4.7 uF"/>
<part name="C6" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="470 nF"/>
<part name="C5" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="100 nF"/>
<part name="VCC3" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device="">
<attribute name="SPICEGROUND" value=""/>
</part>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device="">
<attribute name="SPICEGROUND" value=""/>
</part>
<part name="VCC2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device="">
<attribute name="SPICEGROUND" value=""/>
</part>
<part name="R11" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/5" value="470"/>
<part name="LED2" library="led" library_urn="urn:adsk.eagle:library:259" deviceset="LED" device="SML0603" package3d_urn="urn:adsk.eagle:package:15832/1" value="GREEN"/>
<part name="GND32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device="">
<attribute name="SPICEGROUND" value=""/>
</part>
<part name="LED4" library="led" library_urn="urn:adsk.eagle:library:259" deviceset="LED" device="SML0603" package3d_urn="urn:adsk.eagle:package:15832/1" value="YELLOW"/>
<part name="GND34" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device="">
<attribute name="SPICEGROUND" value=""/>
</part>
<part name="R10" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/5" value="470"/>
<part name="FRAME4" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="A4-FRAME" device=""/>
<part name="C15" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="100nF"/>
<part name="C14" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="1uF"/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="C13" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="0.47uF"/>
<part name="GND17" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND20" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C16" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="35 pF"/>
<part name="GND19" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND18" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="GND" device=""/>
<part name="D3" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CDBU0130L" device="0603" package3d_urn="urn:adsk.eagle:package:9209769/1" value="CDBU0130L"/>
<part name="D4" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CDBU0130L" device="0603" package3d_urn="urn:adsk.eagle:package:9209769/1" value="CDBU0130L"/>
<part name="GND25" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C20" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="0805" package3d_urn="urn:adsk.eagle:package:9688580/1" value="100uF"/>
<part name="U7" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="RF430CL330HIRGTR" device="" package3d_urn="urn:adsk.eagle:package:9496073/4"/>
<part name="GND21" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="C17" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="Tune"/>
<part name="ANT" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="ANTENNA" device="" package3d_urn="urn:adsk.eagle:package:9209768/1"/>
<part name="P+4" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="RF_PWR" device="" value="RF_PWR"/>
<part name="TP1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="TP" device="TEST_PAD" package3d_urn="urn:adsk.eagle:package:9209771/2" value="BATT"/>
<part name="U8" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="XC6504A251MR-G" device="" package3d_urn="urn:adsk.eagle:package:793913/4">
<attribute name="MF" value=""/>
<attribute name="MPN" value=""/>
<attribute name="OC_NEWARK" value="unknown"/>
</part>
<part name="C18" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="1 uF"/>
<part name="GND23" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND22" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CDBU0130L" device="0603" package3d_urn="urn:adsk.eagle:package:9209769/1" value="CDBU0130L"/>
<part name="D2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CDBU0130L" device="0603" package3d_urn="urn:adsk.eagle:package:9209769/1" value="CDBU0130L"/>
<part name="GND26" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device="">
<attribute name="SPICEGROUND" value=""/>
</part>
<part name="VCC11" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="P+3" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="RF_PWR" device="" value="RF_PWR"/>
<part name="R12" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/5" value="470"/>
<part name="GND33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device="">
<attribute name="SPICEGROUND" value=""/>
</part>
<part name="LED3" library="led" library_urn="urn:adsk.eagle:library:259" deviceset="LED" device="SML0603" package3d_urn="urn:adsk.eagle:package:15832/1" value="GREEN"/>
<part name="LOGO1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="PB-FREE" device="" package3d_urn="urn:adsk.eagle:package:9339155/2"/>
<part name="LOGO2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="NTNU_LOGO_BOTTOM_SILK" device=""/>
<part name="BAT1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="BATTERY" device="" package3d_urn="urn:adsk.eagle:package:9535781/2"/>
<part name="D5" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CDBU0130L" device="0603" package3d_urn="urn:adsk.eagle:package:9209769/1" value="CDBU0130L"/>
<part name="S1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="SWITCH" device="" package3d_urn="urn:adsk.eagle:package:9557619/2"/>
<part name="BAT2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="BATTERY" device="" package3d_urn="urn:adsk.eagle:package:9535781/2"/>
<part name="GND29" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U6" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="TMP117" device="" package3d_urn="urn:adsk.eagle:package:9557612/2"/>
<part name="VCC8" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="GND15" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="GND" device=""/>
<part name="C10" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="100 nF"/>
<part name="J2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="PWR_HEADER" device="" package3d_urn="urn:adsk.eagle:package:9557613/2"/>
<part name="GND31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="ANT1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="TP" device="TEST_PAD" package3d_urn="urn:adsk.eagle:package:9209771/2" value="BATT"/>
<part name="ANT2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="TP" device="TEST_PAD" package3d_urn="urn:adsk.eagle:package:9209771/2" value="BATT"/>
<part name="TP4" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="TP" device="" package3d_urn="urn:adsk.eagle:package:9658929/1" value="BATT"/>
<part name="TP6" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="TP" device="" package3d_urn="urn:adsk.eagle:package:9658929/1" value="BATT"/>
<part name="GND12" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="GND" device=""/>
<part name="R7" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/5" value="470"/>
<part name="C19" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="1 uF"/>
<part name="GND24" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device="">
<attribute name="SPICEGROUND" value=""/>
</part>
<part name="TP7" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="TP" device="" package3d_urn="urn:adsk.eagle:package:9658929/1" value="BATT"/>
<part name="S2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="SW_BUTTON" device="" package3d_urn="urn:adsk.eagle:package:9658930/1"/>
<part name="GND30" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device="">
<attribute name="SPICEGROUND" value=""/>
</part>
<part name="XS1" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="2*4_HEADER" device="SMALLER" package3d_urn="urn:adsk.eagle:package:9688578/1" value="24_HEADERSMALLER"/>
<part name="VCC6" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U5" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CY15B108" device="" package3d_urn="urn:adsk.eagle:package:9688579/1"/>
<part name="VCC10" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C12" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="CAPACITOR" device="" package3d_urn="urn:adsk.eagle:package:9209778/2" value="100nF"/>
<part name="GND16" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="VCC7" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="VCC" device="" value="+2V_LDO"/>
<part name="FRAME6" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="A4-FRAME" device=""/>
<part name="XS2" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="2*10_HEADER" device="" package3d_urn="urn:adsk.eagle:package:9774416/1"/>
<part name="R4" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/5" value="0"/>
<part name="R5" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/5" value="0"/>
<part name="R6" library="DST_Project" library_urn="urn:adsk.eagle:library:9209732" deviceset="R603" device="" package3d_urn="urn:adsk.eagle:package:8849207/5" value="0"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="78.74" y="165.1" size="6.4516" layer="94">DST Schematics Explanation</text>
<wire x1="106.68" y1="134.62" x2="106.68" y2="104.14" width="0.508" layer="94"/>
<wire x1="106.68" y1="104.14" x2="165.1" y2="104.14" width="0.508" layer="94"/>
<wire x1="165.1" y1="134.62" x2="106.68" y2="134.62" width="0.508" layer="94"/>
<text x="119.38" y="116.84" size="3.81" layer="94">MCU, Debug</text>
<text x="109.22" y="106.68" size="1.778" layer="90">Sheet: 2</text>
<wire x1="99.06" y1="121.92" x2="99.06" y2="127" width="0.508" layer="90"/>
<wire x1="99.06" y1="127" x2="106.68" y2="119.38" width="0.508" layer="90"/>
<wire x1="106.68" y1="119.38" x2="99.06" y2="111.76" width="0.508" layer="90"/>
<wire x1="99.06" y1="111.76" x2="99.06" y2="116.84" width="0.508" layer="90"/>
<wire x1="86.36" y1="121.92" x2="99.06" y2="121.92" width="0.508" layer="90"/>
<wire x1="86.36" y1="116.84" x2="99.06" y2="116.84" width="0.508" layer="90"/>
<wire x1="5.08" y1="162.56" x2="124.46" y2="162.56" width="0.6096" layer="90" style="shortdash"/>
<wire x1="124.46" y1="162.56" x2="132.08" y2="157.48" width="0.6096" layer="90" style="shortdash"/>
<wire x1="132.08" y1="157.48" x2="139.7" y2="162.56" width="0.6096" layer="90" style="shortdash"/>
<wire x1="139.7" y1="162.56" x2="256.54" y2="162.56" width="0.6096" layer="90" style="shortdash"/>
<wire x1="165.1" y1="134.62" x2="165.1" y2="104.14" width="0.508" layer="94"/>
<text x="205.74" y="114.3" size="3.81" layer="94">Magnometer, 
Accelometer, 
Temp</text>
<text x="195.58" y="106.68" size="1.778" layer="90">Sheet: 2</text>
<wire x1="22.86" y1="76.2" x2="22.86" y2="45.72" width="0.508" layer="94"/>
<wire x1="22.86" y1="45.72" x2="78.74" y2="45.72" width="0.508" layer="94"/>
<wire x1="78.74" y1="45.72" x2="78.74" y2="76.2" width="0.508" layer="94"/>
<wire x1="78.74" y1="76.2" x2="22.86" y2="76.2" width="0.508" layer="94"/>
<text x="30.48" y="60.96" size="3.81" layer="94">Revision History</text>
<text x="25.4" y="48.26" size="2.54" layer="90">Sheet: 4</text>
<wire x1="22.86" y1="134.62" x2="22.86" y2="104.14" width="0.508" layer="94"/>
<wire x1="22.86" y1="104.14" x2="78.74" y2="104.14" width="0.508" layer="94"/>
<wire x1="78.74" y1="104.14" x2="78.74" y2="134.62" width="0.508" layer="94"/>
<wire x1="78.74" y1="134.62" x2="22.86" y2="134.62" width="0.4064" layer="94"/>
<text x="33.02" y="116.84" size="3.81" layer="94">NFC, Memory</text>
<text x="25.4" y="106.68" size="1.778" layer="90">Sheet: 3</text>
<wire x1="193.04" y1="134.62" x2="193.04" y2="104.14" width="0.508" layer="94"/>
<wire x1="193.04" y1="104.14" x2="248.92" y2="104.14" width="0.508" layer="94"/>
<wire x1="248.92" y1="104.14" x2="248.92" y2="134.62" width="0.508" layer="94"/>
<wire x1="248.92" y1="134.62" x2="193.04" y2="134.62" width="0.508" layer="94"/>
<wire x1="86.36" y1="121.92" x2="86.36" y2="127" width="0.508" layer="90"/>
<wire x1="86.36" y1="127" x2="78.74" y2="119.38" width="0.508" layer="90"/>
<wire x1="78.74" y1="119.38" x2="86.36" y2="111.76" width="0.508" layer="90"/>
<wire x1="86.36" y1="111.76" x2="86.36" y2="116.84" width="0.508" layer="90"/>
<wire x1="185.42" y1="121.92" x2="185.42" y2="127" width="0.508" layer="90"/>
<wire x1="185.42" y1="127" x2="193.04" y2="119.38" width="0.508" layer="90"/>
<wire x1="193.04" y1="119.38" x2="185.42" y2="111.76" width="0.508" layer="90"/>
<wire x1="185.42" y1="111.76" x2="185.42" y2="116.84" width="0.508" layer="90"/>
<wire x1="172.72" y1="121.92" x2="185.42" y2="121.92" width="0.508" layer="90"/>
<wire x1="172.72" y1="116.84" x2="185.42" y2="116.84" width="0.508" layer="90"/>
<wire x1="172.72" y1="121.92" x2="172.72" y2="127" width="0.508" layer="90"/>
<wire x1="172.72" y1="127" x2="165.1" y2="119.38" width="0.508" layer="90"/>
<wire x1="165.1" y1="119.38" x2="172.72" y2="111.76" width="0.508" layer="90"/>
<wire x1="172.72" y1="111.76" x2="172.72" y2="116.84" width="0.508" layer="90"/>
<wire x1="106.68" y1="76.2" x2="106.68" y2="45.72" width="0.508" layer="94"/>
<wire x1="106.68" y1="45.72" x2="165.1" y2="45.72" width="0.508" layer="94"/>
<wire x1="165.1" y1="76.2" x2="106.68" y2="76.2" width="0.508" layer="94"/>
<text x="114.3" y="55.88" size="3.81" layer="94">Debuging Interface,
Battery, LDO, TPS</text>
<text x="109.22" y="48.26" size="1.778" layer="90">Sheet: 2</text>
<wire x1="165.1" y1="76.2" x2="165.1" y2="45.72" width="0.508" layer="94"/>
<wire x1="137.16" y1="83.82" x2="142.24" y2="83.82" width="0.508" layer="90"/>
<wire x1="142.24" y1="83.82" x2="134.62" y2="76.2" width="0.508" layer="90"/>
<wire x1="134.62" y1="76.2" x2="127" y2="83.82" width="0.508" layer="90"/>
<wire x1="127" y1="83.82" x2="132.08" y2="83.82" width="0.508" layer="90"/>
<wire x1="137.16" y1="96.52" x2="137.16" y2="83.82" width="0.508" layer="90"/>
<wire x1="132.08" y1="96.52" x2="132.08" y2="83.82" width="0.508" layer="90"/>
<wire x1="137.16" y1="96.52" x2="142.24" y2="96.52" width="0.508" layer="90"/>
<wire x1="142.24" y1="96.52" x2="134.62" y2="104.14" width="0.508" layer="90"/>
<wire x1="134.62" y1="104.14" x2="127" y2="96.52" width="0.508" layer="90"/>
<wire x1="127" y1="96.52" x2="132.08" y2="96.52" width="0.508" layer="90"/>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="176.53" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="146.05" y="5.08" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="238.125" y="5.08" size="2.54" layer="94" font="vector"/>
<attribute name="AUTHOR" x="160.02" y="10.16" size="2.54" layer="94"/>
<attribute name="REV" x="203.2" y="5.08" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
<sheet>
<description>Microcontroller, Debugging interface</description>
<plain>
<text x="86.36" y="152.4" size="3.81" layer="90">MSP430 MICROCONTROLLER</text>
<text x="48.26" y="165.1" size="6.4516" layer="90">Data Storage Tag: Project Schematics (1/2)</text>
<text x="53.34" y="149.86" size="1.778" layer="97">Note: Add a DNI 10 nF
 pulldown capaictor 
on RST that can be u
se after finish programming.</text>
</plain>
<instances>
<instance part="R2" gate="G$1" x="40.64" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="45.466" y="147.5994" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="45.974" y="145.796" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R1" gate="G$1" x="35.56" y="144.78" smashed="yes" rot="R90">
<attribute name="NAME" x="34.29" y="145.8214" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="33.528" y="143.256" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="VCC1" gate="G$1" x="35.56" y="154.94" smashed="yes">
<attribute name="VALUE" x="39.116" y="159.766" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R3" gate="G$1" x="45.72" y="119.38" smashed="yes">
<attribute name="NAME" x="41.91" y="120.8786" size="1.778" layer="95"/>
<attribute name="VALUE" x="41.91" y="113.538" size="1.778" layer="96"/>
</instance>
<instance part="LED1" gate="G$1" x="15.24" y="114.3" smashed="yes">
<attribute name="NAME" x="18.796" y="109.728" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="20.955" y="109.728" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND1" gate="1" x="15.24" y="104.14" smashed="yes">
<attribute name="VALUE" x="12.7" y="101.6" size="1.778" layer="96"/>
</instance>
<instance part="GND2" gate="G$1" x="15.24" y="88.9" smashed="yes">
<attribute name="VALUE" x="12.954" y="84.074" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="G$1" x="71.12" y="83.82" smashed="yes">
<attribute name="VALUE" x="68.326" y="78.994" size="1.778" layer="96"/>
</instance>
<instance part="FRAME2" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="158.75" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="146.05" y="5.08" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="238.125" y="5.08" size="2.54" layer="94" font="vector"/>
<attribute name="AUTHOR" x="160.02" y="10.16" size="2.54" layer="94"/>
<attribute name="REV" x="203.2" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="U1" gate="G$1" x="116.84" y="139.7" smashed="yes">
<attribute name="NAME" x="80.01" y="147.32" size="2.54" layer="95" align="center-left"/>
<attribute name="VALUE" x="82.55" y="73.66" size="2.54" layer="96" align="center-left"/>
</instance>
<instance part="Y1" gate="Y$1" x="46.482" y="93.98" smashed="yes" rot="MR270">
<attribute name="NAME" x="48.26" y="98.552" size="1.778" layer="95" rot="MR180"/>
<attribute name="VALUE" x="50.8" y="92.202" size="1.778" layer="96" rot="MR180"/>
</instance>
<instance part="C1" gate="C$1" x="33.02" y="101.6" smashed="yes" rot="R90">
<attribute name="NAME" x="35.56" y="106.299" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="38.354" y="98.679" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C2" gate="C$1" x="35.56" y="88.9" smashed="yes" rot="R270">
<attribute name="NAME" x="33.02" y="91.821" size="1.778" layer="95"/>
<attribute name="VALUE" x="32.766" y="84.201" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="C$1" x="175.26" y="76.2" smashed="yes" rot="R180">
<attribute name="NAME" x="171.45" y="78.613" size="1.778" layer="95"/>
<attribute name="VALUE" x="167.132" y="74.803" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="C$1" x="185.42" y="76.2" smashed="yes" rot="R180">
<attribute name="NAME" x="186.436" y="78.613" size="1.778" layer="95"/>
<attribute name="VALUE" x="186.182" y="73.787" size="1.778" layer="96"/>
</instance>
<instance part="C6" gate="C$1" x="205.74" y="76.2" smashed="yes" rot="R180">
<attribute name="NAME" x="207.264" y="78.867" size="1.778" layer="95"/>
<attribute name="VALUE" x="207.264" y="74.549" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="C$1" x="195.58" y="78.74" smashed="yes">
<attribute name="NAME" x="196.596" y="78.613" size="1.778" layer="95"/>
<attribute name="VALUE" x="195.834" y="74.549" size="1.778" layer="96"/>
</instance>
<instance part="VCC3" gate="G$1" x="205.74" y="93.98" smashed="yes">
<attribute name="VALUE" x="210.82" y="99.06" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND6" gate="1" x="205.74" y="66.04" smashed="yes">
<attribute name="VALUE" x="203.2" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="180.34" y="68.58" smashed="yes">
<attribute name="VALUE" x="177.8" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="VCC2" gate="G$1" x="180.34" y="93.98" smashed="yes">
<attribute name="VALUE" x="185.42" y="99.06" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND5" gate="1" x="195.58" y="66.04" smashed="yes">
<attribute name="VALUE" x="193.04" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="TP7" gate="G$1" x="203.2" y="139.7" smashed="yes" rot="R270">
<attribute name="NAME" x="200.66" y="141.224" size="1.27" layer="95"/>
</instance>
<instance part="R4" gate="G$1" x="53.34" y="116.84" smashed="yes">
<attribute name="NAME" x="49.53" y="118.3386" size="1.778" layer="95"/>
<attribute name="VALUE" x="49.53" y="113.538" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="60.96" y="111.76" smashed="yes">
<attribute name="NAME" x="57.15" y="113.2586" size="1.778" layer="95"/>
<attribute name="VALUE" x="57.15" y="108.458" size="1.778" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="68.58" y="106.68" smashed="yes">
<attribute name="NAME" x="64.77" y="108.1786" size="1.778" layer="95"/>
<attribute name="VALUE" x="64.77" y="103.378" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="C"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="15.24" y1="109.22" x2="15.24" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="DVSS"/>
<wire x1="76.2" y1="93.98" x2="71.12" y2="93.98" width="0.1524" layer="91"/>
<pinref part="GND3" gate="G$1" pin="GND"/>
<wire x1="71.12" y1="93.98" x2="71.12" y2="91.44" width="0.1524" layer="91"/>
<wire x1="71.12" y1="91.44" x2="71.12" y2="86.36" width="0.1524" layer="91"/>
<wire x1="71.12" y1="86.36" x2="71.12" y2="83.82" width="0.1524" layer="91"/>
<junction x="71.12" y="91.44"/>
<pinref part="U1" gate="G$1" pin="EP"/>
<pinref part="U1" gate="G$1" pin="AVSS"/>
<wire x1="76.2" y1="91.44" x2="71.12" y2="91.44" width="0.1524" layer="91"/>
<wire x1="76.2" y1="86.36" x2="71.12" y2="86.36" width="0.1524" layer="91"/>
<junction x="71.12" y="86.36"/>
</segment>
<segment>
<pinref part="C6" gate="C$1" pin="1"/>
<wire x1="205.74" y1="73.66" x2="205.74" y2="68.58" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C4" gate="C$1" pin="1"/>
<wire x1="185.42" y1="73.66" x2="185.42" y2="71.12" width="0.1524" layer="91"/>
<wire x1="185.42" y1="71.12" x2="180.34" y2="71.12" width="0.1524" layer="91"/>
<pinref part="C3" gate="C$1" pin="1"/>
<wire x1="180.34" y1="71.12" x2="175.26" y2="71.12" width="0.1524" layer="91"/>
<wire x1="175.26" y1="71.12" x2="175.26" y2="73.66" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<junction x="180.34" y="71.12"/>
</segment>
<segment>
<pinref part="C5" gate="C$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="195.58" y1="73.66" x2="195.58" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="C$1" pin="1"/>
<wire x1="30.48" y1="101.6" x2="27.94" y2="101.6" width="0.1524" layer="91"/>
<wire x1="27.94" y1="101.6" x2="27.94" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C2" gate="C$1" pin="2"/>
<wire x1="27.94" y1="93.98" x2="27.94" y2="88.9" width="0.1524" layer="91"/>
<wire x1="27.94" y1="88.9" x2="30.48" y2="88.9" width="0.1524" layer="91"/>
<pinref part="GND2" gate="G$1" pin="GND"/>
<wire x1="15.24" y1="88.9" x2="15.24" y2="93.98" width="0.1524" layer="91"/>
<wire x1="15.24" y1="93.98" x2="27.94" y2="93.98" width="0.1524" layer="91"/>
<junction x="27.94" y="93.98"/>
</segment>
</net>
<net name="+2V_LDO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VCORE"/>
<wire x1="162.56" y1="86.36" x2="205.74" y2="86.36" width="0.1524" layer="91"/>
<pinref part="VCC3" gate="G$1" pin="VCC"/>
<wire x1="205.74" y1="86.36" x2="205.74" y2="93.98" width="0.1524" layer="91"/>
<pinref part="C6" gate="C$1" pin="2"/>
<wire x1="205.74" y1="86.36" x2="205.74" y2="81.28" width="0.1524" layer="91"/>
<junction x="205.74" y="86.36"/>
</segment>
<segment>
<pinref part="C3" gate="C$1" pin="2"/>
<wire x1="175.26" y1="81.28" x2="175.26" y2="83.82" width="0.1524" layer="91"/>
<pinref part="C4" gate="C$1" pin="2"/>
<wire x1="175.26" y1="83.82" x2="180.34" y2="83.82" width="0.1524" layer="91"/>
<wire x1="180.34" y1="83.82" x2="185.42" y2="83.82" width="0.1524" layer="91"/>
<wire x1="185.42" y1="83.82" x2="185.42" y2="81.28" width="0.1524" layer="91"/>
<pinref part="VCC2" gate="G$1" pin="VCC"/>
<junction x="180.34" y="83.82"/>
<wire x1="180.34" y1="93.98" x2="180.34" y2="88.9" width="0.1524" layer="91"/>
<wire x1="180.34" y1="88.9" x2="180.34" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="DVCC"/>
<wire x1="162.56" y1="88.9" x2="180.34" y2="88.9" width="0.1524" layer="91"/>
<junction x="180.34" y="88.9"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="VCC1" gate="G$1" pin="VCC"/>
<wire x1="35.56" y1="149.86" x2="35.56" y2="152.4" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="35.56" y1="152.4" x2="35.56" y2="154.94" width="0.1524" layer="91"/>
<wire x1="40.64" y1="149.86" x2="40.64" y2="152.4" width="0.1524" layer="91"/>
<wire x1="40.64" y1="152.4" x2="35.56" y2="152.4" width="0.1524" layer="91"/>
<junction x="35.56" y="152.4"/>
</segment>
</net>
<net name="SBWTDIO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="RST/NMI/SBWTDIO"/>
<wire x1="76.2" y1="139.7" x2="68.58" y2="139.7" width="0.1524" layer="91"/>
<label x="68.58" y="139.7" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="P1.6/UCB0SIMO/UCB0SDA/TA0.0"/>
<wire x1="76.2" y1="129.54" x2="40.64" y2="129.54" width="0.1524" layer="91"/>
<label x="55.88" y="129.54" size="1.778" layer="95"/>
<label x="30.48" y="129.54" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="40.64" y1="129.54" x2="30.48" y2="129.54" width="0.1524" layer="91"/>
<wire x1="40.64" y1="139.7" x2="40.64" y2="129.54" width="0.1524" layer="91"/>
<junction x="40.64" y="129.54"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="P1.7/UCB0SOMI/UCB0SCL/TA1.0"/>
<wire x1="76.2" y1="124.46" x2="35.56" y2="124.46" width="0.1524" layer="91"/>
<label x="55.88" y="124.46" size="1.778" layer="95"/>
<label x="30.48" y="124.46" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="35.56" y1="124.46" x2="30.48" y2="124.46" width="0.1524" layer="91"/>
<wire x1="35.56" y1="139.7" x2="35.56" y2="124.46" width="0.1524" layer="91"/>
<junction x="35.56" y="124.46"/>
</segment>
</net>
<net name="SPI_SIMO" class="0">
<segment>
<label x="45.72" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="45.72" y1="111.76" x2="55.88" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPI_SOMI" class="0">
<segment>
<wire x1="48.26" y1="116.84" x2="38.1" y2="116.84" width="0.1524" layer="91"/>
<label x="38.1" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SPI_CLK" class="0">
<segment>
<label x="53.34" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="53.34" y1="106.68" x2="63.5" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SBWTCK" class="0">
<segment>
<label x="68.58" y="134.62" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="G$1" pin="TEST/SBWTCK"/>
<wire x1="76.2" y1="134.62" x2="68.58" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED1" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="A"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="15.24" y1="116.84" x2="15.24" y2="119.38" width="0.1524" layer="91"/>
<wire x1="15.24" y1="119.38" x2="40.64" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="Y1" gate="Y$1" pin="2"/>
<pinref part="C2" gate="C$1" pin="1"/>
<wire x1="46.482" y1="88.9" x2="38.1" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PJ.5/XOUT"/>
<wire x1="76.2" y1="99.06" x2="66.04" y2="99.06" width="0.1524" layer="91"/>
<wire x1="66.04" y1="99.06" x2="66.04" y2="88.9" width="0.1524" layer="91"/>
<junction x="46.482" y="88.9"/>
<wire x1="66.04" y1="88.9" x2="46.482" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="Y1" gate="Y$1" pin="1"/>
<pinref part="C1" gate="C$1" pin="2"/>
<wire x1="46.482" y1="101.6" x2="46.482" y2="99.06" width="0.1524" layer="91"/>
<wire x1="38.1" y1="101.6" x2="46.482" y2="101.6" width="0.1524" layer="91"/>
<junction x="46.482" y="101.6"/>
<pinref part="U1" gate="G$1" pin="PJ.4/XIN"/>
<wire x1="76.2" y1="104.14" x2="66.04" y2="104.14" width="0.1524" layer="91"/>
<wire x1="66.04" y1="101.6" x2="46.482" y2="101.6" width="0.1524" layer="91"/>
<wire x1="66.04" y1="104.14" x2="66.04" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MAG-ACC-INT1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="P1.2/TA1.1/TA0CLK/CDOUT/A2*/CD2"/>
<wire x1="162.56" y1="134.62" x2="170.18" y2="134.62" width="0.1524" layer="91"/>
<label x="170.18" y="134.62" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="AVCC"/>
<pinref part="C5" gate="C$1" pin="1"/>
<wire x1="162.56" y1="91.44" x2="195.58" y2="91.44" width="0.1524" layer="91"/>
<wire x1="195.58" y1="91.44" x2="195.58" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FLASH_!RST!" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="P2.2/UCB0CLK"/>
<wire x1="162.56" y1="127" x2="170.18" y2="127" width="0.1524" layer="91"/>
<label x="170.18" y="127" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="NFC_!RST!" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="P1.0/TA0.1/DMAE0/RTCCLK/A0*/CD0/VEREF-*"/>
<wire x1="162.56" y1="139.7" x2="170.18" y2="139.7" width="0.1524" layer="91"/>
<label x="170.18" y="139.7" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ACOUSTIC_TRANS" class="0">
<segment>
<label x="215.9" y="132.08" size="1.27" layer="95" xref="yes"/>
<pinref part="U1" gate="G$1" pin="P1.3/TA1.2/UCB0STE/A3*/CD3"/>
<wire x1="162.56" y1="132.08" x2="203.2" y2="132.08" width="0.1524" layer="91"/>
<pinref part="TP7" gate="G$1" pin="TP"/>
<wire x1="203.2" y1="132.08" x2="215.9" y2="132.08" width="0.1524" layer="91"/>
<wire x1="203.2" y1="134.62" x2="203.2" y2="132.08" width="0.1524" layer="91"/>
<junction x="203.2" y="132.08"/>
</segment>
</net>
<net name="NFC_INT" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="P1.1/TA0.2/TA1CLK/CDOUT/A1*/CD1/VEREF+*"/>
<wire x1="162.56" y1="137.16" x2="170.18" y2="137.16" width="0.1524" layer="91"/>
<label x="170.18" y="137.16" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TO_LED1" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="P1.4/TB0.1/UCA0STE/A4*/CD4"/>
<wire x1="50.8" y1="119.38" x2="76.2" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TO_LED2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PJ.0/TDO/TB0OUTH/SMCLK/CD6"/>
<wire x1="162.56" y1="121.92" x2="170.18" y2="121.92" width="0.1524" layer="91"/>
<label x="170.18" y="121.92" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TO_LED3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PJ.1/TDI/TCLK/MCLK/CD7"/>
<wire x1="162.56" y1="114.3" x2="170.18" y2="114.3" width="0.1524" layer="91"/>
<label x="170.18" y="114.3" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SPI_!CS!1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PJ.2/TMS/ACLK/CD8"/>
<wire x1="162.56" y1="109.22" x2="170.18" y2="109.22" width="0.1524" layer="91"/>
<label x="170.18" y="109.22" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SPI_!CS!2" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PJ.3/TCK/CD9"/>
<wire x1="162.56" y1="104.14" x2="170.18" y2="104.14" width="0.1524" layer="91"/>
<label x="170.18" y="104.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="P1.5/TB0.2/UCA0CLK/A5*/CD5"/>
<wire x1="73.66" y1="106.68" x2="73.66" y2="111.76" width="0.1524" layer="91"/>
<wire x1="73.66" y1="111.76" x2="76.2" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="P2.0/UCA0TXD/UCA0SIMO/TB0CLK/ACLK"/>
<wire x1="66.04" y1="111.76" x2="66.04" y2="114.3" width="0.1524" layer="91"/>
<wire x1="66.04" y1="114.3" x2="76.2" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="U1" gate="G$1" pin="P2.1/UCA0RXD/UCA0SOMI/TB0.0"/>
<wire x1="58.42" y1="116.84" x2="76.2" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>RTC, NFC, Temp, Pressure, Magnometer, Flash</description>
<plain>
<text x="17.78" y="73.66" size="3.81" layer="90">PRESSURE SENSOR</text>
<text x="162.56" y="73.66" size="3.81" layer="90">TEMPERATURE SENSOR</text>
<text x="170.18" y="162.56" size="3.81" layer="90">FLASH MEMORY</text>
<text x="10.16" y="30.48" size="1.778" layer="97">I2C Address = 1110110</text>
<text x="66.04" y="88.9" size="1.778" layer="97">I2C Address = 00011101</text>
<text x="7.62" y="22.86" size="2.54" layer="90">Silkscreen</text>
<text x="5.08" y="162.56" size="3.81" layer="90">ACC. &amp; MAGNETOMETER</text>
<text x="142.24" y="121.92" size="1.778" layer="97">Note about Flash_Reset</text>
<text x="78.74" y="157.48" size="1.778" layer="97">Note about interrupt pins</text>
</plain>
<instances>
<instance part="U3" gate="G$1" x="43.18" y="129.54" smashed="yes">
<attribute name="OC_NEWARK" x="43.18" y="129.54" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="43.18" y="129.54" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="43.18" y="129.54" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="22.606" y="153.162" size="2.54" layer="95"/>
<attribute name="VALUE" x="22.606" y="93.218" size="2.54" layer="95"/>
</instance>
<instance part="GND7" gate="1" x="5.842" y="129.794" smashed="yes">
<attribute name="VALUE" x="3.302" y="127.762" size="1.778" layer="96"/>
</instance>
<instance part="U2" gate="G$1" x="35.56" y="50.8" smashed="yes">
<attribute name="OC_NEWARK" x="35.56" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="35.56" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="35.56" y="50.8" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="25.4" y="63.5" size="2.54" layer="95"/>
<attribute name="VALUE" x="27.94" y="35.56" size="2.54" layer="95"/>
</instance>
<instance part="GND9" gate="1" x="15.24" y="43.18" smashed="yes">
<attribute name="VALUE" x="12.7" y="40.64" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="C$1" x="5.842" y="137.414" smashed="yes">
<attribute name="NAME" x="7.366" y="137.795" size="1.778" layer="95"/>
<attribute name="VALUE" x="7.366" y="132.715" size="1.778" layer="96"/>
</instance>
<instance part="C8" gate="C$1" x="15.24" y="137.16" smashed="yes">
<attribute name="NAME" x="16.764" y="137.541" size="1.778" layer="95"/>
<attribute name="VALUE" x="16.764" y="132.461" size="1.778" layer="96"/>
</instance>
<instance part="C9" gate="C$1" x="15.24" y="53.34" smashed="yes">
<attribute name="NAME" x="16.764" y="53.721" size="1.778" layer="95"/>
<attribute name="VALUE" x="16.764" y="48.641" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="G$1" x="78.74" y="106.68" smashed="yes">
<attribute name="VALUE" x="76.2" y="101.6" size="1.778" layer="96"/>
</instance>
<instance part="VCC5" gate="G$1" x="15.24" y="60.96" smashed="yes">
<attribute name="VALUE" x="17.78" y="66.04" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="VCC4" gate="G$1" x="10.922" y="152.908" smashed="yes">
<attribute name="VALUE" x="14.732" y="158.242" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="U4" gate="G$1" x="190.5" y="142.24" smashed="yes">
<attribute name="NAME" x="177.8" y="154.94" size="2.54" layer="95"/>
<attribute name="VALUE" x="175.26" y="111.76" size="2.54" layer="96"/>
</instance>
<instance part="GND8" gate="G$1" x="15.24" y="132.334" smashed="yes">
<attribute name="VALUE" x="12.7" y="127.762" size="1.778" layer="96"/>
</instance>
<instance part="VCC9" gate="G$1" x="215.9" y="152.4" smashed="yes">
<attribute name="VALUE" x="218.44" y="157.48" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C11" gate="C$1" x="215.9" y="140.208" smashed="yes">
<attribute name="NAME" x="217.424" y="140.589" size="1.778" layer="95"/>
<attribute name="VALUE" x="217.424" y="135.509" size="1.778" layer="96"/>
</instance>
<instance part="GND14" gate="1" x="215.9" y="127" smashed="yes">
<attribute name="VALUE" x="213.36" y="124.46" size="1.778" layer="96"/>
</instance>
<instance part="FRAME3" gate="G$1" x="-2.54" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="156.21" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="143.51" y="5.08" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="235.585" y="5.08" size="2.54" layer="94" font="vector"/>
<attribute name="AUTHOR" x="157.48" y="10.16" size="2.54" layer="94"/>
<attribute name="REV" x="200.66" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="LOGO1" gate="G$1" x="7.62" y="10.16" smashed="yes"/>
<instance part="LOGO2" gate="G$1" x="20.32" y="10.16" smashed="yes"/>
<instance part="U6" gate="G$1" x="203.2" y="50.8" smashed="yes">
<attribute name="NAME" x="190.8556" y="67.5386" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="190.2206" y="26.8986" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="VCC8" gate="G$1" x="160.02" y="63.5" smashed="yes">
<attribute name="VALUE" x="165.1" y="68.58" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND15" gate="G$1" x="223.52" y="38.1" smashed="yes">
<attribute name="VALUE" x="218.948" y="29.718" size="1.778" layer="96"/>
</instance>
<instance part="C10" gate="C$1" x="160.02" y="49.784" smashed="yes" rot="R180">
<attribute name="NAME" x="158.496" y="49.403" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="158.496" y="54.483" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND12" gate="G$1" x="160.02" y="40.64" smashed="yes">
<attribute name="VALUE" x="155.448" y="32.258" size="1.778" layer="96"/>
</instance>
<instance part="XS1" gate="G$1" x="104.14" y="50.8" smashed="yes" rot="MR0"/>
<instance part="VCC6" gate="G$1" x="93.98" y="63.5" smashed="yes">
<attribute name="VALUE" x="96.52" y="68.58" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND11" gate="1" x="93.98" y="40.64" smashed="yes">
<attribute name="VALUE" x="91.44" y="38.1" size="1.778" layer="96"/>
</instance>
<instance part="U5" gate="G$1" x="200.66" y="91.44" smashed="yes">
<attribute name="NAME" x="190.5" y="106.68" size="1.778" layer="95"/>
<attribute name="VALUE" x="187.96" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="VCC10" gate="G$1" x="215.9" y="106.68" smashed="yes">
<attribute name="VALUE" x="218.44" y="111.76" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND13" gate="1" x="170.18" y="81.28" smashed="yes">
<attribute name="VALUE" x="167.64" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="C12" gate="C$1" x="238.76" y="94.488" smashed="yes">
<attribute name="NAME" x="240.284" y="94.869" size="1.778" layer="95"/>
<attribute name="VALUE" x="240.284" y="89.789" size="1.778" layer="96"/>
</instance>
<instance part="GND16" gate="1" x="238.76" y="81.28" smashed="yes">
<attribute name="VALUE" x="236.22" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="VCC7" gate="G$1" x="144.78" y="93.98" smashed="yes">
<attribute name="VALUE" x="147.32" y="99.06" size="1.778" layer="96" rot="R180"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="C9" gate="C$1" pin="2"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="15.24" y1="45.72" x2="15.24" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="22.86" y1="50.8" x2="22.86" y2="48.26" width="0.1524" layer="91"/>
<wire x1="22.86" y1="48.26" x2="15.24" y2="48.26" width="0.1524" layer="91"/>
<junction x="15.24" y="48.26"/>
</segment>
<segment>
<pinref part="C8" gate="C$1" pin="2"/>
<pinref part="GND8" gate="G$1" pin="GND"/>
<wire x1="15.24" y1="132.08" x2="15.24" y2="132.334" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C7" gate="C$1" pin="2"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C11" gate="C$1" pin="2"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="215.9" y1="129.54" x2="215.9" y2="135.128" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="TP"/>
<wire x1="208.28" y1="129.54" x2="210.82" y2="129.54" width="0.1524" layer="91"/>
<junction x="215.9" y="129.54"/>
<pinref part="U4" gate="G$1" pin="GND"/>
<wire x1="210.82" y1="129.54" x2="215.9" y2="129.54" width="0.1524" layer="91"/>
<wire x1="208.28" y1="134.62" x2="210.82" y2="134.62" width="0.1524" layer="91"/>
<wire x1="210.82" y1="134.62" x2="210.82" y2="129.54" width="0.1524" layer="91"/>
<junction x="210.82" y="129.54"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="ADDR"/>
<pinref part="GND10" gate="G$1" pin="GND"/>
<wire x1="71.12" y1="121.92" x2="78.74" y2="121.92" width="0.1524" layer="91"/>
<wire x1="78.74" y1="121.92" x2="78.74" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="GND_1"/>
<wire x1="78.74" y1="114.3" x2="78.74" y2="111.76" width="0.1524" layer="91"/>
<wire x1="78.74" y1="111.76" x2="78.74" y2="109.22" width="0.1524" layer="91"/>
<wire x1="78.74" y1="109.22" x2="78.74" y2="106.68" width="0.1524" layer="91"/>
<wire x1="71.12" y1="114.3" x2="78.74" y2="114.3" width="0.1524" layer="91"/>
<junction x="78.74" y="114.3"/>
<pinref part="U3" gate="G$1" pin="GND_2"/>
<wire x1="71.12" y1="111.76" x2="78.74" y2="111.76" width="0.1524" layer="91"/>
<junction x="78.74" y="111.76"/>
<pinref part="U3" gate="G$1" pin="GND_3"/>
<wire x1="71.12" y1="109.22" x2="78.74" y2="109.22" width="0.1524" layer="91"/>
<junction x="78.74" y="109.22"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="PAD"/>
<wire x1="220.98" y1="43.18" x2="223.52" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="GND"/>
<wire x1="223.52" y1="43.18" x2="223.52" y2="48.26" width="0.1524" layer="91"/>
<wire x1="223.52" y1="48.26" x2="220.98" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="ADD0"/>
<wire x1="220.98" y1="53.34" x2="223.52" y2="53.34" width="0.1524" layer="91"/>
<wire x1="223.52" y1="53.34" x2="223.52" y2="48.26" width="0.1524" layer="91"/>
<junction x="223.52" y="48.26"/>
<pinref part="GND15" gate="G$1" pin="GND"/>
<wire x1="223.52" y1="38.1" x2="223.52" y2="43.18" width="0.1524" layer="91"/>
<junction x="223.52" y="43.18"/>
</segment>
<segment>
<pinref part="GND12" gate="G$1" pin="GND"/>
<pinref part="C10" gate="C$1" pin="1"/>
<wire x1="160.02" y1="40.64" x2="160.02" y2="47.244" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="XS1" gate="G$1" pin="P4"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="99.06" y1="45.72" x2="93.98" y2="45.72" width="0.1524" layer="91"/>
<wire x1="93.98" y1="45.72" x2="93.98" y2="43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VSS"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="177.8" y1="86.36" x2="170.18" y2="86.36" width="0.1524" layer="91"/>
<wire x1="170.18" y1="86.36" x2="170.18" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND16" gate="1" pin="GND"/>
<pinref part="C12" gate="C$1" pin="2"/>
<wire x1="238.76" y1="83.82" x2="238.76" y2="89.408" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+2V_LDO" class="0">
<segment>
<wire x1="15.24" y1="55.88" x2="15.24" y2="60.96" width="0.1524" layer="91"/>
<pinref part="C9" gate="C$1" pin="1"/>
<pinref part="VCC5" gate="G$1" pin="VCC"/>
<pinref part="U2" gate="G$1" pin="VDD"/>
<wire x1="22.86" y1="55.88" x2="15.24" y2="55.88" width="0.1524" layer="91"/>
<junction x="15.24" y="55.88"/>
</segment>
<segment>
<wire x1="215.9" y1="142.748" x2="215.9" y2="142.24" width="0.1524" layer="91"/>
<pinref part="C11" gate="C$1" pin="1"/>
<pinref part="U4" gate="G$1" pin="VCC"/>
<wire x1="215.9" y1="142.748" x2="215.9" y2="149.86" width="0.1524" layer="91"/>
<wire x1="208.28" y1="149.86" x2="210.82" y2="149.86" width="0.1524" layer="91"/>
<pinref part="VCC9" gate="G$1" pin="VCC"/>
<wire x1="210.82" y1="149.86" x2="215.9" y2="149.86" width="0.1524" layer="91"/>
<wire x1="215.9" y1="149.86" x2="215.9" y2="152.4" width="0.1524" layer="91"/>
<junction x="215.9" y="149.86"/>
<pinref part="U4" gate="G$1" pin="!WP"/>
<wire x1="208.28" y1="142.24" x2="210.82" y2="142.24" width="0.1524" layer="91"/>
<wire x1="210.82" y1="142.24" x2="210.82" y2="149.86" width="0.1524" layer="91"/>
<junction x="210.82" y="149.86"/>
</segment>
<segment>
<pinref part="C7" gate="C$1" pin="1"/>
<wire x1="5.842" y1="139.954" x2="5.842" y2="147.32" width="0.1524" layer="91"/>
<pinref part="C8" gate="C$1" pin="1"/>
<wire x1="15.24" y1="139.7" x2="15.24" y2="142.24" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VDD"/>
<wire x1="15.24" y1="142.24" x2="15.24" y2="147.32" width="0.1524" layer="91"/>
<wire x1="17.78" y1="147.32" x2="15.24" y2="147.32" width="0.1524" layer="91"/>
<junction x="15.24" y="147.32"/>
<wire x1="15.24" y1="147.32" x2="10.922" y2="147.32" width="0.1524" layer="91"/>
<wire x1="10.922" y1="147.32" x2="5.842" y2="147.32" width="0.1524" layer="91"/>
<pinref part="VCC4" gate="G$1" pin="VCC"/>
<wire x1="10.922" y1="152.908" x2="10.922" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="IO_VDD"/>
<wire x1="17.78" y1="142.24" x2="15.24" y2="142.24" width="0.1524" layer="91"/>
<junction x="15.24" y="142.24"/>
</segment>
<segment>
<pinref part="VCC8" gate="G$1" pin="VCC"/>
<wire x1="160.02" y1="58.42" x2="160.02" y2="63.5" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="V+"/>
<wire x1="185.42" y1="58.42" x2="160.02" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C10" gate="C$1" pin="2"/>
<wire x1="160.02" y1="54.864" x2="160.02" y2="58.42" width="0.1524" layer="91"/>
<junction x="160.02" y="58.42"/>
</segment>
<segment>
<pinref part="VCC6" gate="G$1" pin="VCC"/>
<wire x1="93.98" y1="63.5" x2="93.98" y2="53.34" width="0.1524" layer="91"/>
<pinref part="XS1" gate="G$1" pin="P1"/>
<wire x1="93.98" y1="53.34" x2="99.06" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="!WP"/>
<wire x1="177.8" y1="91.44" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
<wire x1="144.78" y1="91.44" x2="144.78" y2="93.98" width="0.1524" layer="91"/>
<pinref part="VCC7" gate="G$1" pin="VCC"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="VDD"/>
<pinref part="VCC10" gate="G$1" pin="VCC"/>
<wire x1="205.74" y1="101.6" x2="215.9" y2="101.6" width="0.1524" layer="91"/>
<wire x1="215.9" y1="101.6" x2="215.9" y2="106.68" width="0.1524" layer="91"/>
<pinref part="C12" gate="C$1" pin="1"/>
<wire x1="238.76" y1="97.028" x2="238.76" y2="101.6" width="0.1524" layer="91"/>
<wire x1="238.76" y1="101.6" x2="215.9" y2="101.6" width="0.1524" layer="91"/>
<junction x="215.9" y="101.6"/>
<pinref part="U5" gate="G$1" pin="DNU"/>
<wire x1="205.74" y1="96.52" x2="215.9" y2="96.52" width="0.1524" layer="91"/>
<wire x1="215.9" y1="96.52" x2="215.9" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SDA"/>
<wire x1="71.12" y1="132.08" x2="81.28" y2="132.08" width="0.1524" layer="91"/>
<label x="81.28" y="132.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SDA"/>
<wire x1="55.88" y1="55.88" x2="58.42" y2="55.88" width="0.1524" layer="91"/>
<label x="58.42" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="SDA"/>
<wire x1="185.42" y1="53.34" x2="180.34" y2="53.34" width="0.1524" layer="91"/>
<label x="180.34" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="XS1" gate="G$1" pin="P3"/>
<wire x1="99.06" y1="48.26" x2="93.98" y2="48.26" width="0.1524" layer="91"/>
<label x="93.98" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="SCL"/>
<wire x1="71.12" y1="127" x2="81.28" y2="127" width="0.1524" layer="91"/>
<label x="81.28" y="127" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="SCL"/>
<wire x1="55.88" y1="50.8" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<label x="58.42" y="50.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="SCL"/>
<wire x1="185.42" y1="48.26" x2="180.34" y2="48.26" width="0.1524" layer="91"/>
<label x="180.34" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="XS1" gate="G$1" pin="P2"/>
<wire x1="99.06" y1="50.8" x2="93.98" y2="50.8" width="0.1524" layer="91"/>
<label x="93.98" y="50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MAG-ACC-INT1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="GPIO1"/>
<wire x1="71.12" y1="142.24" x2="76.2" y2="142.24" width="0.1524" layer="91"/>
<label x="76.2" y="142.24" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="FLASH_!RST!" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="!RESET"/>
<wire x1="172.72" y1="129.54" x2="167.64" y2="129.54" width="0.1524" layer="91"/>
<label x="167.64" y="129.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_SIMO" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="SI"/>
<wire x1="172.72" y1="149.86" x2="167.64" y2="149.86" width="0.1524" layer="91"/>
<label x="167.64" y="149.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SI"/>
<wire x1="205.74" y1="86.36" x2="210.82" y2="86.36" width="0.1524" layer="91"/>
<label x="210.82" y="86.36" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SPI_SOMI" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="SO"/>
<wire x1="172.72" y1="144.78" x2="167.64" y2="144.78" width="0.1524" layer="91"/>
<label x="167.64" y="144.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SO"/>
<wire x1="177.8" y1="96.52" x2="170.18" y2="96.52" width="0.1524" layer="91"/>
<label x="170.18" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_CLK" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="SCK"/>
<wire x1="172.72" y1="139.7" x2="167.64" y2="139.7" width="0.1524" layer="91"/>
<label x="167.64" y="139.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="SCK"/>
<wire x1="205.74" y1="91.44" x2="210.82" y2="91.44" width="0.1524" layer="91"/>
<label x="210.82" y="91.44" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SPI_!CS!1" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="!CS"/>
<wire x1="172.72" y1="134.62" x2="167.64" y2="134.62" width="0.1524" layer="91"/>
<label x="167.64" y="134.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_!CS!2" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="!CS"/>
<wire x1="177.8" y1="101.6" x2="170.18" y2="101.6" width="0.1524" layer="91"/>
<label x="170.18" y="101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="111.76" y="86.36" size="1.778" layer="97">I2C Address = 01010000</text>
<text x="66.04" y="167.64" size="2.54" layer="90">NFC interface</text>
<text x="185.42" y="86.36" size="3.81" layer="90">Battery and LDO</text>
<text x="76.2" y="27.94" size="1.778" layer="97">Note: there are 
two connection terminals 
for the battery input. Second one
is on breakout board.

The extra diode is for reverse protection.</text>
<text x="101.6" y="76.2" size="3.81" layer="90">INPUT POWER SWITCH</text>
</plain>
<instances>
<instance part="FRAME4" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="158.75" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="146.05" y="5.08" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="238.125" y="5.08" size="2.54" layer="94" font="vector"/>
<attribute name="AUTHOR" x="160.02" y="10.16" size="2.54" layer="94"/>
<attribute name="REV" x="203.2" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="C15" gate="C$1" x="50.8" y="116.84" smashed="yes">
<attribute name="NAME" x="52.324" y="117.221" size="1.778" layer="95"/>
<attribute name="VALUE" x="52.324" y="112.141" size="1.778" layer="96"/>
</instance>
<instance part="C14" gate="C$1" x="40.64" y="116.84" smashed="yes">
<attribute name="NAME" x="42.164" y="117.221" size="1.778" layer="95"/>
<attribute name="VALUE" x="42.164" y="112.141" size="1.778" layer="96"/>
</instance>
<instance part="P+1" gate="VCC" x="30.48" y="124.968" smashed="yes">
<attribute name="VALUE" x="32.512" y="127" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C13" gate="C$1" x="30.48" y="114.3" smashed="yes" rot="R180">
<attribute name="NAME" x="28.956" y="113.919" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="28.956" y="118.999" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND17" gate="1" x="30.48" y="106.68" smashed="yes">
<attribute name="VALUE" x="27.94" y="104.14" size="1.778" layer="96"/>
</instance>
<instance part="GND20" gate="1" x="67.31" y="99.06" smashed="yes">
<attribute name="VALUE" x="64.77" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="C16" gate="C$1" x="132.08" y="143.51" smashed="yes">
<attribute name="NAME" x="133.604" y="143.891" size="1.778" layer="95"/>
<attribute name="VALUE" x="133.604" y="138.811" size="1.778" layer="96"/>
</instance>
<instance part="GND19" gate="1" x="60.96" y="99.06" smashed="yes">
<attribute name="VALUE" x="58.42" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="GND18" gate="G$1" x="45.72" y="109.22" smashed="yes">
<attribute name="VALUE" x="43.434" y="104.648" size="1.778" layer="96"/>
</instance>
<instance part="D3" gate="G$1" x="182.88" y="147.32" smashed="yes">
<attribute name="NAME" x="180.594" y="149.225" size="1.778" layer="95"/>
<attribute name="VALUE" x="180.594" y="143.891" size="1.778" layer="96"/>
</instance>
<instance part="D4" gate="G$1" x="182.88" y="137.16" smashed="yes">
<attribute name="NAME" x="180.594" y="139.065" size="1.778" layer="95"/>
<attribute name="VALUE" x="180.594" y="133.731" size="1.778" layer="96"/>
</instance>
<instance part="GND25" gate="1" x="208.28" y="132.08" smashed="yes">
<attribute name="VALUE" x="205.74" y="129.54" size="1.778" layer="96"/>
</instance>
<instance part="C20" gate="C$1" x="208.28" y="142.24" smashed="yes">
<attribute name="NAME" x="209.804" y="142.621" size="1.778" layer="95"/>
<attribute name="VALUE" x="209.804" y="137.541" size="1.778" layer="96"/>
</instance>
<instance part="U7" gate="A" x="93.98" y="129.54" smashed="yes">
<attribute name="NAME" x="79.0956" y="151.3586" size="2.0828" layer="95" ratio="6" rot="SR0"/>
<attribute name="VALUE" x="78.4606" y="85.3186" size="2.0828" layer="96" ratio="6" rot="SR0"/>
</instance>
<instance part="GND21" gate="1" x="119.38" y="101.346" smashed="yes">
<attribute name="VALUE" x="116.84" y="98.806" size="1.778" layer="96"/>
</instance>
<instance part="P+2" gate="VCC" x="45.72" y="130.048" smashed="yes">
<attribute name="VALUE" x="47.752" y="132.08" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C17" gate="C$1" x="144.78" y="143.51" smashed="yes">
<attribute name="NAME" x="146.304" y="143.891" size="1.778" layer="95"/>
<attribute name="VALUE" x="146.304" y="138.811" size="1.778" layer="96"/>
</instance>
<instance part="ANT" gate="G$1" x="170.18" y="114.3" smashed="yes" rot="R270">
<attribute name="NAME" x="169.672" y="111.76" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="179.578" y="111.76" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="P+4" gate="1" x="208.28" y="160.02" smashed="yes">
<attribute name="VALUE" x="213.36" y="165.1" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="TP1" gate="G$1" x="132.08" y="35.56" smashed="yes" rot="R90">
<attribute name="NAME" x="134.62" y="34.036" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="U8" gate="G$1" x="203.2" y="63.5" smashed="yes">
<attribute name="OC_NEWARK" x="203.2" y="63.5" size="1.778" layer="96" display="off"/>
<attribute name="MF" x="203.2" y="63.5" size="1.778" layer="96" display="off"/>
<attribute name="MPN" x="203.2" y="63.5" size="1.778" layer="96" display="off"/>
<attribute name="NAME" x="190.5" y="78.74" size="2.54" layer="95"/>
<attribute name="VALUE" x="185.42" y="38.1" size="2.54" layer="95"/>
</instance>
<instance part="C18" gate="C$1" x="154.94" y="60.96" smashed="yes">
<attribute name="NAME" x="156.464" y="61.341" size="1.778" layer="95"/>
<attribute name="VALUE" x="156.464" y="56.261" size="1.778" layer="96"/>
</instance>
<instance part="GND23" gate="1" x="154.94" y="48.26" smashed="yes">
<attribute name="VALUE" x="152.4" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="GND22" gate="1" x="127" y="30.48" smashed="yes">
<attribute name="VALUE" x="124.46" y="27.94" size="1.778" layer="96"/>
</instance>
<instance part="D1" gate="G$1" x="137.16" y="58.42" smashed="yes">
<attribute name="NAME" x="134.874" y="60.325" size="1.778" layer="95"/>
<attribute name="VALUE" x="132.334" y="54.991" size="1.778" layer="96"/>
</instance>
<instance part="D2" gate="G$1" x="137.16" y="48.26" smashed="yes">
<attribute name="NAME" x="134.874" y="50.165" size="1.778" layer="95"/>
<attribute name="VALUE" x="134.874" y="44.831" size="1.778" layer="96"/>
</instance>
<instance part="GND26" gate="1" x="233.68" y="48.26" smashed="yes">
<attribute name="VALUE" x="231.14" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="VCC11" gate="G$1" x="233.68" y="73.66" smashed="yes">
<attribute name="VALUE" x="238.76" y="78.74" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+3" gate="1" x="124.46" y="63.5" smashed="yes">
<attribute name="VALUE" x="121.92" y="66.04" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="BAT1" gate="G$1" x="127" y="40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="124.46" y="37.465" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="ANT1" gate="G$1" x="160.02" y="157.48" smashed="yes" rot="R270">
<attribute name="NAME" x="157.48" y="159.004" size="1.27" layer="95"/>
</instance>
<instance part="ANT2" gate="G$1" x="160.02" y="127" smashed="yes" rot="R90">
<attribute name="NAME" x="162.56" y="125.476" size="1.27" layer="95" rot="R180"/>
</instance>
<instance part="TP4" gate="G$1" x="180.34" y="78.74" smashed="yes" rot="R270">
<attribute name="NAME" x="177.8" y="80.264" size="1.27" layer="95"/>
</instance>
<instance part="R7" gate="G$1" x="180.34" y="60.96" smashed="yes" rot="R270">
<attribute name="NAME" x="181.8386" y="64.77" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="177.038" y="64.77" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C19" gate="C$1" x="180.34" y="45.72" smashed="yes">
<attribute name="NAME" x="181.864" y="46.101" size="1.778" layer="95"/>
<attribute name="VALUE" x="181.864" y="41.021" size="1.778" layer="96"/>
</instance>
<instance part="GND24" gate="1" x="180.34" y="33.02" smashed="yes">
<attribute name="VALUE" x="177.8" y="30.48" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="C13" gate="C$1" pin="1"/>
<wire x1="30.48" y1="109.22" x2="30.48" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C14" gate="C$1" pin="2"/>
<wire x1="40.64" y1="111.76" x2="45.72" y2="111.76" width="0.1524" layer="91"/>
<pinref part="C15" gate="C$1" pin="2"/>
<wire x1="45.72" y1="111.76" x2="50.8" y2="111.76" width="0.1524" layer="91"/>
<junction x="45.72" y="111.76"/>
<wire x1="45.72" y1="111.76" x2="45.72" y2="109.22" width="0.1524" layer="91"/>
<pinref part="GND18" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="GND25" gate="1" pin="GND"/>
<pinref part="C20" gate="C$1" pin="2"/>
<wire x1="208.28" y1="134.62" x2="208.28" y2="137.16" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="185.42" y1="137.16" x2="208.28" y2="137.16" width="0.1524" layer="91"/>
<junction x="208.28" y="137.16"/>
</segment>
<segment>
<pinref part="U7" gate="A" pin="E2(TDI)"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="73.66" y1="104.14" x2="67.31" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U7" gate="A" pin="E1(TDO)"/>
<wire x1="73.66" y1="106.68" x2="67.31" y2="106.68" width="0.1524" layer="91"/>
<junction x="67.31" y="101.6"/>
<pinref part="U7" gate="A" pin="E0(TMS)"/>
<wire x1="73.66" y1="109.22" x2="67.31" y2="109.22" width="0.1524" layer="91"/>
<wire x1="67.31" y1="109.22" x2="67.31" y2="106.68" width="0.1524" layer="91"/>
<junction x="67.31" y="104.14"/>
<wire x1="67.31" y1="106.68" x2="67.31" y2="104.14" width="0.1524" layer="91"/>
<wire x1="67.31" y1="104.14" x2="67.31" y2="101.6" width="0.1524" layer="91"/>
<junction x="67.31" y="106.68"/>
</segment>
<segment>
<pinref part="U7" gate="A" pin="!CS!/SCMS"/>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="73.66" y1="114.3" x2="60.96" y2="114.3" width="0.1524" layer="91"/>
<wire x1="60.96" y1="114.3" x2="60.96" y2="101.6" width="0.1524" layer="91"/>
<pinref part="U7" gate="A" pin="SCK"/>
<wire x1="73.66" y1="116.84" x2="60.96" y2="116.84" width="0.1524" layer="91"/>
<wire x1="60.96" y1="116.84" x2="60.96" y2="114.3" width="0.1524" layer="91"/>
<junction x="60.96" y="114.3"/>
</segment>
<segment>
<pinref part="U7" gate="A" pin="VSS"/>
<wire x1="114.3" y1="106.68" x2="119.38" y2="106.68" width="0.1524" layer="91"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="119.38" y1="106.68" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U7" gate="A" pin="EP"/>
<wire x1="119.38" y1="104.14" x2="119.38" y2="103.886" width="0.1524" layer="91"/>
<wire x1="114.3" y1="104.14" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
<junction x="119.38" y="104.14"/>
</segment>
<segment>
<pinref part="C18" gate="C$1" pin="2"/>
<pinref part="GND23" gate="1" pin="GND"/>
<wire x1="154.94" y1="55.88" x2="154.94" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="VSS"/>
<wire x1="226.06" y1="55.88" x2="233.68" y2="55.88" width="0.1524" layer="91"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="233.68" y1="55.88" x2="233.68" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="BAT1" gate="G$1" pin="-"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="127" y1="35.56" x2="127" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="C19" gate="C$1" pin="2"/>
<wire x1="180.34" y1="35.56" x2="180.34" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U7" gate="A" pin="SDA/SI"/>
<wire x1="73.66" y1="142.24" x2="63.5" y2="142.24" width="0.1524" layer="91"/>
<label x="63.5" y="142.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U7" gate="A" pin="SCL/SO"/>
<wire x1="73.66" y1="147.32" x2="63.5" y2="147.32" width="0.1524" layer="91"/>
<label x="63.5" y="147.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NFC_INT" class="0">
<segment>
<pinref part="U7" gate="A" pin="INTO"/>
<wire x1="114.3" y1="114.3" x2="119.38" y2="114.3" width="0.1524" layer="91"/>
<label x="119.38" y="114.3" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="NFC_!RST!" class="0">
<segment>
<pinref part="U7" gate="A" pin="!RST!"/>
<wire x1="114.3" y1="119.38" x2="119.38" y2="119.38" width="0.1524" layer="91"/>
<label x="119.38" y="119.38" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="+2V_LDO" class="0">
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<pinref part="C13" gate="C$1" pin="2"/>
<wire x1="30.48" y1="122.428" x2="30.48" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U7" gate="A" pin="VCORE"/>
<wire x1="73.66" y1="134.62" x2="33.02" y2="134.62" width="0.1524" layer="91"/>
<wire x1="33.02" y1="134.62" x2="33.02" y2="119.38" width="0.1524" layer="91"/>
<wire x1="33.02" y1="119.38" x2="30.48" y2="119.38" width="0.1524" layer="91"/>
<junction x="30.48" y="119.38"/>
</segment>
<segment>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<wire x1="45.72" y1="127.508" x2="45.72" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U7" gate="A" pin="VCC"/>
<wire x1="73.66" y1="121.92" x2="50.8" y2="121.92" width="0.1524" layer="91"/>
<pinref part="C14" gate="C$1" pin="1"/>
<wire x1="50.8" y1="121.92" x2="45.72" y2="121.92" width="0.1524" layer="91"/>
<wire x1="40.64" y1="119.38" x2="40.64" y2="121.92" width="0.1524" layer="91"/>
<wire x1="40.64" y1="121.92" x2="45.72" y2="121.92" width="0.1524" layer="91"/>
<junction x="45.72" y="121.92"/>
<pinref part="C15" gate="C$1" pin="1"/>
<wire x1="50.8" y1="119.38" x2="50.8" y2="121.92" width="0.1524" layer="91"/>
<junction x="50.8" y="121.92"/>
<junction x="40.64" y="121.92"/>
</segment>
<segment>
<pinref part="U8" gate="G$1" pin="VOUT"/>
<wire x1="226.06" y1="71.12" x2="233.68" y2="71.12" width="0.1524" layer="91"/>
<wire x1="233.68" y1="71.12" x2="233.68" y2="73.66" width="0.1524" layer="91"/>
<pinref part="VCC11" gate="G$1" pin="VCC"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="U7" gate="A" pin="ANT1"/>
<wire x1="114.3" y1="147.32" x2="132.08" y2="147.32" width="0.1524" layer="91"/>
<pinref part="C16" gate="C$1" pin="1"/>
<wire x1="132.08" y1="147.32" x2="132.08" y2="146.05" width="0.1524" layer="91"/>
<pinref part="C17" gate="C$1" pin="1"/>
<wire x1="132.08" y1="147.32" x2="144.78" y2="147.32" width="0.1524" layer="91"/>
<wire x1="144.78" y1="147.32" x2="144.78" y2="146.05" width="0.1524" layer="91"/>
<wire x1="144.78" y1="147.32" x2="160.02" y2="147.32" width="0.1524" layer="91"/>
<junction x="144.78" y="147.32"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="160.02" y1="147.32" x2="175.26" y2="147.32" width="0.1524" layer="91"/>
<wire x1="175.26" y1="147.32" x2="180.34" y2="147.32" width="0.1524" layer="91"/>
<pinref part="ANT" gate="G$1" pin="P$1"/>
<wire x1="175.26" y1="127" x2="175.26" y2="147.32" width="0.1524" layer="91"/>
<junction x="175.26" y="147.32"/>
<pinref part="ANT1" gate="G$1" pin="TP"/>
<wire x1="160.02" y1="152.4" x2="160.02" y2="147.32" width="0.1524" layer="91"/>
<junction x="160.02" y="147.32"/>
</segment>
</net>
<net name="LDO_VIN" class="0">
<segment>
<wire x1="149.86" y1="68.58" x2="149.86" y2="58.42" width="0.1524" layer="91"/>
<wire x1="149.86" y1="58.42" x2="149.86" y2="48.26" width="0.1524" layer="91"/>
<wire x1="149.86" y1="48.26" x2="139.7" y2="48.26" width="0.1524" layer="91"/>
<pinref part="C18" gate="C$1" pin="1"/>
<wire x1="154.94" y1="63.5" x2="154.94" y2="68.58" width="0.1524" layer="91"/>
<wire x1="149.86" y1="68.58" x2="154.94" y2="68.58" width="0.1524" layer="91"/>
<junction x="149.86" y="68.58"/>
<pinref part="D2" gate="G$1" pin="C"/>
<label x="160.02" y="60.96" size="1.778" layer="95"/>
<pinref part="U8" gate="G$1" pin="VIN"/>
<junction x="154.94" y="68.58"/>
<wire x1="154.94" y1="68.58" x2="180.34" y2="68.58" width="0.1524" layer="91"/>
<wire x1="180.34" y1="68.58" x2="182.88" y2="68.58" width="0.1524" layer="91"/>
<wire x1="137.16" y1="68.58" x2="149.86" y2="68.58" width="0.1524" layer="91"/>
<pinref part="TP4" gate="G$1" pin="TP"/>
<wire x1="180.34" y1="73.66" x2="180.34" y2="68.58" width="0.1524" layer="91"/>
<junction x="180.34" y="68.58"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="139.7" y1="58.42" x2="149.86" y2="58.42" width="0.1524" layer="91"/>
<junction x="149.86" y="58.42"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="180.34" y1="66.04" x2="180.34" y2="68.58" width="0.1524" layer="91"/>
<label x="137.16" y="68.58" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="RF_PWR" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="134.62" y1="58.42" x2="124.46" y2="58.42" width="0.1524" layer="91"/>
<wire x1="124.46" y1="58.42" x2="124.46" y2="60.96" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+12V"/>
</segment>
<segment>
<pinref part="D3" gate="G$1" pin="C"/>
<pinref part="C20" gate="C$1" pin="1"/>
<wire x1="185.42" y1="147.32" x2="208.28" y2="147.32" width="0.1524" layer="91"/>
<wire x1="208.28" y1="147.32" x2="208.28" y2="144.78" width="0.1524" layer="91"/>
<wire x1="208.28" y1="147.32" x2="208.28" y2="157.48" width="0.1524" layer="91"/>
<junction x="208.28" y="147.32"/>
<pinref part="P+4" gate="1" pin="+12V"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="BAT1" gate="G$1" pin="+"/>
<wire x1="127" y1="43.18" x2="127" y2="48.26" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="127" y1="48.26" x2="132.08" y2="48.26" width="0.1524" layer="91"/>
<pinref part="TP1" gate="G$1" pin="TP"/>
<wire x1="132.08" y1="48.26" x2="134.62" y2="48.26" width="0.1524" layer="91"/>
<wire x1="132.08" y1="40.64" x2="132.08" y2="48.26" width="0.1524" layer="91"/>
<junction x="132.08" y="48.26"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="172.72" y1="137.16" x2="180.34" y2="137.16" width="0.1524" layer="91"/>
<pinref part="ANT" gate="G$1" pin="P$2"/>
<wire x1="172.72" y1="127" x2="172.72" y2="137.16" width="0.1524" layer="91"/>
<junction x="172.72" y="137.16"/>
<pinref part="C17" gate="C$1" pin="2"/>
<wire x1="144.78" y1="137.16" x2="144.78" y2="138.43" width="0.1524" layer="91"/>
<wire x1="144.78" y1="137.16" x2="160.02" y2="137.16" width="0.1524" layer="91"/>
<junction x="144.78" y="137.16"/>
<pinref part="C16" gate="C$1" pin="2"/>
<pinref part="U7" gate="A" pin="ANT2"/>
<wire x1="160.02" y1="137.16" x2="172.72" y2="137.16" width="0.1524" layer="91"/>
<wire x1="114.3" y1="137.16" x2="132.08" y2="137.16" width="0.1524" layer="91"/>
<wire x1="132.08" y1="137.16" x2="132.08" y2="138.43" width="0.1524" layer="91"/>
<wire x1="132.08" y1="137.16" x2="144.78" y2="137.16" width="0.1524" layer="91"/>
<junction x="132.08" y="137.16"/>
<pinref part="ANT2" gate="G$1" pin="TP"/>
<wire x1="160.02" y1="132.08" x2="160.02" y2="137.16" width="0.1524" layer="91"/>
<junction x="160.02" y="137.16"/>
</segment>
</net>
<net name="LDO_CE" class="0">
<segment>
<pinref part="U8" gate="G$1" pin="CE"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="182.88" y1="55.88" x2="180.34" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C19" gate="C$1" pin="1"/>
<wire x1="180.34" y1="48.26" x2="180.34" y2="55.88" width="0.1524" layer="91"/>
<junction x="180.34" y="55.88"/>
<label x="177.8" y="55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="180.34" y1="55.88" x2="177.8" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="99.06" y="162.56" size="5.08" layer="90">BREAKOFF BOARD</text>
<text x="38.1" y="137.16" size="3.81" layer="90">DEBUG SPY-BI-WIRE</text>
</plain>
<instances>
<instance part="FRAME6" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="176.53" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="146.05" y="5.08" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="238.125" y="5.08" size="2.54" layer="94" font="vector"/>
<attribute name="AUTHOR" x="160.02" y="10.16" size="2.54" layer="94"/>
<attribute name="REV" x="203.2" y="5.08" size="2.54" layer="94"/>
</instance>
<instance part="J2" gate="G$1" x="193.04" y="154.94" smashed="yes">
<attribute name="NAME" x="190.5" y="162.56" size="1.778" layer="95"/>
<attribute name="VALUE" x="190.5" y="144.78" size="1.778" layer="96"/>
</instance>
<instance part="GND31" gate="1" x="205.74" y="149.606" smashed="yes">
<attribute name="VALUE" x="203.2" y="147.066" size="1.778" layer="96"/>
</instance>
<instance part="D5" gate="G$1" x="160.02" y="83.82" smashed="yes">
<attribute name="NAME" x="157.734" y="85.725" size="1.778" layer="95"/>
<attribute name="VALUE" x="152.654" y="77.851" size="1.778" layer="96"/>
</instance>
<instance part="S1" gate="1" x="147.32" y="81.28" smashed="yes" rot="R90">
<attribute name="VALUE" x="144.78" y="77.47" size="1.778" layer="96" rot="R180"/>
<attribute name="NAME" x="149.225" y="74.93" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="BAT2" gate="G$1" x="137.16" y="76.2" smashed="yes" rot="R90">
<attribute name="NAME" x="134.62" y="73.025" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="GND29" gate="1" x="137.16" y="65.786" smashed="yes">
<attribute name="VALUE" x="134.62" y="63.246" size="1.778" layer="96"/>
</instance>
<instance part="S2" gate="1" x="175.26" y="45.72" smashed="yes">
<attribute name="NAME" x="168.91" y="43.815" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="171.45" y="48.895" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND30" gate="1" x="175.26" y="33.02" smashed="yes">
<attribute name="VALUE" x="172.72" y="30.48" size="1.778" layer="96"/>
</instance>
<instance part="R12" gate="G$1" x="213.36" y="43.18" smashed="yes" rot="R270">
<attribute name="NAME" x="214.8586" y="44.45" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="210.058" y="44.45" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND33" gate="1" x="213.36" y="33.02" smashed="yes">
<attribute name="VALUE" x="210.82" y="30.48" size="1.778" layer="96"/>
</instance>
<instance part="LED3" gate="G$1" x="213.36" y="55.88" smashed="yes">
<attribute name="NAME" x="216.916" y="51.308" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="219.075" y="51.308" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R11" gate="G$1" x="175.26" y="101.6" smashed="yes" rot="R180">
<attribute name="NAME" x="173.99" y="105.1814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="179.07" y="104.902" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="LED2" gate="G$1" x="213.36" y="93.98" smashed="yes">
<attribute name="NAME" x="216.916" y="89.408" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="219.075" y="86.868" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND32" gate="1" x="213.36" y="83.82" smashed="yes">
<attribute name="VALUE" x="210.82" y="81.28" size="1.778" layer="96"/>
</instance>
<instance part="LED4" gate="G$1" x="223.52" y="93.98" smashed="yes">
<attribute name="NAME" x="227.076" y="89.408" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="229.235" y="86.868" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND34" gate="1" x="223.52" y="83.82" smashed="yes">
<attribute name="VALUE" x="220.98" y="81.28" size="1.778" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="175.26" y="106.68" smashed="yes" rot="R180">
<attribute name="NAME" x="173.99" y="110.2614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="179.07" y="109.982" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C21" gate="C$1" x="25.4" y="104.14" smashed="yes" rot="R180">
<attribute name="NAME" x="30.734" y="107.061" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="35.814" y="103.759" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND27" gate="1" x="25.4" y="96.52" smashed="yes">
<attribute name="VALUE" x="23.114" y="94.488" size="1.778" layer="96"/>
</instance>
<instance part="R8" gate="G$1" x="25.4" y="132.08" smashed="yes" rot="R90">
<attribute name="NAME" x="24.13" y="135.6614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="24.13" y="132.842" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="VCC12" gate="G$1" x="25.4" y="139.7" smashed="yes">
<attribute name="VALUE" x="27.94" y="144.78" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="J1" gate="G$1" x="73.66" y="114.3" smashed="yes">
<attribute name="VALUE" x="59.69" y="101.6" size="2.54" layer="96"/>
<attribute name="NAME" x="69.85" y="125.222" size="2.54" layer="95"/>
</instance>
<instance part="R9" gate="G$1" x="43.18" y="104.14" smashed="yes" rot="R90">
<attribute name="NAME" x="41.6814" y="100.33" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="46.482" y="100.33" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND28" gate="1" x="50.8" y="104.14" smashed="yes">
<attribute name="VALUE" x="48.26" y="101.6" size="1.778" layer="96"/>
</instance>
<instance part="TP6" gate="G$1" x="101.6" y="127" smashed="yes" rot="R270">
<attribute name="NAME" x="99.06" y="128.524" size="1.27" layer="95"/>
</instance>
<instance part="XS2" gate="G$1" x="45.72" y="45.72" smashed="yes">
<attribute name="NAME" x="45.72" y="63.5" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="38.1" y="30.48" size="1.778" layer="95" font="vector"/>
</instance>
<instance part="XS2" gate="G$2" x="63.5" y="45.72" smashed="yes" rot="MR0">
<attribute name="NAME" x="63.5" y="63.5" size="1.778" layer="95" font="vector" rot="MR180"/>
<attribute name="VALUE" x="71.12" y="30.48" size="1.778" layer="95" font="vector" rot="MR0"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="200.66" y1="154.94" x2="205.74" y2="154.94" width="0.1524" layer="91"/>
<wire x1="205.74" y1="154.94" x2="205.74" y2="152.4" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="3"/>
<wire x1="200.66" y1="152.4" x2="205.74" y2="152.4" width="0.1524" layer="91"/>
<pinref part="GND31" gate="1" pin="GND"/>
<wire x1="205.74" y1="152.4" x2="205.74" y2="152.146" width="0.1524" layer="91"/>
<junction x="205.74" y="152.4"/>
</segment>
<segment>
<pinref part="GND29" gate="1" pin="GND"/>
<pinref part="BAT2" gate="G$1" pin="-"/>
<wire x1="137.16" y1="68.326" x2="137.16" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S2" gate="1" pin="P"/>
<wire x1="175.26" y1="40.64" x2="175.26" y2="35.56" width="0.1524" layer="91"/>
<pinref part="GND30" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND33" gate="1" pin="GND"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="213.36" y1="38.1" x2="213.36" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="C"/>
<pinref part="GND32" gate="1" pin="GND"/>
<wire x1="213.36" y1="88.9" x2="213.36" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED4" gate="G$1" pin="C"/>
<wire x1="223.52" y1="88.9" x2="223.52" y2="86.36" width="0.1524" layer="91"/>
<pinref part="GND34" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C21" gate="C$1" pin="1"/>
<pinref part="GND27" gate="1" pin="GND"/>
<wire x1="25.4" y1="101.6" x2="25.4" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="GND"/>
<wire x1="55.88" y1="111.76" x2="50.8" y2="111.76" width="0.1524" layer="91"/>
<pinref part="GND28" gate="1" pin="GND"/>
<wire x1="50.8" y1="111.76" x2="50.8" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="XS2" gate="G$1" pin="1"/>
<wire x1="30.48" y1="58.42" x2="25.4" y2="58.42" width="0.1524" layer="91"/>
<label x="25.4" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="XS2" gate="G$2" pin="10"/>
<wire x1="78.74" y1="35.56" x2="83.82" y2="35.56" width="0.1524" layer="91"/>
<label x="83.82" y="35.56" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="XS2" gate="G$2" pin="1"/>
<wire x1="78.74" y1="58.42" x2="83.82" y2="58.42" width="0.1524" layer="91"/>
<label x="83.82" y="58.42" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="XS2" gate="G$1" pin="2"/>
<wire x1="25.4" y1="55.88" x2="30.48" y2="55.88" width="0.1524" layer="91"/>
<label x="25.4" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="NFC_INT" class="0">
<segment>
<pinref part="XS2" gate="G$2" pin="3"/>
<wire x1="78.74" y1="53.34" x2="83.82" y2="53.34" width="0.1524" layer="91"/>
<label x="83.82" y="53.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="NFC_!RST!" class="0">
<segment>
<pinref part="XS2" gate="G$1" pin="3"/>
<wire x1="30.48" y1="53.34" x2="25.4" y2="53.34" width="0.1524" layer="91"/>
<label x="25.4" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_SOMI" class="0">
<segment>
<wire x1="99.06" y1="109.22" x2="109.22" y2="109.22" width="0.1524" layer="91"/>
<wire x1="109.22" y1="109.22" x2="109.22" y2="111.76" width="0.1524" layer="91"/>
<label x="109.22" y="111.76" size="1.778" layer="95" xref="yes"/>
<pinref part="J1" gate="G$1" pin="UART_TXD"/>
</segment>
<segment>
<pinref part="XS2" gate="G$2" pin="6"/>
<wire x1="78.74" y1="45.72" x2="83.82" y2="45.72" width="0.1524" layer="91"/>
<label x="83.82" y="45.72" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LDO_VIN" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="200.66" y1="157.48" x2="205.74" y2="157.48" width="0.1524" layer="91"/>
<label x="205.74" y="157.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="D5" gate="G$1" pin="C"/>
<wire x1="162.56" y1="83.82" x2="167.64" y2="83.82" width="0.1524" layer="91"/>
<label x="167.64" y="83.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LED3" gate="G$1" pin="A"/>
<wire x1="213.36" y1="58.42" x2="213.36" y2="66.04" width="0.1524" layer="91"/>
<wire x1="213.36" y1="66.04" x2="223.52" y2="66.04" width="0.1524" layer="91"/>
<label x="223.52" y="66.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="VCC_TARGET"/>
<wire x1="99.06" y1="119.38" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<label x="106.68" y="119.38" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="XS2" gate="G$1" pin="10"/>
<wire x1="30.48" y1="35.56" x2="25.4" y2="35.56" width="0.1524" layer="91"/>
<label x="25.4" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_SIMO" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="UART_RXD"/>
<wire x1="99.06" y1="106.68" x2="109.22" y2="106.68" width="0.1524" layer="91"/>
<label x="109.22" y="106.68" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="XS2" gate="G$2" pin="7"/>
<wire x1="78.74" y1="43.18" x2="83.82" y2="43.18" width="0.1524" layer="91"/>
<label x="83.82" y="43.18" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="A"/>
<pinref part="S1" gate="1" pin="3"/>
<wire x1="152.4" y1="83.82" x2="157.48" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="BAT2" gate="G$1" pin="+"/>
<wire x1="137.16" y1="78.74" x2="137.16" y2="81.28" width="0.1524" layer="91"/>
<pinref part="S1" gate="1" pin="1"/>
<wire x1="137.16" y1="81.28" x2="142.24" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_FET_TOOL" class="0">
<segment>
<pinref part="S1" gate="1" pin="2"/>
<wire x1="142.24" y1="91.44" x2="137.16" y2="91.44" width="0.1524" layer="91"/>
<label x="137.16" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="VCC_TOOL"/>
<wire x1="99.06" y1="121.92" x2="101.6" y2="121.92" width="0.1524" layer="91"/>
<label x="106.68" y="121.92" size="1.27" layer="95" xref="yes"/>
<pinref part="TP6" gate="G$1" pin="TP"/>
<wire x1="101.6" y1="121.92" x2="106.68" y2="121.92" width="0.1524" layer="91"/>
<junction x="101.6" y="121.92"/>
</segment>
</net>
<net name="LDO_CE" class="0">
<segment>
<pinref part="S2" gate="1" pin="S"/>
<wire x1="175.26" y1="50.8" x2="175.26" y2="58.42" width="0.1524" layer="91"/>
<wire x1="175.26" y1="58.42" x2="182.88" y2="58.42" width="0.1524" layer="91"/>
<label x="182.88" y="58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="XS2" gate="G$2" pin="9"/>
<wire x1="78.74" y1="38.1" x2="83.82" y2="38.1" width="0.1524" layer="91"/>
<label x="83.82" y="38.1" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="LED3" gate="G$1" pin="C"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="213.36" y1="50.8" x2="213.36" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED2" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="A"/>
<wire x1="213.36" y1="96.52" x2="213.36" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="213.36" y1="101.6" x2="180.34" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="LED3" class="0">
<segment>
<pinref part="LED4" gate="G$1" pin="A"/>
<wire x1="223.52" y1="96.52" x2="223.52" y2="106.68" width="0.1524" layer="91"/>
<wire x1="223.52" y1="106.68" x2="180.34" y2="106.68" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="TO_LED3" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="170.18" y1="106.68" x2="162.56" y2="106.68" width="0.1524" layer="91"/>
<label x="162.56" y="106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="XS2" gate="G$2" pin="5"/>
<wire x1="78.74" y1="48.26" x2="83.82" y2="48.26" width="0.1524" layer="91"/>
<label x="83.82" y="48.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TO_LED2" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="170.18" y1="101.6" x2="162.56" y2="101.6" width="0.1524" layer="91"/>
<label x="162.56" y="101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="XS2" gate="G$1" pin="5"/>
<wire x1="30.48" y1="48.26" x2="25.4" y2="48.26" width="0.1524" layer="91"/>
<label x="25.4" y="48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+2V_LDO" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="VCC12" gate="G$1" pin="VCC"/>
<wire x1="25.4" y1="139.7" x2="25.4" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SBWTDIO" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="TDO/TDI"/>
<wire x1="25.4" y1="121.92" x2="55.88" y2="121.92" width="0.1524" layer="91"/>
<label x="20.32" y="121.92" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="C21" gate="C$1" pin="2"/>
<wire x1="25.4" y1="109.22" x2="25.4" y2="121.92" width="0.1524" layer="91"/>
<wire x1="25.4" y1="121.92" x2="20.32" y2="121.92" width="0.1524" layer="91"/>
<junction x="25.4" y="121.92"/>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="25.4" y1="127" x2="25.4" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="XS2" gate="G$1" pin="4"/>
<wire x1="30.48" y1="50.8" x2="25.4" y2="50.8" width="0.1524" layer="91"/>
<label x="25.4" y="50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SBWTCK" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="TCK"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="55.88" y1="114.3" x2="43.18" y2="114.3" width="0.1524" layer="91"/>
<wire x1="43.18" y1="114.3" x2="43.18" y2="109.22" width="0.1524" layer="91"/>
<label x="40.64" y="114.3" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="43.18" y1="114.3" x2="40.64" y2="114.3" width="0.1524" layer="91"/>
<junction x="43.18" y="114.3"/>
</segment>
<segment>
<pinref part="XS2" gate="G$2" pin="4"/>
<wire x1="78.74" y1="50.8" x2="83.82" y2="50.8" width="0.1524" layer="91"/>
<label x="83.82" y="50.8" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="TEST/VPP" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="43.18" y1="99.06" x2="43.18" y2="96.52" width="0.1524" layer="91"/>
<wire x1="43.18" y1="96.52" x2="104.14" y2="96.52" width="0.1524" layer="91"/>
<wire x1="104.14" y1="96.52" x2="104.14" y2="114.3" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="TEST/VPP"/>
<wire x1="104.14" y1="114.3" x2="99.06" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPI_!CS!2" class="0">
<segment>
<pinref part="XS2" gate="G$1" pin="7"/>
<wire x1="30.48" y1="43.18" x2="25.4" y2="43.18" width="0.1524" layer="91"/>
<label x="25.4" y="43.18" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SPI_CLK" class="0">
<segment>
<pinref part="XS2" gate="G$1" pin="8"/>
<wire x1="30.48" y1="40.64" x2="25.4" y2="40.64" width="0.1524" layer="91"/>
<label x="25.4" y="40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="FLASH_!RST!" class="0">
<segment>
<pinref part="XS2" gate="G$1" pin="9"/>
<wire x1="30.48" y1="38.1" x2="25.4" y2="38.1" width="0.1524" layer="91"/>
<label x="25.4" y="38.1" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ACOUSTIC_TRANS" class="0">
<segment>
<pinref part="XS2" gate="G$2" pin="2"/>
<wire x1="78.74" y1="55.88" x2="83.82" y2="55.88" width="0.1524" layer="91"/>
<label x="83.82" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="XS2" gate="G$2" pin="8"/>
<wire x1="78.74" y1="40.64" x2="83.82" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPI_!CS!1" class="0">
<segment>
<pinref part="XS2" gate="G$1" pin="6"/>
<wire x1="30.48" y1="45.72" x2="25.4" y2="45.72" width="0.1524" layer="91"/>
<label x="25.4" y="45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="106.68" y="165.1" size="6.4516" layer="94">Revision History</text>
<text x="35.56" y="149.86" size="2.1844" layer="94">Version</text>
<text x="10.16" y="149.86" size="2.1844" layer="94">Date</text>
<text x="55.88" y="149.86" size="2.1844" layer="94">Revision Summary</text>
<text x="121.92" y="149.86" size="2.1844" layer="94">Author</text>
<text x="121.92" y="142.24" size="1.778" layer="94">Ali Aljumaili</text>
<text x="121.92" y="134.62" size="1.778" layer="94">Ali Aljumaili</text>
<text x="35.56" y="142.24" size="1.778" layer="94">1.0.0</text>
<text x="10.16" y="142.748" size="1.778" layer="94">December 2017</text>
<text x="55.88" y="142.24" size="1.778" layer="94">- First release</text>
<text x="10.414" y="135.128" size="1.778" layer="94">February 2019</text>
<text x="35.56" y="134.62" size="1.778" layer="94">1.1.0</text>
<text x="10.16" y="97.028" size="1.778" layer="94">March 2019</text>
<text x="121.92" y="96.52" size="1.778" layer="94">Ali Aljumaili</text>
<text x="35.56" y="96.52" size="1.778" layer="94">1.2.0</text>
<text x="55.88" y="114.3" size="1.778" layer="94">- Extend SMD pads
- Change MSP430F2272 to MSPFR2633.
- Change debugging interface from 
  JTAG to SBW.
- Removed RTC.
- Add 32 KHz crystal
- Add energy harvesting circuit.
- Add milling to insulate temp and mag-accel sensors.
- Board size reduced to 40 mm x 13 mm</text>
<wire x1="30.48" y1="157.48" x2="30.48" y2="142.24" width="0.6096" layer="90"/>
<wire x1="30.48" y1="142.24" x2="30.48" y2="134.62" width="0.6096" layer="90"/>
<wire x1="30.48" y1="134.62" x2="30.48" y2="96.52" width="0.6096" layer="90"/>
<wire x1="30.48" y1="96.52" x2="30.48" y2="33.02" width="0.6096" layer="90"/>
<wire x1="30.48" y1="142.24" x2="10.16" y2="142.24" width="0.6096" layer="90"/>
<wire x1="30.48" y1="134.62" x2="10.16" y2="134.62" width="0.6096" layer="90"/>
<wire x1="30.48" y1="96.52" x2="10.16" y2="96.52" width="0.6096" layer="90"/>
<text x="55.88" y="83.82" size="1.778" layer="94">- Change temperature sensor
  from SI7051 to TMP117.
- Change LDO from 2.5V to 2.0V variant.
- Change MCU from MSP430FR2633 to 
  MSP430FR2633 to MSP430FR5738.
- Add front page and revision history.</text>
<text x="35.56" y="73.66" size="1.778" layer="94">1.2.1</text>
<text x="55.88" y="60.96" size="1.778" layer="94">- Add breakoutboard for pressure sensor.
- Add breakoutboard for testing.
- Add reset switch.
- Add power switch.
- Add FRAM chip.
- Update silk layer</text>
<text x="121.92" y="73.66" size="1.778" layer="94">Ali Aljumaili</text>
<text x="10.16" y="74.168" size="1.778" layer="94">March 2019</text>
<wire x1="30.48" y1="73.66" x2="10.16" y2="73.66" width="0.6096" layer="90"/>
</plain>
<instances>
<instance part="FRAME5" gate="G$1" x="0" y="0" smashed="yes">
<attribute name="DRAWING_NAME" x="184.15" y="15.24" size="2.54" layer="94" font="vector"/>
<attribute name="LAST_DATE_TIME" x="146.05" y="5.08" size="2.286" layer="94" font="vector"/>
<attribute name="SHEET" x="238.125" y="5.08" size="2.54" layer="94" font="vector"/>
<attribute name="AUTHOR" x="160.02" y="10.16" size="2.54" layer="94"/>
<attribute name="REV" x="203.2" y="5.08" size="2.54" layer="94"/>
</instance>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>

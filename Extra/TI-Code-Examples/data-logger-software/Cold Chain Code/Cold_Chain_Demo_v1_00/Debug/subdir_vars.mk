################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Datalogger.c \
../HDC1000.c \
../OPT3001.c \
../RF430CL331H.c \
../RF430_I2C.c \
../RF430_Request_Processor.c \
../RTC.c \
../TIME.c \
../TMP112.c \
../main.c \
../system_pre_init.c 

OBJS += \
./Datalogger.obj \
./HDC1000.obj \
./OPT3001.obj \
./RF430CL331H.obj \
./RF430_I2C.obj \
./RF430_Request_Processor.obj \
./RTC.obj \
./TIME.obj \
./TMP112.obj \
./main.obj \
./system_pre_init.obj 

C_DEPS += \
./Datalogger.pp \
./HDC1000.pp \
./OPT3001.pp \
./RF430CL331H.pp \
./RF430_I2C.pp \
./RF430_Request_Processor.pp \
./RTC.pp \
./TIME.pp \
./TMP112.pp \
./main.pp \
./system_pre_init.pp 

C_DEPS__QUOTED += \
"Datalogger.pp" \
"HDC1000.pp" \
"OPT3001.pp" \
"RF430CL331H.pp" \
"RF430_I2C.pp" \
"RF430_Request_Processor.pp" \
"RTC.pp" \
"TIME.pp" \
"TMP112.pp" \
"main.pp" \
"system_pre_init.pp" 

OBJS__QUOTED += \
"Datalogger.obj" \
"HDC1000.obj" \
"OPT3001.obj" \
"RF430CL331H.obj" \
"RF430_I2C.obj" \
"RF430_Request_Processor.obj" \
"RTC.obj" \
"TIME.obj" \
"TMP112.obj" \
"main.obj" \
"system_pre_init.obj" 

C_SRCS__QUOTED += \
"../Datalogger.c" \
"../HDC1000.c" \
"../OPT3001.c" \
"../RF430CL331H.c" \
"../RF430_I2C.c" \
"../RF430_Request_Processor.c" \
"../RTC.c" \
"../TIME.c" \
"../TMP112.c" \
"../main.c" \
"../system_pre_init.c" 



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
Datalogger.obj: ../Datalogger.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/bin/cl430" -vmspx --abi=eabi --code_model=large --data_model=large -O0 --include_path="C:/TI/ccsv6/ccsv6/ccs_base/msp430/include" --include_path="C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/include" --advice:power="all" --advice:hw_config="all" -g --define=__MSP430FR5969__ --define=_MPU_ENABLE --diag_warning=225 --display_error_number --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="Datalogger.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

HDC1000.obj: ../HDC1000.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/bin/cl430" -vmspx --abi=eabi --code_model=large --data_model=large -O0 --include_path="C:/TI/ccsv6/ccsv6/ccs_base/msp430/include" --include_path="C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/include" --advice:power="all" --advice:hw_config="all" -g --define=__MSP430FR5969__ --define=_MPU_ENABLE --diag_warning=225 --display_error_number --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="HDC1000.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

OPT3001.obj: ../OPT3001.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/bin/cl430" -vmspx --abi=eabi --code_model=large --data_model=large -O0 --include_path="C:/TI/ccsv6/ccsv6/ccs_base/msp430/include" --include_path="C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/include" --advice:power="all" --advice:hw_config="all" -g --define=__MSP430FR5969__ --define=_MPU_ENABLE --diag_warning=225 --display_error_number --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="OPT3001.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

RF430CL331H.obj: ../RF430CL331H.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/bin/cl430" -vmspx --abi=eabi --code_model=large --data_model=large -O0 --include_path="C:/TI/ccsv6/ccsv6/ccs_base/msp430/include" --include_path="C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/include" --advice:power="all" --advice:hw_config="all" -g --define=__MSP430FR5969__ --define=_MPU_ENABLE --diag_warning=225 --display_error_number --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="RF430CL331H.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

RF430_I2C.obj: ../RF430_I2C.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/bin/cl430" -vmspx --abi=eabi --code_model=large --data_model=large -O0 --include_path="C:/TI/ccsv6/ccsv6/ccs_base/msp430/include" --include_path="C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/include" --advice:power="all" --advice:hw_config="all" -g --define=__MSP430FR5969__ --define=_MPU_ENABLE --diag_warning=225 --display_error_number --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="RF430_I2C.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

RF430_Request_Processor.obj: ../RF430_Request_Processor.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/bin/cl430" -vmspx --abi=eabi --code_model=large --data_model=large -O0 --include_path="C:/TI/ccsv6/ccsv6/ccs_base/msp430/include" --include_path="C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/include" --advice:power="all" --advice:hw_config="all" -g --define=__MSP430FR5969__ --define=_MPU_ENABLE --diag_warning=225 --display_error_number --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="RF430_Request_Processor.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

RTC.obj: ../RTC.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/bin/cl430" -vmspx --abi=eabi --code_model=large --data_model=large -O0 --include_path="C:/TI/ccsv6/ccsv6/ccs_base/msp430/include" --include_path="C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/include" --advice:power="all" --advice:hw_config="all" -g --define=__MSP430FR5969__ --define=_MPU_ENABLE --diag_warning=225 --display_error_number --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="RTC.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

TIME.obj: ../TIME.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/bin/cl430" -vmspx --abi=eabi --code_model=large --data_model=large -O0 --include_path="C:/TI/ccsv6/ccsv6/ccs_base/msp430/include" --include_path="C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/include" --advice:power="all" --advice:hw_config="all" -g --define=__MSP430FR5969__ --define=_MPU_ENABLE --diag_warning=225 --display_error_number --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="TIME.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

TMP112.obj: ../TMP112.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/bin/cl430" -vmspx --abi=eabi --code_model=large --data_model=large -O0 --include_path="C:/TI/ccsv6/ccsv6/ccs_base/msp430/include" --include_path="C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/include" --advice:power="all" --advice:hw_config="all" -g --define=__MSP430FR5969__ --define=_MPU_ENABLE --diag_warning=225 --display_error_number --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="TMP112.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

main.obj: ../main.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/bin/cl430" -vmspx --abi=eabi --code_model=large --data_model=large -O0 --include_path="C:/TI/ccsv6/ccsv6/ccs_base/msp430/include" --include_path="C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/include" --advice:power="all" --advice:hw_config="all" -g --define=__MSP430FR5969__ --define=_MPU_ENABLE --diag_warning=225 --display_error_number --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="main.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

system_pre_init.obj: ../system_pre_init.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/bin/cl430" -vmspx --abi=eabi --code_model=large --data_model=large -O0 --include_path="C:/TI/ccsv6/ccsv6/ccs_base/msp430/include" --include_path="C:/TI/ccsv6/ccsv6/tools/compiler/msp430_4.3.3/include" --advice:power="all" --advice:hw_config="all" -g --define=__MSP430FR5969__ --define=_MPU_ENABLE --diag_warning=225 --display_error_number --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="system_pre_init.pp" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '



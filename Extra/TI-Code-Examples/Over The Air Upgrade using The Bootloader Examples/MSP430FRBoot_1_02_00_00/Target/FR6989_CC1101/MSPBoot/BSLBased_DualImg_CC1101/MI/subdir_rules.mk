################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
MI/TI_MSPBoot_MI_FRAMDualImg.obj: ../MI/TI_MSPBoot_MI_FRAMDualImg.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.3.LTS/bin/cl430" -vmspx --code_model=large --data_model=large -O3 --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.3.LTS/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Target/FR6989_CC1101/MSPBoot" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Target/FR6989_CC1101/MSPBoot/MI" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Target/FR6989_CC1101/MSPBoot/Comm" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Target/FR6989_CC1101/MSPBoot/AppMgr" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Target/FR6989_CC1101/MSPBoot/hal_mcu" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Target/FR6989_CC1101/MSPBoot/radio_drv" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Target/FR6989_CC1101/MSPBoot/radio_drv/cc1101_drv" --advice:power_severity=suppress --advice:power="all" --advice:hw_config="all" -g --gcc --define=MSPBoot_20bit --define=MSPBoot_BSL --define=MSPBoot_CI_CC1101 --define=__MSP430FR6989__ --define=_MPU_ENABLE --diag_warning=225 --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --abi=eabi --printf_support=minimal --preproc_with_compile --preproc_dependency="MI/TI_MSPBoot_MI_FRAMDualImg.d" --obj_directory="MI" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '



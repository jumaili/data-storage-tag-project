################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
MI/%.obj: ../MI/%.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/bin/cl430" -vmspx --data_model=restricted -O3 --use_hw_mpy=F5 --include_path="C:/ti/CCS8_2/ccsv8/ccs_base/msp430/include" --include_path="C:/ti/CCS8_2/ccsv8/ccs_base/msp430/include" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR5969_UART" --include_path="C:/ti/include" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR5969_UART" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR5969_UART/AppMgr" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR5969_UART/Comm" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR5969_UART/MI" --advice:power="all" --advice:hw_config=all --define=MSPBoot_DualImg --define=MSPBoot_BSL --define=MSPBoot_CI_UART --define=__MSP430FR5969__ --define=_MPU_ENABLE --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="MI/$(basename $(<F)).d_raw" --obj_directory="MI" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
AppMgr/%.obj: ../AppMgr/%.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/CCS8_2/ccsv8/tools/compiler/ti-cgt-msp430_18.1.3.LTS/bin/cl430" -vmspx -O3 --use_hw_mpy=F5 --include_path="C:/ti/CCS8_2/ccsv8/ccs_base/msp430/include" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR2433_UART" --include_path="C:/ti/CCS8_2/ccsv8/tools/compiler/ti-cgt-msp430_18.1.3.LTS/include" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR2433_UART" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR2433_UART/AppMgr" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR2433_UART/Comm" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR2433_UART/MI" --advice:power=all --advice:hw_config=all --define=MSPBoot_20bit --define=MSPBoot_BSL --define=MSPBoot_CI_UART --define=__MSP430FR2433__ -g --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="AppMgr/$(basename $(<F)).d_raw" --obj_directory="AppMgr" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '



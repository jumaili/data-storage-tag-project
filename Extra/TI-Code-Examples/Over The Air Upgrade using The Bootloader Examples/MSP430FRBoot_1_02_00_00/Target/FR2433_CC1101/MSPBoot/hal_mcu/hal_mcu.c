/* --COPYRIGHT--,BSD-3-Clause
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
/******************************************************************************
 *  Filename: hal_mcu.c
 *
 *  Description: Configuration of MCU core registers
 
 *******************************************************************************/

#include "msp430.h"
#include "hal_spi_rf.h"

#if defined (__MSP430FR2433__)
/*******************************************************************************
 * @brief Setup all the peripherals of the MSP430, set the CPU speed to 16MHz,
 *        enable the 32KHz and configure WDT for a 1 sec tick speed.
 *
 *        (MSP430F5529 version)
 *
 * @param  none
 *
 * @return none
 *******************************************************************************/
void msp_setup(void) {

	// Enable the interupts on port 2 to catch the user button (TRXEB)

	BUTTON_DIR   &= ~(BUTTON1_PIN | BUTTON2_PIN);     // input direction
	BUTTON_OUT   |=  BUTTON1_PIN | BUTTON2_PIN;       // set high on port
	BUTTON_PxIE  |=  BUTTON1_PIN | BUTTON2_PIN;       // enable interupt
	BUTTON_PxIES |=  BUTTON1_PIN | BUTTON2_PIN;       // Hi/lo edge
	BUTTON_REN   |=  BUTTON1_PIN | BUTTON2_PIN;       // Pull up resistor
	BUTTON_PxIFG &= ~(BUTTON1_PIN | BUTTON2_PIN);     // IFG cleared

	// Configure one FRAM waitstate as required by the device datasheet for MCLK
	// operation beyond 8MHz _before_ configuring the clock system.
	FRCTL0 = FRCTLPW | NWAITS_1;

	// Clock System Setup
    __bis_SR_register(SCG0);                           // disable FLL
    CSCTL3 |= SELREF__REFOCLK;                         // Set REFO as FLL reference source
    CSCTL0 = 0;                                        // clear DCO and MOD registers
    CSCTL1 &= ~(DCORSEL_7);                            // Clear DCO frequency select bits first
    CSCTL1 |= DCORSEL_5;                               // Set DCO = 16MHz
    CSCTL2 = FLLD_0 + 487;                             // DCOCLKDIV = 16MHz
    __delay_cycles(3);
    __bic_SR_register(SCG0);                           // enable FLL
    while(CSCTL7 & (FLLUNLOCK0 | FLLUNLOCK1));         // FLL locked

    CSCTL4 |= SELMS__DCOCLKDIV | SELA__REFOCLK;        // set default REFO(~32768Hz) as ACLK source, ACLK = 32768Hz
                                                       // default DCOCLKDIV as MCLK and SMCLK source

	PM5CTL0 &= ~LOCKLPM5;
}

#endif

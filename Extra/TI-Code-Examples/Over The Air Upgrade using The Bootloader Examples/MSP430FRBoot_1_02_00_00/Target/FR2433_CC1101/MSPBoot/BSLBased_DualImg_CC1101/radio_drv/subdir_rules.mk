################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
radio_drv/%.obj: ../radio_drv/%.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/CCS8_2/ccsv8/tools/compiler/ti-cgt-msp430_18.1.3.LTS/bin/cl430" -vmspx --code_model=large --data_model=large -O3 --opt_for_speed=0 --use_hw_mpy=F5 --include_path="C:/ti/CCS8_2/ccsv8/ccs_base/msp430/include" --include_path="C:/ti/CCS8_2/ccsv8/tools/compiler/ti-cgt-msp430_18.1.3.LTS/include" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR2433_CC1101" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR2433_CC1101/MI" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR2433_CC1101/Comm" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR2433_CC1101/AppMgr" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR2433_CC1101/hal_mcu" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR2433_CC1101/radio_drv" --include_path="C:/Myworks/CCSV8_WORK/MSPBoot_FR2433_CC1101/radio_drv/cc1101_drv" --advice:power="all" --advice:power_severity=suppress --advice:hw_config=all --define=MSPBoot_20bit --define=MSPBoot_BSL --define=MSPBoot_CI_CC1101 --define=__MSP430FR2433__ -g --gcc --printf_support=minimal --diag_warning=225 --display_error_number --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="radio_drv/$(basename $(<F)).d_raw" --obj_directory="radio_drv" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '



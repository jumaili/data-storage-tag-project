/*
 * \file   main.c
 *
 * \brief  Sample application for FR5969 using MSPBoot
 *      This example places application in the appropiate area
 *      of memory (check linker file) and shows how to use interrupts 
*/
/* --COPYRIGHT--,BSD-3-Clause
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
#include "msp430.h"
#include <stdint.h>
#include "TI_MSPBoot_Mgr_Vectors.h"

//force something to be big enough to need upper memory, for testing
const unsigned char testArray[20000] = {0xAA};
volatile unsigned char test;

/******************************************************************************
 *
 * @brief   Main function
 *  This example application performs the following functions:
 *  - Toggle LED1 (P4.6) at startup (to indicate App1 execution)
 *  - Toggles LED1 using a timer periodic interrupt (demonstrates vector redirection)
 *  - Forces Boot mode when button S2 (P1.1) is pressed (demonstrates vector
 *      redirection and Boot Mode call
 *
 * @return  none
 *****************************************************************************/
int main( void )
{
  // Stop watchdog timer to prevent time out reset
  WDTCTL = WDTPW + WDTHOLD;

  //code so that tstArray doesn't get optimized out
//  test = testArray[5];
//  test++;

    // Toggle LED1 in P4.6
    P4DIR |= BIT6;
    P4OUT |= BIT6;
    PM5CTL0 &= ~LOCKLPM5; //unlock GPIOs so settings take effect
    __delay_cycles(500000);
    P4OUT &= ~BIT6;
    __delay_cycles(500000);
    P4OUT |= BIT6;
    __delay_cycles(500000);
    P4OUT &= ~BIT6;

    // Start P1.1 (S2 button) as interrupt with pull-up
    P1OUT |= BIT1;
    P1REN |= BIT1;
    P1IES |= BIT1;
    P1IE |= BIT1;
    P1IFG &= ~BIT1;

    // Start Timer interrupt
    TA0CCTL0 = CCIE;                             // CCR0 interrupt enabled
    TA0CCR0 = 1000-1;
    TA0CTL = TASSEL_1 + MC_1;                  // ACLK, upmode

    __bis_SR_register(LPM3_bits + GIE);
    __no_operation();


  return 0;
}

/******************************************************************************
 *
 * @brief   Timer A Interrupt service routine
 *  This routine simply toggles an LED but it shows how to declare interrupts
 *   in Application space
 *   Note that this function prototype should be accessible by 
 *   TI_MSPBoot_Mgr_Vectors.c
 *
 * @return  none
 *****************************************************************************/
#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{
  P4OUT ^= BIT6;                            // Toggle P1.0
  TA0CCTL0 &= ~CCIFG;
}

/******************************************************************************
 *
 * @brief   Port 1 Interrupt service routine
 *  Forces Boot mode when button S2 (P4.1) is pressed
 *   Note that this function prototype should be accessible by TI_MSPBoot_Mgr_Vectors.c
 *
 * @return  none
 *****************************************************************************/
#pragma vector = PORT1_VECTOR
__interrupt void Port_Isr(void)
{
    P1IFG = 0;
    TI_MSPBoot_JumpToBoot();
}

/******************************************************************************
 *
 * @brief   Dummy Interrupt service routine
 *  This ISR will be executed if an undeclared interrupt is called
 *
 * @return  none
 *****************************************************************************/
#pragma vector = AES256_VECTOR,RTC_VECTOR,PORT4_VECTOR,PORT3_VECTOR,TIMER3_A1_VECTOR,\
		TIMER3_A0_VECTOR,PORT2_VECTOR,TIMER2_A1_VECTOR,TIMER2_A0_VECTOR,\
        TIMER1_A1_VECTOR,TIMER1_A0_VECTOR,DMA_VECTOR,USCI_A1_VECTOR,TIMER0_A1_VECTOR,\
		ADC12_VECTOR,USCI_B0_VECTOR,USCI_A0_VECTOR,WDT_VECTOR,TIMER0_B1_VECTOR,\
		TIMER0_B0_VECTOR,COMP_E_VECTOR,UNMI_VECTOR,SYSNMI_VECTOR
__interrupt void Dummy_Isr(void)
{
    while(1)
        ;
}

/* --COPYRIGHT--,BSD-3-Clause
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
#include <stdint.h>
#include <msp430.h>


#include "bsl.h"
#include "Crc.h"

void BSL_Init(void)
{
  BSL_Comm_Init();
}

uint8_t BSL_sendCommand(uint8_t cmd)
{
    tBSLPacket tPacket;
    tPacket.ui8Header = BSL_HEADER;
    tPacket.tPayload.ui8Command = cmd;
    tPacket.ui8Length = 1;
    tPacket.ui16Checksum = crc16MakeBitwise(tPacket);
    BSL_sendPacket(tPacket);

    if(cmd != BSL_JMP_APP_CMD)
        return BSL_getResponse();

    return 0;
}


uint8_t BSL_programMemorySegment(uint32_t addr, const uint8_t* data,
        uint32_t len)
{
    uint16_t xferLen;
    uint8_t res;

    while (len > 0)
    {
        if (len > 16)
        {
            xferLen = 16;
        } else
        {
            xferLen = len;
        }

        tBSLPacket tPacket;
        tPacket.tPayload.ui8Command = BSL_RX_APP_CMD;
        tPacket.tPayload.ui8pData = data;
        tPacket.ui8Length = xferLen + 4;
        tPacket.tPayload.ui8Addr_H = ((uint8_t) (addr >> 16));
        tPacket.tPayload.ui8Addr_M = ((uint8_t) (addr >> 8));
        tPacket.tPayload.ui8Addr_L = ((uint8_t) (addr & 0xFF));
        tPacket.ui16Checksum = crc16MakeBitwise(tPacket);
        BSL_sendPacket(tPacket);
        //TEST
        if(addr >= 0x23F00)
        	__no_operation();
        //END TEST
        res = BSL_getResponse();

        if (res != BSL_OK_RES)
            break;

        len -= xferLen;
        addr += xferLen;
        data += xferLen;
    }

    return res;
}



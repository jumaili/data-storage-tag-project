/* --COPYRIGHT--,BSD-3-Clause
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
#include <stdint.h>
#include <msp430.h>

#include "bsl.h"

#define TIMEOUT_COUNT   1500000
#define BAUDRATE 57600

#if (BAUDRATE == 115200)
#define UCOSx UCOS16
#define UCBRx 4
#define UCBRFx UCBRF_5
#define UCBRSx 0x55
#elif (BAUDRATE == 57600)
#define UCOSx UCOS16
#define UCBRx 8
#define UCBRFx UCBRF_10
#define UCBRSx 0xF7
#elif (BAUDRATE == 9600)
#define UCOSx UCOS16
#define UCBRx 52
#define UCBRFx UCBRF_1
#define UCBRSx 0x49
#endif

void BSL_Comm_Init(void) {
	/* Initializing UART Module */
	UCA3CTLW0 = UCSWRST | UCSSEL_2;   // USCI held in reset and use SMCLK = 8MHz
	P6SEL0 = BIT0 + BIT1;                     // P1.1 = RXD, P1.2=TXD
	P6SEL1 &= ~(BIT0 + BIT1);
	UCA3BRW = UCBRx;                             // 8MHz 9600
	UCA3MCTLW = UCOSx | (UCBRSx << 1) | UCBRFx; // Modulation BRSx=0x49, BRFx=1, UCOS16=1
	UCA3CTL1 &= ~UCSWRST;                   // **Initialize USCI state machine**
}

/* Reads one byte from BSL target */
uint8_t BSL_getResponse(void) {
	uint32_t timeout = TIMEOUT_COUNT;

	while ((!(UCA3IFG & UCRXIFG)) && (timeout-- > 0))
		;

	if (!timeout)
		return 0xFF;
	else
		return UCA3RXBUF;

}

/* Checks if a slave is responding */
uint8_t BSL_slavePresent(void) {
	// NO ACK in UART
	return 1;
}

/* Sends single I2C Byte (start and stop included) */
void BSL_sendSingleByte(uint8_t ui8Byte) {
	uint32_t timeout = TIMEOUT_COUNT;
	while ((!(UCA3IFG & UCTXIFG)) && (timeout-- > 0))
		;

	if (!timeout)
		return;
	else
		UCA3TXBUF = ui8Byte;
}

/* Sends application image to BSL target (start and stop included)
 * This is a polling function and is blocking. */
void BSL_sendPacket(tBSLPacket tPacket) {
	uint16_t ii;
	volatile uint8_t crch, crcl;

	BSL_sendSingleByte(0x80);
	BSL_sendSingleByte(tPacket.ui8Length);
	BSL_sendSingleByte(tPacket.tPayload.ui8Command);

	if (tPacket.ui8Length > 1) {
		BSL_sendSingleByte(tPacket.tPayload.ui8Addr_L);
		BSL_sendSingleByte(tPacket.tPayload.ui8Addr_M);
		BSL_sendSingleByte(tPacket.tPayload.ui8Addr_H);
		for (ii = 0; ii < (tPacket.ui8Length - 4); ii++) {
			BSL_sendSingleByte(tPacket.tPayload.ui8pData[ii]);
		}
	}

	crcl = (uint8_t) (tPacket.ui16Checksum & 0xFF);
	BSL_sendSingleByte(crcl);

	crch = (uint8_t) (tPacket.ui16Checksum >> 8);
	BSL_sendSingleByte(crch);

}

void BSL_flush(void) {
	UCA3IFG &= ~(UCRXIFG);
}

################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../TargetApps/App1_FR5994_CC1101_BSLBased_20bit_FR5Host.c \
../TargetApps/App1_FR5994_CC1101_BSLBased_DualImg_FR5Host.c \
../TargetApps/App1_FR5994_UART_BSLBased_20bit_FR5Host.c \
../TargetApps/App1_FR5994_UART_BSLBased_DualImg_FR5Host.c \
../TargetApps/App1_FR6047_CC1101_BSLBased_20bit_FR5Host.c \
../TargetApps/App1_FR6047_CC1101_BSLBased_DualImg_FR5Host.c \
../TargetApps/App2_FR5994_CC1101_BSLBased_20bit_FR5Host.c \
../TargetApps/App2_FR5994_CC1101_BSLBased_DualImg_FR5Host.c \
../TargetApps/App2_FR5994_UART_BSLBased_20bit_FR5Host.c \
../TargetApps/App2_FR5994_UART_BSLBased_DualImg_FR5Host.c \
../TargetApps/App2_FR6047_CC1101_BSLBased_20bit_FR5Host.c \
../TargetApps/App2_FR6047_CC1101_BSLBased_DualImg_FR5Host.c \
../TargetApps/SHARP96_Demo.c 

C_DEPS += \
./TargetApps/App1_FR5994_CC1101_BSLBased_20bit_FR5Host.d \
./TargetApps/App1_FR5994_CC1101_BSLBased_DualImg_FR5Host.d \
./TargetApps/App1_FR5994_UART_BSLBased_20bit_FR5Host.d \
./TargetApps/App1_FR5994_UART_BSLBased_DualImg_FR5Host.d \
./TargetApps/App1_FR6047_CC1101_BSLBased_20bit_FR5Host.d \
./TargetApps/App1_FR6047_CC1101_BSLBased_DualImg_FR5Host.d \
./TargetApps/App2_FR5994_CC1101_BSLBased_20bit_FR5Host.d \
./TargetApps/App2_FR5994_CC1101_BSLBased_DualImg_FR5Host.d \
./TargetApps/App2_FR5994_UART_BSLBased_20bit_FR5Host.d \
./TargetApps/App2_FR5994_UART_BSLBased_DualImg_FR5Host.d \
./TargetApps/App2_FR6047_CC1101_BSLBased_20bit_FR5Host.d \
./TargetApps/App2_FR6047_CC1101_BSLBased_DualImg_FR5Host.d \
./TargetApps/SHARP96_Demo.d 

OBJS += \
./TargetApps/App1_FR5994_CC1101_BSLBased_20bit_FR5Host.obj \
./TargetApps/App1_FR5994_CC1101_BSLBased_DualImg_FR5Host.obj \
./TargetApps/App1_FR5994_UART_BSLBased_20bit_FR5Host.obj \
./TargetApps/App1_FR5994_UART_BSLBased_DualImg_FR5Host.obj \
./TargetApps/App1_FR6047_CC1101_BSLBased_20bit_FR5Host.obj \
./TargetApps/App1_FR6047_CC1101_BSLBased_DualImg_FR5Host.obj \
./TargetApps/App2_FR5994_CC1101_BSLBased_20bit_FR5Host.obj \
./TargetApps/App2_FR5994_CC1101_BSLBased_DualImg_FR5Host.obj \
./TargetApps/App2_FR5994_UART_BSLBased_20bit_FR5Host.obj \
./TargetApps/App2_FR5994_UART_BSLBased_DualImg_FR5Host.obj \
./TargetApps/App2_FR6047_CC1101_BSLBased_20bit_FR5Host.obj \
./TargetApps/App2_FR6047_CC1101_BSLBased_DualImg_FR5Host.obj \
./TargetApps/SHARP96_Demo.obj 

OBJS__QUOTED += \
"TargetApps\App1_FR5994_CC1101_BSLBased_20bit_FR5Host.obj" \
"TargetApps\App1_FR5994_CC1101_BSLBased_DualImg_FR5Host.obj" \
"TargetApps\App1_FR5994_UART_BSLBased_20bit_FR5Host.obj" \
"TargetApps\App1_FR5994_UART_BSLBased_DualImg_FR5Host.obj" \
"TargetApps\App1_FR6047_CC1101_BSLBased_20bit_FR5Host.obj" \
"TargetApps\App1_FR6047_CC1101_BSLBased_DualImg_FR5Host.obj" \
"TargetApps\App2_FR5994_CC1101_BSLBased_20bit_FR5Host.obj" \
"TargetApps\App2_FR5994_CC1101_BSLBased_DualImg_FR5Host.obj" \
"TargetApps\App2_FR5994_UART_BSLBased_20bit_FR5Host.obj" \
"TargetApps\App2_FR5994_UART_BSLBased_DualImg_FR5Host.obj" \
"TargetApps\App2_FR6047_CC1101_BSLBased_20bit_FR5Host.obj" \
"TargetApps\App2_FR6047_CC1101_BSLBased_DualImg_FR5Host.obj" \
"TargetApps\SHARP96_Demo.obj" 

C_DEPS__QUOTED += \
"TargetApps\App1_FR5994_CC1101_BSLBased_20bit_FR5Host.d" \
"TargetApps\App1_FR5994_CC1101_BSLBased_DualImg_FR5Host.d" \
"TargetApps\App1_FR5994_UART_BSLBased_20bit_FR5Host.d" \
"TargetApps\App1_FR5994_UART_BSLBased_DualImg_FR5Host.d" \
"TargetApps\App1_FR6047_CC1101_BSLBased_20bit_FR5Host.d" \
"TargetApps\App1_FR6047_CC1101_BSLBased_DualImg_FR5Host.d" \
"TargetApps\App2_FR5994_CC1101_BSLBased_20bit_FR5Host.d" \
"TargetApps\App2_FR5994_CC1101_BSLBased_DualImg_FR5Host.d" \
"TargetApps\App2_FR5994_UART_BSLBased_20bit_FR5Host.d" \
"TargetApps\App2_FR5994_UART_BSLBased_DualImg_FR5Host.d" \
"TargetApps\App2_FR6047_CC1101_BSLBased_20bit_FR5Host.d" \
"TargetApps\App2_FR6047_CC1101_BSLBased_DualImg_FR5Host.d" \
"TargetApps\SHARP96_Demo.d" 

C_SRCS__QUOTED += \
"../TargetApps/App1_FR5994_CC1101_BSLBased_20bit_FR5Host.c" \
"../TargetApps/App1_FR5994_CC1101_BSLBased_DualImg_FR5Host.c" \
"../TargetApps/App1_FR5994_UART_BSLBased_20bit_FR5Host.c" \
"../TargetApps/App1_FR5994_UART_BSLBased_DualImg_FR5Host.c" \
"../TargetApps/App1_FR6047_CC1101_BSLBased_20bit_FR5Host.c" \
"../TargetApps/App1_FR6047_CC1101_BSLBased_DualImg_FR5Host.c" \
"../TargetApps/App2_FR5994_CC1101_BSLBased_20bit_FR5Host.c" \
"../TargetApps/App2_FR5994_CC1101_BSLBased_DualImg_FR5Host.c" \
"../TargetApps/App2_FR5994_UART_BSLBased_20bit_FR5Host.c" \
"../TargetApps/App2_FR5994_UART_BSLBased_DualImg_FR5Host.c" \
"../TargetApps/App2_FR6047_CC1101_BSLBased_20bit_FR5Host.c" \
"../TargetApps/App2_FR6047_CC1101_BSLBased_DualImg_FR5Host.c" \
"../TargetApps/SHARP96_Demo.c" 



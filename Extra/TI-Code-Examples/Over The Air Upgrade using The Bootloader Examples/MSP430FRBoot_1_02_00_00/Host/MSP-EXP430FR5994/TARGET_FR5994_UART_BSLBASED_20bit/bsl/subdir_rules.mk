################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
bsl/bsl.obj: ../bsl/bsl.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.3.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5994/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5994/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5994" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.3.LTS/include" --advice:power="all" --advice:power_severity=suppress --advice:hw_config="all" -g --define=TARGET_FR5994_UART_BSLBASED_20bit --define=__MSP430FR5994__ --define=_MPU_ENABLE --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --abi=eabi --printf_support=minimal --preproc_with_compile --preproc_dependency="bsl/bsl.d" --obj_directory="bsl" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
hal_mcu/hal_fr24_timerA1.obj: ../hal_mcu/hal_fr24_timerA1.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power="all" --advice:power_severity=suppress --advice:hw_config=all --define=TARGET_FR2433_UART_BSLBASED_16bit --define=__MSP430FR2433__ -g --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="hal_mcu/hal_fr24_timerA1.d" --obj_directory="hal_mcu" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

hal_mcu/hal_mcu.obj: ../hal_mcu/hal_mcu.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power="all" --advice:power_severity=suppress --advice:hw_config=all --define=TARGET_FR2433_UART_BSLBASED_16bit --define=__MSP430FR2433__ -g --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="hal_mcu/hal_mcu.d" --obj_directory="hal_mcu" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '



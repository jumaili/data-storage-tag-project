################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
TargetApps/App1_FR2433_CC1101_BSLBased_16bit_FR2Host.obj: ../TargetApps/App1_FR2433_CC1101_BSLBased_16bit_FR2Host.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power="all" --advice:power_severity=suppress --advice:hw_config=all --define=TARGET_FR2433_UART_BSLBASED_DUALIMG --define=__MSP430FR2433__ -g --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="TargetApps/App1_FR2433_CC1101_BSLBased_16bit_FR2Host.d" --obj_directory="TargetApps" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

TargetApps/App1_FR2433_CC1101_BSLBased_DualImg_FR2Host.obj: ../TargetApps/App1_FR2433_CC1101_BSLBased_DualImg_FR2Host.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power="all" --advice:power_severity=suppress --advice:hw_config=all --define=TARGET_FR2433_UART_BSLBASED_DUALIMG --define=__MSP430FR2433__ -g --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="TargetApps/App1_FR2433_CC1101_BSLBased_DualImg_FR2Host.d" --obj_directory="TargetApps" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

TargetApps/App1_FR2433_UART_BSLBased_16bit_FR2Host.obj: ../TargetApps/App1_FR2433_UART_BSLBased_16bit_FR2Host.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power="all" --advice:power_severity=suppress --advice:hw_config=all --define=TARGET_FR2433_UART_BSLBASED_DUALIMG --define=__MSP430FR2433__ -g --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="TargetApps/App1_FR2433_UART_BSLBased_16bit_FR2Host.d" --obj_directory="TargetApps" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

TargetApps/App1_FR2433_UART_BSLBased_DualImg_FR2Host.obj: ../TargetApps/App1_FR2433_UART_BSLBased_DualImg_FR2Host.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power="all" --advice:power_severity=suppress --advice:hw_config=all --define=TARGET_FR2433_UART_BSLBASED_DUALIMG --define=__MSP430FR2433__ -g --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="TargetApps/App1_FR2433_UART_BSLBased_DualImg_FR2Host.d" --obj_directory="TargetApps" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

TargetApps/App2_FR2433_CC1101_BSLBased_16bit_FR2Host.obj: ../TargetApps/App2_FR2433_CC1101_BSLBased_16bit_FR2Host.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power="all" --advice:power_severity=suppress --advice:hw_config=all --define=TARGET_FR2433_UART_BSLBASED_DUALIMG --define=__MSP430FR2433__ -g --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="TargetApps/App2_FR2433_CC1101_BSLBased_16bit_FR2Host.d" --obj_directory="TargetApps" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

TargetApps/App2_FR2433_CC1101_BSLBased_DualImg_FR2Host.obj: ../TargetApps/App2_FR2433_CC1101_BSLBased_DualImg_FR2Host.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power="all" --advice:power_severity=suppress --advice:hw_config=all --define=TARGET_FR2433_UART_BSLBASED_DUALIMG --define=__MSP430FR2433__ -g --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="TargetApps/App2_FR2433_CC1101_BSLBased_DualImg_FR2Host.d" --obj_directory="TargetApps" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

TargetApps/App2_FR2433_UART_BSLBased_16bit_FR2Host.obj: ../TargetApps/App2_FR2433_UART_BSLBased_16bit_FR2Host.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power="all" --advice:power_severity=suppress --advice:hw_config=all --define=TARGET_FR2433_UART_BSLBASED_DUALIMG --define=__MSP430FR2433__ -g --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="TargetApps/App2_FR2433_UART_BSLBased_16bit_FR2Host.d" --obj_directory="TargetApps" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

TargetApps/App2_FR2433_UART_BSLBased_DualImg_FR2Host.obj: ../TargetApps/App2_FR2433_UART_BSLBased_DualImg_FR2Host.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR2433" --include_path="C:/ti/ccsv7/tools/compiler/ti-cgt-msp430_16.9.4.LTS/include" --advice:power="all" --advice:power_severity=suppress --advice:hw_config=all --define=TARGET_FR2433_UART_BSLBASED_DUALIMG --define=__MSP430FR2433__ -g --printf_support=minimal --diag_warning=225 --diag_wrap=off --display_error_number --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --preproc_with_compile --preproc_dependency="TargetApps/App2_FR2433_UART_BSLBased_DualImg_FR2Host.d" --obj_directory="TargetApps" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '



/* --COPYRIGHT--,BSD-3-Clause
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

#include <msp430.h> 
#include <stdint.h>
#include <stdbool.h>

/*! Define the Target processor: TARGET_FR2433 */
// Pre-Defined Target configuration (Properties->Build->Advanced Options->Predefined Symbols)


//
// Target Application files
// Contain the sample applications sent to the target
//

#if (defined(TARGET_FR2433_UART_BSLBASED_16bit))
#include "TargetApps\App1_FR2433_UART_BSLBased_16bit_FR2Host.c"
#include "TargetApps\App2_FR2433_UART_BSLBased_16bit_FR2Host.c"
#elif (defined(TARGET_FR2433_UART_BSLBASED_DUALIMG))
#include "TargetApps\App1_FR2433_UART_BSLBased_DualImg_FR2Host.c"
#include "TargetApps\App2_FR2433_UART_BSLBased_DualImg_FR2Host.c"
#elif (defined(TARGET_FR2433_CC1101_BSLBASED_16bit))
#include "TargetApps\App1_FR2433_CC1101_BSLBased_16bit_FR2Host.c"
#include "TargetApps\App2_FR2433_CC1101_BSLBased_16bit_FR2Host.c"
#elif (defined(TARGET_FR2433_CC1101_BSLBASED_DUALIMG))
#include "TargetApps\App1_FR2433_CC1101_BSLBased_DualImg_FR2Host.c"
#include "TargetApps\App2_FR2433_CC1101_BSLBased_DualImg_FR2Host.c"
#else
#error Define a valid target
#endif


#include "bsl.h"
//#include "Crc.h"

/* Statics */
static bool sentBSLFlipFlop;
static bool bPerformBSL;

#if (defined(TARGET_FR2433_UART_BSLBASED_16bit))
static uint16_t CRC_Addr = 0xC400;
static uint16_t App_StartAddress = 0xC403;
static uint16_t App_EndAddress = 0xF7FF;
#elif (defined(TARGET_FR2433_UART_BSLBASED_DUALIMG))
static uint16_t CRC_Addr = 0xC400;
static uint16_t App_StartAddress = 0xC403;
static uint16_t App_EndAddress = 0xD9FF;
#elif (defined(TARGET_FR2433_CC1101_BSLBASED_16bit))
static uint16_t CRC_Addr = 0xC400;
static uint16_t App_StartAddress = 0xC403;
static uint16_t App_EndAddress = 0xEFFF;
#elif (defined(TARGET_FR2433_CC1101_BSLBASED_DUALIMG))
static uint16_t CRC_Addr = 0xC400;
static uint16_t App_StartAddress = 0xC403;
static uint16_t App_EndAddress = 0xD9FF;
#endif
static uint16_t CRC_Val1, CRC_Val2;


void Software_Trim();                       // Software Trim to get the best DCOFTRIM value
#define MCLK_FREQ_MHZ 8                     // MCLK = 8MHz


/* Error Checking Macro */
#define CHECK_RESPONSE()  if(res != BSL_OK_RES)             \
{                                             \
    break;                                    \
}

static uint16_t Calc_App_CRC(uint16_t * Addr_array, uint16_t *Size_array, uint8_t ** DataPtr_Array, uint8_t num_arrays);

int main(void)
{
    uint8_t res;
    uint8_t section;

    /* Stop the watchdog timer */
    WDTCTL = WDTPW | WDTHOLD;
    __bis_SR_register(SCG0);                 // disable FLL
    CSCTL1 = DCOFTRIMEN | DCOFTRIM0 | DCOFTRIM1 | DCORSEL_3;                         // Set DCO = 8Mhz
    CSCTL2 = FLLD_0 + 243;                   // DCODIV = 8MHz
    __delay_cycles(3);
    __bic_SR_register(SCG0);                 // enable FLL
    Software_Trim();                         // Software Trim to get the best DCOFTRIM value

    CSCTL4 = SELA__REFOCLK + SELMS__DCOCLKDIV;                // set ACLK = VLO




    // MCLK=SMCLK=DCO

    /* Calculate CRC for both applications */
    CRC_Val1 = Calc_App_CRC( (uint16_t *)&App1_Addr[0],
                              (uint16_t *)&App1_Size[0],
                              (uint8_t **)&App1_Ptr[0],
                              sizeof(App1_Addr)/ sizeof (App1_Addr[0]));
    CRC_Val2 = Calc_App_CRC( (uint16_t *)&App2_Addr[0],
                              (uint16_t *)&App2_Size[0],
                              (uint8_t **)&App2_Ptr[0],
                              sizeof(App2_Addr)/ sizeof (App2_Addr[0]));

    /* Resetting our event flag */
    bPerformBSL = false;
    sentBSLFlipFlop = false;

    BSL_Init();

    /* Start P2.3 (S1 button) as interrupt with pull-up */
    P2OUT |= (BIT3);
    P1OUT &= ~BIT0;
    P1DIR |= BIT0;
    P2REN |= BIT3;
    P2IES |= BIT3;
    P2IFG &= ~BIT3;
    P2IE |= BIT3;
    //set P1.1 LED to show we are ready
    P2OUT |= BIT1;
    P2DIR |= BIT1;
    PM5CTL0 &= ~LOCKLPM5;

    while (1)
    {
        P2IFG &= ~BIT3; // Clear button flag before going to sleep
        __bis_SR_register(LPM0_bits + GIE);
        __disable_interrupt();

        while (bPerformBSL == true)
        {


            if (!BSL_slavePresent())
                break;
            /* Sending the BSL Entry command to user app on target */
            BSL_sendSingleByte(VBOOT_ENTRY_CMD);

            __delay_cycles(80000);
            BSL_flush();

            __delay_cycles(500000);     //Wait for boot target device for DCO trim when App2 jump to App1
            /* Sending the version command */
            res = BSL_sendCommand(BSL_VERSION_CMD);


            /* Blinking LED to signal BSL start */
            P1OUT ^= (BIT0);
            __delay_cycles(2500000);
            P1OUT ^= (BIT0);
            __delay_cycles(2500000);
            P1OUT ^= (BIT0);
            __delay_cycles(2500000);
            P1OUT ^= (BIT0);



            /* Making sure the version matches (only check upper nibble) */
            if ((res&0xF0) == (VBOOT_VERSION&0xF0))
            {
                /* Erasing the user application */
                res = BSL_sendCommand(BSL_ERASE_APP_CMD);
                CHECK_RESPONSE();

                if(!sentBSLFlipFlop)
                {

                    for (section = 0; section < (sizeof(App1_Addr)/ sizeof (App1_Addr[0])) ; section++)
                    {
                        /* Sending the segments*/
                        res = BSL_programMemorySegment(App1_Addr[section],
                                App1_Ptr[section], App1_Size[section]);
                        CHECK_RESPONSE();
                    }
                    /* Sending the CRC */
                    res = BSL_programMemorySegment(CRC_Addr,
                            (uint8_t *) &CRC_Val1, 2);
                    CHECK_RESPONSE();
                }
                else
                {
                	for (section = 0; section < (sizeof(App2_Addr)/ sizeof (App2_Addr[0])) ; section++)
					{
						/* Sending the segments*/
						res = BSL_programMemorySegment(App2_Addr[section],
								App2_Ptr[section], App2_Size[section]);
						CHECK_RESPONSE();
					}

					/* Sending the CRC */
					res = BSL_programMemorySegment(CRC_Addr,
							(uint8_t *) &CRC_Val2, 2);
					CHECK_RESPONSE();
                }
                /* Jumping to user code */
                res = BSL_sendCommand(BSL_JMP_APP_CMD);
                // End of cycle completed OK
                __delay_cycles(2500000);
                P1OUT ^= (BIT0);
                __delay_cycles(2500000);
                P1OUT ^= (BIT0);
                __delay_cycles(2500000);
                P1OUT ^= (BIT0);
            }

            bPerformBSL = false;
            sentBSLFlipFlop = !sentBSLFlipFlop;
        }
    }
}

/* Push button that will invoke BSL send */
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
    P2IFG &= ~BIT3;
    bPerformBSL = true;
    __bic_SR_register_on_exit(LPM0_bits);
}

/* Calculate the CRC of the application*/
static uint16_t Calc_App_CRC(uint16_t * Addr_array, uint16_t *Size_array, uint8_t ** DataPtr_Array, uint8_t num_arrays)
{
    uint16_t addr;
    uint8_t i;

    CRCINIRES = 0xFFFF;

    // Calculate CRC for the whole Application address range
    for (addr=App_StartAddress; addr <= App_EndAddress; addr++)
    {
        for (i = 0; i < num_arrays; i ++)
        {
            // Check if address is defined by application image
            if ( (addr >= Addr_array[i]) &&
                 (addr < (Addr_array[i] + Size_array[i])) )
            {
                // If address is defined, add it to the CRC
            	CRCDIRB_L = DataPtr_Array[i][addr-Addr_array[i]];
                break;
            }
        }
        if (i==num_arrays)
        {
            // If not, simply add 0xFF
        	CRCDIRB_L = 0xFF;
        }
    }
    return CRCINIRES;
}


void Software_Trim()
{
    unsigned int oldDcoTap = 0xffff;
    unsigned int newDcoTap = 0xffff;
    unsigned int newDcoDelta = 0xffff;
    unsigned int bestDcoDelta = 0xffff;
    unsigned int csCtl0Copy = 0;
    unsigned int csCtl1Copy = 0;
    unsigned int csCtl0Read = 0;
    unsigned int csCtl1Read = 0;
    unsigned int dcoFreqTrim = 3;
    unsigned char endLoop = 0;

    do
    {
        CSCTL0 = 0x100;                         // DCO Tap = 256
        do
        {
            CSCTL7 &= ~DCOFFG;                  // Clear DCO fault flag
        }while (CSCTL7 & DCOFFG);               // Test DCO fault flag

        __delay_cycles((unsigned int)3000 * MCLK_FREQ_MHZ);// Wait FLL lock status (FLLUNLOCK) to be stable
                                                           // Suggest to wait 24 cycles of divided FLL reference clock
        while((CSCTL7 & (FLLUNLOCK0 | FLLUNLOCK1)) && ((CSCTL7 & DCOFFG) == 0));

        csCtl0Read = CSCTL0;                   // Read CSCTL0
        csCtl1Read = CSCTL1;                   // Read CSCTL1

        oldDcoTap = newDcoTap;                 // Record DCOTAP value of last time
        newDcoTap = csCtl0Read & 0x01ff;       // Get DCOTAP value of this time
        dcoFreqTrim = (csCtl1Read & 0x0070)>>4;// Get DCOFTRIM value

        if(newDcoTap < 256)                    // DCOTAP < 256
        {
            newDcoDelta = 256 - newDcoTap;     // Delta value between DCPTAP and 256
            if((oldDcoTap != 0xffff) && (oldDcoTap >= 256)) // DCOTAP cross 256
                endLoop = 1;                   // Stop while loop
            else
            {
                dcoFreqTrim--;
                CSCTL1 = (csCtl1Read & (~(DCOFTRIM0+DCOFTRIM1+DCOFTRIM2))) | (dcoFreqTrim<<4);
            }
        }
        else                                   // DCOTAP >= 256
        {
            newDcoDelta = newDcoTap - 256;     // Delta value between DCPTAP and 256
            if(oldDcoTap < 256)                // DCOTAP cross 256
                endLoop = 1;                   // Stop while loop
            else
            {
                dcoFreqTrim++;
                CSCTL1 = (csCtl1Read & (~(DCOFTRIM0+DCOFTRIM1+DCOFTRIM2))) | (dcoFreqTrim<<4);
            }
        }

        if(newDcoDelta < bestDcoDelta)         // Record DCOTAP closest to 256
        {
            csCtl0Copy = csCtl0Read;
            csCtl1Copy = csCtl1Read;
            bestDcoDelta = newDcoDelta;
        }

    }while(endLoop == 0);                      // Poll until endLoop == 1

    CSCTL0 = csCtl0Copy;                       // Reload locked DCOTAP
    CSCTL1 = csCtl1Copy;                       // Reload locked DCOFTRIM
    while(CSCTL7 & (FLLUNLOCK0 | FLLUNLOCK1)); // Poll until FLL is locked
}






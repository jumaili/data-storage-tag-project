################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
radio_drv/%.obj: ../radio_drv/%.c $(GEN_OPTS) | $(GEN_FILES)
	@echo 'Building file: "$<"'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/CCS8_2/ccsv8/tools/compiler/ti-cgt-msp430_18.1.3.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/CCS8_2/ccsv8/ccs_base/msp430/include" --include_path="C:/Myworks/CCSV8_WORK/MSP-EXP430FR6989_MSPBootHost/bsl" --include_path="C:/Myworks/CCSV8_WORK/MSP-EXP430FR6989_MSPBootHost/crc" --include_path="C:/Myworks/CCSV8_WORK/MSP-EXP430FR6989_MSPBootHost" --include_path="C:/ti/CCS8_2/ccsv8/tools/compiler/ti-cgt-msp430_18.1.3.LTS/include" --advice:power="all" --advice:power_severity=suppress --advice:hw_config=all -g --define=TARGET_FR6989_UART_BSLBASED_20bit --define=__MSP430FR6989__ --define=_MPU_ENABLE --diag_wrap=off --display_error_number --diag_warning=225 --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --abi=eabi --printf_support=minimal --preproc_with_compile --preproc_dependency="radio_drv/$(basename $(<F)).d_raw" --obj_directory="radio_drv" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: "$<"'
	@echo ' '



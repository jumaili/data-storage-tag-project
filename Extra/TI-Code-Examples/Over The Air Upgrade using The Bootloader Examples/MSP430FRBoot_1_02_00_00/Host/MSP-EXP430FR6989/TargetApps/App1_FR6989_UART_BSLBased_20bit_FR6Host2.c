/* --COPYRIGHT--,BSD-3-Clause
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
//
// Filename: App1_FR6989_UART_BSLBased_20bit_FR5Host.c
// Generated from App1_MSPBoot_FR6989_UART.txt
// Tue Oct  4 15:09:02 2016


#include <stdint.h>


#define App1_SIZE   286

const uint32_t App1_Addr[2] = {
    0x4404,	// Address segment0
    0xf7c6,	// Address segment1
};

const uint32_t App1_Size[2] = {
228,   // Size segment0
58,   // Size segment1
};

const uint8_t App1_0[] = {
0x81,0x00,0x00,0x24,0xb0,0x13,0xe4,0x44,0x0c,0x43,0xb0,0x13,0x32,0x44,0xb0,0x13,
0xde,0x44,0x4f,0x14,0xc2,0x43,0x1c,0x02,0xb0,0x13,0xc6,0x44,0x4b,0x16,0x00,0x13,
0xd2,0xe3,0x02,0x02,0x92,0xc3,0x42,0x03,0x00,0x13,0xff,0x3f,0x03,0x43,0xb2,0x40,
0x80,0x5a,0x5c,0x01,0xd2,0xd3,0x04,0x02,0xd2,0xd3,0x02,0x02,0x92,0xc3,0x30,0x01,
0x1e,0x14,0x3d,0x40,0x44,0x28,0x1e,0x43,0x1d,0x83,0x0e,0x73,0xfd,0x23,0x0d,0x93,
0xfb,0x23,0x1d,0x16,0x03,0x43,0xd2,0xc3,0x02,0x02,0x1e,0x14,0x3d,0x40,0x44,0x28,
0x1e,0x43,0x1d,0x83,0x0e,0x73,0xfd,0x23,0x0d,0x93,0xfb,0x23,0x1d,0x16,0x03,0x43,
0xd2,0xd3,0x02,0x02,0x1e,0x14,0x3d,0x40,0x44,0x28,0x1e,0x43,0x1d,0x83,0x0e,0x73,
0xfd,0x23,0x0d,0x93,0xfb,0x23,0x1d,0x16,0x03,0x43,0xd2,0xc3,0x02,0x02,0xe2,0xd2,
0x02,0x02,0xe2,0xd2,0x06,0x02,0xe2,0xd2,0x18,0x02,0xe2,0xd2,0x1a,0x02,0xe2,0xc2,
0x1c,0x02,0xb2,0x40,0x10,0x00,0x42,0x03,0xb2,0x40,0xe7,0x03,0x52,0x03,0xb2,0x40,
0x10,0x01,0x40,0x03,0x03,0x43,0x32,0xd0,0xd8,0x00,0x03,0x43,0x03,0x43,0x0c,0x43,
0x10,0x01,0xb2,0x40,0xde,0xc0,0x00,0x1c,0xd2,0xd3,0x02,0x1c,0x32,0xc2,0x03,0x43,
0xb2,0x40,0x04,0xa5,0x20,0x01,0xff,0x3f,0x03,0x43,0x03,0x43,0xff,0x3f,0x03,0x43,
0x1c,0x43,0x10,0x01,
};

const uint8_t App1_1[] = {
0x2e,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,
0x2e,0x44,0x2e,0x44,0x16,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,
0x2e,0x44,0x24,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,
0x2e,0x44,0x2e,0x44,0x2e,0x44,0x2e,0x44,0x04,0x44,
};

const uint8_t *App1_Ptr[2] = {
App1_0,
App1_1,
};


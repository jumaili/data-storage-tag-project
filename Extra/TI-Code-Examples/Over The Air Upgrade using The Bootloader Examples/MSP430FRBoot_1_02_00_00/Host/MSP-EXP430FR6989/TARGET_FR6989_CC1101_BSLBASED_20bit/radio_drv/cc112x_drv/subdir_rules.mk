################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
radio_drv/cc112x_drv/cc112x_drv.obj: ../radio_drv/cc112x_drv/cc112x_drv.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/bin/cl430" -vmspx --abi=eabi --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/A0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSPBoot_2_00_00_00/Host/MSP-EXP430FR6989/Src/bsl" --include_path="C:/Users/A0274016/workspace_v6_0/MSP-EXP430FR6989_MSPBootHost/hal_mcu" --include_path="C:/Users/A0274016/workspace_v6_0/MSP-EXP430FR6989_MSPBootHost/radio_drv" --include_path="C:/Users/A0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSPBoot_2_00_00_00/Host/MSP-EXP430FR6989/Src/crc" --include_path="C:/Users/A0274016/workspace_v6_0/MSP-EXP430FR6989_MSPBootHost" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/include" --advice:power_severity=suppress --advice:power="all" --advice:hw_config=all -g --define=TARGET_FR6989_CC1101_BSLBASED_20bit --define=__MSP430FR6989__ --define=_MPU_ENABLE --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="radio_drv/cc112x_drv/cc112x_drv.pp" --obj_directory="radio_drv/cc112x_drv" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

radio_drv/cc112x_drv/cc112x_utils.obj: ../radio_drv/cc112x_drv/cc112x_utils.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/bin/cl430" -vmspx --abi=eabi --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/A0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSPBoot_2_00_00_00/Host/MSP-EXP430FR6989/Src/bsl" --include_path="C:/Users/A0274016/workspace_v6_0/MSP-EXP430FR6989_MSPBootHost/hal_mcu" --include_path="C:/Users/A0274016/workspace_v6_0/MSP-EXP430FR6989_MSPBootHost/radio_drv" --include_path="C:/Users/A0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSPBoot_2_00_00_00/Host/MSP-EXP430FR6989/Src/crc" --include_path="C:/Users/A0274016/workspace_v6_0/MSP-EXP430FR6989_MSPBootHost" --include_path="C:/ti/ccsv6/tools/compiler/ti-cgt-msp430_4.4.6/include" --advice:power_severity=suppress --advice:power="all" --advice:hw_config=all -g --define=TARGET_FR6989_CC1101_BSLBASED_20bit --define=__MSP430FR6989__ --define=_MPU_ENABLE --display_error_number --diag_warning=225 --diag_wrap=off --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="radio_drv/cc112x_drv/cc112x_utils.pp" --obj_directory="radio_drv/cc112x_drv" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '



################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Each subdirectory must supply rules for building sources it contributes
radio_drv/cc1101_drv/cc1101_drv.obj: ../radio_drv/cc1101_drv/cc1101_drv.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/msp430_15.12.3.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5969/radio_drv/cc1101_drv" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5969/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5969/hal_mcu" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5969/radio_drv" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5969/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5969" --include_path="C:/ti/ccsv7/tools/compiler/msp430_15.12.3.LTS/include" --advice:power_severity=suppress --advice:power="all" --advice:hw_config=all -g --define=TARGET_FR5969_CC1101_BSLBASED_DUALIMG --define=__MSP430FR5969__ --define=_MPU_ENABLE --display_error_number --diag_warning=225 --diag_wrap=off --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="radio_drv/cc1101_drv/cc1101_drv.d_raw" --obj_directory="radio_drv/cc1101_drv" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

radio_drv/cc1101_drv/cc1101_utils.obj: ../radio_drv/cc1101_drv/cc1101_utils.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv7/tools/compiler/msp430_15.12.3.LTS/bin/cl430" -vmspx --data_model=large --use_hw_mpy=F5 --include_path="C:/ti/ccsv7/ccs_base/msp430/include" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5969/radio_drv/cc1101_drv" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5969/bsl" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5969/hal_mcu" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5969/radio_drv" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5969/crc" --include_path="C:/Users/a0274016/Documents/TIDM-FRAMSUB1GHZOTA/MSP430FRBoot_1_01_00_00/Host/MSP-EXP430FR5969" --include_path="C:/ti/ccsv7/tools/compiler/msp430_15.12.3.LTS/include" --advice:power_severity=suppress --advice:power="all" --advice:hw_config=all -g --define=TARGET_FR5969_CC1101_BSLBASED_DUALIMG --define=__MSP430FR5969__ --define=_MPU_ENABLE --display_error_number --diag_warning=225 --diag_wrap=off --abi=eabi --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="radio_drv/cc1101_drv/cc1101_utils.d_raw" --obj_directory="radio_drv/cc1101_drv" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '



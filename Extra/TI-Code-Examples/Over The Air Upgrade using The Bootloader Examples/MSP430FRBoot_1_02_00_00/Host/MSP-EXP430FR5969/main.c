/* --COPYRIGHT--,BSD-3-Clause
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/

#include <msp430.h> 
#include <stdint.h>
#include <stdbool.h>

/*! Define the Target processor: TARGET_FR5969 */
// Pre-Defined Target configuration (Properties->Build->Advanced Options->Predefined Symbols)


//
// Target Application files
// Contain the sample applications sent to the target
//

#if (defined(TARGET_FR5969_UART_BSLBASED_20bit))
#include "TargetApps\App1_FR5969_UART_BSLBased_20bit_FR5Host.c"
#include "TargetApps\App2_FR5969_UART_BSLBased_20bit_FR5Host.c"
#elif (defined(TARGET_FR5969_UART_BSLBASED_DUALIMG))
#include "TargetApps\App1_FR5969_UART_BSLBased_DualImg_FR5Host.c"
#include "TargetApps\App2_FR5969_UART_BSLBased_DualImg_FR5Host.c"
#elif (defined(TARGET_FR5969_CC1101_BSLBASED_20bit))
#include "TargetApps\App1_FR5969_CC1101_BSLBased_20bit_FR5Host.c"
#include "TargetApps\App2_FR5969_CC1101_BSLBased_20bit_FR5Host.c"
#elif (defined(TARGET_FR5969_CC1101_BSLBASED_DUALIMG))
#include "TargetApps\App1_FR5969_CC1101_BSLBased_DualImg_FR5Host.c"
#include "TargetApps\App2_FR5969_CC1101_BSLBased_DualImg_FR5Host.c"
#else
#error Define a valid target
#endif


#include "bsl.h"
//#include "Crc.h"

/* Statics */
static bool sentBSLFlipFlop;
static bool bPerformBSL;

#if (defined(TARGET_FR5969_UART_BSLBASED_20bit))
static uint16_t CRC_Addr = 0x4400;
static uint16_t App_StartAddress = 0x4403;
static uint16_t App_EndAddress = 0xF7FF;
static uint32_t App_StartAddress_Upper = 0x10000;
static uint32_t App_EndAddress_Upper = 0x13FFF;
#elif (defined(TARGET_FR5969_UART_BSLBASED_DUALIMG))
static uint16_t CRC_Addr = 0x4400;
static uint16_t App_StartAddress = 0x4403;
static uint16_t App_EndAddress = 0xB9FF;
#elif (defined(TARGET_FR5969_CC1101_BSLBASED_20bit))
static uint16_t CRC_Addr = 0x4400;
static uint16_t App_StartAddress = 0x4403;
static uint16_t App_EndAddress = 0xEFFF;
static uint32_t App_StartAddress_Upper = 0x10000;
static uint32_t App_EndAddress_Upper = 0x13FFF;
#elif (defined(TARGET_FR5969_CC1101_BSLBASED_DUALIMG))
static uint16_t CRC_Addr = 0x4400;
static uint16_t App_StartAddress = 0x4403;
static uint16_t App_EndAddress = 0xB9FF;
#endif
static uint16_t CRC_Val1, CRC_Val2;


/* Error Checking Macro */
#define CHECK_RESPONSE()  if(res != BSL_OK_RES)             \
{                                             \
    break;                                    \
}

static uint32_t Calc_App_CRC(uint32_t * Addr_array, uint32_t *Size_array, uint8_t ** DataPtr_Array, uint8_t num_arrays);

int main(void)
{
    uint8_t res;
    uint8_t section;

    /* Stop the watchdog timer */
    WDTCTL = WDTPW | WDTHOLD;

    CSCTL0_H = CSKEY_H;
    CSCTL1 = DCOFSEL_6;                         // Set DCO = 8Mhz
    CSCTL2 = SELA__VLOCLK + SELM__DCOCLK + SELS__DCOCLK;  // set ACLK = VLO
                                                          // MCLK=SMCLK=DCO
    CSCTL3 = DIVA__1 + DIVS__1 + DIVM__1;                 // Divide DCO/1


    /* Calculate CRC for both applications */
    CRC_Val1 = Calc_App_CRC( (uint32_t *)&App1_Addr[0],
                              (uint32_t *)&App1_Size[0],
                              (uint8_t **)&App1_Ptr[0],
                              sizeof(App1_Addr)/ sizeof (App1_Addr[0]));
    CRC_Val2 = Calc_App_CRC( (uint32_t *)&App2_Addr[0],
                              (uint32_t *)&App2_Size[0],
                              (uint8_t **)&App2_Ptr[0],
                              sizeof(App2_Addr)/ sizeof (App2_Addr[0]));

    /* Resetting our event flag */
    bPerformBSL = false;
    sentBSLFlipFlop = false;

    BSL_Init();

    /* Start P4.5 (S1 button) as interrupt with pull-up */
    P4OUT |= (BIT5);
    P4OUT &= ~BIT6;
    P4DIR |= BIT6;
    P4REN |= BIT5;
    P4IES |= BIT5;
    P4IFG &= ~BIT5;
    P4IE |= BIT5;
    //set P1.0 LED to show we are ready
    P1OUT |= BIT0;
    P1DIR |= BIT0;
    PM5CTL0 &= ~LOCKLPM5;

    while (1)
    {
        P4IFG &= ~BIT5; // Clear button flag before going to sleep
        __bis_SR_register(LPM0_bits + GIE);
        __disable_interrupt();

        while (bPerformBSL == true)
        {
            /* Blinking LED to signal BSL start */
            P4OUT ^= (BIT6);
            __delay_cycles(2500000);
            P4OUT ^= (BIT6);
            __delay_cycles(2500000);
            P4OUT ^= (BIT6);
            __delay_cycles(2500000);
            P4OUT ^= (BIT6);

            if (!BSL_slavePresent())
                break;
            /* Sending the BSL Entry command to user app on target */
            BSL_sendSingleByte(VBOOT_ENTRY_CMD);
            __delay_cycles(80000);

            BSL_flush();

            /* Sending the version command */
            res = BSL_sendCommand(BSL_VERSION_CMD);

            /* Making sure the version matches (only check upper nibble) */
            if ((res&0xF0) == (VBOOT_VERSION&0xF0))
            {
                /* Erasing the user application */
                res = BSL_sendCommand(BSL_ERASE_APP_CMD);
                CHECK_RESPONSE();

                if(!sentBSLFlipFlop)
                {

                    for (section = 0; section < (sizeof(App1_Addr)/ sizeof (App1_Addr[0])) ; section++)
                    {
                        /* Sending the segments*/
                        res = BSL_programMemorySegment(App1_Addr[section],
                                App1_Ptr[section], App1_Size[section]);
                        CHECK_RESPONSE();
                    }
                    /* Sending the CRC */
                    res = BSL_programMemorySegment(CRC_Addr,
                            (uint8_t *) &CRC_Val1, 2);
                    CHECK_RESPONSE();
                }
                else
                {
                	for (section = 0; section < (sizeof(App2_Addr)/ sizeof (App2_Addr[0])) ; section++)
					{
						/* Sending the segments*/
						res = BSL_programMemorySegment(App2_Addr[section],
								App2_Ptr[section], App2_Size[section]);
						CHECK_RESPONSE();
					}

					/* Sending the CRC */
					res = BSL_programMemorySegment(CRC_Addr,
							(uint8_t *) &CRC_Val2, 2);
					CHECK_RESPONSE();
                }
                /* Jumping to user code */
                res = BSL_sendCommand(BSL_JMP_APP_CMD);
                // End of cycle completed OK
                __delay_cycles(2500000);
                P4OUT ^= (BIT6);
                __delay_cycles(2500000);
                P4OUT ^= (BIT6);
                __delay_cycles(2500000);
                P4OUT ^= (BIT6);
            }

            bPerformBSL = false;
            sentBSLFlipFlop = !sentBSLFlipFlop;
        }
    }
}

/* Push button that will invoke BSL send */
#pragma vector=PORT4_VECTOR
__interrupt void Port_4(void)
{
    P4IFG &= ~BIT5;
    bPerformBSL = true;
    __bic_SR_register_on_exit(LPM0_bits);
}

/* Calculate the CRC of the application*/
static uint32_t Calc_App_CRC(uint32_t * Addr_array, uint32_t *Size_array, uint8_t ** DataPtr_Array, uint8_t num_arrays)
{
    uint16_t addr;
    uint8_t i;

    CRCINIRES = 0xFFFF;

    // Calculate CRC for the whole Application address range
    for (addr=App_StartAddress; addr <= App_EndAddress; addr++)
    {
        for (i = 0; i < num_arrays; i ++)
        {
            // Check if address is defined by application image
            if ( (addr >= Addr_array[i]) &&
                 (addr < (Addr_array[i] + Size_array[i])) )
            {
                // If address is defined, add it to the CRC
            	CRCDIRB_L = DataPtr_Array[i][addr-Addr_array[i]];
                break;
            }
        }
        if (i==num_arrays)
        {
            // If not, simply add 0xFF
        	CRCDIRB_L = 0xFF;
        }
    }
#if (defined(TARGET_FR5969_UART_BSLBASED_20bit) || defined(TARGET_FR5969_CC1101_BSLBASED_20bit))
    // CRC includes the upper Application address range
    uint32_t addr20;
    for (addr20=App_StartAddress_Upper; addr20 <= App_EndAddress_Upper; addr20++)
    {
        for (i = 0; i < num_arrays; i ++)
        {
            // Check if address is defined by application image
            if ( (addr20 >= Addr_array[i]) &&
                 (addr20 < (Addr_array[i] + Size_array[i])) )
            {
                // If address is defined, add it to the CRC
            	CRCDIRB_L = DataPtr_Array[i][addr20-Addr_array[i]];
                break;
            }
        }
        if (i==num_arrays)
        {
            // If not, simply add 0xFF
        	CRCDIRB_L = 0xFF;
        }
    }
#endif
    return CRCINIRES;
}

# --COPYRIGHT--,BSD-3-Clause
#  Copyright (c) 2017, Texas Instruments Incorporated
#  All rights reserved.
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions
#  are met:
# 
#  *  Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
# 
#  *  Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
# 
#  *  Neither the name of Texas Instruments Incorporated nor the names of
#     its contributors may be used to endorse or promote products derived
#     from this software without specific prior written permission.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
#  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
#  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
#  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# --/COPYRIGHT--

#Perl script used to generate linker files for MSPFRBoot
#   Check help for usage
#

use strict;
use warnings;
use POSIX qw/strftime/;
use Getopt::Long;
use List::Util qw[min max];

print( "\n MSP430FRBootLinkerGen.pl V1.0 \n");
print( "Texas Instruments Inc 2016 \n");

my $filename_tempBootCCS  = "template_Boot_CCS_flex.tmpl";
my $filename_tempAppCCS  = "template_App_CCS_flex.tmpl";

my $filename;
my $dual_image = 0;
my $lnk_file;
my $boot_size;
my $infoboot_size = "0xc0";
my $num_shared_vectors;
my $help = 0;
GetOptions ("file=s"        => \$filename,      # string
            "dual_image" => \$dual_image,    # string, optional
            "lnk_file=s"    => \$lnk_file,      # string
            "boot_size=s{1}" => \$boot_size,     # string
            "shared_vectors=s{1}" => \$num_shared_vectors, #string
            "help" => \$help
            )      
  or die("Error in command line arguments\n");

if ((!$lnk_file) || (!$filename)||(!$boot_size)||($help))
{
    print("Error in command line arguments\n");
    help();
    die;
}

if(hex($boot_size) % 0x400 != 0 || hex($boot_size) == 0)
{
    die("Error: Boot size must be a multiple of 0x400!");
}

# Parsing test globals

# get the device number
my $device = getdeviceinFile($lnk_file);
if($device ne 0)
{
    print ($device, "\n");
}
else
{
    die("Error: Invalid linker file - device name not found!\n");
}


# get the memory type, flash or FRAM
my $memory = "FLASH";
if($device =~ /FR/i )
{
    $memory = "FRAM";
}

# get addresses for main memory 
my @flash_data = getAddrLenInFile($lnk_file, $memory); # Get address for Flash/FRAM
my @flash2_data = getAddrLenInFile($lnk_file, $memory . '2'); # Get address for Flash2/FRAM2

# get addresses for RAM
my @RAM_data = getAddrLenInFile($lnk_file, " RAM "); # Get address for RAM

#get address for vectors
my @vectors_data = getAddrLenInFile($lnk_file, "INT00"); # Get address for vectors start

#------------------------------------------
my $date = strftime('%m-%d-%Y',localtime);

my $flash_vectors_addr=hex($vectors_data[0]);
my $flash_boot_start_addr=hex('10000') - hex($boot_size);
my $vector_size=hex('FFFE') - hex($vectors_data[0]);
my $shared_vectors_size=6;
if($num_shared_vectors) #if user specified a number of shared vectors, use that
{
    $shared_vectors_size = hex($num_shared_vectors)*2; 
}


if($flash_boot_start_addr < (hex($flash_data[0])+$vector_size))
{
    die("Error: Boot size too large to fit in lower memory.\n")
}

my $RAM_start_addr=hex($RAM_data[0]);
my $RAM_end_addr=hex($RAM_data[0]) + hex($RAM_data[1]) -1;

#calculate dual_size such that out of unused flash (non-boot) half for each app
my $dual_start_addr;
my $dual_size = 0;
my $dual_end_addr;
my $appl_start_addr;
my $appl_end_addr;
my $flex_start_addr;
my $flex_end_addr;
my $flex_type;
if ($dual_image)
{
    $dual_size = hex('0x10000') - hex($flash_data[0]) + hex($flash2_data[1]) - hex($boot_size); #total flash available for app + download
    $dual_end_addr = hex($flash2_data[0]) + hex($flash2_data[1]) -1; #download always ends at the end of flash
    if(($dual_size/2 + hex($flash_data[0])) < $flash_boot_start_addr) #splitting flash in half between app and download, determine if the line falls before or after boot
    {
        ########## Flash start
        # App
        ##########
        # Flex = Download
        ##########
        # Boot
        ########## 0x10000
        # Download
        ########## Flash 2 end
        
        $dual_start_addr = hex($flash_data[0]) + ($dual_size/2); #download starts at flash start + 1/2 remaining non-boot space
        #flex is part of download space
        $flex_type = "download";
        $flex_start_addr = $dual_start_addr;
        $flex_end_addr = $flash_boot_start_addr-1;
        $appl_start_addr = hex($flash_data[0]);
        $appl_end_addr = $flex_start_addr-1;
    }
    else
    {
        ########## Flash start
        # App
        ##########
        # Boot
        ########## 0x10000
        # Flex = App
        ##########
        # Download
        ########## Flash 2 end
        
        $dual_start_addr= hex($flash2_data[0]) + hex($flash2_data[1]) - ($dual_size/2); #download starts at end of available flash - 1/2 remaining non-boot space
        #flex is part of app space
        $flex_type = "application";
        $flex_start_addr = hex($flash2_data[0]);
        $flex_end_addr = $dual_start_addr - 1;
        $appl_start_addr = hex($flash_data[0]);
        $appl_end_addr = $flash_boot_start_addr-1;
    }

    print("App + Download Size: ", gethex($dual_size), "\n");
    print("Download start: ", gethex($dual_start_addr), "\n");
    print("App start: ", gethex($appl_start_addr), "\n");
    print("Flex start: ", gethex($flex_start_addr), "\n");
    print("Boot start: ", gethex($flash_boot_start_addr), "\n");
}
else
{
    $appl_start_addr=hex($flash_data[0]);
    $appl_end_addr = $flash_boot_start_addr - 1;
    $flex_start_addr=hex($flash2_data[0]);
    $flex_end_addr=hex($flash2_data[0]) + hex($flash2_data[1])-1;
}
my $vector_addr = $appl_end_addr - 1 - $vector_size;

#open templates
open(my $FBCT, '<', $filename_tempBootCCS) or die "Could not open file '$filename_tempBootCCS' $!";
open(my $FACT, '<', $filename_tempAppCCS) or die "Could not open file '$filename_tempAppCCS' $!";

#Specify the name of output files
my $outputdir = './output/';
mkdir $outputdir unless -d $outputdir;
my $filename_OutBootCCS = $outputdir.$filename.'_Boot.cmd';
my $filename_OutAppCCS = $outputdir.$filename.'_App.cmd';

#Open output files
open(my $FBCO, '>', $filename_OutBootCCS) or die "Could not open file '$filename_OutBootCCS' $!";
open(my $FACO, '>', $filename_OutAppCCS) or die "Could not open file '$filename_OutAppCCS' $!";

my @temp_text;

##### Replace text in Boot CCS file
{

    @temp_text = <$FBCT>;
    replacetextinFile('<DATE>', $date);
    if ($device){ replacetextinFile('<DEVICE>', $device); }
    replacetextinFile('<RAM_START_ADDR>', gethex($RAM_start_addr));
    replacetextinFile('<RAM_END_ADDR>', gethex($RAM_end_addr));
    replacetextinFile('<RAM_PASSWD_ADDR>', gethex($RAM_start_addr+0));
    replacetextinFile('<RAM_STATCTRL_ADDR>', gethex($RAM_start_addr+2));
    replacetextinFile('<RAM_CISM_ADDR>', gethex($RAM_start_addr+3));
    replacetextinFile('<RAM_CALLBACKPTR_ADDR>', gethex($RAM_start_addr+4));
    replacetextinFile('<RAM_UNUSED_ADDR>', gethex($RAM_start_addr+8));

    replacetextinFile('<APPL_START_ADDR>', gethex($appl_start_addr));
    replacetextinFile('<APPL_END_ADDR>', gethex($appl_end_addr));
    replacetextinFile('<FLEX_START_ADDR>', gethex($flex_start_addr));
    replacetextinFile('<FLEX_END_ADDR>', gethex($flex_end_addr));
    
    replacetextinFile('<VECTOR_ADDR>', gethex($vector_addr));
    if($dual_image)
    {
        if($flex_type eq "download")
        {
            #flex section is part of the download area
            replacetextinFile('<DOWNLOAD>', "_Down_Start = 0x".gethex(0x10000).
            ";     /* Download Area */\n_Down_End = 0x".gethex($dual_end_addr).
            ";        /*End of Download Area */\n");
            # vectors at end of appl section
            #replacetextinFile('<VECTOR_ADDR>', gethex($appl_end_addr -1 -$vector_size));
            
            replacetextinFile('<BOOT1>', "\n/* App area     : ".gethex($appl_start_addr).'-'.gethex($appl_end_addr)."*/\n".
                                           '/* Download area: '.gethex($flex_start_addr).'-'.gethex($flex_end_addr).' & '.gethex(0x10000).'-'.gethex($dual_end_addr)."*/\n".
                                           '/* Boot area    : '.gethex($flash_boot_start_addr).'-'.gethex(0xFFFF)."*/\n");
            replacetextinFile('<CHECKSUMS>',"    _Appl_Reset_Vector = (_Flex_Start - 2);\n".
                                            "    _Down_Checksum = (_Flex_Start);\n".
                                            "    _Down_Checksum_8 = (_Flex_Start+2);\n".
                                            "    _Down_Start_Memory = (_Flex_Start+3);\n".
                                            "    _Down_CRC_Size1 = (_Flex_End - _Down_Start_Memory + 1);\n".
                                            "    _Down_CRC_Size2 = (_Down_End - _Down_Start + 1);\n".
                                            "    _Down_Offset_Size = (_Appl_Start +_Flex_End - _Flex_Start);\n".
                                            "    _Down_Offset1 = (_Flex_Start - _Appl_Start);\n".
                                            "    _Down_Offset2 = (_Down_Start + _Flex_Start - _Appl_Start - __Boot_Start);\n");       


        }
        else
        {
            #flex section is part of the application area
            replacetextinFile('<DOWNLOAD>', "_Down_Start = 0x".gethex($dual_start_addr).
            ";     /* Download Area */\n_Down_End = 0x".gethex($dual_end_addr).
            ";        /*End of Download Area */\n");
            # vectors at end of flex section
            #replacetextinFile('<VECTOR_ADDR>', gethex($flex_end_addr -1 -$vector_size));
            
            replacetextinFile('<BOOT1>', "\n/* App area     : ".gethex($appl_start_addr).'-'.gethex($appl_end_addr).' & '.gethex($flex_start_addr).'-'.gethex($flex_end_addr)."*/\n".
                                   '/* Download area: '.gethex($dual_start_addr).'-'.gethex($dual_end_addr)."*/\n".
                                   '/* Boot area    : '.gethex($flash_boot_start_addr).'-'.gethex(0xFFFF)."*/\n");
            replacetextinFile('<CHECKSUMS>',"    _Appl_Reset_Vector = (__Boot_Start - 2);\n".
                                            "    _Down_Checksum = (_Down_Start);\n".
                                            "    _Down_Checksum_8 = (_Down_Start+2);\n".
                                            "    _Down_Start_Memory = (_Down_Start+3);\n".
                                            "    _Down_CRC_Size1 = (_Down_End - _Down_Start_Memory + 1);\n".
                                            "    _Down_CRC_Size2 = (_Down_End - _Down_Start_Memory + 1);\n".
                                            "    _Down_Offset_Size = (__Boot_Start - 1);\n".
                                            "    _Down_Offset1 = (_Down_Start - _Appl_Start);\n".
                                            "    _Down_Offset2 = (__Boot_Start - _Appl_Start + _Down_Start - _Flex_Start);");                      
        
        }        
    }
    else
    {
        replacetextinFile('<DOWNLOAD>',"");
        replacetextinFile('<CHECKSUMS>',"    _Appl_Reset_Vector = (__Boot_Start - 2);\n");
        replacetextinFile('<VECTOR_SIZE>', gethex($vector_size));
        #replacetextinFile('<VECTOR_ADDR>', gethex($flash_boot_start_addr -2 -$vector_size));
        replacetextinFile('<BOOT1>', "");

    }

    replacetextinFile('<FLASH_BOOT_START_ADDR>', gethex($flash_boot_start_addr));
    replacetextinFile('<FLASH_VECTORS_ADDR>', gethex($flash_vectors_addr));
    replacetextinFile('<SHARED_VECTORS_SIZE>', $shared_vectors_size);
    my $shared_vector_addr = (hex($flash_data[0]) + hex($flash_data[1])) - $shared_vectors_size;
    replacetextinFile('<SHARED_VECTORS_ADDR>', gethex($shared_vector_addr));


    replacetextinFile('<RAM_LEN>', gethex($RAM_end_addr - ($RAM_start_addr+8)+1 ));
    replacetextinFile('<FLASH_BOOT_LEN>', gethex($shared_vector_addr - $flash_boot_start_addr));
    replacetextinFile('<FLASH_VECTORS_LEN>', gethex(0xFFFE - $flash_vectors_addr));

    print $FBCO @temp_text ;    
}

##### Replace text in App CCS file

{
    @temp_text = <$FACT>;
    replacetextinFile('<DATE>', $date);
    if ($device){ replacetextinFile('<DEVICE>', $device); }
    replacetextinFile('<RAM_START_ADDR>', gethex($RAM_start_addr));
    replacetextinFile('<RAM_END_ADDR>', gethex($RAM_end_addr));
    replacetextinFile('<RAM_PASSWD_ADDR>', gethex($RAM_start_addr+0));
    replacetextinFile('<RAM_STATCTRL_ADDR>', gethex($RAM_start_addr+2));
    replacetextinFile('<RAM_CISM_ADDR>', gethex($RAM_start_addr+3));
    replacetextinFile('<RAM_CALLBACKPTR_ADDR>', gethex($RAM_start_addr+4));
    replacetextinFile('<RAM_UNUSED_ADDR>', gethex($RAM_start_addr+8));

    replacetextinFile('<APPL_START_ADDR>', gethex($appl_start_addr));
    replacetextinFile('<APPL_END_ADDR>', gethex($appl_end_addr));
    replacetextinFile('<FLEX_START_ADDR>', gethex($flex_start_addr));
    replacetextinFile('<FLEX_END_ADDR>', gethex($flex_end_addr));
    
    replacetextinFile('<FLASH_BOOT_START_ADDR>', gethex($flash_boot_start_addr));
    replacetextinFile('<FLASH_VECTORS_ADDR>', gethex($flash_vectors_addr));
    replacetextinFile('<SHARED_VECTORS_SIZE>', $shared_vectors_size);
    my $shared_vector_addr = (hex($flash_data[0]) + hex($flash_data[1])) - $shared_vectors_size;
    replacetextinFile('<SHARED_VECTORS_ADDR>', gethex($shared_vector_addr));

    replacetextinFile('<VECTOR_SIZE>', gethex($vector_size));
    
    my $vector_addr = $appl_end_addr - 1 - $vector_size;
    replacetextinFile('<APP_FLASH_LEN>', gethex($vector_addr-($appl_start_addr+3)));

    if($dual_image)
    {
        if($flex_type eq "download")
        {
            replacetextinFile('<BOOT1>', "\n/* App area     : ".gethex($appl_start_addr).'-'.gethex($appl_end_addr)."*/\n".
                                           '/* Download area: '.gethex($flex_start_addr).'-'.gethex($flex_end_addr).' & '.gethex(0x10000).'-'.gethex($dual_end_addr)."*/\n".
                                           '/* Boot area    : '.gethex($flash_boot_start_addr).'-'.gethex(0xFFFF)."*/\n");
            
            replacetextinFile('<APP_FLASH2_START>', '');
            replacetextinFile('<FLASH_SECTIONS>',   ".text       : {} >> FLASH               /* CODE                 */\n".
                                                    "    .cinit      : {} > FLASH        /* INITIALIZATION TABLES*/ \n".
                                                    "    .const      : {} >> FLASH       /* CONSTANT DATA        */ \n"
                                                    );    
        }
        else
        {          
            replacetextinFile('<BOOT1>', "\n/* App area     : ".gethex($appl_start_addr).'-'.gethex($appl_end_addr).' & '.gethex($flex_start_addr).'-'.gethex($flex_end_addr)."*/\n".
                                   '/* Download area: '.gethex($dual_start_addr).'-'.gethex($dual_end_addr)."*/\n".
                                   '/* Boot area    : '.gethex($flash_boot_start_addr).'-'.gethex(0xFFFF)."*/\n");
                                   
            replacetextinFile('<APP_FLASH2_START>', "FLASH2                  : origin = 0x".gethex($flex_start_addr).', length = 0x'.gethex($flex_end_addr - $flex_start_addr + 1));     
            replacetextinFile('<FLASH_SECTIONS>',   "#ifndef __LARGE_DATA_MODEL__\n".
                                                    "        .text       : {} >> FLASH               /* CODE                 */\n".
                                                    "    #else \n".
                                                    "        .text       : {} >> FLASH | FLASH2      /* CODE                 */ \n".
                                                    "    #endif \n\n".
                                                    "        .cinit      : {} > FLASH        /* INITIALIZATION TABLES*/ \n".
                                                    "    #ifndef __LARGE_DATA_MODEL__ \n".
                                                    "        .const      : {} >> FLASH       /* CONSTANT DATA        */ \n".
                                                    "    #else \n".
                                                    "        .const      : {} >> FLASH2 | FLASH    /* CONSTANT DATA        */ \n".
                                                    "    #endif \n");    
        }
    }
    else
    {
        replacetextinFile('<BOOT1>', "");
        replacetextinFile('<APP_FLASH2_START>',"FLASH2                  : origin = 0x".gethex($flex_start_addr).', length = 0x'.gethex($flex_end_addr - $flex_start_addr + 1));
        replacetextinFile('<FLASH_SECTIONS>',   "#ifndef __LARGE_DATA_MODEL__\n".
                                                "        .text       : {} >> FLASH               /* CODE                 */\n".
                                                "    #else \n".
                                                "        .text       : {} >> FLASH | FLASH2      /* CODE                 */ \n".
                                                "    #endif \n\n".
                                                "        .cinit      : {} > FLASH        /* INITIALIZATION TABLES*/ \n".
                                                "    #ifndef __LARGE_DATA_MODEL__ \n".
                                                "        .const      : {} >> FLASH       /* CONSTANT DATA        */ \n".
                                                "    #else \n".
                                                "        .const      : {} >> FLASH2 | FLASH    /* CONSTANT DATA        */ \n".
                                                "    #endif \n");  
    }
    my $app_reset_addr = ($appl_end_addr - 1);
    my $app_vector_table = "";
    
    # Special case using interrupt vectors in CCS for FRAM parts
    if ($memory =~ /FRAM/)
    {
        my $count=0;
        for (my $ii=$vector_addr; $ii < $app_reset_addr ; $ii +=2)
        {
            $app_vector_table = $app_vector_table."INT".sprintf("%02d", $count++)."            : origin = 0x".gethex($ii).", length = 0x0002\n    ";
        }
    }
    else
    {
        $app_vector_table = "APP_VECTORS       : origin = 0x<VECTOR_ADDR>, length = <VECTOR_SIZE>";
    }

    replacetextinFile('<APP_VECTORS_MEM>', $app_vector_table);
    replacetextinFile('<VECTOR_SIZE>', ($vector_size));
    replacetextinFile('<VECTOR_ADDR>', gethex($vector_addr));
    

    replacetextinFile('<RAM_LEN>', gethex($RAM_end_addr - ($RAM_start_addr+8)+1 ));
    replacetextinFile('<APP_FLASH_START>', gethex($appl_start_addr+3));
    
    replacetextinFile('<FLASH_BOOT_LEN>', gethex($shared_vector_addr - $flash_boot_start_addr));
    
    replacetextinFile('<APP_RESET_VECTOR>', gethex($app_reset_addr));
    
    # Special case when using interrupt vectors in CCS
    my $app_vector_sections;
    if ($memory =~ /FRAM/)
    {
        $app_vector_sections = getVectors($lnk_file);
    }
    else
    {
        $app_vector_sections = ".APP_VECTORS : {} > APP_VECTORS /* INTERRUPT TABLE            */";
    }
    replacetextinFile('<APP_VECTORS_SECTIONS>', $app_vector_sections);

    if ($dual_image) {
        replacetextinFile('<BOOT1>', "\n/* Download area: ".gethex($dual_start_addr).'-'.gethex($flash_boot_start_addr-1)."*/\n".
                                       '/* App area     : '.gethex($appl_start_addr).'-'.gethex($dual_start_addr-1)."*/\n".
                                       '/* Boot area    : '.gethex($flash_boot_start_addr).'-'.gethex(0xFFFF)."*/\n".
                                       '_FLASHDOWN_START = 0x'.gethex($dual_start_addr).'; /* Start of download area */');
    }else{
        replacetextinFile('<BOOT1>', "");
    }
    print $FACO @temp_text ;    
}

close $FBCO; close $FBCT; 
close $FACO; close $FACT; 


#param 0 = text to find
#param 1 = file
sub getAddrLenInFile{
    my ($file, $search_str) = @_;
    my @val = (0,0);
    open my $fh, '<', $file or die "Could not open '$file' $!\n";

    while (my $line = <$fh>) #go through one line at a time
    {
        chomp $line;
    
    # find the line containing the memory section definition we are looking for
       if($line =~ /$search_str/)
       {
       # parse the address
            my $temp = index($line, ': origin = ');
            my $temp2 = index($line, ',');
            $val[0] = substr($line, $temp + 11, ($temp2 - ($temp + 11)));

        #parse the length    
            $temp = index($line, 'length = ');
            $val[1] = substr($line, $temp + 9);
            
            last;
       }
    }
    return @val;
}

#param 0 = text to find
#param 1 = file
sub getdeviceinFile{
    my ($file) = @_;
    my $val = 0;
    open my $fh, '<', $file or die "Could not open '$file' $!\n";

    while (my $line = <$fh>) #go through one line at a time
    {
        chomp $line;
    
       if($line =~ /-l/)
       {
            my $temp = index($line, '-l ');
            my $temp2 = index($line, '.cmd');
            $val = substr($line, $temp + 3, ($temp2 - ($temp + 3)));
            last;
       }
    }
    return $val;
}

sub getVectors{
    my ($file) = @_;
    my $vectors;
    open(my $fh, '<', $file) or die "Could not open file '$file' $!";
    
    #copy file to array
    my $vectors_found = 0; #have we found start of interrupt vectors in file
    while (my $line = <$fh>) #go through one line at a time
    {
        if($line =~ /.int00/) #check for start of interrupt vectors in SECTIONS area of linker file
        {
            $vectors_found = 1;
            $vectors .= "/* MSP430 INTERRUPT VECTORS          */\n";
        }
        if($line =~ /.reset/)   #check for end of interrupt vectors in SECTIONS area of linker file
        {
            last;
        }
        if($vectors_found)
        {
            $vectors .= $line;
        }
    }
    return $vectors;
}

#param 0 = text to replace
#param 1 = new text
#param 2 = file
sub replacetextinFile{
    my ($in_text, $out_text) = @_;
    for (@temp_text)
    {
        s/$in_text/$out_text/g;
    }
}

sub gethex{
    return sprintf("%X", $_[0]);
}

#Help 
sub help{ 
    print "
================================================================================
    Generates linker files for MSP430FRBoot
================================================================================
[] denotes optional field
ALL NUMBERS SHOULD BE HEX NOT DECIMAL
MSPFRBootLinkerGen.pl [-help] 
                      -file <filename>
                      -lnk_file <lnk_msp430.cmd>
                      -boot_size <size>
                      [-dual_image]
                      [-shared_vectors] <number>

        
  -file             Specifies the prefix of output files. 
   <filename>       Prefix of output files. The following files will be 
                    generated:
                    ./output/<filename>_Boot.cmd(linker file for Bootloader)
                    ./output/<filename>_App.cmd(linker file for App).
  -lnk_file         Specifies the default linker file for the device being used. 
  <lnk_msp430.cmd>  Default linker file for the device being used. 
                    Must be in same directory or specify a path.
  -boot_size        Specifies size of bootloader area. 
  <size>            Size of bootloader. (Only increments of 0x400 allowed).
  -dual_image       If enabled, the output files include definitions supporting
                    dual image.
  -shared_vectors   Specifies the number of shared vectors. If not specified,
                    default value is 3 vectors. 
  <number>          Number of shared vectors (in hex)
  -help             Displays this help dialog. 
================================================================================
See the MSP430FRBoot User's Guide for more information.
================================================================================
EXAMPLE: 
MSP430FR5969 2kB bootloader with dual image support. 
The memory map to be generated is as follows:

RAM: 0x1C00-0x23FF
Application: 0x4400-BDFF
    App_code: 0x4400-0xBD8E
    Table: 0xBD90-0xBDFD
    APP_Reset_vector: 0xBDFE-0xBDFF
Download Area: 0xBE00-F7FF & 0x10000-0x13FFF
Bootloader: 0xF800-0xFFFF
    Boot_Code: 0xF800-0xFFDB
    Shared_vectors: 0xFF7A-0xFF7F
    Reserved Signatures: 0xFF80-0xFF8F
    Vectors: 0xFF90-0xFFFD
    Reset_Vectors: 0xFFFE-0xFFFF

This command generates both the bootloader and application linker files:
    
perl MSPFRBootLinkerGen.pl -file lnk_msp430fr5969_UART_2KB_Dual 
    -lnk_file lnk_msp430fr5969.cmd -boot_size 0x800 -dual_image

"; 
    exit; 
} 

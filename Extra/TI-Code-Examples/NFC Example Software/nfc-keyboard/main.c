#include "msp430.h"
#include "ringbuf.h"
#include "key.h"
#include "string.h"
#include "main.h"
tRingBufObject ringbuffer;
unsigned char buf[120];
volatile unsigned int test = 0;

///each key occupies 3 bytes, to modify this const, you have to modify
//many places such as NDEF Data array, interrup handling
const unsigned char MAX_KEY_NO_IN_NDEF=6;


unsigned char NDEF_Application_Data[] = {
//NDEF Tag Application Name
		0xD2, 0x76, 0x00, 0x00, 0x85, 0x01, 0x01,

		//Capability Container ID
		0xE1, 0x03,

		//Capability Container
		0x00, 0x0F, //CCLEN
		0x20,	//Mapping version 2.0
		0x00, 0x3B,	//MLe (49 bytes); Maximum R-APDU data size
		0x00, 0x34,	//MLc (52 bytes); Maximum C-APDU data size
		0x04, //Tag, File Control TLV (4 = NDEF file)
		0x06, //Length, File Control TLV (6 = 6 bytes of data for this tag)
		0xE1, 0x04,	//File Identifier
		0x0C, 0x02, //Max NDEF size (50 bytes)
		0x00, //NDEF file read access condition, read access without any security
		0x00, //NDEF file write access condition; write access without any security

		//NDEF File ID
		0xE1, 0x04,

		//the 27th bytes
		//NDEF File for Hello World  (48 bytes total length)
//	jn	0x00, 0x12, //NLEN; NDEF length (3 byte long message)
		0x00, 0x1e, //NLEN; NDEF length (3 byte long message)
		0xD1, 0x01, 0x1a, 0x54, //T
		0x02, 0x65, 0x6E, //'e', 'n',

		//the 36th byte, start of keyboard data payloads
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, //NDEF data; Empty NDEF message, length should match NLEN
		//jn add extra 6bytes to support 3rd key and 4th key
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		//jn add another 6bytes to support 5th key and 6th key
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00,

};

unsigned char RxData[2] = { 0, 0 };
unsigned char TxData[2] = { 0, 0 };
unsigned char TxAddr[2] = { 0, 0 };

/****************************************************************************/
/* Code-binary that opens on ETW and retrims LF oscillator to below 280kHz  */
/****************************************************************************/
unsigned char func_retrim_osc[] = { 0xB2, 0x40, 0x11, 0x96, 0x10, 0x01, 0xB2,
		0x40, 0x60, 0x03, 0x18, 0x01, 0x30, 0x41 };

struct key_value {
	unsigned char temp_key[6];
} kv[12] = { { 0x00, 0x00, 0x1C, 0x00, 0xF0, 0x1C }, { 0x00, 0x00, 0x32, 0x00,
		0xF0, 0x32 }, { 0x00, 0x00, 0x12, 0x00, 0x00, 0x2D }, { 0x00, 0xF0,
		0x12, 0x00, 0x00, 0x3B }, { 0x00, 0x00, 0x58, 0x00, 0xF0, 0x58 }, {
		0x00, 0x00, 0x24, 0x00, 0xF0, 0x24 }, { 0x00, 0x00, 0x2B, 0x00, 0xF0,
		0x2B }, { 0x00, 0x00, 0x3E, 0x00, 0xF0, 0x3E }, { 0x00, 0x00, 0x12,
		0x00, 0x00, 0x16 }, { 0x00, 0xF0, 0x16, 0x00, 0x00, 0x2C }, { 0x00,
		0xF0, 0x2C, 0x00, 0xF0, 0x12 }, { 0x00, 0x00, 0x58, 0x00, 0xF0, 0x58 } };

void main(void) {
	WDTCTL = WDTPW + WDTHOLD;

	Init_System();

	Init_CL330();

	__delay_cycles(2000000);
	Init_Keyboard();

	Reconfig_Clock();

	_enable_interrupts();

//	_disable_interrupts();
	while (1) {
		unsigned char i;

		for (i = 0; i < MAX_ROW; i++) {
			scan_rows(i);

		}

		__no_operation();

	}
}

/**
 * @brief Initialize the system include gpio and clock. MCLK/ACLK/SMCLK
 * is set to 1Mhz to lower the power consumption in start period of time.
 * @return none
 */

void Init_System(void) {

	// init all GPIO to output high
	P1DIR = 0xFF;
	P1OUT = 0x3F;
	P2DIR = 0xFF;
	P2OUT = 0xFF;
	P3DIR = 0xFF;
	P3OUT = 0xFF;
	P4DIR = BIT0 + BIT1;
	PJDIR |= BIT0 + BIT1 + BIT2 + BIT4 + BIT5;

	PJOUT &= ~BIT0;
	PJOUT &= ~BIT2;
	P1OUT &= ~BIT6;
	P1OUT &= ~BIT7;

	kb_port_init();
	// Init SMCLK = MCLk = ACLK = 1MHz

	CSCTL0_H = 0xA5;
	CSCTL1 |= DCOFSEL0 + DCOFSEL1;          // Set max. DCO setting = 8MHz
	CSCTL2 = SELA_3 + SELS_3 + SELM_3;      // set ACLK = MCLK = DCO
	CSCTL3 = DIVA_3 + DIVS_3 + DIVM_3;      // set all dividers to 1MHz

}

/**
 * @brief CL330 is powered by the GPIO of the MCU, so does the
 * I2C pull-up resistors. So we need enable the power before
 * configure I2C interface. After that, a sequence of step is
 * performed to make CL330 ready for NDEF handling.
 * @return none
 */
void Init_CL330(void) {

	//Power CL330
	PJDIR |= BIT0;
	PJOUT |= BIT0;
	__delay_cycles(80000 / 8);

	// Configure pins for I2C
	PORT_I2C_SEL0 &= ~(SCL + SDA);
	PORT_I2C_SEL1 |= SCL + SDA;

	//configure eUSCI for I2C
	UCB0CTL1 |= UCSWRST;	            //Software reset enabled
	UCB0CTLW0 |= UCMODE_3 + UCMST + UCSYNC + UCTR;//I2C mode, Master mode, sync, transmitter
	UCB0CTLW0 |= UCSSEL_2;                    // SMCLK = 1MHz

	UCB0BRW = 5; //baudrate = SMLK/5 = 200kHz
	UCB0I2CSA = 0x0028;	//slave address - determined by pins E0, E1, and E2 on the RF430CL330H
	UCB0CTL1 &= ~UCSWRST;

	//reset CL330
	PJOUT &= ~BIT2;
	__delay_cycles(10000 / 8);
	PJOUT |= BIT2;
//	__delay_cycles(200000 / 8);
	__delay_cycles(200000 / 8);
//	PJOUT |= BIT0;

	while (!(Read_Register(STATUS_REG) & READY))
		; //wait until READY bit has been set
//	PJOUT &= ~BIT0;
		//WORKAROUND CS

	Write_Register(TEST_MODE_REG, TEST_MODE_KEY);   //unlock test mode
	Write_Register(CONTROL_REG, TEST430_ENABLE); //enable test mode, now have to use actual addresses
	Write_Register(0x0202, 0x0000);                 //clear POUT to set CS low
	Write_Register(0x2814, 0); //exit test mode (CONTROL_REG is at real address 0x2814)
	Write_Register(TEST_MODE_REG, 0);               //lock test reg

	/****************************************************************************/
	/* Load Oscillator re-trim code into RAM and activate it                    */
	/****************************************************************************/
	Write_Continuous(0x800, func_retrim_osc, 14);
	Write_Register(TEST_FUNCTION_REG, 0x800); // Set pointer to function just loaded into RAM
	Write_Register(TEST_MODE_REG, TEST_MODE_KEY + BIT8);   // Call test function
	Write_Register(TEST_MODE_REG, 0);             // Lock the TEST mode register

	//Configure RF430CL330H for Typical Usage Scenario

	//write NDEF memory with Capability Container + NDEF message
	Write_Continuous(0, NDEF_Application_Data, sizeof(NDEF_Application_Data));

	//Enable interrupts for End of Read and End of Write
	Write_Register(INT_ENABLE_REG, EOW_INT_ENABLE + EOR_INT_ENABLE);
	test = Read_Register(INT_ENABLE_REG);

	//Configure INTO pin for active low and enable RF
	Write_Register(CONTROL_REG, INT_ENABLE + INTO_DRIVE + RF_ENABLE);
	PJOUT |= BIT1;

}

/**
 * @brief A ring buffer is used to smooth the data exchange between
 * the key handling module and NDEF handling module.
 * @return none
 */
void Init_Keyboard(void) {

	RingBufInit(&ringbuffer, buf, sizeof(buf));
	init_ram();
	kb_port_init();

}

/**
 * @brief re-configure MCLK to 8Mhz, ACLK to 500KHz for timer A0,
 *  no change on SMCLK (for I2C)
 * @return none
 */
void Reconfig_Clock() {

	CSCTL0_H = 0xA5;
	CSCTL1 |= DCOFSEL0 + DCOFSEL1;          // Set max. DCO setting = 8MHz
	CSCTL2 = SELA_3 + SELS_3 + SELM_3;      // set ACLK = MCLK = DCO
	CSCTL3 = DIVA_4 + DIVS_3 + DIVM_1;   // MCLK=8Mhz, ACLK = 0.5MHz, SMCLK=1MHz

	//Timer A0 interrupt interval -> 125ms
	TA0CCTL0 = CCIE;                           // TACCR0 interrupt enabled
	TA0CCR0 = 62500;	//125ms @500K ACLK
	TA0CTL = TASSEL_1 + MC_1;  //ACLK + Up mode(counts to TA0CCR0)

}

/**
 * @brief reads the register at reg_addr, returns the result
 * @return none
 */
unsigned int Read_Register(unsigned int reg_addr) {
	TxAddr[0] = reg_addr >> 8; 		//MSB of address
	TxAddr[1] = reg_addr & 0xFF; 	//LSB of address

	UCB0CTLW1 = UCASTP_1;
	UCB0TBCNT = 0x0002;
	UCB0CTL1 &= ~UCSWRST;
	UCB0CTL1 |= UCTXSTT + UCTR;		//start i2c write operation
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxAddr[0];
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxAddr[1];
	while (!(UCB0IFG & UCBCNTIFG))
		;
	UCB0CTL1 &= ~UCTR; 			//i2c read operation
	UCB0CTL1 |= UCTXSTT; 			//repeated start
	while (!(UCB0IFG & UCRXIFG0))
		;
	RxData[0] = UCB0RXBUF;
	UCB0CTLW0 |= UCTXSTP; //send stop after next RX
	while (!(UCB0IFG & UCRXIFG0))
		;
	RxData[1] = UCB0RXBUF;
	while (!(UCB0IFG & UCSTPIFG))
		;     // Ensure stop condition got sent
	UCB0CTL1 |= UCSWRST;

	return RxData[1] << 8 | RxData[0];
}

/**
 * @brief reads the register at reg_addr, returns the result
 * @return none
 */
unsigned int Read_Register_BIP8(unsigned int reg_addr) {
	unsigned char BIP8 = 0;
	TxAddr[0] = reg_addr >> 8; 		//MSB of address
	TxAddr[1] = reg_addr & 0xFF; 	//LSB of address

	UCB0CTLW1 = UCASTP_1;
	UCB0TBCNT = 0x0002;
	UCB0CTL1 &= ~UCSWRST;
	UCB0CTL1 |= UCTXSTT + UCTR;		//start i2c write operation

	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxAddr[0];
	BIP8 ^= TxAddr[0];
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxAddr[1];
	BIP8 ^= TxAddr[1];

	while (!(UCB0IFG & UCBCNTIFG))
		;
	UCB0CTL1 &= ~UCTR; 			//i2c read operation
	UCB0CTL1 |= UCTXSTT; 			//repeated start

	while (!(UCB0IFG & UCRXIFG0))
		;
	RxData[0] = UCB0RXBUF;
	BIP8 ^= RxData[0];
	while (!(UCB0IFG & UCRXIFG0))
		;
	RxData[1] = UCB0RXBUF;
	BIP8 ^= RxData[1];

	UCB0CTLW0 |= UCTXSTP; //send stop after next RX
	while (!(UCB0IFG & UCRXIFG0))
		;
	//RxData[1] = UCB0RXBUF;
	if (BIP8 != UCB0RXBUF)
		__no_operation();

	while (!(UCB0IFG & UCSTPIFG))
		;     // Ensure stop condition got sent
	UCB0CTL1 |= UCSWRST;

	return RxData[0] << 8 | RxData[1];
}

/**
 * @brief Continuous read data_length bytes and store in the area "read_data"
 * @return none
 */
void Read_Continuous(unsigned int reg_addr, unsigned char* read_data,
		unsigned int data_length) {
	unsigned int i;
	TxAddr[0] = reg_addr >> 8; 		//MSB of address
	TxAddr[1] = reg_addr & 0xFF; 	//LSB of address

	UCB0CTLW1 = UCASTP_1;
	UCB0TBCNT = 0x0002;
	UCB0CTL1 &= ~UCSWRST;
	UCB0CTL1 |= UCTXSTT + UCTR;		//start i2c write operation

	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxAddr[0];
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxAddr[1];
	while (!(UCB0IFG & UCBCNTIFG))
		;
	UCB0CTL1 &= ~UCTR; 			//i2c read operation
	UCB0CTL1 |= UCTXSTT; 			//repeated start
	while (!(UCB0IFG & UCRXIFG0))
		;

	for (i = 0; i < data_length - 1; i++) {
		while (!(UCB0IFG & UCRXIFG0))
			;
		read_data[i] = UCB0RXBUF;
		//read_data[i] = UCB0RXBUF << 8;
		if (i == data_length - 1)
			UCB0CTL1 |= UCTXSTP; //send stop after next RX
		//while(!(UCB0IFG & UCRXIFG0));
		//read_data[i] |= UCB0RXBUF;
	}

	UCB0CTLW0 |= UCTXSTP; //send stop after next RX
	while (!(UCB0IFG & UCRXIFG0))
		;
	read_data[i] = UCB0RXBUF;
	while (!(UCB0IFG & UCSTPIFG))
		;     // Ensure stop condition got sent
	UCB0CTL1 |= UCSWRST;
}

/**
 * @brief Writes the register at reg_addr with value
 * @return none
 */
void Write_Register(unsigned int reg_addr, unsigned int value) {
	TxAddr[0] = reg_addr >> 8; 		//MSB of address
	TxAddr[1] = reg_addr & 0xFF; 	//LSB of address
	TxData[0] = value >> 8;
	TxData[1] = value & 0xFF;

	UCB0CTLW1 = UCASTP_1;
	UCB0TBCNT = 0x0004;
	UCB0CTL1 &= ~UCSWRST;
	UCB0CTL1 |= UCTXSTT + UCTR;		//start i2c write operation
	//write the address
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxAddr[0];
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxAddr[1];
	//write the data
	while (!(UCB0IFG & UCTXIFG0))
		;
	//UCB0TXBUF = TxData[0];
	UCB0TXBUF = TxData[1];
	while (!(UCB0IFG & UCTXIFG0))
		;
	//UCB0TXBUF = TxData[1];
	UCB0TXBUF = TxData[0];
	while (!(UCB0IFG & UCBCNTIFG))
		;
	UCB0CTL1 |= UCTXSTP;
	while (!(UCB0IFG & UCSTPIFG))
		;     // Ensure stop condition got sent
	UCB0CTL1 |= UCSWRST;

}

/**
 * @brief writes the register at reg_addr with value
 * @return none
 */
void Write_Register_BIP8(unsigned int reg_addr, unsigned int value) {
	unsigned char BIP8 = 0;

	TxAddr[0] = reg_addr >> 8; 		//MSB of address
	TxAddr[1] = reg_addr & 0xFF; 	//LSB of address
	TxData[0] = value >> 8;
	TxData[1] = value & 0xFF;

	UCB0CTLW1 = UCASTP_1;
	UCB0TBCNT = 0x0005;
	UCB0CTL1 &= ~UCSWRST;
	UCB0CTL1 |= UCTXSTT + UCTR;		//start i2c write operation

	//write the address
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxAddr[0];
	BIP8 ^= TxAddr[0];
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxAddr[1];
	BIP8 ^= TxAddr[1];

	//write the data
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxData[0];
	BIP8 ^= TxData[0];
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxData[1];
	BIP8 ^= TxData[1];

	//send BIP8 byte
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = BIP8;

	while (!(UCB0IFG & UCBCNTIFG))
		;
	UCB0CTL1 |= UCTXSTP;
	while (!(UCB0IFG & UCSTPIFG))
		;     // Ensure stop condition got sent
	UCB0CTL1 |= UCSWRST;

}

/**
 * @brief writes the register at reg_addr and incrementing
 * addresses with the data at "write_data" of length data_length
 * @return none
 */
void Write_Continuous(unsigned int reg_addr, unsigned char* write_data,
		unsigned int data_length) {
	unsigned int i;

	TxAddr[0] = reg_addr >> 8; 		//MSB of address
	TxAddr[1] = reg_addr & 0xFF; 	//LSB of address

	UCB0CTLW1 = UCASTP_1;
	UCB0TBCNT = data_length; //data_length is in words, TBCNT is in bytes, so multiply by 2
	UCB0CTL1 &= ~UCSWRST;
	UCB0CTL1 |= UCTXSTT + UCTR;		//start i2c write operation
	//write the address
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxAddr[0];
	while (!(UCB0IFG & UCTXIFG0))
		;
	UCB0TXBUF = TxAddr[1];
	//write the data
	/*for(i = 0; i < data_length; i++)
	 {
	 while(!(UCB0IFG & UCTXIFG0));
	 UCB0TXBUF = write_data[i] >> 8;
	 while(!(UCB0IFG & UCTXIFG0));
	 UCB0TXBUF = write_data[i];
	 }*/
	for (i = 0; i < data_length; i++) {
		while (!(UCB0IFG & UCTXIFG0))
			;
		UCB0TXBUF = write_data[i];
	}

	while (!(UCB0IFG & UCTXIFG0))
		;
	while (!(UCB0IFG & UCBCNTIFG))
		;
	UCB0CTL1 |= UCTXSTP;
	while (!(UCB0IFG & UCSTPIFG))
		;     // Ensure stop condition got sent
	UCB0CTL1 |= UCSWRST;

}

/**
 * @brief The keyboard data is protected by CRC16 (MODBUS)
 * CRC initialized value: 0xFFFF
 * CRC poly: A001
 * @return none
 */
void CRC16(unsigned char *ptr, unsigned char len) {
	unsigned int crc = 0xFFFF;
	unsigned char i, j;
	for (j = 0; j < len; j++) {
		crc ^= *ptr++;
		for (i = 0; i < 8; i++) {
			if (crc & 0x01) {
				crc >>= 1;
				crc ^= 0xa001;
			} else {
				crc >>= 1;
			}
		}
	}
	ptr[1] = (crc >> 8);
	ptr[0] = (crc & 0x00FF);
}

unsigned char j = 0;
unsigned int seq = 0;

//
/**
 * @brief Timer A0 interrupt service routine
 * 125ms interrupt interval to write the keyboard data to NDEF module. The data
 * is stored in the ring buffer.
 * Max two key scan code will be written into CL330 during one interrupt event.
 * There are 3 bytes for 1 scan code, so 6 bytes is reserved in the static buffer
 * within NDEF message.
 * If only one key event need to be written, the left 3 bytes is cleared.
 * @return none
 */
#pragma vector = TIMER0_A0_VECTOR
__interrupt void Timer_A(void) {

	unsigned char i = 0;
	unsigned char rbyte = 0;

	rbyte = RingBufUsed(&ringbuffer);

	if (rbyte >= 18) {
	for (i = 0; i < 18; i++)
		NDEF_Application_Data[38 + i] = 0;

		seq++;
		if (seq > 0x7FFF)
			seq = 0x0000;
		NDEF_Application_Data[35] = (seq >> 8) & 0xff;
		NDEF_Application_Data[36] = seq & 0xff;

		//length
		NDEF_Application_Data[37] = 3+18+2;

		NDEF_Application_Data[38 + 0] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 1] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 2] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 3] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 4] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 5] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 6] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 7] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 8] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 9] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 10] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 11] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 12] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 13] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 14] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 15] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 16] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 17] = RingBufReadOne(&ringbuffer);

		CRC16(NDEF_Application_Data + 35, 3+18);
		Write_Continuous(0, NDEF_Application_Data,
				sizeof(NDEF_Application_Data)-(MAX_KEY_NO_IN_NDEF*3)+18);

	} else
		if (rbyte >= 15) {
			for (i = 0; i < 15; i++)
				NDEF_Application_Data[38 + i] = 0;

			seq++;
			if (seq > 0x7FFF)
				seq = 0x0000;
			NDEF_Application_Data[35] = (seq >> 8) & 0xff;
			NDEF_Application_Data[36] = seq & 0xff;

			//length
			NDEF_Application_Data[37] = 3+15+2;

			NDEF_Application_Data[38 + 0] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 1] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 2] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 3] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 4] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 5] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 6] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 7] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 8] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 9] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 10] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 11] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 12] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 13] = RingBufReadOne(&ringbuffer);
			NDEF_Application_Data[38 + 14] = RingBufReadOne(&ringbuffer);

			CRC16(NDEF_Application_Data + 35, 3+15);
			Write_Continuous(0, NDEF_Application_Data,
					sizeof(NDEF_Application_Data)-(MAX_KEY_NO_IN_NDEF*3)+15);

		} else

		if (rbyte >= 12) {
			for (i = 0; i < 12; i++)
				NDEF_Application_Data[38 + i] = 0;

		seq++;
		if (seq > 0x7FFF)
			seq = 0x0000;
		NDEF_Application_Data[35] = (seq >> 8) & 0xff;
		NDEF_Application_Data[36] = seq & 0xff;

		//length
		NDEF_Application_Data[37] = 3+12+2;

		NDEF_Application_Data[38 + 0] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 1] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 2] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 3] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 4] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 5] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 6] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 7] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 8] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 9] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 10] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 11] = RingBufReadOne(&ringbuffer);

		CRC16(NDEF_Application_Data + 35, 3+12);
		Write_Continuous(0, NDEF_Application_Data,
				sizeof(NDEF_Application_Data)-(MAX_KEY_NO_IN_NDEF*3)+12);

	} else if (rbyte >= 9) {
		for (i = 0; i < 9; i++)
			NDEF_Application_Data[38 + i] = 0;

		seq++;
		if (seq > 0x7FFF)
			seq = 0x0000;
		NDEF_Application_Data[35] = (seq >> 8) & 0xff;
		NDEF_Application_Data[36] = seq & 0xff;

		//length
		NDEF_Application_Data[37] = 3+9+2;

		NDEF_Application_Data[38 + 0] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 1] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 2] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 3] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 4] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 5] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 6] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 7] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 8] = RingBufReadOne(&ringbuffer);

		CRC16(NDEF_Application_Data + 35, 3+9);
		Write_Continuous(0, NDEF_Application_Data,
				sizeof(NDEF_Application_Data)-(MAX_KEY_NO_IN_NDEF*3)+9);

	} else if (rbyte >= 6) {
		for (i = 0; i < 6; i++)
			NDEF_Application_Data[38 + i] = 0;

		seq++;
		if (seq > 0x7FFF)
			seq = 0x0000;
		NDEF_Application_Data[35] = (seq >> 8) & 0xff;
		NDEF_Application_Data[36] = seq & 0xff;

		//length
		NDEF_Application_Data[37] = 3+6+2;

		NDEF_Application_Data[38 + 0] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 1] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 2] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 3] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 4] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 5] = RingBufReadOne(&ringbuffer);

		CRC16(NDEF_Application_Data + 35, 3+6);
		Write_Continuous(0, NDEF_Application_Data,
				sizeof(NDEF_Application_Data)-(MAX_KEY_NO_IN_NDEF*3)+6);

	} else if (rbyte >= 3) {
		for (i = 0; i < 3; i++)
			NDEF_Application_Data[38 + i] = 0;
		seq++;
		if (seq > 0x7FFF)
			seq = 0x0000;
		NDEF_Application_Data[35] = (seq >> 8) & 0xff;
		NDEF_Application_Data[36] = seq & 0xff;

		//length
		NDEF_Application_Data[37] = 3+3+2;

		NDEF_Application_Data[38 + 0] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 1] = RingBufReadOne(&ringbuffer);
		NDEF_Application_Data[38 + 2] = RingBufReadOne(&ringbuffer);

		CRC16(NDEF_Application_Data + 35, 3+3);
		Write_Continuous(0, NDEF_Application_Data,sizeof(NDEF_Application_Data)-(MAX_KEY_NO_IN_NDEF*3)+3);

	}

}

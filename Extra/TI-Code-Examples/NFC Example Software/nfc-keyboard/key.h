/*
 * nfckb.h
 *
 *  Created on: Jul 11, 2013
 *      Author: a0283087
 */

#ifndef KEY_H
#define KEY_H

void scan_cols(unsigned char row);
void kb_port_init(void);
unsigned char read_a_key(unsigned char col);
void scan_cols(unsigned char row);
void scan_rows(unsigned char row);
void send_key(unsigned char row, unsigned char col,unsigned char flag);
void check_key(unsigned char row, unsigned char col, unsigned char value);
void init_ram(void);

#define MAX_ROW 8
#define MAX_COL 18

#define DEBUNCE_T 3

#define ROW_0_LOW P4DIR|=BIT0;P4OUT&=~BIT0
#define ROW_1_LOW P3DIR|=BIT1;P3OUT&=~BIT1
#define ROW_2_LOW PJDIR|=BIT4;PJOUT&=~BIT4
#define ROW_3_LOW P3DIR|=BIT3;P3OUT&=~BIT3
#define ROW_4_LOW P3DIR|=BIT0;P3OUT&=~BIT0
#define ROW_5_LOW P2DIR|=BIT7;P2OUT&=~BIT7
#define ROW_6_LOW P3DIR|=BIT4;P3OUT&=~BIT4
#define ROW_7_LOW P2DIR|=BIT6;P2OUT&=~BIT6

#define ROW_0_HIGHZ P4DIR&=~BIT0;P4REN&=~BIT0;P4OUT|=BIT0
#define ROW_1_HIGHZ P3DIR&=~BIT1;P3REN&=~BIT1;P3OUT|=BIT1
#define ROW_2_HIGHZ PJDIR&=~BIT4;PJREN&=~BIT4;PJOUT|=BIT4
#define ROW_3_HIGHZ P3DIR&=~BIT3;P3REN&=~BIT3;P3OUT|=BIT3
#define ROW_4_HIGHZ P3DIR&=~BIT0;P3REN&=~BIT0;P3OUT|=BIT0
#define ROW_5_HIGHZ P2DIR&=~BIT7;P2REN&=~BIT7;P2OUT|=BIT7
#define ROW_6_HIGHZ P3DIR&=~BIT4;P3REN&=~BIT4;P3OUT|=BIT4
#define ROW_7_HIGHZ P2DIR&=~BIT6;P2REN&=~BIT6;P2OUT|=BIT6

#define COL_VAL_0 P2IN&BIT3
#define COL_VAL_1 P2IN&BIT5
#define COL_VAL_2 P2IN&BIT4
#define COL_VAL_3 PJIN&BIT5
#define COL_VAL_4 P4IN&BIT1
#define COL_VAL_5 P2IN&BIT1
#define COL_VAL_6 P2IN&BIT2
#define COL_VAL_7 P1IN&BIT5
#define COL_VAL_8 P1IN&BIT4
#define COL_VAL_9 P3IN&BIT5
#define COL_VAL_10 P1IN&BIT3
#define COL_VAL_11 P2IN&BIT0
#define COL_VAL_12 P1IN&BIT1
#define COL_VAL_13 P3IN&BIT7
#define COL_VAL_14 P1IN&BIT2
#define COL_VAL_15 P3IN&BIT2
#define COL_VAL_16 P3IN&BIT6
#define COL_VAL_17 P1IN&BIT0




#endif /* KEY_H */

#include <msp430f5310.h>

#include "key.h"
#include "ringbuf.h"

//delay cycles when pull down the row line
#define READ_PIN_DELAY 10

extern tRingBufObject ringbuffer;
unsigned char key_value[MAX_ROW][MAX_COL] = { 0x1 };
const unsigned long scan_table[MAX_ROW][MAX_COL] = {
		//row 0
		//print screen, q, w, e, r, u, i, o ,p
		0, 0xf015, 0xf01d, 0xf024, 0xf02d, 0xf03c, 0xf043, 0xf044, 0xf04d, 0, 0,
		0, 0, 0, 0, 0, 0, 0,

		//row 1
		//null, tab, cap, f3, t, y, ], f7, [, null, bksp, null, null, null, null, Lshift, win, null
		0, 0xf00d, 0xf058, 0xf004, 0xf02c, 0xf035, 0xf05b, 0xf083, 0xf054, 0,
		0xf066, 0, 0, 0, 0, 0xf012, 0xe0f02f, 0,

		//row 2
		//null, a, s, d, f, j, k, l, ;, null, \, null, null, null,null,Rshift,null,null
		0, 0xf01c, 0xf01b, 0xf023, 0xf02b, 0xf03b, 0xf042, 0xf04b, 0xf04c, 0,
		0xf05d, 0, 0, 0, 0, 0xf059, 0, 0,

		//row3
		//null, esc, null,f4,g, h, f6,null, ', Lalt, f11, space, null, null, Arrow-up, null,null,null
		0, 0xf076, 0, 0xf00c, 0xf034, 0xf033, 0xf00b, 0, 0xf052, 0xf011, 0xf078,
		0xf029, 0, 0, 0xe0f075, 0, 0, 0,

		//row4
		//Rctrl, z, x, c, v, m, ",", ., null, null, ENTER, null, null, null, null, null, null, null,
		0xe0f014, 0xf01a, 0xf022, 0xf021, 0xf02a, 0xf03a, 0xf041, 0xf049, 0, 0,
		0xf05a, 0, 0, 0, 0, 0, 0, 0,

		//row5
		//null, null, null, null, b, n, null, null(app), /, Ralt, f12, Arrow-down, Arrow-right, null,  Arrow-left, null, null, null
		0, 0, 0, 0, 0xf032, 0xf031, 0, 0, 0xf04a, 0xe0f011, 0xf007, 0xe0f072,
		0xe0f074, 0, 0xe0f06b, 0, 0, 0,

		//row6
		//fn, `, f1, f2, 5, 6, =, f8, -, null, f9, delete, insert, null, null, Lctrl, null, null
		0, 0xf00e, 0xf005, 0xf006, 0xf02e, 0xf036, 0xf055, 0xf00a, 0xf04e, 0,
		0xf001, 0xe0f071, 0xe0f070, 0, 0, 0xf014, 0, 0,

		//row7
		//f5, 1, 2, 3, 4, 7, 8, 9, 0, pause, f10, null, null, null, null, null, null, null
		0xf003, 0xf016, 0xf01e, 0xf026, 0xf025, 0xf03d, 0xf03e, 0xf046, 0xf045,
		0, 0xf009, 0, 0, 0, 0, 0, 0, 0

};

/**
 * @brief Initialize GPIOs that occupied by keyboard matrix
 */
//*****************************************************************************
void kb_port_init(void) {
// Y0 -Y7
	//P4.0, P3.1, PJ.4, P3.3, P3.0, P2.7, P3.4, P2.6
	// ALL OUT PUT HIGH

	P4DIR |= BIT0;
	P4OUT |= BIT0;

	P3DIR |= BIT1;
	P3OUT |= BIT1;

	PJDIR |= BIT4;
	PJOUT |= BIT4;

	P3DIR |= BIT3;
	P3OUT |= BIT3;

	P3DIR |= BIT0;
	P3OUT |= BIT0;

	P2DIR |= BIT7;
	P2OUT |= BIT7;

	P3DIR |= BIT4;
	P3OUT |= BIT4;

	P2DIR |= BIT6;
	P2OUT |= BIT6;

// X0 - X17
	//P2.3, P2.5, P2.4, PJ.5, P4.1, P2.1, P2.2, P1.5, P1.4, P3.5, P1.3, P2.0, P1.1, P3.7, P1.2, P3.2, P3.6, P1.0

// all pull-up, input mode
	P1DIR &= ~(BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5);
	P1REN |= (BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5);
	P1OUT |= (BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5);

	P2DIR &= ~(BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5);
	P2REN |= (BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5);
	P2OUT |= (BIT0 | BIT1 | BIT2 | BIT3 | BIT4 | BIT5);

	P3DIR &= ~(BIT2 | BIT5 | BIT6 | BIT7);
	P3REN |= (BIT2 | BIT5 | BIT6 | BIT7);
	P3OUT |= (BIT2 | BIT5 | BIT6 | BIT7);

	P4DIR &= ~(BIT1);
	P4REN |= (BIT1);
	P4OUT |= (BIT1);

	PJDIR &= ~(BIT5);
	PJREN |= (BIT5);
	PJOUT |= (BIT5);

}

/**
 * @brief Read a IO, which is connected to a column of keyboard matrix.
 * There are 18 columns in one row of the keyboard matrix.
 * @param col the column need to be read
 * @return return a bit value, a "1" means this column read as a High voltage,
 * while a "0" means low voltage on the pin.
 */
//*****************************************************************************

unsigned char read_a_key(unsigned char col) {

	switch (col) {

	case 0:
		return COL_VAL_0;
	case 1:
		return COL_VAL_1;
	case 2:
		return COL_VAL_2;
	case 3:
		return COL_VAL_3;
	case 4:
		return COL_VAL_4;
	case 5:
		return COL_VAL_5;
	case 6:
		return COL_VAL_6;
	case 7:
		return COL_VAL_7;
	case 8:
		return COL_VAL_8;
	case 9:
		return COL_VAL_9;
	case 10:
		return COL_VAL_10;
	case 11:
		return COL_VAL_11;
	case 12:
		return COL_VAL_12;
	case 13:
		return COL_VAL_13;
	case 14:
		return COL_VAL_14;
	case 15:
		return COL_VAL_15;
	case 16:
		return COL_VAL_16;
	case 17:
		return COL_VAL_17;

	}
	return 0xff;

}

/**
 * @brief Scan all the columns in one row, that is, read all the IO state
 * in this row and check if the key state need to be updated.
 * @param row the row need to be read
 * @return none
 * while a "0" means low voltage on the pin.
 */

void scan_cols(unsigned char row) {

	unsigned char col = 0;
	unsigned char v = 0;

	__delay_cycles(READ_PIN_DELAY);
	for (col = 0; col < MAX_COL; col++) {

		v = read_a_key(col);
		check_key(row, col, v);

	}

}

/**
 * @brief Scan all the rows
 * @param row row number we want to
 * @return none
 * while a "0" means low voltage on the pin.
 */

void scan_rows(unsigned char row) {

	switch (row) {

	case 0:
		ROW_0_LOW;
		scan_cols(row);
		ROW_0_HIGHZ;
		break;
	case 1:
		ROW_1_LOW;
		scan_cols(row);

		ROW_1_HIGHZ;
		break;
	case 2:
		ROW_2_LOW;
		scan_cols(row);

		ROW_2_HIGHZ;
		break;
	case 3:
		ROW_3_LOW;
		scan_cols(row);

		ROW_3_HIGHZ;
		break;
	case 4:
		ROW_4_LOW;
		scan_cols(row);

		ROW_4_HIGHZ;
		break;
	case 5:
		ROW_5_LOW;
		scan_cols(row);

		ROW_5_HIGHZ;
		break;
	case 6:
		ROW_6_LOW;
		scan_cols(row);

		ROW_6_HIGHZ;
		break;
	case 7:
		ROW_7_LOW;
		scan_cols(row);

		ROW_7_HIGHZ;
		break;
	default:
		break;
	}


}
/**
 * @brief Scan all the columns in one row, that is, read all the IO state
 * in this row and check if the key state need to be updated.
 * @param row the row need to be read
 * @return none
 * while a "0" means low voltage on the pin.
 */

//breakflag==0 --> key pressed
void send_key(unsigned char row, unsigned char col, unsigned char makeflag) {

	unsigned long key;

	unsigned long t = scan_table[row][col];
	if (makeflag != 0) {	// send break code

		key = scan_table[row][col];

	} else {	//send make code
		if ((t & 0xff0000) != 0) { // for 3 bytes code, only use byte[2] and [byte0]
			key = (t & 0xff0000) >> 8 | (t & 0xff);

		} else
			key = t & 0xff;
	}
	__no_operation();

	int rbyte = RingBufFree(&ringbuffer);
	if (rbyte >= 3) {

		RingBufWriteOne(&ringbuffer, key >> 16);
		RingBufWriteOne(&ringbuffer, key >> 8);
		RingBufWriteOne(&ringbuffer, key);
	} else {
		__no_operation();

	}
	//ring_buf.put(scan_tab[row,col]);
}

/**
 * @brief Check and calculate dedicate key is pressed or released,
 * key debounce is performed before the key state decided
 * @param row The row that the key located
 * @param col The column that the key located
 * @param value The voltage measure from the key, 1 for high and 0 for zero
 * @return none
 */

void check_key(unsigned char row, unsigned char col, unsigned char value) {

	static last_row = 0;
	static last_col = 0;
	unsigned char t = 0;
	unsigned char debounce = 0;
	unsigned char v = 0;
	t = key_value[row][col];
	debounce = (t & 0xfe) >> 1;
	v = t & 0x01;

	if (((value == 0) && (v == 0)) || ((value != 0) && (v != 0))) { //key state does not change

//check if it is in debouncing
		if (debounce != 0) { //in debouncing
			debounce++;
			if ((debounce ) >= DEBUNCE_T) { // debounce done, output

				//key state not changed, only clear debounce time
				key_value[row][col] &= 0x1;

				send_key(row, col, value);

			} else { // debounce time increased by 1

				key_value[row][col] += 0x2;
			}
		}

		return;

	} else { // key state is changed


		//update key state and start debounce timer
		if (value == 0) { // update key status according to current GPIO Pin State
			key_value[row][col] =0x10;
		} else {
			key_value[row][col] = 0x11;

		}

	}

}

void init_ram(void) {
	int row = 0;
	int col = 0;
	for (row = 0; row < MAX_ROW; row++) {
		for (col = 0; col < MAX_COL; col++) {
			key_value[row][col] = 0x1;
		}
	}

}


################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../lnk_msp430fr5739.cmd 

ASM_SRCS += \
../int.asm 

C_SRCS += \
../key.c \
../main.c \
../ringbuf.c 

C_DEPS += \
./key.d \
./main.d \
./ringbuf.d 

OBJS += \
./int.obj \
./key.obj \
./main.obj \
./ringbuf.obj 

ASM_DEPS += \
./int.d 

OBJS__QUOTED += \
"int.obj" \
"key.obj" \
"main.obj" \
"ringbuf.obj" 

C_DEPS__QUOTED += \
"key.d" \
"main.d" \
"ringbuf.d" 

ASM_DEPS__QUOTED += \
"int.d" 

ASM_SRCS__QUOTED += \
"../int.asm" 

C_SRCS__QUOTED += \
"../key.c" \
"../main.c" \
"../ringbuf.c" 



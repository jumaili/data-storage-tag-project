/**
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
 * File {kmx62.c}
 */

/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */
#include <sensor_drivers/kmx62.h>

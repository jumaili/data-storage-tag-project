/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
 *****************************************************************************
                  {datalogger.h} - Driver for DST Application
 *****************************************************************************
 */

#ifndef APPLICATION_LAYER_DATALOGGER_H_
#define APPLICATION_LAYER_DATALOGGER_H_
/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */

/* ------------------------------------------------------------------------------------------------
 *                                           Enums
 * ------------------------------------------------------------------------------------------------
 */
typedef enum {
    Temp_Only,
    Pressure_Only,
    Acceleration_Only,
    Magnetometer,Only,
    Time_Only,
    Temperature_Pressure_Acceleration_Magnometer
}Datalogger_Modes_t;

typedef enum {
    Fahrenheit = 0,
    Celcius
} Temp_Modes_t;

/* ------------------------------------------------------------------------------------------------
 *                                           Defines
 * ------------------------------------------------------------------------------------------------
 */


/* ------------------------------------------------------------------------------------------------
 *                                      Function Prototypes
 * ------------------------------------------------------------------------------------------------
 */

#endif /* APPLICATION_LAYER_DATALOGGER_H_ */

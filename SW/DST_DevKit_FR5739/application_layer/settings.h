/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
 *****************************************************************************
               {settings.h} - Application Settings (mostly global)
 *****************************************************************************
 */

#ifndef APPLICATION_LAYER_SETTINGS_H_
#define APPLICATION_LAYER_SETTINGS_H_
/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */
#include "../application_layer/fsm.h"
#include "stdint.h"

typedef struct Settings_Global_t {
    uint8_t ui8PollingInterval; /* Polling Interval used by RTC for alarm in Minutes */
}Settings_Global_t;

//TODO: not sure where to use yet.
typedef struct Flags_t {

}Flags_t;


/* ------------------------------------------------------------------------------------------------
 *                                         Setting variables GLOBAL
 * ------------------------------------------------------------------------------------------------
 */
States_t CurrentState_t = Start;
//DEBUGGER MODE STATE
//FLAGS

#endif /* APPLICATION_LAYER_SETTINGS_H_ */

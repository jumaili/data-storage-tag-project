/**
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
 * File {main.c}
 *
 ********************************************************************************
 *                       DATA STORAGE TAG FIRMWARE Description
 * This is the main entry point for the DST firmware developed in 2019 as a
 * part of Ali Aljumaili Master Thesis. The firmware is divided into folders for
 * peripherals-drivers, sensor-drivers, storage-drivers and finally, application
 * layer folder where the datalogger and wireless NFC functionalities exist
 ********************************************************************************
//            DST Rev.2.0.0 Simplified Schematics
//
//                   MSP430FR5739
//            ------------------------
//         /|\|                  P1.1|<---NFC_INT0
//          | |                  P1.2|<---MAG-ACC_INT1
//          --|RST                   |
 *            |                      |
//      |---- |PJ.4/XIN    P1.3/TA1.2|--> Acoustic Transmitter
//      32 kHz|                  P2.2|--> FLASH_RST
//      |---- |PJ.5/XOUT         P1.0|--> NFC_RST
 *            |                      |
//     LED1<--|P1.4              P1.6|--> I2C-SDA
//     LED2<--|PJ.0              P1.7|--> I2C-SCL
//     LED3<--|PJ.1                  |
//            |                  P2.1|<-- SPI - SOMI
//  SPI-CS1<--|PJ.2              P2.0|--> SPI - SIMO
//  SPI-CS2<--|PJ.3              P1.5|--> SPI - CLK
//
 */

/* ------------------------------------------------------------------------------------------------
 *                                          Includes
 * ------------------------------------------------------------------------------------------------
 */
#include <application_layer/fsm.h>
#include <peripheral_drivers/mcu.h>
#include <sensor_drivers/rtc.h>

void main(void)
{
    MCU_Init();                         /* Initialize WDT, GPIO's and Clocks */

    RTC_B_Init();                       /* Starts the Real Time Clock with a default polling Interval */

    __enable_interrupt();               /* Enable Global Interrupts for using ISR */

    while(1)
    {



//        FSM_Init();                     /* Starts the finite-state-machine */
    }
}

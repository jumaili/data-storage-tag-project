/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
****************************************************************************
                  {mcu.h} - Driver for the MSP430FR5738 Peripherals
*****************************************************************************
*/

#ifndef PERIPHERAL_DRIVERS_MCU_H_
#define PERIPHERAL_DRIVERS_MCU_H_

/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */
#include <msp430.h>

/* ------------------------------------------------------------------------------------------------
 *                                           Defines
 * ------------------------------------------------------------------------------------------------
 */
#define MCU_SPI_CLK_PIN BIT5
#define MCU_SPI_SOMI_PIN BIT1
#define MCU_SPI_SIMO_PIN BIT0

#define MCU_I2C_SDA_PIN BIT6
#define MCU_I2C_SCL_PIN BIT7

#define MCU_NFC_INT_PIN     BIT1
#define MCU_ACC_MAG_INT_PIN BIT2

#define MCU_NFC_RST_PIN BIT0
#define MCU_FLASH_RST_PIN BIT2

#define MCU_ACC_MAG_PIN BIT2
#define MCU_ACOUSTIC_TRANSMITTER_PIN BIT3

#define LED1 BIT4
#define LED2 BIT0
#define LED3 BIT1

#define LED1_OUT P1OUT
#define LED2_OUT PJOUT
#define LED3_OUT PJOUT

#define LED1_DIR P1DIR
#define LED2_DIR PJDIR
#define LED3_DIR PJDIR

#define LED1_POWER_ON   LED1_DIR |=LED1; LED1_OUT|=LED1;
#define LED1_POWER_OFF  LED1_OUT &= ~LED1;
#define LED2_POWER_ON   LED2_DIR |=LED2; LED2_OUT|=LED2;
#define LED2_POWER_OFF  LED2_OUT &= ~LED2;
#define LED3_POWER_ON   LED3_DIR |=LED3; LED3_OUT|=LED3;
#define LED3_POWER_OFF  LED3_OUT &= ~LED3;

#define XIN_PIN  BIT4
#define XOUT_PIN BIT5

#define SELECT_ACLK_OUTPUT_PIN_2 BIT2
/* ------------------------------------------------------------------------------------------------
 *                                           Prototypes
 * ------------------------------------------------------------------------------------------------
 */
void MCU_Stop_Watchdog(void);
void MCU_Configure_GPIO(void);
void MCU_Init(void);
void MCU_Configure_Clocks(void);

#endif /* PERIPHERAL_DRIVERS_MCU_H_ */

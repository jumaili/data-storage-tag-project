/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
****************************************************************************
                  {mcu.c} - Driver for the MSP430FR5738 Peripherals
*****************************************************************************
*/

/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */
#include <driverlib/MSP430FR57xx/inc/hw_memmap.h>
#include <driverlib/MSP430FR57xx/cs.h>
#include <peripheral_drivers/mcu.h>

/* Sets up the MSP430FR738 GPIO, Watchdog and Clocks */
void MCU_Init(void){
    MCU_Stop_Watchdog();                    /* Stops the WDT*/
    MCU_Configure_GPIO();                   /* Setup GPIOs */
    MCU_Configure_Clocks();                 /* Configure DCO and ACLK */
}

/* Stops the watchdog timer to avoid timing out during start-up */
void MCU_Stop_Watchdog(void)
{
     WDTCTL = WDTPW | WDTHOLD;              /* Stop watchdog timer from
                                            timing out during initial start-up */
}

/* Initialize MSP430 General Purpose Input Output Ports
 *  The GPIO registers should be set in a specific order:
 *   PxOUT --> PxSEL or PxSELx -->  PxDIR -->  PxREN -->  PxIES --> PxIFG -->  PxIE
 *   (see section 8.2.6 of the MSP430 User's manual )
*/
void MCU_Configure_GPIO(void)
{
/*================================ Port 1 GPIO Setup ================================*/
    P1OUT = MCU_NFC_RST_PIN;                        /* Pins Output Registers PORT1 */

    P1SEL1 = (MCU_I2C_SDA_PIN | MCU_I2C_SCL_PIN /* Port 1 Port Select Register */
            | MCU_SPI_CLK_PIN);                 /* Select I2C and SPI */

    P1DIR = (MCU_ACC_MAG_PIN|                   /* PORT 1 Pins Direction Registers */
            MCU_ACOUSTIC_TRANSMITTER_PIN);      /* Set pins as output */

    P1REN = MCU_NFC_RST_PIN;                    /* PORT 1 Enable Pullup for NFC_RST */

    P1IES = (MCU_NFC_INT_PIN |                  /* PORT1 Interrupt Edge Select Register */
            MCU_ACC_MAG_INT_PIN);

    P1IFG = 0;                                  /* Port 1 Interrupt Flag Register */

    P1IE = (MCU_NFC_INT_PIN |
            MCU_ACC_MAG_INT_PIN);               /* Port 1 Enable Interrupts for pins */

    /*================================ Port 2 GPIO Setup ================================*/
    P2OUT = MCU_FLASH_RST_PIN;                  /* Pins Output Registers PORT2 */

    P2SEL1 = (MCU_SPI_SIMO_PIN |
            MCU_SPI_SOMI_PIN);                  /* Port 2 Port Select Register */

    //Configure ACLK to Pin2.0
    P2DIR |= BIT0;
    P2SEL0 |= BIT0;
    P2SEL1 |= BIT0 ;
    P2DIR = 0;                                  /* Set All PORT2 Pins as Input*/

    P2REN = MCU_FLASH_RST_PIN;                  /* Enable Pullup for FLASH */

    P2IES = 0;                                  /* Interrupt Edge Select Register */

    P2IFG = 0;                                  /* Interrupt Flag Register */

    /*================================ Port J GPIO Setup ================================*/

    PJSEL0  =  XIN_PIN | XOUT_PIN;              /* Set up XIN and XOUT for bypass crystal mode*/

    //PJSEL0 |= BIT0 | BIT1; /* Output MCLK and SMCLK to J0 and J1 (only for testing)*/

    PJDIR |= BIT2;                              /* Set pin as output for ACLK */
    PJSEL0 |= SELECT_ACLK_OUTPUT_PIN_2;         /* Set PJ.2 as ACLK for testing */
    PJSEL1= 0; // for test

    PJDIR   = LED2 |LED3;                       /* Set pins as output for LED2 and LED3 */
}


/* Sets up the DCO, SMCLK and  XT1 as a source for ACLK*/
void MCU_Configure_Clocks(void)
{
    CS_setExternalClockSource(32768,0);             /*   XT1 Frequency -  32768 Hz,
                                                     *   XT2 Frequency -  0 Hz     */

    CS_turnOnXT1WithTimeout(XT1DRIVE_3,100000);     /* Start XT1 crystal in low frequency mode */

    CS_setDCOFreq (CS_DCORSEL_1, CS_DCOFSEL_1);     /* Set DCO frequency to 20 MHz */
    //CS_setDCOFreq (CS_DCORSEL_0, CS_DCOFSEL_3);     /* Set DCO frequency to 8 MHz */

    CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT,   //* Setting MCLK source from CS_DCOCLK_SELECT
                     CS_CLOCK_DIVIDER_1);           //* with the divider of CS_CLOCK_DIVIDER_1.

    CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT,  //* Setting SMCLK source from CS_DCOCLK_SELECT with the
                       CS_CLOCK_DIVIDER_32);         //* the divider of CS_CLOCK_DIVIDER_8

    CS_initClockSignal(CS_ACLK, CS_XT1CLK_SELECT,  //* Setting ACLK source from CS_XT1CLK_SELECT
                        CS_CLOCK_DIVIDER_1);        //* with the divider of CS_CLOCK_DIVIDER_1.

    CS_clearAllOscFlagsWithTimeout(100000);        /* Clears all oscillator fault flags including
                                                    * global oscillator fault flag before switching clock sources. */
}

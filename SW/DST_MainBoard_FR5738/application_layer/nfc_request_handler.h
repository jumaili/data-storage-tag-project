/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
 *****************************************************************************
             {nfc_request_handler.h} - Driver for the NFC Interface
 *****************************************************************************
 */

#ifndef APPLICATION_LAYER_NFC_REQUEST_HANDLER_H_
#define APPLICATION_LAYER_NFC_REQUEST_HANDLER_H_

/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */
#include "../application_layer/fsm.h"

/* ------------------------------------------------------------------------------------------------
 *                                           Enums
 * ------------------------------------------------------------------------------------------------
 */
typedef enum {
    None,
    Set_Mode,
    Set_Time,
    Set_Date,
    Set_Polling_Interval,
    Reset_CMD,
    Start_CMD,
    Stop_CMD,
    Clear_Data_CMD
} Datalogger_Commands_t;

/* ------------------------------------------------------------------------------------------------
 *                                          Function Prototypes
 * ------------------------------------------------------------------------------------------------
 */
void Reset_Data_Logger(void);




#endif /* APPLICATION_LAYER_NFC_REQUEST_HANDLER_H_ */

/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
 *****************************************************************************
               {settings.h} - Application Settings (mostly global)
 *****************************************************************************
 */

#ifndef APPLICATION_LAYER_SETTINGS_H_
#define APPLICATION_LAYER_SETTINGS_H_
/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */
#include "application_layer/fsm.h"
#include "stdint.h"
#include "driverlib.h"
#include <nfc_request_handler.h>

typedef struct Settings_Global_t {
    uint8_t ui8PollingInterval; /* Polling Interval used by RTC for alarm in Minutes */
}Settings_Global_t;


extern uint32_t buffer; //global variable for testing to store I2C data


/* ------------------------------------------------------------------------------------------------
 *                                         Global States
 * ------------------------------------------------------------------------------------------------
 */

extern States_t CurrentState_t;

/* Used in FSM */
#pragma NOINIT (Command_Received_t);
extern Datalogger_Commands_t Command_Received_t;

//States_t CurrentState_t = Start;
//DEBUGGER MODE STATE
//FLAGS


#endif /* APPLICATION_LAYER_SETTINGS_H_ */

/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
 *****************************************************************************
             {rtc.c} - Driver for the real time clock
 *****************************************************************************
*/

/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */
#include <rtc.h>
#include <driverlib/MSP430FR57xx/rtc_b.h>

void RTC_B_Init()
{
    //*  */
    Calendar calenderTime;

    calenderTime.Seconds = 0;
    calenderTime.Minutes = 0;
    calenderTime.Hours = 0;
    calenderTime.DayOfWeek = 3;
    calenderTime.DayOfMonth = 1;
    calenderTime.Month = 12;
    calenderTime.Year = 2019;


    RTC_B_configureCalendarAlarmParam alarm_time;

    alarm_time.minutesAlarm = 1;
    /* Initialize the settings to operate the RTC in Calendar mode */
    RTC_B_initCalendar(RTC_B_BASE, &calenderTime, RTC_B_FORMAT_BCD);

    /* Set and enable the desired Calendar Alarm settings */
    RTC_B_configureCalendarAlarm(RTC_B_BASE,&alarm_time
    );

    /* Set a single specified Calendar interrupt condition */
    RTC_B_setCalendarEvent(RTC_B_BASE, RTC_B_CALENDAREVENT_MINUTECHANGE);

    /* Enable selected RTC interrupt sources */
//    RTC_B_enableInterrupt(RTC_B_BASE, RTC_B_OSCILLATOR_FAULT_INTERRUPT|RTC_B_CLOCK_ALARM_INTERRUPT|RTC_B_CLOCK_READ_READY_INTERRUPT|RTC_B_TIME_EVENT_INTERRUPT);
//
    /* Start the RTC */
    RTC_B_startClock(RTC_B_BASE);
}


void RTC_B_Set_Alarm(uint8_t ui8PollingInterval)
{
    RTC_B_holdClock(RTC_B_BASE);    /* Hold RTC*/

    //uint8_t Temperary_time = 0;
    //TODO: taken pollinginternval and check range

    //set new RTCALARM MINUTE

    RTC_B_startClock(RTC_B_BASE);  /* Start the RTC by clearing RTCHOLD */

}
//
#pragma vector=RTC_VECTOR
__interrupt void RTC_B_ISR(void)
{
    switch (__even_in_range(RTCIV, RTCIV_RTCOFIFG)) // * Equivalent to switch(RTCIV) but allows compiler to generate more
    {                                               // * efficient code for the switch statement.
        case RTCIV_NONE:                   /* No interrupts pending */
            __no_operation();
            break;
        case RTCIV_RTCRDYIFG:              /* RTC ready: RTCRDYIFG */
            __no_operation();
            break;
        case RTCIV_RTCTEVIFG:              /* RTC interval timer: RTCTEVIFG */
            __no_operation();
            break;
        case RTCIV_RTCAIFG:                /* RTC user alarm: RTCAIFG */

             /*Toggle LED */
            break;

        case RTCIV_RTCOFIFG: break;        /* RTC Oscillator fault */
        default: break;
    }
}

/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
****************************************************************************
               {timer.h} - Driver for timer MSP430FR5738
*****************************************************************************
*/

#ifndef CORE_TIMER_H_
#define CORE_TIMER_H_

/* ------------------------------------------------------------------------------------------------
 *                                      Includes
 * ------------------------------------------------------------------------------------------------
 */
#include <msp430.h>
#include <stdint.h>


#define TIMER_B_STOP_MODE                                                  MC_0
#define TIMER_B_UP_MODE                                                    MC_1
#define TIMER_B_CONTINUOUS_MODE                                            MC_2
#define TIMER_B_UPDOWN_MODE                                                MC_3


/* ------------------------------------------------------------------------------------------------
 *                                      Function Prototypes
 * ------------------------------------------------------------------------------------------------
 */
void Low_Power_Delay_ms(uint32_t ms);
void Timer_B0_Init(void);

#endif /* CORE_TIMER_H_ */

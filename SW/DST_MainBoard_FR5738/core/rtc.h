/*
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
 *****************************************************************************
             {rtc.h} - Driver for the real time clock
 *****************************************************************************
*/

#ifndef DEVICES_RTC_H_
#define DEVICES_RTC_H_
/* ------------------------------------------------------------------------------------------------
 *                                           Includes
 * ------------------------------------------------------------------------------------------------
 */
#include <msp430.h>
#include <stdint.h>
/* ------------------------------------------------------------------------------------------------
 *                                      Function Prototypes
 * ------------------------------------------------------------------------------------------------
 */
void RTC_B_Init(void);
void RTC_B_Set_Alarm(uint8_t ui8PollingInterval);
__interrupt void RTC_B_ISR(void);
#endif /* DEVICES_RTC_H_ */

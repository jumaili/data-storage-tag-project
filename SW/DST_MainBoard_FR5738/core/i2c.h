//#############################################################################
//
//! \file   i2c.h
//!
//! \brief  This is the i2c interface driver, it is dependent on driverlib for
//!         low level register configurations.
//!         This modules configures the eUSCIB0, transmit data over I2C bus by
//!         the way of interrupt and read data from the I2C by the way of interrupt.
//!         This module also configures the eUSCIB0 clock source, frequency,
//!         peripheral address and initialize I2C ports.
//!
//! (C) Copyright 2019, Ali Aljumaili
//
//#############################################################################


#ifndef CORE_I2C_H_
#define CORE_I2C_H_

//*****************************************************************************
// the includes
//*****************************************************************************
#include <msp430.h>
#include <stdint.h>



//! \brief This defines the I/O ports needed by the USCIBO peripheral.
//  these can be edited in case of changing port or MCU.
#define I2C_PORT_I2C_OUT    P1OUT
#define I2C_PORT_I2C_DIR    P1DIR
#define I2C_PORT_I2C_SEL    P1SEL0
#define I2C_SDA_PIN         BIT6
#define I2C_SCL_PIN         BIT7


typedef unsigned char bool_t;

//! \brief This defines the I/O ports needed by the USCIBO peripheral. It makes
//! sense to declare the port here at the top so in case they change, they are
//!edited only here.
#define PORT_I2C_OUT    P1OUT
#define PORT_I2C_DIR    P1DIR
#define PORT_I2C_SEL    P1SEL0
#define SDA BIT6
#define SCL BIT7

//! \brief These defines are needed to keep track of data during read and write process.
#define START           1
#define CONTINUE        0
#define FAIL            0
#define PASS            1
#define SET             1
#define CLEAR           0
#define I2C_NACK_RCVD   2
#define I2C_TRANSMIT    3
#define LPM_MODE        LPM0_bits
#define I2C_ALLOW       (I2C_READY_IN & I2C_READY_PIN)

uint8_t RF430_I2C_State;
uint8_t RF430_I2C_Start;            //AK 10-23-2013



//! \brief This declares both the register address and data.
//!
typedef struct
{
    unsigned char configReg;
    unsigned char data;
} i2cCmd_t;


//*****************************************************************************
//
//! \brief  This function is called only once and it is called when the MSP430
//!         is initializing. Furthermore, the function is called indirectly by
//!         the I2C peripheral interface module when it itself is initializing.
//!         This function configures the USCIBO peripheral to act as a I2C peripheral,
//!         set to master transmitting mode, with SMCLK clock frequency
//!         with auto detection of frequency and set the daterate to 100 kbps with no
//!         automatic stop bit generation.
//!
//! \param  none
//
//! \return None
//
//*****************************************************************************

void I2C_Init(void);

//*****************************************************************************
//
//! \brief  This function is called only once and it is called when the MSP430
//!         is initializing. This function select I2C from eUSCIB (P1.6 and P1.7 as)
//! \param  none
//
//! \return None
//
//*****************************************************************************
void I2C_Init_Ports(void);

//*****************************************************************************
//
//! \brief  This function sets the slave address for the eUSCB_0 module.
//!
//! \param slaveAddr   Destination address of the remote IC
//
//! \return None
//
//*****************************************************************************
void I2C_Set_SlaveAddr(uint8_t slaveAddr);


//*****************************************************************************
//
//! \brief  This function transmits a single byte on the I2C bus, the slave
//!         address must be configured before calling this function.
//! \param txData Pointer to a buffer which contains the data to be written
//
//! \return None
//
//*****************************************************************************
void I2C_Send_Single_Byte(uint8_t * txData);

#endif /* CORE_I2C_H_ */

//#############################################################################

//
//! \file   i2c.c
//!
//! \brief  please read i2c.h
//!
//! (C) Copyright 2019, Ali Aljumaili
//
//#############################################################################

//*****************************************************************************
// the includes
//*****************************************************************************
#include <core/i2c.h>
#include <driverlib.h>
#include <settings.h> // for global buffer

void I2C_Init(void)
{
    EUSCI_B_I2C_initMasterParam Settings_I2C_Parm = {0};
    Settings_I2C_Parm.selectClockSource = EUSCI_B_I2C_CLOCKSOURCE_SMCLK;
    Settings_I2C_Parm.i2cClk = CS_getSMCLK();
    Settings_I2C_Parm.dataRate = EUSCI_B_I2C_SET_DATA_RATE_100KBPS;
    Settings_I2C_Parm.byteCounterThreshold = 0;
    Settings_I2C_Parm.autoSTOPGeneration = EUSCI_B_I2C_NO_AUTO_STOP;
    EUSCI_B_I2C_initMaster(EUSCI_B0_BASE, &Settings_I2C_Parm); /* Sets the Master Configurations defined in settings.h*/

    EUSCI_B_I2C_enable(EUSCI_B0_BASE); /* Enable I2C by reseting the UCSWRST bit */
}

void I2C_Set_SlaveAddr(uint8_t slaveAddr)
{
    EUSCI_B_I2C_setSlaveAddress(EUSCI_B0_BASE,slaveAddr);
}


void I2C_Send_Single_Byte(uint8_t * txData)
{
    EUSCI_B_I2C_masterSendSingleByte(EUSCI_B0_BASE,&txData);
}

void I2C_Enable_Interrupt(void)
{
    //UCB0IE |= UCSTTIE; /* Enable Start Condition Interrupt*/
    //UCB0IE |= UCTXIE0; /* Enable Transmit Interrupt*/

    UCB0IE |= UCRXIE0;  /* Enable Receive Interrupt*/
}


#pragma vector = USCI_B0_VECTOR
__interrupt void USCIB0_ISR(void)
{
    switch(__even_in_range(UCB0IV,0x1E))
    {
    case 0x00: break; // Vector 0: No interrupts break;
    case 0x02: break; // Vector 2: ALIFG break;
    case 0x04: UCB0CTL1 |= UCTXSTT; // I2C start condition
    break; // Vector 4: NACKIFG break;
    case 0x06: break; // Vector 6: STTIFG break;
    case 0x08: break; // Vector 8: STPIFG break;
    case 0x0a:

        break; // Vector 10: RXIFG3 break;
    case 0x0c: break; // Vector 14: TXIFG3 break;
    case 0x0e:
        buffer = EUSCI_B_I2C_masterReceiveMultiByteNext(EUSCI_B0_BASE); // Reads the first byte of the buffer and store it into the buffer
        EUSCI_B_I2C_masterReceiveMultiByteFinish(EUSCI_B0_BASE); //stop and ignore the input.
        break; // Vector 16: RXIFG2 break;
    case 0x10: break; // Vector 18: TXIFG2 break;
    case 0x12:
        buffer = EUSCI_B_I2C_masterReceiveMultiByteNext(EUSCI_B0_BASE); // Reads the first byte of the buffer and store it into the buffer
        EUSCI_B_I2C_masterReceiveMultiByteFinish(EUSCI_B0_BASE); //stop and ignore the input.
        break; // Vector 20: RXIFG1 break;
    case 0x14: break; // Vector 22: TXIFG1 break;
    case 0x16:
        buffer = EUSCI_B_I2C_masterReceiveMultiByteNext(EUSCI_B0_BASE); // Reads the first byte of the buffer and store it into the buffer
        EUSCI_B_I2C_masterReceiveMultiByteFinish(EUSCI_B0_BASE); //stop and ignore the input.
        __no_operation();
        break; // Vector 24: RXIFG0
    case 0x18: break; // Vector 26: TXIFG0 break;
    case 0x1a: break; // Vector 28: BCNTIFG break;
    case 0x1c: break; // Vector 30: clock low timeout break;
    case 0x1e: break; // Vector 32: 9th bit break;
    default: break;
    }
}

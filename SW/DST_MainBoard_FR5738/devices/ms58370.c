//#############################################################################
//
//! \file   ms5738.c
//!
//! \brief  driver for presure sensor
//!
//! (C) Copyright 2019, Ali Aljumaili
//
//#############################################################################


//*****************************************************************************
// the includes
//*****************************************************************************
#include <core/i2c.h>
#include <devices/ms58370.h>
#include <driverlib.h> //TO BE MOVED to I2C



/**
 * \brief Configures the I2C master to be used with the MS5837 device.
 */
void MS5837_Init(void)
{
    I2C_Set_SlaveAddr(MS5837_ADDR);     /* Set I2C Slave Address to MS5837 ADDR */

    I2C_Send_Single_Byte(MS5837_RESET); /* S-- > Device Address --> W --> A --> RESET COMMAND --> A --> Stop */
}

//uint32_t *adcValue
void MS5837_Convert_And_Read_ADC(void)
{

    //I2C_Set_SlaveAddr(MS5837_ADDR);     /* Set I2C Slave Address to MS5837 ADDR */
   // I2C_Send_Single_Byte(MS5837_RESET); /* S-- > Device Address --> W --> A --> RESET COMMAND --> A --> Stop */

    //__delay_cycles(48000); // wait 2 ms after reset.

    //(1/24MHz)*X=1ms,          MCLK= 24 Mhz (MCLK is the source for delay cycles)
    //X=24000 1 ms

    I2C_Send_Single_Byte(MS5837_CONVERT_D1_8192); //issue command to start ADC pressure conversion D1

    __delay_cycles(432000); // delay for 18 ms

    I2C_Send_Single_Byte(MS5837_ADC_READ); //issue the command for ADC read

    __delay_cycles(432000); // delay for 18 ms

    EUSCI_B_I2C_masterReceiveStart(EUSCI_B0_BASE); //set I2C in Read Mode

    //read the 3 byte buffer.
    UCB0IE |=  UCRXIE0;               //receive interrupt enable
    // then check interrupt
    __no_operation();
}


void MS5837_Calculate()
{
    int32_t dT = 0;
    int64_t SENS = 0;
    int64_t OFF = 0;
    int32_t SENSi = 0;
    int32_t OFFi = 0;
    int32_t Ti = 0;
    int64_t OFF2 = 0;
    int64_t SENS2 = 0;


}


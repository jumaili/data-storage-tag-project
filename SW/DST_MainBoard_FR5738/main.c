/**
 * Use of this software is copyright Ali Aljumaili and licensed under
 * the MIT license found in the LICENSE file associated to this repository.
 * Copyright (c) 2019, Ali Aljumaili
 * File {main.c}
 *
 ********************************************************************************
 *                       DATA STORAGE TAG FIRMWARE Description
 * This is the main entry point for the DST firmware developed in 2019 as a
 * part of Ali Aljumaili Master Thesis. The firmware is divided into folders for
 * peripherals-drivers, sensor-drivers, storage-drivers and finally, application
 * layer folder where the datalogger and wireless NFC functionalities exist
 ********************************************************************************
//            DST Rev.2.0.0 Simplified Schematics
//
//                   MSP430FR5738
//            ------------------------
//         /|\|                  P1.1|<---NFC_INT0
//          | |                  P1.2|<---MAG-ACC_INT1
//          --|RST                   |
 *            |                      |
//      |---- |PJ.4/XIN    P1.3/TA1.2|--> Acoustic Transmitter
//      32 kHz|                  P2.2|--> FLASH_RST
//      |---- |PJ.5/XOUT         P1.0|--> NFC_RST
 *            |                      |
//     LED1<--|P1.4              P1.6|--> I2C-SDA
//     LED2<--|PJ.0              P1.7|--> I2C-SCL
//     LED3<--|PJ.1                  |
//            |                  P2.1|<-- SPI - SOMI
//  SPI-CS1<--|PJ.2              P2.0|--> SPI - SIMO
//  SPI-CS2<--|PJ.3              P1.5|--> SPI - CLK
//
 */

/* ------------------------------------------------------------------------------------------------
 *                                          Includes
 * ------------------------------------------------------------------------------------------------
 */
#include <core/mcu.h>
#include <core/rtc.h>
#include <fsm.h>

#include <settings.h>
#include <msp430.h>

#include <ctpl.h>
#include <devices/ms58370.h>

#include <stdint.h>


void main(void)
{
    MCU_Init();                    /* Initialize  WDT, GPIO's and Clocks */

    RTC_B_Init();                  /* Starts the Real Time Clock with a default polling Interval */

    Timer_B0_Init();               /* Configures the timer to up mode. */

    I2C_Init();                    /* Enables the I2C Interface eUSCI_B0 */

    MS5837_Init();                 /* Resets the pressure sensor as per datasheet */

    TMP117_Init();

    __enable_interrupt();          /* Enable Global Interrupts for using ISR */


    while(1)
    {

       FSM_Init();                /* Starts the finite-state-machine, program in control of handlers from now on */
    }
}


/*
 * This function will be called before main and initialization of variables.
 * The ctpl_init() function must be called at the start to enable the compute
 * through power loss library.
 */
int _system_pre_init(void)
{
    /* Initialize ctpl library */
    ctpl_init();

    /* Insert application pre-init code here. */

    return 1;
}

// This code is commented, it was used to test power loss  SVSH and SVSL
//void Reset_ISR(void) {
//
//   switch (__even_in_range(SYSRSTIV, SYSRSTIV_PMMKEY))
//   {
//       case SYSRSTIV_NONE:                       // No Interrupt pending
//                               __no_operation();
//                               break;
//       case SYSRSTIV_BOR:                        // SYSRSTIV : BOR
//                               __no_operation();
//                               break;
//       case SYSRSTIV_RSTNMI:                     // SYSRSTIV : RST/NMI
//                               __no_operation();
//                               break;
//       case SYSRSTIV_DOBOR:                      // SYSRSTIV : Do BOR
//                               __no_operation();
//                               break;
//       case SYSRSTIV_LPM5WU:                     // SYSRSTIV : Port LPM5 Wake Up
//                               __no_operation();
//                               break;
//       case SYSRSTIV_SECYV:                      // SYSRSTIV : Security violation
//                               __no_operation();
//                               break;
//       case SYSRSTIV_SVSLIFG:                       // SYSRSTIV : SVSL
//                               __no_operation();
//                               break;
//       case SYSRSTIV_SVSHIFG:                       // SYSRSTIV : SVSH
//                               __no_operation();
//                               break;
//
//       case SYSRSTIV_DOPOR:                      // SYSRSTIV : Do POR
//                               __no_operation();
//                               break;
//       case SYSRSTIV_WDTTO:                      // SYSRSTIV : WDT Time out
//                               __no_operation();
//                               break;
//       case SYSRSTIV_WDTKEY:                     // SYSRSTIV : WDTKEY violation
//                               __no_operation();
//                               break;
//
//       default: break;
//   }
//}
